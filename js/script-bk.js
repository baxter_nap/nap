qwe = {};

$(document).ready(function() {
	if (check_device()=='desktop') {
		if ($('.table-naf').length>0) {
			console.log('.table-naf');
			assessment_height = $(window).height()-500;
		}else if ($('.table-nt').length>0){
			console.log('.table-nt');
			assessment_height = $(window).height()-500;
		}else if ($('.table-screening').length>0){
			console.log('.table-screening');
			assessment_height = $(window).height()-180;
		}else{
			console.log('.no');
			assessment_height = $(window).height()-100;
		}
		$('.assessment-box').css('height', assessment_height+'px');
		$('.assessment-box').css('overflow', 'auto');
		$('body').css('overflow', 'hidden');
		// console.log('desktop',assessment_height)
	}else{
		assessment_height = $('body').height();
		// console.log('mobile',assessment_height)
	}

	$('.body input').change(function(event) {
		if ($(this).attr('no-get')==undefined&&$(this).val()!="-") {
			input_checked = $(this).closest('.body').find('input:checked')
			val = []
			sum = 0
			for (var i = 0; i < input_checked.length; i++) {
				val.push($(input_checked[i]).val())
				sum += parseInt($(input_checked[i]).val())
				if ($(this).attr('maxscore')!=undefined) {
					sum = $(this).attr('maxscore')
				}
			}
			result = $(input_checked[0]).attr('type')=="checkbox" ? sum : val.join('-') ;
			$(this).closest('.body').find('.score').html(result);

			//sum and set result all
			score_all = $('.body .score')
			sum_all = 0
			check_all_show = 0
			for (var i = 0; i < score_all.length; i++) {
				if ($(score_all[i]).attr('noscore')==undefined) {
					sum_all += $(score_all[i])[0].innerText!="" ? parseInt($(score_all[i]).text()) : 0;
					check_all_show += $(score_all[i])[0].innerText!="" ? 1 : 0;
				}
			}
			$('.result_all').html(sum_all)

			if ($('.table-naf').length>0) {
				set_assessment_result('naf')
			}else if ($('.table-nt').length>0) {
				set_assessment_result('nt')
			}
		}
	});

	$('#myTabs a').click(function (e) {
		e.preventDefault()
		console.log(this)
		$(this).tab('show')
	})
});

function cal_bmi() {
	weight = $('#weight_current').val()
	height = $('#height').val()/100
	if (weight!=""&&height!="") {
		bmi = weight/Math.pow(height,2);
		$('.bmi').slideDown('fast');
		$('#bmi').val(bmi.toFixed(2));
		if (bmi<18.5||bmi>25) {
			$('#c_3_yes').prop('checked', true);
			$('.bmi input').addClass('red_1')
			$('.bmi input').removeClass('green_3')
			$('.bmi label').addClass('red_1')
			$('.bmi label').removeClass('green_3')
		}else{
			$('#c_3_no').prop('checked', true);
			$('.bmi input').addClass('green_3')
			$('.bmi input').removeClass('red_1')
			$('.bmi label').addClass('green_3')
			$('.bmi label').removeClass('red_1')
		}
	}else{
		$('.bmi').slideUp('fast');
		$('#bmi').val(0);
		$('#c_3_no').prop('checked', true);
	}
}

function check_alert() {
	if ($('input[type="radio"]:checked').length==4) {
		if ($('.radio_yes:checked').length>=2) {
			$('.table_no').slideDown('fast');
			$('.table_yes').slideUp('fast');
		}else{
			$('.table_no').slideUp('fast');
			$('.table_yes').slideDown('fast');
		}
		var sc_ar = {}
		var sc = $('input[type="radio"]:checked');
		for (var i = 0; i < sc.length; i++) {
			var val = $(sc[i]).attr('id').split('_')[2]=="yes" ? 1 : 0;
			var key = $(sc[i]).attr('id').split('_')[0]+'_'+$(sc[i]).attr('id').split('_')[1]
			set_obj(sc_ar,key,val)
		}
		var check_val = $('.form-control');
		for (var i = 0; i < check_val.length; i++) {
			if ($(check_val[i]).val()!=""&&$(check_val[i]).val()!=null) {
				set_obj(sc_ar,$(check_val[i]).attr('id'),$(check_val[i]).val())
			}
		}
		set_qwe(sc_ar);
	}
}

function del(page,id) {
	swal({
		title: "Are you sure?",
		text: "You will Delete!",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Yes, delete it!",
		closeOnConfirm: false
	},
	function(){
		$.ajax({
			url: base_url+page+'/fn_delete',
			type: 'POST',
			data: {id: id},
		})
		.done(function(res) {
			console.log(res);
			if (res==1) {
				swal({
					title: "Deleted!",
					text: "Delete row success",
					type: "success",
					showConfirmButton: false,
					timer: 1500
				},function () {
					window.location.href = base_url+page
				})
			}else{
				swal("Error!", "Contact super admin", "error");
			}
		})
		.fail(function() {
			console.log("error");
		})
	});
}

function checkout(page,hn_code,patient_ward_id,status) {
	if (status==1) {
		var text = 'คนไข้ออกจากโรงพยาบาลใช่หรือไม่'
	}else{
		var text = 'คนไข้เสียชีวิตใช่หรือไม่'
	}
	swal({
		title: "คุณแน่ใจหรือไม่?",
		text: text,
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "ใช่",
		cancelButtonText: "ยกเลิก",
		closeOnConfirm: false
	},
	function(){
		$.ajax({
			url: base_url+page+'/fn_checkout',
			type: 'POST',
			data: {hn_code: hn_code,status: status},
		})
		.done(function(res) {
			console.log(res);
			if (res==1) {
				swal({
					title: "เรียบร้อย",
					text: "ทำรายการเรียบร้อย",
					type: "success",
					showConfirmButton: false,
					timer: 1500
				},function () {
					window.location.href = base_url+'patients/lists/'+patient_ward_id
				})
			}else{
				swal("Error!", "Contact super admin", "error");
			}
		})
		.fail(function() {
			console.log("error");
		})
	});
}

function get_naf() {
	console.log('get_naf')
	var check_val_ar = {};
	var check_val_length = 0;
	check_val = $('input[type="number"]');
	for (var i = 0; i < $(check_val).length; i++) {
		// console.log('number : '+$(check_val[i]).attr('id')+' : '+$(check_val[i]).val())
		if ($(check_val[i]).val()!="") {
			set_obj(check_val_ar,$(check_val[i]).attr('id'),$(check_val[i]).val())
			check_val_length++;
		}
	}
	check_val = $('input[type="radio"]:checked');
	for (var i = 0; i < $(check_val).length; i++) {
		// console.log('radio : '+$(check_val[i]).attr('name')+' : '+$(check_val[i]).attr('id').split('_')[$(check_val[i]).attr('id').split('_').length-1])
		set_obj(check_val_ar,$(check_val[i]).attr('name'),$(check_val[i]).attr('id').split('_')[$(check_val[i]).attr('id').split('_').length-1])
		check_val_length++;
	}
	check_val = $('input[type="checkbox"]:checked');
	for (var i = 0; i < $(check_val).length; i++) {
		// console.log('checkbox : '+$(check_val[i]).attr('name')+' : 1')
		set_obj(check_val_ar,$(check_val[i]).attr('name'),1)
		check_val_length++;
	}
	check_val = $('input[type="text"]');
	for (var i = 0; i < $(check_val).length; i++) {
		// console.log('text : '+$(check_val[i]).attr('id')+' : '+$(check_val[i]).val())
		if ($(check_val[i]).val()!="") {
			set_obj(check_val_ar,$(check_val[i]).attr('id'),$(check_val[i]).val())
			check_val_length++;
		}
	}
	check_val = $('input[type="hidden"]');
	for (var i = 0; i < $(check_val).length; i++) {
		// console.log('hidden : '+$(check_val[i]).attr('id')+' : '+$(check_val[i]).val())
		if ($(check_val[i]).val()!="") {
			set_obj(check_val_ar,$(check_val[i]).attr('id'),$(check_val[i]).val())
			check_val_length++;
		}
	}
	setTimeout(function() {
		check_val = $('div.score_get');
		for (var i = 0; i < $(check_val).length; i++) {
			if ($(check_val[i]).html()!="") {
				set_obj(check_val_ar,$(check_val[i]).attr('id'),$(check_val[i]).text())
				check_val_length++;
			}
		}
		set_qwe(check_val_ar);
		if (check_required(required)) {
			$('.assessment-complete').slideDown('fast');
		}else{
			$('.assessment-complete').slideUp('fast');
		}
	}, 300);
}

function get_nt() {
	var check_val_ar = {};
	var check_val_length = 0;
	check_val = $('input[type="number"]');
	for (var i = 0; i < $(check_val).length; i++) {
		// console.log('number : '+$(check_val[i]).attr('id')+' : '+$(check_val[i]).val())
		if ($(check_val[i]).val()!="") {
			if ($(check_val[i]).attr('id')!=undefined) {
				set_obj(check_val_ar,$(check_val[i]).attr('id'),$(check_val[i]).val())
				check_val_length++;
			}
		}
	}
	check_val = $('select');
	for (var i = 0; i < $(check_val).length; i++) {
		// console.log('select : '+$(check_val[i]).attr('id')+' : '+$(check_val[i]).val())
		if ($(check_val[i]).val()!="") {
			set_obj(check_val_ar,$(check_val[i]).attr('id'),$(check_val[i]).val())
			check_val_length++;
		}
	}
	check_val = $('input[type="radio"]:checked');
	for (var i = 0; i < $(check_val).length; i++) {
		// console.log('radio : '+$(check_val[i]).attr('name')+' : '+$(check_val[i]).attr('id').split('_')[$(check_val[i]).attr('id').split('_').length-1])
		set_obj(check_val_ar,$(check_val[i]).attr('name'),$(check_val[i]).attr('id').split('_')[$(check_val[i]).attr('id').split('_').length-1])
		check_val_length++;
	}
	check_val = $('input[type="checkbox"]:checked');
	for (var i = 0; i < $(check_val).length; i++) {
		// console.log('checkbox : '+$(check_val[i]).attr('name')+' : 1')
		if ($(check_val[i]).attr('name')!=undefined) {
			set_obj(check_val_ar,$(check_val[i]).attr('name'),1)
			check_val_length++;
		}
	}
	check_val = $('input.get_noset[type="checkbox"]');
	for (var i = 0; i < $(check_val).length; i++) {
		console.log('get_noset : '+$(check_val[i]).attr('data-name')+' : -1')
		if ($(check_val[i]).prop('checked')==false) {
			set_obj(check_val_ar,$(check_val[i]).attr('data-name'),-1)
			check_val_length++;
		}
	}
	check_val = $('input[type="text"]');
	for (var i = 0; i < $(check_val).length; i++) {
		// console.log('text : '+$(check_val[i]).attr('id')+' : '+$(check_val[i]).val())
		if ($(check_val[i]).val()!="") {
			set_obj(check_val_ar,$(check_val[i]).attr('id'),$(check_val[i]).val())
			check_val_length++;
		}
	}
	check_val = $('input[type="hidden"]');
	for (var i = 0; i < $(check_val).length; i++) {
		// console.log('hidden : '+$(check_val[i]).attr('id')+' : '+$(check_val[i]).val())
		if ($(check_val[i]).val()!="") {
			set_obj(check_val_ar,$(check_val[i]).attr('id'),$(check_val[i]).val())
			check_val_length++;
		}
	}
	setTimeout(function() {
		check_val = $('div.score_get');
		for (var i = 0; i < $(check_val).length; i++) {
			// console.log('div.score_get : '+$(check_val[i]).attr('id')+' : '+$(check_val[i]).text())
			if ($(check_val[i]).html()!="") {
				set_obj(check_val_ar,$(check_val[i]).attr('id'),$(check_val[i]).text())
				check_val_length++;
			}
		}
		
		set_qwe(check_val_ar);
		if (check_required(required)) {
			$('.assessment-complete').slideDown('fast');
		}else{
			$('.assessment-complete').slideUp('fast');
		}
	}, 300);

}

function set_assessment_result(type) {
	console.log('set_assessment_result')
	red_result = '#e74c3c';
	yellow_result = '#f39c12';
	green_result = '#176920';
	color_result = ""
	if (type=="naf") {
		if (sum_all<=5) {
			assessment_result = assessment_result_a
			color_result = green_result
		}else if (sum_all>=6&&sum_all<=10) {
			assessment_result = assessment_result_b
			color_result = yellow_result
		}else if (sum_all>=11) {
			assessment_result = assessment_result_c
			color_result = red_result
		}
		$('.assessment-result-grade').html(assessment_result[0]);
		$('.assessment-result-description').html(assessment_result[1]);
		$('.assessment-result-time').html(assessment_result[2]);
		qwe.score = sum_all
		qwe.grade = assessment_result[0]
		qwe.date_next = assessment_result[3]
	}else if (type=="nt") {
		if (sum_all<=4) {
			assessment_result = assessment_result_1
			color_result = green_result
		}else if (sum_all>=5&&sum_all<=7) {
			assessment_result = assessment_result_2
			color_result = green_result
		}else if (sum_all>=8&&sum_all<=10) {
			assessment_result = assessment_result_3
			color_result = yellow_result
		}else if (sum_all>=11) {
			assessment_result = assessment_result_4
			color_result = red_result
		}
		$('.assessment-result-grade').html(assessment_result[0]);
		$('.assessment-result-description').html(assessment_result[1]);
		$('.assessment-result-time').html(assessment_result[2]);
		qwe.score = sum_all
		qwe.grade = assessment_result[0]
		qwe.date_next = assessment_result[3]
	}
	$('.assessment-result').css('color', color_result);
}

function set_todb(func,redirec,type=-1) {
	if (type!=-1) {
		set_assessment_result(type)
	}
	$.ajax({
		url: base_url+func,
		type: 'POST',
		data: {data: qwe},
	})
	.done(function(res) {
		console.log(res);
		res = JSON.parse(res);
		if (res.status=='success') {
			swal({
				title: "Success!",
				text: res.msg,
				type: "success",
				showConfirmButton: false,
				timer: 1500
			},function () {
				window.location.href = base_url+redirec
			})
		}else{
			swal("Error!", res.msg, "error");
		}
	})
	.fail(function() {
		console.log("error");
	});
}

function set_obj(obj,name,val) {
	return obj[name] = val;
}

function set_qwe(data) {
	qwe = data
}

function check_required(required) {
	// console.log('----- check_required -----')
	var count = 0
	for (var i = 0; i < required.length; i++) {
		// console.log(required[i]+' : '+qwe.hasOwnProperty(required[i]))
		if (qwe.hasOwnProperty(required[i])) {
			count++;
		}
	}
	return count==required.length;
}

function formatDate(date) {
	var monthNames = [
		"มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม"
	];

	var day = date.getDate();
	var monthIndex = date.getMonth();
	var year = date.getFullYear();

	return day + ' ' + monthNames[monthIndex] + ' ' + year;
}

function check_device() {
	console.log('check_device(): width',$(window).width())
	if ($(window).width()>768) {
		console.log('check_device(): desktop')
		return 'desktop'
	}else if($(window).width()<=768&&$(window).width()>480){
		console.log('check_device(): tablet')
		return 'tablet'
	}else{
		console.log('check_device(): mobile')
		return 'mobile'
	}
}

function toggle_show(obj,show,hide) {
	console.log('click',$('.screening-history.show').length)
	if ($('.screening-history.show').length) {
		$(obj).closest('.table-screening').find('.screening-history').removeClass('show')
		$('.screening-history').animate({'height': hide}, 300,function () {
			$(obj).html('<i class="fa fa-angle-down"></i>')
		})
	}else{
		$(obj).closest('.table-screening').find('.screening-history').addClass('show')
		$('.screening-history').animate({'height': show}, 300,function () {
			$(obj).html('<i class="fa fa-angle-up"></i>')
		})
	}
	
}

Object.size = function(obj) {
	var size = 0, key;
	for (key in obj) {
		if (obj.hasOwnProperty(key)) size++;
	}
	return size;
};

Array.prototype.remove = function() {
	var what, a = arguments, L = a.length, ax;
	while (L && this.length) {
		what = a[--L];
		while ((ax = this.indexOf(what)) !== -1) {
			this.splice(ax, 1);
		}
	}
	return this;
};

