qwe = {};
$(document).ready(function() {
	// alert(Date())
	if (check_device()=='desktop') {
		$('.datepicker').datepicker({ dateFormat: 'yy-mm-dd' })

		$('.datepicker.start').change(function(event) {
			// console.log('datepicker start');
			$('.datepicker.end').datepicker("option", "minDate", $('.datepicker.start').val() );
		});

		$('.datepicker.end').change(function(event) {
			// console.log('datepicker end');
			$('.datepicker.start').datepicker("option", "maxDate", $('.datepicker.end').val() );
		});
	}else{
		$('.datepicker').attr('type','date');
		
		$('.datepicker.start').change(function(event) {
			// console.log('datepicker start');
			$('.datepicker.end').attr("min", $('.datepicker.start').val() );
		});

		$('.datepicker.end').change(function(event) {
			// console.log('d end');
			$('.datepicker.start').attr("max", $('.datepicker.end').val() );
		});
	}
	
	if (check_device()=='desktop') {
		if ($('.table-naf').length>0) {
			console.log('.table-naf');
			assessment_height = $(window).height()-500;
		}else if ($('.table-nt').length>0){
			console.log('.table-nt');
			assessment_height = $(window).height()-500;
		}else if ($('.table-screening').length>0){
			console.log('.table-screening');
			assessment_height = $(window).height()-180;
		}else{
			console.log('.no');
			assessment_height = $(window).height()-100;
		}
		$('.assessment-box').css('height', assessment_height+'px');
		$('.assessment-box').css('overflow', 'auto');
		$('body').css('overflow', 'hidden');
		// console.log('desktop',assessment_height)
	}else{
		assessment_height = $('body').height();
		// console.log('mobile',assessment_height)
	}

	$('.body input').change(function(event) {
		if ($(this).attr('no-get')==undefined&&$(this).val()!="-") {
			input_checked = $(this).closest('.body').find('input:checked')
			val = []
			sum = 0
			for (var i = 0; i < input_checked.length; i++) {
				val.push($(input_checked[i]).val())
				sum += parseInt($(input_checked[i]).val())
				if ($(this).attr('maxscore')!=undefined) {
					sum = $(this).attr('maxscore')
				}
			}
			result = $(input_checked[0]).attr('type')=="checkbox" ? sum : val.join('-') ;
			$(this).closest('.body').find('.score').html(result);

			get_score()
		}
	});

	$('#myTabs a').click(function (e) {
		e.preventDefault()
		console.log(this)
		$(this).tab('show')
	})
});

function get_score() {
	//sum and set result all
	score_all = $('.body .score')
	sum_all = 0
	check_all_show = 0
	for (var i = 0; i < score_all.length; i++) {
		if ($(score_all[i]).attr('noscore')==undefined) {
			sum_all += $(score_all[i])[0].innerText!="" ? parseInt($(score_all[i]).text()) : 0;
			check_all_show += $(score_all[i])[0].innerText!="" ? 1 : 0;
		}
	}
	$('.result_all').html(sum_all)
	if ($('.table-naf').length>0) {
		set_assessment_result('naf')
	}else if ($('.table-nt').length>0) {
		set_assessment_result('nt')
	}
}

function cal_bmi() {
	weight = $('#weight_current').val()
	height = $('#height').val()/100
	if (weight!=""&&height!="") {
		bmi = weight/Math.pow(height,2);
		$('.bmi').slideDown('fast');
		$('#bmi').val(bmi.toFixed(2));
		if (bmi<18.5||bmi>25) {
			$('#c_3_yes').prop('checked', true);
			$('.bmi input').addClass('red_1')
			$('.bmi input').removeClass('green_3')
			$('.bmi label').addClass('red_1')
			$('.bmi label').removeClass('green_3')
		}else{
			$('#c_3_no').prop('checked', true);
			$('.bmi input').addClass('green_3')
			$('.bmi input').removeClass('red_1')
			$('.bmi label').addClass('green_3')
			$('.bmi label').removeClass('red_1')
		}
	}else{
		$('.bmi').slideUp('fast');
		$('#bmi').val(0);
		$('#c_3_no').prop('checked', true);
	}
}

function new_tabs(url) {
	$(window).off('beforeunload');
	var a = window.open(url ,'_newtab' + Math.floor(Math.random()*999999));
	a.focus();
}

function check_warning(type,id) {
	swal({
		title: "ยืนยันการตรวจจากแพทย์โภชนาการ",
		text: "คุณคือแพทย์โภชนาการใช่หรือไม่?",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "ใช่",
		cancelButtonText: "ไม่ใช่",
		closeOnConfirm: false
	},
	function(){
		$.ajax({
			url: base_url+'screening/fn_update_status',
			type: 'POST',
			data: {id: id,type: type},
		})
		.done(function(res) {
			console.log(res);
			res = JSON.parse(res)
			if (res.status==1) {
				swal({
					title: "เรียบร้อย!",
					text: "ยืนยันการตรวจเรียบร้อย",
					type: "success",
					showConfirmButton: false,
					timer: 1500
				},function () {
					window.location.reload()
				})
			}else{
				swal("Error!", "Contact super admin", "error");
			}
		})
		.fail(function() {
			console.log("error");
			swal("Error Ajax!", "Contact super admin", "error");
		})
	});
}

function check_alert() {
	if ($('input[type="radio"]:checked').length==4) {
		if ($('.radio_yes:checked').length>=2) {
			$('.table_no').slideDown('fast');
			$('.table_yes').slideUp('fast');
		}else{
			$('.table_no').slideUp('fast');
			$('.table_yes').slideDown('fast');
		}
		var sc_ar = {}
		var sc = $('input[type="radio"]:checked');
		for (var i = 0; i < sc.length; i++) {
			var val = $(sc[i]).attr('id').split('_')[2]=="yes" ? 1 : 0;
			var key = $(sc[i]).attr('id').split('_')[0]+'_'+$(sc[i]).attr('id').split('_')[1]
			set_obj(sc_ar,key,val)
		}
		var check_val = $('.form-control');
		for (var i = 0; i < check_val.length; i++) {
			console.log($(check_val[i]).attr('no-get'))
			if ($(check_val[i]).attr('no-get')==undefined) {
				if ($(check_val[i]).val()!=""&&$(check_val[i]).val()!=null) {
					set_obj(sc_ar,$(check_val[i]).attr('id'),$(check_val[i]).val())
				}
			}
		}
		set_qwe(sc_ar);
	}
}

function del(page,id) {
	swal({
		title: "Are you sure?",
		text: "You will Delete!",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Yes, delete it!",
		closeOnConfirm: false
	},
	function(){
		$.ajax({
			url: base_url+page+'/fn_delete',
			type: 'POST',
			data: {id: id},
		})
		.done(function(res) {
			console.log(res);
			res = JSON.parse(res)
			if (res.status==1) {
				swal({
					title: "Deleted!",
					text: "Delete row success",
					type: "success",
					showConfirmButton: false,
					timer: 1500
				},function () {
					$(window).off('beforeunload');
					window.location.href = base_url+res.redirect
				})
			}else{
				swal("Error!", "Contact super admin", "error");
			}
		})
		.fail(function() {
			console.log("error");
			swal("Error Ajax!", "Contact super admin", "error");
		})
	});
}

function checkout(page,hn_code,patient_ward_id,status) {
	if (status==1) {
		var text = 'คนไข้ออกจากโรงพยาบาลใช่หรือไม่'
	}else{
		var text = 'คนไข้เสียชีวิตใช่หรือไม่'
	}
	swal({
		title: "คุณแน่ใจหรือไม่?",
		text: text,
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "ใช่",
		cancelButtonText: "ยกเลิก",
		closeOnConfirm: false
	},
	function(){
		$.ajax({
			url: base_url+page+'/fn_checkout',
			type: 'POST',
			data: {hn_code: hn_code,status: status},
		})
		.done(function(res) {
			console.log(res);
			if (res==1) {
				swal({
					title: "เรียบร้อย",
					text: "ทำรายการเรียบร้อย",
					type: "success",
					showConfirmButton: false,
					timer: 1500
				},function () {
					$(window).off('beforeunload');
					window.location.href = base_url+'patients/lists/'+patient_ward_id
				})
			}else{
				swal("Error!", "Contact super admin", "error");
			}
		})
		.fail(function() {
			console.log("error");
		})
	});
}

function get_naf() {
	console.log('get_naf')
	var check_val_ar = {};
	var check_val_length = 0;
	check_val = $('input[type="number"]');
	for (var i = 0; i < $(check_val).length; i++) {
		// console.log('number : '+$(check_val[i]).attr('id')+' : '+$(check_val[i]).val())
		if ($(check_val[i]).val()!="") {
			set_obj(check_val_ar,$(check_val[i]).attr('id'),$(check_val[i]).val())
			check_val_length++;
		}
	}
	check_val = $('input[type="radio"]:checked');
	for (var i = 0; i < $(check_val).length; i++) {
		// console.log('radio : '+$(check_val[i]).attr('name')+' : '+$(check_val[i]).attr('id').split('_')[$(check_val[i]).attr('id').split('_').length-1])
		set_obj(check_val_ar,$(check_val[i]).attr('name'),$(check_val[i]).attr('id').split('_')[$(check_val[i]).attr('id').split('_').length-1])
		check_val_length++;
	}
	check_val = $('input[type="checkbox"]:checked');
	for (var i = 0; i < $(check_val).length; i++) {
		// console.log('checkbox : '+$(check_val[i]).attr('name')+' : 1')
		set_obj(check_val_ar,$(check_val[i]).attr('name'),1)
		check_val_length++;
	}
	check_val = $('input[type="text"]');
	for (var i = 0; i < $(check_val).length; i++) {
		// console.log('text : '+$(check_val[i]).attr('id')+' : '+$(check_val[i]).val())
		if ($(check_val[i]).val()!="") {
			set_obj(check_val_ar,$(check_val[i]).attr('id'),$(check_val[i]).val())
			check_val_length++;
		}
	}
	check_val = $('input[type="hidden"]');
	for (var i = 0; i < $(check_val).length; i++) {
		// console.log('hidden : '+$(check_val[i]).attr('id')+' : '+$(check_val[i]).val())
		if ($(check_val[i]).val()!=""&&$(check_val[i]).attr('no-get')==undefined) {
			set_obj(check_val_ar,$(check_val[i]).attr('id'),$(check_val[i]).val())
			check_val_length++;
		}
	}
	setTimeout(function() {
		check_val = $('div.score_get');
		for (var i = 0; i < $(check_val).length; i++) {
			if ($(check_val[i]).html()!="") {
				set_obj(check_val_ar,$(check_val[i]).attr('id'),$(check_val[i]).text())
				check_val_length++;
			}
		}
		set_qwe(check_val_ar);
		if (check_required(required)) {
			$('.assessment-complete').slideDown('fast');
			$('.assessment-warning').slideUp('fast');
		}else{
			$('.assessment-complete').slideUp('fast');
			$('.assessment-warning').slideDown('fast');
		}
	}, 300);
}

function get_nt() {
	var check_val_ar = {};
	var check_val_length = 0;
	check_val = $('input[type="number"]');
	for (var i = 0; i < $(check_val).length; i++) {
		// console.log('number : '+$(check_val[i]).attr('id')+' : '+$(check_val[i]).val())
		if ($(check_val[i]).val()!="") {
			if ($(check_val[i]).attr('id')!=undefined) {
				set_obj(check_val_ar,$(check_val[i]).attr('id'),$(check_val[i]).val())
				check_val_length++;
			}
		}
	}
	check_val = $('select');
	for (var i = 0; i < $(check_val).length; i++) {
		// console.log('select : '+$(check_val[i]).attr('id')+' : '+$(check_val[i]).val())
		if ($(check_val[i]).val()!="") {
			set_obj(check_val_ar,$(check_val[i]).attr('id'),$(check_val[i]).val())
			check_val_length++;
		}
	}
	check_val = $('input[type="radio"]:checked');
	for (var i = 0; i < $(check_val).length; i++) {
		// console.log('radio : '+$(check_val[i]).attr('name')+' : '+$(check_val[i]).attr('id').split('_')[$(check_val[i]).attr('id').split('_').length-1])
		set_obj(check_val_ar,$(check_val[i]).attr('name'),$(check_val[i]).attr('id').split('_')[$(check_val[i]).attr('id').split('_').length-1])
		check_val_length++;
	}
	check_val = $('input[type="checkbox"]:checked');
	for (var i = 0; i < $(check_val).length; i++) {
		// console.log('checkbox : '+$(check_val[i]).attr('name')+' : 1')
		if ($(check_val[i]).attr('name')!=undefined) {
			set_obj(check_val_ar,$(check_val[i]).attr('name'),1)
			check_val_length++;
		}
	}
	check_val = $('input.get_noset[type="checkbox"]');
	for (var i = 0; i < $(check_val).length; i++) {
		console.log('get_noset : '+$(check_val[i]).attr('data-name')+' : -1')
		if ($(check_val[i]).prop('checked')==false) {
			set_obj(check_val_ar,$(check_val[i]).attr('data-name'),-1)
			check_val_length++;
		}
	}
	check_val = $('input[type="text"]');
	for (var i = 0; i < $(check_val).length; i++) {
		// console.log('text : '+$(check_val[i]).attr('id')+' : '+$(check_val[i]).val())
		if ($(check_val[i]).val()!="") {
			set_obj(check_val_ar,$(check_val[i]).attr('id'),$(check_val[i]).val())
			check_val_length++;
		}
	}
	check_val = $('input[type="hidden"]');
	for (var i = 0; i < $(check_val).length; i++) {
		// console.log('hidden : '+$(check_val[i]).attr('id')+' : '+$(check_val[i]).val())
		if ($(check_val[i]).val()!=""&&$(check_val[i]).attr('no-get')==undefined) {
			set_obj(check_val_ar,$(check_val[i]).attr('id'),$(check_val[i]).val())
			check_val_length++;
		}
	}
	setTimeout(function() {
		check_val = $('div.score_get');
		for (var i = 0; i < $(check_val).length; i++) {
			// console.log('div.score_get : '+$(check_val[i]).attr('id')+' : '+$(check_val[i]).text())
			if ($(check_val[i]).html()!="") {
				set_obj(check_val_ar,$(check_val[i]).attr('id'),$(check_val[i]).text())
				check_val_length++;
			}
		}
		
		set_qwe(check_val_ar);
		if (check_required(required)) {
			$('.assessment-complete').slideDown('fast');
			$('.assessment-warning').slideUp('fast');
		}else{
			$('.assessment-complete').slideUp('fast');
			$('.assessment-warning').slideDown('fast');
		}
	}, 300);

}

function set_assessment_result(type) {
	console.log('set_assessment_result')
	red_result = '#e74c3c';
	yellow_result = '#f39c12';
	green_result = '#176920';
	color_result = ""
	if (type=="naf") {
		assessment_result_a = ['NAF-A','0-5 คะแนน (NAF = A : Normal-Mild malnutrition) ไม่พบความเสี่ยงต่อการเกิดภาวะทุพโภชนาการ พยาบาลจะทำหน้าที่ประเมินภาวะโภชนาการซ้ำ','ภายใน 7 วัน',7]
		assessment_result_b = ['NAF-B','6-10 คะแนน (NAF = B : Moderate malnutrition) กรุณาแจ้งให้แพทย์และนักกำหนดอาหาร/นักโภชนาการทราบผลทันทีพบความเสี่ยงต่อการเกิดภาวะทุพโภชนาการ ให้นักกำหนดอาหาร/นักโภชนาการ ทำการประเมินภาวะโภชนาการและให้แพทย์ทำการดูแลรักษา','ภายใน 3 วัน',3]
		assessment_result_c = ['NAF-C','<label class="red_1">&#8805; 11 คะแนน (NAF = C : Severe malnutrition) กรุณาแจ้งให้แพทย์และนักกำหนดอาหาร/นักโภชนาการทราบผลทันที มีภาวะทุพโภชนาการ ให้นักกำหนดอาหาร/นักโภชนาการ ทำการประเมินภาวะโภชนาการและให้แพทย์ทำการดูแลรักษา</label>','<label class="red_1">ภายใน 24 ชั่วโมง</label>',1]
		if (sum_all<=5) {
			assessment_result = assessment_result_a
			if ($('#bmi').val()>=17&&$('#bmi').val()<18.50) {
				assessment_result[1] = '0-5 คะแนน (NAF = A : Normal-Mild malnutrition) <br> ไม่มีภาวะทุพโภชนาการให้ทำการประเมินภาวะโภชนาการซ้ำภายใน 7 วัน'
			}
			color_result = green_result
		}else if (sum_all>=6&&sum_all<=10) {
			assessment_result = assessment_result_b
			if ($('#bmi').val()>=16&&$('#bmi').val()<17) {
				assessment_result[1] = '6-10 คะแนน (NAF = B : Moderate malnutrition) <br> E44.0 - Moderate malnutrition <br> กรุณาแจ้งให้แพทย์และนักกำหนดอาหาร/นักโภชนาการทราบผลทันทีพบความเสี่ยงต่อการเกิดภาวะทุพโภชนาการ ให้นักกำหนดอาหาร/นักโภชนาการ ทำการประเมินภาวะโภชนาการและให้แพทย์ทำการดูแลรักษา'
			}
			color_result = yellow_result
		}else if (sum_all>=11) {
			assessment_result = assessment_result_c
			if ($('#bmi').val()<16) {
				assessment_result[1] = '<label class="red_1">&#8805; 11 คะแนน (NAF = C : Severe malnutrition) <br> E43 - Severe malnutrition <br> กรุณาแจ้งให้แพทย์และนักกำหนดอาหาร/นักโภชนาการทราบผลทันที มีภาวะทุพโภชนาการ ให้นักกำหนดอาหาร/นักโภชนาการ ทำการประเมินภาวะโภชนาการและให้แพทย์ทำการดูแลรักษา</label>'
			}
			color_result = red_result
		}
		$('.assessment-result-grade').html(assessment_result[0]);
		$('.assessment-result-description').html(assessment_result[1]);
		$('.assessment-result-time').html(assessment_result[2]);
		qwe.score = sum_all
		qwe.grade = assessment_result[0]
		qwe.date_next = assessment_result[3]
	}else if (type=="nt") {
		assessment_result_1 = ['NT-1','ระดับภาวะทุพโภชนาการไม่มีหรือมีความเสี่ยง','ติดตามประเมินทุก 6-8 สัปดาห์',42]
		assessment_result_2 = ['NT-2','ระดับภาวะทุพโภชนาการเล็กน้อย','ติดตามระเมินทุก 4-6 สัปดาห์',28]
		assessment_result_3 = ['NT-3','ระดับภาวะทุพโภชนาการปานกลาง ควรเริ่มให้โภชนบำบัด','ประเมินทุก 3-7 วัน',3]
		assessment_result_4 = ['NT-4','ระดับภาวะทุพโภชนาการรุนแรง พิจารณาส่งปรึกษาทีมโภชนบำบัด ทันที','ภายใน 24 ชั่วโมง',1]
		if (sum_all<=4) {
			assessment_result = assessment_result_1
			color_result = green_result
		}else if (sum_all>=5&&sum_all<=7) {
			assessment_result = assessment_result_2
			if ($('#bmi').val()>=17&&$('#bmi').val()<18.50) {
				assessment_result[1] = 'ระดับภาวะทุพโภชนาการเล็กน้อย E44.1 - Mild malnutrition (Mild protein - calorie malnutrition)'
			}
			color_result = green_result
		}else if (sum_all>=8&&sum_all<=10) {
			assessment_result = assessment_result_3
			if ($('#bmi').val()>=16&&$('#bmi').val()<17) {
				assessment_result[1] = 'ระดับภาวะทุพโภชนาการปานกลาง E44.0 - Moderate malnutrition (Moderate protein - calorie malnutrition) <br> ควรเริ่มให้โภชนบำบัด'
			}
			color_result = yellow_result
		}else if (sum_all>=11) {
			assessment_result = assessment_result_4
			if ($('#bmi').val()<16) {
				assessment_result[1] = 'ระดับภาวะทุพโภชนาการรุนแรง E43 - Severe malnutrition (Unspecified severe protein - calorie malnutrition) <br> พิจารณาส่งปรึกษาทีมโภชนบำบัด ทันที'
			}
			color_result = red_result
		}
		$('.assessment-result-grade').html(assessment_result[0]);
		$('.assessment-result-description').html(assessment_result[1]);
		$('.assessment-result-time').html(assessment_result[2]);
		qwe.score = sum_all
		qwe.grade = assessment_result[0]
		qwe.date_next = assessment_result[3]
	}
	$('.assessment-result').css('color', color_result);
}

function set_todb(func,redirec,type) {
	if (type!=-1) {
		set_assessment_result(type)
	}
	$(".se-pre-con").fadeIn("slow");
	$.ajax({
		url: base_url+func,
		type: 'POST',
		data: {data: qwe},
	})
	.done(function(res) {
		console.log(res);
		res = JSON.parse(res);
		$(".se-pre-con").fadeOut("slow");
		if (res.status=='success') {
			swal({
				title: "Success!",
				text: res.msg,
				type: "success",
				showConfirmButton: false,
				timer: 1500
			},function () { 
			    $(window).off('beforeunload');
				if (redirec=="patients/lists/") {
					window.location.href = base_url+redirec+'/'+res.patient_ward
				}else if(redirec=='screening/create_naf'||redirec=='screening/create_nt'){
					window.location.href = base_url+redirec+'/'+res.screening_id
				}else{
					window.location.href = base_url+redirec
				}
				
			})
		}else{
			swal("Error!", res.msg, "error");
		}
	})
	.fail(function() {
		$(".se-pre-con").fadeOut("slow");
		console.log("error");
		swal("Error Ajax!", "Contact super admin", "error");
	});
}

function set_obj(obj,name,val) {
	return obj[name] = val;
}

function set_qwe(data) {
	qwe = data
}

function focus_warning(name) {
	$('input[name="'+name+'"]').focus();
}

function check_required(required) {
	// console.log('----- check_required -----')
	var count = 0
	tmp_req = ''
	for (var i = 0; i < required.length; i++) {
		// console.log(required[i]+' : '+qwe.hasOwnProperty(required[i]))
		if (qwe.hasOwnProperty(required[i])) {
			count++;
		}else{
			tmp_req += '<a class="link" onclick=focus_warning("'+required[i]+'")><div class="body"><div class="col-sm-8 col-xs-8 list">'+required_name[i]+'</div><div class="col-sm-4 col-xs-4 list">ตอบคำถาม</div></div></a>'
		}
	}
	$('.list-required').html(tmp_req)
	return count==required.length;
}

function get_last_param() {
	return document.URL.split('/')[document.URL.split('/').length-1];
}

function formatDate(date) {
	var monthNames = [
		"มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม"
	];

	var day = date.getDate();
	var monthIndex = date.getMonth();
	var year = date.getFullYear();

	return day + ' ' + monthNames[monthIndex] + ' ' + year;
}

function check_device() {
	console.log('check_device(): width',$(window).width())
	if ($(window).width()>768) {
		console.log('check_device(): desktop')
		return 'desktop'
	}else if($(window).width()<=768&&$(window).width()>480){
		console.log('check_device(): tablet')
		return 'tablet'
	}else{
		console.log('check_device(): mobile')
		return 'mobile'
	}
}

function toggle_show(obj,show,hide) {
	console.log('click',$('.screening-history.show').length)
	if ($('.screening-history.show').length) {
		$(obj).closest('.table-screening').find('.screening-history').removeClass('show')
		$('.screening-history').animate({'height': hide}, 300,function () {
			$(obj).html('<i class="fa fa-angle-down"></i>')
		})
	}else{
		$(obj).closest('.table-screening').find('.screening-history').addClass('show')
		$('.screening-history').animate({'height': show}, 300,function () {
			$(obj).html('<i class="fa fa-angle-up"></i>')
		})
	}
	
}

Object.size = function(obj) {
	var size = 0, key;
	for (key in obj) {
		if (obj.hasOwnProperty(key)) size++;
	}
	return size;
}

Array.prototype.remove = function() {
	var what, a = arguments, L = a.length, ax;
	while (L && this.length) {
		what = a[--L];
		while ((ax = this.indexOf(what)) !== -1) {
			this.splice(ax, 1);
		}
	}
	return this;
}

