var gulp        = require('gulp');
var browserSync = require('browser-sync');
var reload      = browserSync.reload;
var sass        = require('gulp-sass');
 
// browser-sync task for starting the server.
gulp.task('browser-sync', function() {
    //watch files
    var files = [ //ไฟล์ที่ต้องการให้รีเฟรชหน้าเว็บเมื่อมีการแก้ไข
    './css/*.css',
    './application/controllers/*.php',
    './application/views/*.php',
    './application/views/backoffice/*.php',
    './application/config/*.php',
    './js/*.js',
    './*.html',
    './**/*.html',
    ];
 
    //initialize browsersync
    browserSync.init(files, {
    //browsersync with a php server
    proxy: "localhost/baxter/index.php", // พาร์ทไฟล์ที่ต้องการรีไดเรคเมื่อเปิด
    notify: false
    });
});
 
// Sass task, will run when any SCSS files change & BrowserSync
// will auto-update browsers
gulp.task('sass', function () {
  return gulp.src('./sass/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./css'));
});
 gulp.task('default',['browser-sync'], function () {
  gulp.watch('./sass/**/*.scss', ['sass']);
});

