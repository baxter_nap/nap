<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script type="text/javascript">
	required = ['n2_1','n3_1','n4_1','n5_1','n5_2','n7_1']
	required_name = [
					'2.1 น้ำหนักปัจจุบัน',
					'3. รูปร่างของผู้ป่วย',
					'4. น้ำหนักเปลี่ยนใน 4 สัปดาห์',
					'5.1 ลักษณะอาหาร',
					'5.2 ปริมาณที่กิน',
					'7. ความสามารถในการเข้าถึงอาหาร',
				]
	sum_all = 0
	$(document).ready(function() {
		check_required(required)
	});
</script>
<div class="head-box row">
	<div class="col-sm-10 col-xs-8">
		<div class="title pink_1">แบบประเมิน Assessment NAF</div>
		<div class="sub hidden-xs">*Reference : Surat Komindr ,et al. simplified malnutrition tool for Thai patients Asia Pac J Clin Nutr 2013;22(4):516-521</div>
	</div>
	<div class="col-sm-2 col-xs-4">
		<div class="title pink_1">คะแนนที่ได้</div>
	</div>
</div>

<div class="assessment-box">
	<div class="row box table-screening table-naf">
		<div class="header no-radius pink_1">
			<div class="col-sm-12 col-xs-12">1. ส่วนสูง / ความยาวตัว / ความยาวช่วงแขนจากปลายนิ้วกลางทั้ง 2 ข้าง</div>
		</div>
		<div class="body">
			<div class="col-sm-12 col-xs-12 form-group">
				<script type="text/javascript">
					function checkminmax(obj,type,point,min,max) {
						if ($(obj).val()<min||$(obj).val()>max) {
							swal({
								title: "Warning",
								text: type+'ของผู้ป่วยไม่ควรต่ำกว่า '+min+' '+point+' หรือ มากกว่า '+max+' '+point,
								type: "warning",
							},function () {
								$(obj).parent().addClass('has-error');
								$(obj).val("");
								$(obj).focus();
							})
						}else{
							$(obj).parent().removeClass('has-error');
						}
					}
				</script>
				<div class="col-sm-10 col-xs-10 question_assessment">
					<div class="col-sm-5 col-xs-12 label-input">ส่วนสูง</div>
					<div class="col-sm-7 col-xs-12 form-group">
						<input type="number" name="" id="n1_1" onchange="get_naf();checkminmax(this,'ส่วนสูง','เซนติเมตร',0,250)" value="<?=$data['height']?>" class="form-control">
						<label class="sub-label" for="n1_1">เซนติเมตร</label>
					</div>
					<div class="col-sm-5 col-xs-12 label-input">ความยาวตัว</div>
					<div class="col-sm-7 col-xs-12 form-group">
						<input type="number" name="" id="n1_2" onchange="get_naf();checkminmax(this,'ความยาวตัว','เซนติเมตร',0,200)" class="form-control">
						<label class="sub-label" for="n1_2">เซนติเมตร</label>
					</div>
					<div class="col-sm-5 col-xs-12 label-input">ความยาวช่วงแขน</div>
					<div class="col-sm-7 col-xs-12 form-group">
						<input type="number" name="" id="n1_3" onchange="get_naf();checkminmax(this,'ความยาวช่วงแขน','เซนติเมตร',0,250)" class="form-control">
						<label class="sub-label" for="n1_3">เซนติเมตร</label>
					</div>
				</div>
				<div class="col-sm-2 col-xs-2 score"></div>
			</div>
		</div>
		<div class="header no-radius pink_1">
			<div class="col-sm-12 col-xs-12">2. น้ำหนักและค่าดัชนีมวลกาย (ค่าดัชนีมวลกาย (BMI) = น้ำหนัก (กก.)/ ส่วนสูง(ม.)<sup>2</sup>)</div>
		</div>
		<div class="body">
			<div class="col-sm-12 col-xs-12 form-group">
				<div class="col-sm-10 col-xs-10 question_assessment">
					<div class="row">
						<div class="col-sm-12 col-xs-12">2.1 น้ำหนักปัจจุบัน</div>
					</div>	
					<script type="text/javascript">
						function check_n2_1(status) {
							console.log('status',status)
							if (status) {
								$('.check_n2_1').slideDown('fast')
								$('.check_n2_2').slideUp('fast')
								$('.n2_3 input:checked').prop('checked',false)
								$('.n2_3 .score').html("")
								$('.n2_4 input:checked').prop('checked',false)
								$('.n2_4 .score').html("")
								required.push("n2_1_weight")
								required.push("n2_2")
								required.remove("n2_3")
								required.remove("n2_4")
							}else{
								$('.check_n2_1').slideUp('fast')
								$('.check_n2_2').slideDown('fast')
								$('.n2_2 input:checked').prop('checked',false)
								$('.n2_2 .score').html("")
								$('#n2_1_weight').val('');
								required.push("n2_3")
								required.push("n2_4")
								required.remove("n2_1_weight")
								required.remove("n2_2")
							}
							get_naf()
						}
						$(document).ready(function() {
							cal_bmi_naf()
						});
						function cal_bmi_naf(){
							weight = $('#n2_1_weight').val()
							height = $('#n1_1').val()/100
							if (weight!=""&&height!="") {
								bmi = weight/Math.pow(height,2);
								if (bmi<17) {
									$('#n2_2_1').prop('checked', true)
									$('#n2_2_score').html(2)
								}else if(bmi>=17&&bmi<=18){
									$('#n2_2_2').prop('checked', true)
									$('#n2_2_score').html(1)
								}else if(bmi>18&&bmi<30){
									$('#n2_2_3').prop('checked', true)
									$('#n2_2_score').html(0)
								}else if(bmi>=30){
									$('#n2_2_4').prop('checked', true)
									$('#n2_2_score').html(1)
								}
								$('#bmi').val(bmi.toFixed(2));
								get_naf()
							}
						}
					</script>
					<div class="col-sm-6 col-xs-12">
						<input type="radio" class="mtl with-gap" name="n2_1" value="1" onclick="check_n2_1(1)" id="n2_1_1">
						<label for="n2_1_1">ชั่งในท่านอน (1)</label>
					</div>
					<div class="col-sm-6 col-xs-12">
						<input type="radio" class="mtl with-gap" name="n2_1" value="0" onclick="check_n2_1(1)" id="n2_1_2">
						<label for="n2_1_2">ชั่งในท่ายืน (0)</label>
					</div>
					<div class="col-sm-6 col-xs-12">
						<input type="radio" class="mtl with-gap" name="n2_1" value="0" onclick="check_n2_1(0)" id="n2_1_3">
						<label for="n2_1_3">ชั่งไม่ได้ (0)</label>
					</div>
					<div class="col-sm-6 col-xs-12">
						<input type="radio" class="mtl with-gap" name="n2_1" value="0" onclick="check_n2_1(1)" id="n2_1_4">
						<label for="n2_1_4">ญาติบอก (0)</label>
					</div>
					<div class="col-sm-5 col-xs-12 label-input check_n2_1" style="display: none;">น้ำหนักปัจจุบัน</div>
					<div class="col-sm-7 col-xs-12 form-group check_n2_1" style="display: none;">
						<input type="hidden" id="bmi" value="">
						<input type="number" name="n2_1_weight" id="n2_1_weight" onchange="get_naf();checkminmax(this,'น้ำหนักปัจจุบัน','กิโลกรัม',0,300)" onkeyup="cal_bmi_naf()" value="<?=$data['weight_current']?>" class="form-control">
						<label class="sub-label" for="n2_1_weight">กิโลกรัม</label>
					</div>
				</div>
				<div class="col-sm-2 col-xs-2 score score_get" id="n2_1_score"></div>
			</div>
		</div>
		<div class="body n2_2 check_n2_1" style="display: none;">
			<div class="col-sm-12 col-xs-12 form-group">
				<div class="col-sm-10 col-xs-10 question_assessment">
					<div class="row">
						<div class="col-sm-12 col-xs-12">2.2 BMI</div>
					</div>
					<div class="col-sm-6 col-xs-12">
						<input type="radio" class="mtl with-gap" name="n2_2" onchange="get_naf()" value="2" id="n2_2_1" readonly>
						<label>BMI < 17.0 กก./ม<sup>2</sup> (2)</label>
					</div>
					<div class="col-sm-6 col-xs-12">
						<input type="radio" class="mtl with-gap" name="n2_2" onchange="get_naf()" value="1" id="n2_2_2" readonly>
						<label>BMI 17.0-18.0 กก./ม<sup>2</sup> (1)</label>
					</div>
					<div class="col-sm-6 col-xs-12">
						<input type="radio" class="mtl with-gap" name="n2_2" onchange="get_naf()" value="0" id="n2_2_3" readonly>
						<label>BMI 18.1-29.9 กก./ม<sup>2</sup> (0)</label>
					</div>
					<div class="col-sm-6 col-xs-12">
						<input type="radio" class="mtl with-gap" name="n2_2" onchange="get_naf()" value="1" id="n2_2_4" readonly>
						<label>BMI &#8805; 30.0 กก./ม<sup>2</sup> (1)</label>
					</div>
				</div>
				<div class="col-sm-2 col-xs-2 score score_get" id="n2_2_score"></div>
			</div>
		</div>
		<div class="body n2_3 check_n2_2" style="display: none;">
			<div class="col-sm-12 col-xs-12 form-group">
				<div class="col-sm-10 col-xs-10 question_assessment">
					<div class="row">
						<div class="col-sm-12 col-xs-12">2.3 ผล Albumin</div>
					</div>
					<div class="col-sm-6 col-xs-12">
						<input type="radio" class="mtl with-gap" name="n2_3" onchange="get_naf()" value="3" id="n2_3_1">
						<label for="n2_3_1">&#8804; 2.5 g/dl (<25 g/l) (3) </label>
					</div>
					<div class="col-sm-6 col-xs-12">
						<input type="radio" class="mtl with-gap" name="n2_3" onchange="get_naf()" value="2" id="n2_3_2">
						<label for="n2_3_2">2.6 - 2.9 g/dl (26-29 g/l) (2)</label>
					</div>
					<div class="col-sm-6 col-xs-12">
						<input type="radio" class="mtl with-gap" name="n2_3" onchange="get_naf()" value="1" id="n2_3_3">
						<label for="n2_3_3">3.0 - 3.5 g/dl (30-35 g/l) (1)</label>
					</div>
					<div class="col-sm-6 col-xs-12">
						<input type="radio" class="mtl with-gap" name="n2_3" onchange="get_naf()" value="0" id="n2_3_4">
						<label for="n2_3_4">> 3.5 g/dl (35 g/l) (0)</label>
					</div>
				</div>
				<div class="col-sm-2 col-xs-2 score score_get" id="n2_3_score"></div>
			</div>
		</div>
		<div class="body n2_4 check_n2_2" style="display: none;">
			<div class="col-sm-12 col-xs-12 form-group">
				<div class="col-sm-10 col-xs-10 question_assessment">
					<div class="row">
						<div class="col-sm-12 col-xs-12">2.4 ผล TLC (Total Lymphocyte Count)</div>
					</div>
					<div class="col-sm-6 col-xs-12">
						<input type="radio" class="mtl with-gap" name="n2_4" onchange="get_naf()" value="3" id="n2_4_1">
						<label for="n2_4_1">&#8804; 1,000 cells/mm<sup>2</sup> (3) </label>
					</div>
					<div class="col-sm-6 col-xs-12">
						<input type="radio" class="mtl with-gap" name="n2_4" onchange="get_naf()" value="2" id="n2_4_2">
						<label for="n2_4_2">1,001-1,200 cells/mm<sup>2</sup> (2) </label>
					</div>
					<div class="col-sm-6 col-xs-12">
						<input type="radio" class="mtl with-gap" name="n2_4" onchange="get_naf()" value="1" id="n2_4_3">
						<label for="n2_4_3">1,201-1,500 cells/mm<sup>2</sup> (1) </label>
					</div>
					<div class="col-sm-6 col-xs-12">
						<input type="radio" class="mtl with-gap" name="n2_4" onchange="get_naf()" value="0" id="n2_4_4">
						<label for="n2_4_4">> 1,500 cells/mm<sup>2</sup> (0) </label>
					</div>
				</div>
				<div class="col-sm-2 col-xs-2 score score_get" id="n2_4_score"></div>
			</div>
		</div>
		<div class="header no-radius pink_1">
			<div class="col-sm-12 col-xs-12">3. รูปร่างของผู้ป่วย</div>
		</div>
		<div class="body">
			<div class="col-sm-12 col-xs-12 form-group">
				<div class="col-sm-10 col-xs-10 question_assessment">
					<div class="col-sm-6 col-xs-12">
						<input type="radio" class="mtl with-gap" name="n3_1" onchange="get_naf()" value="2" id="n3_1">
						<label for="n3_1">ผอมมาก (2)</label>
					</div>
					<div class="col-sm-6 col-xs-12">
						<input type="radio" class="mtl with-gap" name="n3_1" onchange="get_naf()" value="1" id="n3_2">
						<label for="n3_2">ผอม (1)</label>
					</div>
					<div class="col-sm-6 col-xs-12">
						<input type="radio" class="mtl with-gap" name="n3_1" onchange="get_naf()" value="1" id="n3_3">
						<label for="n3_3">อ้วนมาก (1)</label>
					</div>
					<div class="col-sm-6 col-xs-12">
						<input type="radio" class="mtl with-gap" name="n3_1" onchange="get_naf()" value="0" id="n3_4">
						<label for="n3_4">ปกติ-อ้วนปานกลาง (0)</label>
					</div>
				</div>
				<div class="col-sm-2 col-xs-2 score score_get" id="n3_1_score"></div>
			</div>
		</div>
		<div class="header no-radius pink_1">
			<div class="col-sm-12 col-xs-12">4. น้ำหนักเปลี่ยนใน 4 สัปดาห์</div>
		</div>
		<div class="body">
			<div class="col-sm-12 col-xs-12 form-group">
				<div class="col-sm-10 col-xs-10 question_assessment">
					<div class="col-sm-6 col-xs-12">
						<input type="radio" class="mtl with-gap" name="n4_1" onchange="get_naf()" value="2" id="n4_1">
						<label for="n4_1">ลดลง / ผอมลง (2)</label>
					</div>
					<div class="col-sm-6 col-xs-12">
						<input type="radio" class="mtl with-gap" name="n4_1" onchange="get_naf()" value="1" id="n4_2">
						<label for="n4_2">เพิ่มขึ้น / อ้วนขึ้น (1)</label>
					</div>
					<div class="col-sm-6 col-xs-12">
						<input type="radio" class="mtl with-gap" name="n4_1" onchange="get_naf()" value="0" id="n4_3">
						<label for="n4_3">ไม่ทราบ (0)</label>
					</div>
					<div class="col-sm-6 col-xs-12">
						<input type="radio" class="mtl with-gap" name="n4_1" onchange="get_naf()" value="0" id="n4_4">
						<label for="n4_4">คงเดิม (0)</label>
					</div>
				</div>
				<div class="col-sm-2 col-xs-2 score score_get" id="n4_1_score"></div>
			</div>
		</div>
		<div class="header no-radius pink_1">
			<div class="col-sm-12 col-xs-12">5. อาหารที่กินในช่วง 2 สัปดาห์ที่ผ่านมา</div>
		</div>
		<div class="body">
			<div class="col-sm-12 col-xs-12 form-group">
				<div class="col-sm-10 col-xs-10 question_assessment">
					<div class="row">
						<div class="col-sm-12 col-xs-12">5.1 ลักษณะอาหาร</div>
					</div>
					<div class="col-sm-6 col-xs-12">
						<input type="radio" class="mtl with-gap" name="n5_1" onchange="get_naf()" value="2" id="n5_1_1">
						<label for="n5_1_1">อาหารน้ำๆ (2)</label>
					</div>
					<div class="col-sm-6 col-xs-12">
						<input type="radio" class="mtl with-gap" name="n5_1" onchange="get_naf()" value="2" id="n5_1_2">
						<label for="n5_1_2">อาหารเหลวๆ (2)</label>
					</div>
					<div class="col-sm-6 col-xs-12">
						<input type="radio" class="mtl with-gap" name="n5_1" onchange="get_naf()" value="1" id="n5_1_3">
						<label for="n5_1_3">อาหารนุ่มกว่าปกติ (1)</label>
					</div>
					<div class="col-sm-6 col-xs-12">
						<input type="radio" class="mtl with-gap" name="n5_1" onchange="get_naf()" value="0" id="n5_1_4">
						<label for="n5_1_4">อาหารเหมือนปกติ (0)</label>
					</div>
				</div>
				<div class="col-sm-2 col-xs-2 score score_get" id="n5_1_score"></div>
			</div>
		</div>
		<div class="body">
			<div class="col-sm-12 col-xs-12 form-group">
				<div class="col-sm-10 col-xs-10 question_assessment">
					<div class="row">
						<div class="col-sm-12 col-xs-12">5.2 ปริมาณที่กิน</div>
					</div>
					<div class="col-sm-6 col-xs-12">
						<input type="radio" class="mtl with-gap" name="n5_2" onchange="get_naf()" value="2" id="n5_2_1">
						<label for="n5_2_1">กินน้อยมาก (2)</label>
					</div>
					<div class="col-sm-6 col-xs-12">
						<input type="radio" class="mtl with-gap" name="n5_2" onchange="get_naf()" value="1" id="n5_2_2">
						<label for="n5_2_2">กินน้อยลง (1)</label>
					</div>
					<div class="col-sm-6 col-xs-12">
						<input type="radio" class="mtl with-gap" name="n5_2" onchange="get_naf()" value="0" id="n5_2_3">
						<label for="n5_2_3">กินมากขึ้น (0)</label>
					</div>
					<div class="col-sm-6 col-xs-12">
						<input type="radio" class="mtl with-gap" name="n5_2" onchange="get_naf()" value="0" id="n5_2_4">
						<label for="n5_2_4">กินเท่าปกติ (0)</label>
					</div>
				</div>
				<div class="col-sm-2 col-xs-2 score score_get" id="n5_2_score"></div>
			</div>
		</div>
		<div class="header no-radius pink_1">
			<div class="col-sm-12 col-xs-12">6. อาการต่อเนื่อง > 2 สัปดาห์ที่ผ่านมา (เลือกได้มากกว่า 1 ช่อง)</div>
		</div>
		<div class="body">
			<div class="col-sm-12 col-xs-12 form-group">
				<div class="col-sm-10 col-xs-10 question_assessment">
					<div class="row">
						<div class="col-sm-12 col-xs-12">6.1 ปัญหาทางการเคี้ยว/กลืนอาหาร</div>
					</div>
					<div class="col-sm-6 col-xs-12">
						<input type="checkbox" class="mtl filled-in" name="n6_1_1" onchange="get_naf()" value="2" id="n6_1_1">
						<label for="n6_1_1">สำลัก (2)</label>
					</div>
					<div class="col-sm-6 col-xs-12">
						<input type="checkbox" class="mtl filled-in" name="n6_1_3" onchange="get_naf()" value="2" id="n6_1_3">
						<label for="n6_1_3">เคี้ยว/กลืนลำบาก/ได้อาหารทางสายยาง (2)</label>
					</div>
					<div class="col-sm-6 col-xs-12">
						<input type="checkbox" class="mtl filled-in" name="n6_1_2" onchange="get_naf()" value="0" id="n6_1_2">
						<label for="n6_1_2">กลืนได้ปกติ (0)</label>
					</div>
				</div>
				<div class="col-sm-2 col-xs-2 score score_get" id="n6_1_score"></div>
			</div>
		</div>
		<div class="body">
			<div class="col-sm-12 col-xs-12 form-group">
				<div class="col-sm-10 col-xs-10 question_assessment">
					<div class="row">
						<div class="col-sm-12 col-xs-12">6.2 ปัญหาระบบทางเดินอาหาร</div>
					</div>
					<div class="col-sm-6 col-xs-12">
						<input type="checkbox" class="mtl filled-in" name="n6_2_1" onchange="get_naf()" value="2" id="n6_2_1">
						<label for="n6_2_1">ท้องเสีย (2)</label>
					</div>
					<div class="col-sm-6 col-xs-12">
						<input type="checkbox" class="mtl filled-in" name="n6_2_2" onchange="get_naf()" value="2" id="n6_2_2">
						<label for="n6_2_2">ปวดท้อง (2)</label>
					</div>
					<div class="col-sm-6 col-xs-12">
						<input type="checkbox" class="mtl filled-in" name="n6_2_3" onchange="get_naf()" value="0" id="n6_2_3">
						<label for="n6_2_3">ปกติ (0)</label>
					</div>
				</div>
				<div class="col-sm-2 col-xs-2 score score_get" id="n6_2_score"></div>
			</div>
		</div>
		<div class="body">
			<div class="col-sm-12 col-xs-12 form-group">
				<div class="col-sm-10 col-xs-10 question_assessment">
					<div class="row">
						<div class="col-sm-12 col-xs-12">6.3 ปัญหาระหว่างการกินอาหาร</div>
					</div>
					<div class="col-sm-6 col-xs-12">
						<input type="checkbox" class="mtl filled-in" name="n6_3_1" onchange="get_naf()" value="2" id="n6_3_1">
						<label for="n6_3_1">อาเจียน (2)</label>
					</div>
					<div class="col-sm-6 col-xs-12">
						<input type="checkbox" class="mtl filled-in" name="n6_3_2" onchange="get_naf()" value="2" id="n6_3_2">
						<label for="n6_3_2">คลื่นไส้ (2)</label>
					</div>
					<div class="col-sm-6 col-xs-12">
						<input type="checkbox" class="mtl filled-in" name="n6_3_3" onchange="get_naf()" value="0" id="n6_3_3">
						<label for="n6_3_3">ปกติ (0)</label>
					</div>
				</div>
				<div class="col-sm-2 col-xs-2 score score_get" id="n6_3_score"></div>
			</div>
		</div>
		<div class="header no-radius pink_1">
			<div class="col-sm-12 col-xs-12">7. ความสามารถในการเข้าถึงอาหาร</div>
		</div>
		<div class="body">
			<div class="col-sm-12 col-xs-12 form-group">
				<div class="col-sm-10 col-xs-10 question_assessment">
					<div class="col-sm-6 col-xs-12">
						<input type="radio" class="mtl with-gap" name="n7_1" onchange="get_naf()" value="2" id="n7_1">
						<label for="n7_1">นอนติดเตียง (2)</label>
					</div>
					<div class="col-sm-6 col-xs-12">
						<input type="radio" class="mtl with-gap" name="n7_1" onchange="get_naf()" value="1" id="n7_2">
						<label for="n7_2">ต้องมีผู้ช่วยบ้าง (1)</label>
					</div>
					<div class="col-sm-6 col-xs-12">
						<input type="radio" class="mtl with-gap" name="n7_1" onchange="get_naf()" value="0" id="n7_3">
						<label for="n7_3">นั่งๆ นอนๆ (0)	</label>
					</div>
					<div class="col-sm-6 col-xs-12">
						<input type="radio" class="mtl with-gap" name="n7_1" onchange="get_naf()" value="0" id="n7_4">
						<label for="n7_4">ปกติ (0)</label>
					</div>
				</div>
				<div class="col-sm-2 col-xs-2 score score_get" id="n7_1_score"></div>
			</div>
		</div>
		<div class="header no-radius pink_1">
			<div class="col-sm-12 col-xs-12">8. โรคที่เป็นอยู่ โดยต้องแจ้งให้นักกำหนดอาหาร/นักโภชนาการทราบ (เลือกได้มากกว่า 1 ช่อง)</div>
		</div>
		<script type="text/javascript">
			$(document).ready(function() {
				type_8 = ""
				$(".n8").click(function(event){
					if ($(".n8:checked").length!=0) {
						if (type_8!=$(this).attr('data-type')&&type_8!="") {
					    	event.preventDefault();
							swal("", "กรุณาเลือกข้อ 8.1 หรือ 8.2 อย่างใดอย่างหนึ่ง!", "warning");
							$("#"+$(this).attr('data-type')+"_score").html("")
							get_score();
							setTimeout(function() {
								get_naf();
							}, 300);
						}else{
							if ($(this).attr('data-type')=="n8_1") {
								type_8 = "n8_1"
							}else{
								type_8 = "n8_2"
							}
						}
					}else{
						type_8 = ""
					}
				});
			});
			
		</script>
		<div class="body">
			<div class="col-sm-12 col-xs-12 form-group">
				<div class="col-sm-10 col-xs-10 question_assessment">
					<div class="row">
						<div class="col-sm-12 col-xs-12">8.1 โรคที่มีความรุนแรงน้อยถึงปานกลาง (3 คะแนน)</div>
					</div>
					<?php
						$n8_1 = array(
							'DM (เบาหวาน) (3)',
							'CKD-ESRD (ไตเรื้อรัง) (3)',
							'Septicemia (ติดเชื้อในกระแสเลือด) (3)',
							'Solid Cancer (มะเร็งทั่วไป) (3)',
							'Chronic heart failure (หัวใจล้มเหลวเรื้อรัง) (3)',
							'Hip fracture (ข้อสะโพกหัก) (3)',
							'COPD (ปอดอุดกั้นเรื้อรัง) (3)',
							'Severe head injury (บาดเจ็บที่ศีรษะรุนแรง) (3)',
							'&#8805; 2<sup>๐</sup> of burn (แผลไฟไหม้ระดับ 2 ขึ้นไป) (3)',
							'CLD/Cirrhosis/Hepati cencaph (ตับเรื้อรัง) (3)',
							'อื่นๆ (3) <input type="text" class="form-control" id="n8_1_11_name" onkeyup="get_naf()" placeholder="กรุณาระบุโรค">'
						);

						$i=0; 
						foreach ($n8_1 as $key => $value): 
						$i++;
					?>
					<div class="col-sm-12 col-xs-12 form-inline">
						<input type="checkbox" class="mtl filled-in n8 n8_1" data-type="n8_1" maxscore="3" name="n8_1_<?=$i?>" onchange="get_naf()" value="3" id="n8_1_<?=$i?>">
						<label for="n8_1_<?=$i?>"><?=$value;?></label>
					</div>
					<?php endforeach ?>
				</div>
				<div class="col-sm-2 col-xs-2 score score_get" id="n8_1_score"></div>
			</div>
		</div>
		<div class="body">
			<div class="col-sm-12 col-xs-12 form-group">
				<div class="col-sm-10 col-xs-10 question_assessment">
					<div class="row">
						<div class="col-sm-12 col-xs-12">8.2 โรคที่มีความรุนแรงมาก (6 คะแนน)</div>
					</div>
					<?php
						$n8_2 = array(
							'Severe pneumonia (ปอดบวมขั้นรุนแรง) (6)',
							'Critically ill (ผู้ป่วยวิกฤติ) (6)',
							'Multiple fracture (กระดูกหักหลายตำแหน่ง) (6)',
							'Stroke/CVA (อัมพาต) (6)',
							'Malignant hematologic disease/Bone marrow transplant (มะเร็งเม็ดเลือด/ปลูกถ่ายไขกระดูก) (6)',
							'อื่นๆ (6) <input type="text" class="form-control" id="n8_2_6_name" onkeyup="get_naf()" placeholder="กรุณาระบุโรค">'
						);

						$i=0; 
						foreach ($n8_2 as $key => $value): 
						$i++;
					?>
					<div class="col-sm-12 col-xs-12 form-inline">
						<input type="checkbox" class="mtl filled-in n8 n8_2" data-type="n8_2" maxscore="6" name="n8_2_<?=$i?>" onchange="get_naf()" value="6" id="n8_2_<?=$i?>">
						<label for="n8_2_<?=$i?>"><?=$value;?></label>
					</div>
					<?php endforeach ?>
				</div>
				<div class="col-sm-2 col-xs-2 score score_get" id="n8_2_score"></div>
			</div>
		</div>
		<div class="btn-bar assessment-warning">
			<div class="col-sm-12 col-xs-12 header">คุณยังตอบคำถามไม่ครบ</div>
			<div class="list-required"></div>
		</div>
		<div class="header no-radius pink_1 assessment-complete" style="display: none;">
			<div class="col-sm-10 col-xs-10"><!-- ครั้งที่ 1  -->คะแนนรวม</div>
			<div class="col-sm-2 col-xs-2 result_score text-center result_all"></div>
		</div>
		<div class="header no-radius b_pink_1 assessment-result assessment-complete" style="display: none;">
			<div class="col-sm-4 col-xs-12">ผลการประเมิน</div>
			<div class="col-sm-4 col-xs-6"><label><span class="result_all"></span> คะแนน</label></div>
			<div class="col-sm-4 col-xs-6"><label class="assessment-result-grade"></label></div>
		</div>
		<div class="btn-bar assessment-complete" style="display: none;">
			<h3 class="assessment-result-description"></h3>
			<h1 class="assessment-result-time"></h1>
			<button class="btn btn_big btn-color pink" onclick="set_todb('screening/fn_create_naf','patients/lists/','naf')">บันทึกผลการประเมิน</button>
			<h3>หรือบันทึกผลการประเมินและทำการประเมิน NT ด้วย</h3>
			<button class="btn btn_big btn-color violet" onclick="set_todb('screening/fn_create_naf','screening/create_nt','naf')">Assessment NT</button>
		</div>
	</div>
</div>
