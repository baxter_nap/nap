<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
<head>
	<title>Main</title>
	<?php $this->load->view('_config'); ?>
	<script type="text/javascript">
		$(document).ready(function() {
			$(window).on('beforeunload', function(){
                return 'คุณจะออกจากการทำฟอร์มนี้ใช่หรือไม่';
           	});
			cal_bmi();
			check_alert();
			$('.form-control').change(function(event) {
				get_check()
			});
			get_check()
		});

		function get_check() {
			var check_val_ar = {};
			var check_val_length = 0;
			var check_val = $('.form-control');
			for (var i = 0; i < check_val.length; i++) {
				if ($(check_val[i]).val()!=""&&$(check_val[i]).val()!=null&&$(check_val[i]).attr('no-get')==undefined) {
					set_obj(check_val_ar,$(check_val[i]).attr('id'),$(check_val[i]).val())
					check_val_length++;
				}
			}
			// console.log(check_val_ar,check_val_length)
			if (check_val_ar.age!=undefined&&check_val_ar.bmi!=undefined&&check_val_ar.date_admission!=undefined&&check_val_ar.diagnose!=undefined&&check_val_ar.diagnose_system_id!=undefined&&check_val_ar.height!=undefined&&check_val_ar.hn_code!=undefined&&check_val_ar.sex!=undefined&&check_val_ar.weight_assessment!=undefined&&check_val_ar.weight_current!=undefined) {
				$('.table-screening-form').slideDown('fast');
				set_qwe(check_val_ar);
			}else{
				$('.table-screening-form').slideUp('fast');
			}
		}
	</script>
</head>
<body>
	<?php $this->load->view('_header'); ?>
	<div class="container user">
		<div class="row ">
			<?php $this->load->view('_left_bar.php'); ?>
			<div class="col-sm-10 col-xs-12 content">
				<div class="row green_2 top-bar">
					<div class="col-xs-10 back"></div>
					<div class="col-xs-2 score"></div>
				</div>
				<div class="row">
					<div class="col-xs-8 title">Screening</div>
					<div class="col-xs-4 btn-bar"></div>
				</div>
				<div class="row ">
					<div class="col-xs-12 assessment-box">
						<div class="row box table-screening">
							<div class="header blue_1">
								<div class="col-sm-6 col-xs-12">ข้อมูลผู้ป่วย</div>
								<div class="col-sm-6 col-xs-12">วันที่ทำการประเมิน : <?=date('d/m/Y')?></div>
							</div>
							<div class="body">
								<div class="col-sm-6 col-xs-12 header-form form-group">
									<label for="hn_code">HN</label>
									<input type="text" name="hn_code" value="<?=$data['hn_code']?>" id="hn_code" class="form-control">
								</div>
								<div class="col-sm-6 col-xs-12 header-form form-group">
									<label for="name">ชื่อผู้ป่วย</label>
									<input type="text" name="name" value="<?=$data['name']?>" id="name" class="form-control">
								</div>
								<div class="col-sm-6 col-xs-12 form-group">
									<label for="patient_ward_id">หอผู้ป่วย</label>
									<select class="form-control" name="patient_ward_id" id="patient_ward_id">
										<option value="0" disabled selected>เลือกหอผู้ป่วย</option>
										<?php foreach ($patient_ward as $key => $value): 
											$selected = $value['id']==$data['patient_ward_id'] ? "selected" : "";
										?>
											<option value="<?=$value['id']?>" <?=$selected?>><?=$value['title']?></option>
										<?php endforeach ?>
									</select>
								</div>
								<div class="col-sm-6 col-xs-12 header-form form-group">
									<label for="diagnose">การวินิจฉัยโรค</label>
									<input type="text" name="diagnose" value="<?=$data['diagnose']?>" id="diagnose" class="form-control">
								</div>
								<div class="col-sm-6 col-xs-12 form-group">
									<label for="diagnose_system_id">ระบบวินิจฉัยโรค</label>
									<select class="form-control" name="diagnose_system_id" id="diagnose_system_id">
										<option value="0" disabled selected>ระบบวินิจฉัยโรค</option>
										<?php foreach ($diagnose_system as $key => $value): 
											$selected = $value['id']==$data['diagnose_system_id'] ? "selected" : "";
										?>
											<option value="<?=$value['id']?>" <?=$selected?>><?=$value['title']?></option>
										<?php endforeach ?>
									</select>
								</div>
								<div class="col-sm-6 col-xs-12 form-group">
									<label for="date_admission">วันที่เข้ารับการรักษา </label>
									<input type="text" name="date_admission" value="<?=$data['date_admission']?>" class="form-control datepicker" id="date_admission">
								</div>
								<div class="col-sm-6 col-xs-12 form-group">
									<label for="sex">เพศ</label>
									<select class="form-control" name="sex" id="sex">
									<?php
										$selected_1 = $data['sex']==1 ? "selected" : "";
										$selected_2 = $data['sex']==2 ? "selected" : "";
									?>
										<option value="1" <?=$selected_1?>>ชาย</option>
										<option value="2" <?=$selected_2?>>หญิง</option>
									</select>
								</div>
								<script type="text/javascript">
									function set_age(type) {
										var age = $('#age').val()
										var max_age = 130
										var be = $('#be').val()
										var curent_year = moment().year()+543
										var max_year = curent_year-max_age
										if (type=='be') {
											if (max_year<be&&be<curent_year) {
												$('#age').val(curent_year-be)
												$("#"+type).parent().removeClass('has-error');
											}else{
												swal({
													title: "Warning",
													text: 'อายุของผู้ป่วยต้องมากกว่า 0 หรือ น้อยกว่า '+max_age+' ปี',
													type: "warning",
												},function () {
													$("#"+type).parent().addClass('has-error');
													$("#age").val("");
													$("#be").val("");
													$("#"+type).focus();
													get_check()
												})
											}
										}else if (type=='age'){
											if (max_age>age&&age>0) {
												$('#be').val(curent_year-age)
												$("#"+type).parent().removeClass('has-error');
											}else{
												swal({
													title: "Warning",
													text: 'อายุของผู้ป่วยต้องมากกว่า 0 หรือ น้อยกว่า '+max_age+' ปี',
													type: "warning",
												},function () {
													$("#"+type).parent().addClass('has-error');
													$("#age").val("");
													$("#be").val("");
													$("#"+type).focus();
													get_check()
												})
											}
										}
									}
								</script>
								<div class="col-sm-3 col-xs-6 form-group">
									<label for="age">อายุ</label>
									<input type="number" name="age" value="<?=$data['age']?>" onchange="set_age('age')" class="form-control" id="age">
									<label class="sub-label" for="age">ปี</label>
								</div>
								<div class="col-sm-3 col-xs-6 form-group">
									<label for="be">พ.ศ.</label>
									<input type="number" name="be" class="form-control" onchange="set_age('be')" id="be" no-get>
								</div>
								<script type="text/javascript">
									function checkminmax(obj,type,point,min,max) {
										if ($(obj).val()<min||$(obj).val()>max) {
											swal({
												title: "Warning",
												text: type+'ของผู้ป่วยไม่ควรต่ำกว่า '+min+' '+point+' หรือ มากกว่า '+max+' '+point,
												type: "warning",
											},function () {
												$(obj).parent().addClass('has-error');
												$(obj).val("");
												$(obj).focus();
												$('#bmi').val("");
												get_check()
											})
										}else{
											$(obj).parent().removeClass('has-error');
										}
									}
								</script>
								<div class="col-sm-6 col-xs-12 form-group">
									<label for="height">ความสูง (จากการวัดจริงหรือตามบัตรประชาชน)</label>
									<input type="number" name="height" value="<?=$data['height']?>" onchange="checkminmax(this,'ความสูง','เซนติเมตร',10,250)" id="height" onkeyup="cal_bmi()" class="form-control">
									<label class="sub-label" for="height">เซนติเมตร</label>
								</div>
								<div class="col-sm-6 col-xs-12 form-group">
									<label for="weight_current">น้ำหนักปัจจุบัน</label>
									<input type="number" name="weight_current" value="<?=$data['weight_current']?>" onchange="checkminmax(this,'น้ำหนัก','กิโลกรัม',20,300)" id="weight_current" onkeyup="cal_bmi()" class="form-control">
									<label class="sub-label" for="weight_current">กิโลกรัม</label>
								</div>
								<div class="col-sm-6 col-xs-12 form-group">
									<label for="weight_assessment">น้ำหนักประเมินโดยการ</label>
									<select class="form-control" name="weight_assessment" id="weight_assessment">
									<?php
										$selected_1 = $data['weight_assessment']==1 ? "selected" : "";
										$selected_2 = $data['weight_assessment']==2 ? "selected" : "";
										$selected_3 = $data['weight_assessment']==3 ? "selected" : "";
									?>
										<option value="" disabled selected>เลือกวิธีการประเมิน</option>
										<option value="1" <?=$selected_1?>>การชั่ง</option>
										<option value="2" <?=$selected_2?>>การซักถาม</option>
										<option value="3" <?=$selected_3?>>การประเมิน</option>
									</select>
								</div>
								<div class="col-sm-6 col-xs-12 form-group bmi" style="display: none;">
									<label class="red_1" for="bmi">BMI</label>
									<input type="text" id="bmi" name="bmi" class="form-control red_1">
								</div>
							</div>
						</div>

						<div class="row box table-screening-form">
							<div class="header blue_1">
								<div class="col-sm-12 col-xs-12">
									แบบประเมินแบบ Screening
								</div>
							</div>
							<div class="body">
								<div class="col-sm-12 col-xs-12">
									<div class="row">
										<div class="col-sm-6 question">
											1. ผู้ป่วยมีน้ำหนักตัวลดลง โดยไม่ได้ตั้งใจในช่วง 6 เดือนที่ผ่านมาหรือไม่?
										</div>
										<div class="col-sm-6">
											<div class="form-group radio-btn">
											<?php
												$checked_c1_yes = $data['c_1']=="1" ? "checked" : "";
												$checked_c1_no = $data['c_1']=="0" ? "checked" : "";
											?>
												<input type="radio" onchange="check_alert()" class="radio_yes" name="c_1" id="c_1_yes" <?=$checked_c1_yes?>>
												<label class="btn red" for="c_1_yes">ใช่</label>
												<input type="radio" onchange="check_alert()" class="radio_no" name="c_1" id="c_1_no" <?=$checked_c1_no?>>
												<label class="btn green" for="c_1_no">ไม่ใช่</label>
											</div>
										</div>
									</div>
								</div>
								<div class="col-sm-12 col-xs-12">
									<div class="row">
										<div class="col-sm-6 question">
											2. ผู้ป่วยได้รับอาหารน้อยกว่าที่เคยได้ (เกินกว่า 7 วัน)
										</div>
										<div class="col-sm-6">
											<div class="form-group radio-btn">
											<?php
												$checked_c2_yes = $data['c_2']=="1" ? "checked" : "";
												$checked_c2_no = $data['c_2']=="0" ? "checked" : "";
											?>
												<input type="radio" onchange="check_alert()" class="radio_yes" name="c_2" id="c_2_yes" <?=$checked_c2_yes?>>
												<label class="btn red" for="c_2_yes">ใช่</label>
												<input type="radio" onchange="check_alert()" class="radio_no" name="c_2" id="c_2_no" <?=$checked_c2_no?>>
												<label class="btn green" for="c_2_no">ไม่ใช่</label>
											</div>
										</div>
									</div>
								</div>
								<div class="col-sm-12 col-xs-12">
									<div class="row">
										<div class="col-sm-6 question">
											3. BMI < 18.5 หรือ > 25.0 Kg/m2 หรือไม่
										</div>
										<div class="col-sm-6">
											<div class="form-group radio-btn">
											<?php
												$checked_c3_yes = $data['c_3']=="1" ? "checked" : "";
												$checked_c3_no = $data['c_3']=="0" ? "checked" : "";
											?>
												<input type="radio" onchange="check_alert()" readonly class="radio_yes" name="c_3" id="c_3_yes" <?=$checked_c3_yes?>>
												<label class="btn red">ใช่</label>
												<input type="radio" onchange="check_alert()" readonly class="radio_no" name="c_3" id="c_3_no" <?=$checked_c3_no?>>
												<label class="btn green">ไม่ใช่</label>
											</div>
										</div>
									</div>
								</div>
								<div class="col-sm-12 col-xs-12">
									<div class="row">
										<div class="col-sm-6 question">
											4. ผู้ป่วยมีภาวะโรควิกฤต หรือกึ่งวิกฤตร่วมด้วยหรือไม่?
										</div>
										<div class="col-sm-6">
											<div class="form-group radio-btn">
											<?php
												$checked_c4_yes = $data['c_4']=="1" ? "checked" : "";
												$checked_c4_no = $data['c_4']=="0" ? "checked" : "";
											?>
												<input type="radio" onchange="check_alert()" class="radio_yes" name="c_4" id="c_4_yes" <?=$checked_c4_yes?>>
												<label class="btn red" for="c_4_yes">ใช่</label>
												<input type="radio" onchange="check_alert()" class="radio_no" name="c_4" id="c_4_no" <?=$checked_c4_no?>>
												<label class="btn green" for="c_4_no">ไม่ใช่</label>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="row box table_yes" style="display: none;">
							<div class="header green_3">
								<div class="col-sm-12 col-xs-12 text-center">
									ผลการคัดกรอง
								</div>
							</div>
							<body>
								<div class="text-center">
									<h3>ควรประเมินซ้ำอีกครั้งภายใน 7 วัน (ภายใน 21 มิถุนายน 2560)</h3>
								</div>
								<div class="btn-bar">
									<button class="btn btn_big btn-color blue" onclick="set_todb('screening/fn_edit','screening/create',-1)">บันทึกผลการประเมิน</button>
								</div>
							</body>
						</div>
						<div class="row box table_no" style="display: none;">
							<div class="header red_1">
								<div class="col-sm-12 col-xs-12 text-center">
									ผลการคัดกรอง
								</div>
							</div>
							<div class="body">
								<div class="text-center">
									<h3>ให้ Notify นักกำหนดอาหาร/ทีมโภชนาการบำบัดเพื่อประเมินภาวะโภชนาการ </h3>
									<p class="text-center">เลือกแบบประเมินโภชนาการ</p>
								</div>
								<div class="btn-bar">
									<a class="btn btn_big btn-color pink" onclick="set_todb('screening/fn_edit','screening/create_naf',-1)">Assessment NAF</a>
									<a class="btn btn_big btn-color violet" onclick="set_todb('screening/fn_edit','screening/create_nt',-1)">Assessment NT</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>