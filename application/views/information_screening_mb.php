<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
<head>
	<title>Main</title>
	<?php $this->load->view('_config_export'); ?>
	<script type="text/javascript">
		$(document).ready(function() {
			$('body').css('overflow', 'auto');
			$('#cmd').click(function () {
				save()
			});
		});
		function save() {
			$('#cmd').hide('fast','',function () {
				<?php if ($platform!="iOS") { ?>
				print();
				<?php }else{ ?>
				toPDF()
				<?php } ?>
	    		setTimeout(function() {
	    			$('#cmd').show('fast');
	    		}, 1000);
            });
		}
	</script>
</head>
<body class="export">
	<div class="page" style="width: 1000px;height:1455px;overflow: hidden;">		
		<img src="<?=base_url().'../images/form-screening.jpg'?>" width="100%">
		<button id="cmd" class="btn btn-color export">Print</button>
	</div>
</body>
</html>