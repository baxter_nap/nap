<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
<head>
	<title>Main</title>
	<?php $this->load->view('_config'); ?>
	<script type="text/javascript">
		$(document).ready(function() {
			$(window).on('beforeunload', function(){
                return 'คุณจะออกจากการทำฟอร์มนี้ใช่หรือไม่';
           	});
		});
	</script>
</head>
<body>
	<?php $this->load->view('_header'); ?>
	<div class="container user">
		<div class="row">
			<?php $this->load->view('_left_bar.php'); ?>
			<div class="col-sm-10 col-xs-12 content">
				<div class="row green_2 top-bar">
					<div class="col-sm-10 col-xs-6 back"><a href="<?=base_url().'screening/edit/'.$_SESSION['last_screening_id']?>">< ย้อนกลับ</a></div>
					<div class="col-sm-2 col-xs-6 score text-right">คะแนนรวม = <span class="result_all"></span></div>
				</div>
				<div class="row">
					<div class="col-sm-8 col-xs-12 title">Screening > Assessment NT ครั้งที่ <?=$count?></div>
					<div class="col-sm-4 col-xs-12 btn-bar"></div>
				</div>
				<div class="row">
					<div class="col-xs-12 ">
						<div class="row box table-screening">
							<div class="header blue_1">
								<div class="col-sm-6 col-xs-12">ข้อมูลผู้ป่วย</div>
								<div class="col-sm-6 col-xs-12">วันที่ทำการประเมิน : <?=date('d/m/Y')?></div>
							</div>
							<div class="body screening-history">
								<div class="col-sm-6 col-xs-12 form-group">
									<label>HN</label>
									<div class="detail">
										<?=$data['hn_code']?>
										<input type="hidden" name="hn_code" id="hn_code" value="<?=$data['hn_code']?>">	
										<input type="hidden" name="bmi" id="bmi" value="<?=$data['bmi']?>" no-get>	
									</div>
								</div>
								<div class="col-sm-6 col-xs-12 form-group">
									<label>ชื่อผู้ป่วย</label>
									<div class="detail"><?php echo $data['name']=="" ? '-' : $data['name']; ?></div>
								</div>
								<div class="col-sm-6 col-xs-12 form-group">
									<label>หอผู้ป่วย</label>
									<div class="detail"><?=$data['patient_ward_title']?></div>
								</div>
								<div class="col-sm-6 col-xs-12 form-group">
									<label>การวินิจฉัยโรค</label>
									<div class="detail"><?php echo $data['diagnose']=="" ? '-' : $data['diagnose']; ?></div>
								</div>
								<div class="col-sm-6 col-xs-12 form-group">
									<label>ส่วนสูง</label>
									<div class="detail"><?=$data['height']?></div>
								</div>
								<div class="col-sm-6 col-xs-12 form-group">
									<label>น้ำหนักปัจจุบัน</label>
									<div class="detail">
										<input type="text" name="weight_current" id="weight_current" value="<?=$data['weight_current']?>" class="form-control">
										<label class="sub-label">กิโลกรัม</label>
									</div>
								</div>
							</div>
							<div class="col-sm-12 cursor-bar hidden-xs">
								<label onclick="toggle_show(this,239,85)" class=cursor><i class="fa fa-angle-down"></i></label>
							</div>
						</div>
						<?php require_once(APPPATH.'views/_assessment_nt.php'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>