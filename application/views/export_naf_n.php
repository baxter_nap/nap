<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
<head>
	<title>Main</title>
	<?php $this->load->view('_config_export'); ?>
	<script src="<?=base_url()?>../js/jspdf.debug.js"></script>
	<script src="<?=base_url()?>../js/html2canvas.js"></script>
	<script src="<?=base_url()?>../js/posi_naf.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('body').css('overflow', 'auto');
		});
	</script>
</head>
<body class="export">
	<?php
		// echo"<pre>";print_r($data);echo "</pre>";
		// echo"<pre>";print_r($data_old);echo "</pre>";
		$debug = 0;
		$pnum = floor(($num-1)/3)*3;
		$num = $num % 3 == 0 ? 3:$num % 3;
	?>
	<div class="page" style="width: 1000px;max-height: 1455px;overflow: hidden;">
		<label class="name"><?= isset($data_old['name']) ? $data_old['name'] : '-' ?></label>
		<?php if ($debug==1||$data_old['sex']==1): ?>
			<label class="sex_1"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data_old['sex']==2): ?>
			<label class="sex_2"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<label class="age"><?=$data_old['age']?></label>
		<label class="hn_code"><?=$data_old['hn_code']?></label>
		<label class="date_admission"><?=date_format(date_create($data_old['date_admission']),'d/m/Y')?></label>
		<label class="diagnose_title"><?=$data_old['diagnose']?></label>
		<label class="n1_1"><?=$data['n1_1']?></label>
		<label class="n1_2"><?=$data['n1_2']?></label>
		<label class="n1_3"><?=$data['n1_3']?></label>
		<!-- n2_1 -->
		<?php if ($debug==1||$data['n2_1']==1): ?>
			<label class="n2_1_1"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n2_1']==2): ?>
			<label class="n2_1_2"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n2_1']==3): ?>
			<label class="n2_1_3"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n2_1']==4): ?>
			<label class="n2_1_4"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<!-- n2_2 -->
		<?php if ($debug==1||isset($data['n2_2'])): ?>
			<?php if ($debug==1||$data['n2_2']==1): ?>
				<label class="n2_2_1"><i class="fa fa-check"></i></label>
			<?php endif ?>
			<?php if ($debug==1||$data['n2_2']==2): ?>
				<label class="n2_2_2"><i class="fa fa-check"></i></label>
			<?php endif ?>
			<?php if ($debug==1||$data['n2_2']==3): ?>
				<label class="n2_2_3"><i class="fa fa-check"></i></label>
			<?php endif ?>
			<?php if ($debug==1||$data['n2_2']==4): ?>
				<label class="n2_2_4"><i class="fa fa-check"></i></label>
			<?php endif ?>
		<?php endif ?>
		<!-- n2_3 -->
		<?php if ($debug==1||isset($data['n2_3'])): ?>
			<?php if ($debug==1||$data['n2_3']==1): ?>
				<label class="n2_3_1"><i class="fa fa-check"></i></label>
			<?php endif ?>
			<?php if ($debug==1||$data['n2_3']==2): ?>
				<label class="n2_3_2"><i class="fa fa-check"></i></label>
			<?php endif ?>
			<?php if ($debug==1||$data['n2_3']==3): ?>
				<label class="n2_3_3"><i class="fa fa-check"></i></label>
			<?php endif ?>
			<?php if ($debug==1||$data['n2_3']==4): ?>
				<label class="n2_3_4"><i class="fa fa-check"></i></label>
			<?php endif ?>
		<?php endif ?>
		<!-- n2_4 -->
		<?php if ($debug==1||isset($data['n2_4'])): ?>
			<?php if ($debug==1||$data['n2_4']==1): ?>
				<label class="n2_4_1"><i class="fa fa-check"></i></label>
			<?php endif ?>
			<?php if ($debug==1||$data['n2_4']==2): ?>
				<label class="n2_4_2"><i class="fa fa-check"></i></label>
			<?php endif ?>
			<?php if ($debug==1||$data['n2_4']==3): ?>
				<label class="n2_4_3"><i class="fa fa-check"></i></label>
			<?php endif ?>
			<?php if ($debug==1||$data['n2_4']==4): ?>
				<label class="n2_4_4"><i class="fa fa-check"></i></label>
			<?php endif ?>
		<?php endif ?>
		<!-- n3_1 -->
		<?php if ($debug==1||$data['n3_1']==1): ?>
			<label class="n3_1_1"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n3_1']==2): ?>
			<label class="n3_1_2"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n3_1']==3): ?>
			<label class="n3_1_3"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n3_1']==4): ?>
			<label class="n3_1_4"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<!-- n4_1 -->
		<?php if ($debug==1||$data['n4_1']==1): ?>
			<label class="n4_1_1"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n4_1']==2): ?>
			<label class="n4_1_2"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n4_1']==3): ?>
			<label class="n4_1_3"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n4_1']==4): ?>
			<label class="n4_1_4"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<!-- n5_1 -->
		<?php if ($debug==1||$data['n5_1']==1): ?>
			<label class="n5_1_1"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n5_1']==2): ?>
			<label class="n5_1_2"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n5_1']==3): ?>
			<label class="n5_1_3"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n5_1']==4): ?>
			<label class="n5_1_4"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<!-- n5_2 -->
		<?php if ($debug==1||$data['n5_2']==1): ?>
			<label class="n5_2_1"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n5_2']==2): ?>
			<label class="n5_2_2"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n5_2']==3): ?>
			<label class="n5_2_3"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n5_2']==4): ?>
			<label class="n5_2_4"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<!-- n6_1 -->
		<?php if ($debug==1||isset($data['n6_1_1'])): ?>
			<?php if ($debug==1||$data['n6_1_1']==1): ?>
				<label class="n6_1_1"><i class="fa fa-check"></i></label>
			<?php endif ?>
		<?php endif ?>
		<?php if ($debug==1||isset($data['n6_1_2'])): ?>
			<?php if ($debug==1||$data['n6_1_2']==1): ?>
				<label class="n6_1_2"><i class="fa fa-check"></i></label>
			<?php endif ?>
		<?php endif ?>
		<?php if ($debug==1||isset($data['n6_1_3'])): ?>
			<?php if ($debug==1||$data['n6_1_3']==1): ?>
				<label class="n6_1_3"><i class="fa fa-check"></i></label>
			<?php endif ?>
		<?php endif ?>
		<!-- n6_2 -->
		<?php if ($debug==1||isset($data['n6_2_1'])): ?>
			<?php if ($debug==1||$data['n6_2_1']==1): ?>
				<label class="n6_2_1"><i class="fa fa-check"></i></label>
			<?php endif ?>
		<?php endif ?>
		<?php if ($debug==1||isset($data['n6_2_2'])): ?>
			<?php if ($debug==1||$data['n6_2_2']==1): ?>
				<label class="n6_2_2"><i class="fa fa-check"></i></label>
			<?php endif ?>
		<?php endif ?>
		<?php if ($debug==1||isset($data['n6_2_3'])): ?>
			<?php if ($debug==1||$data['n6_2_3']==1): ?>
				<label class="n6_2_3"><i class="fa fa-check"></i></label>
			<?php endif ?>
		<?php endif ?>
		<!-- n6_3 -->
		<?php if ($debug==1||isset($data['n6_3_1'])): ?>
			<?php if ($debug==1||$data['n6_3_1']==1): ?>
				<label class="n6_3_1"><i class="fa fa-check"></i></label>
			<?php endif ?>
		<?php endif ?>
		<?php if ($debug==1||isset($data['n6_3_2'])): ?>
			<?php if ($debug==1||$data['n6_3_2']==1): ?>
				<label class="n6_3_2"><i class="fa fa-check"></i></label>
			<?php endif ?>
		<?php endif ?>
		<?php if ($debug==1||isset($data['n6_3_3'])): ?>
			<?php if ($debug==1||$data['n6_3_3']==1): ?>
				<label class="n6_3_3"><i class="fa fa-check"></i></label>
			<?php endif ?>
		<?php endif ?>
		<!-- n7_1 -->
		<?php if ($debug==1||$data['n7_1']==1): ?>
			<label class="n7_1_1"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n7_1']==2): ?>
			<label class="n7_1_2"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n7_1']==3): ?>
			<label class="n7_1_3"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n7_1']==4): ?>
			<label class="n7_1_4"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<!-- n8_1 -->
		<?php if ($debug==1||isset($data['n8_1_1'])&&$data['n8_1_1']==1): ?>
			<label class="n8_1_1"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||isset($data['n8_1_2'])&&$data['n8_1_2']==1): ?>
			<label class="n8_1_2"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||isset($data['n8_1_3'])&&$data['n8_1_3']==1): ?>
			<label class="n8_1_3"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||isset($data['n8_1_4'])&&$data['n8_1_4']==1): ?>
			<label class="n8_1_4"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||isset($data['n8_1_5'])&&$data['n8_1_5']==1): ?>
			<label class="n8_1_5"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||isset($data['n8_1_6'])&&$data['n8_1_6']==1): ?>
			<label class="n8_1_6"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||isset($data['n8_1_7'])&&$data['n8_1_7']==1): ?>
			<label class="n8_1_7"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||isset($data['n8_1_8'])&&$data['n8_1_8']==1): ?>
			<label class="n8_1_8"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||isset($data['n8_1_9'])&&$data['n8_1_9']==1): ?>
			<label class="n8_1_9"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||isset($data['n8_1_10'])&&$data['n8_1_10']==1): ?>
			<label class="n8_1_10"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||isset($data['n8_1_11'])&&$data['n8_1_11']==1): ?>
			<label class="n8_1_11"><i class="fa fa-check"></i></label>
			<label class="n8_1_11_name"><?=$data['n8_1_11_name']?></label>
		<?php endif ?>
		<!-- n8_2 -->
		<?php if ($debug==1||isset($data['n8_2_1'])&&$data['n8_2_1']==1): ?>
			<label class="n8_2_1"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||isset($data['n8_2_2'])&&$data['n8_2_2']==1): ?>
			<label class="n8_2_2"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||isset($data['n8_2_3'])&&$data['n8_2_3']==1): ?>
			<label class="n8_2_3"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||isset($data['n8_2_4'])&&$data['n8_2_4']==1): ?>
			<label class="n8_2_4"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||isset($data['n8_2_5'])&&$data['n8_2_5']==1): ?>
			<label class="n8_2_5"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||isset($data['n8_2_6'])&&$data['n8_2_6']==1): ?>
			<label class="n8_2_6"><i class="fa fa-check"></i></label>
			<label class="n8_2_6_name"><?=$data['n8_2_6_name']?></label>
		<?php endif ?>

		<?php
			$data['n2_2_score'] = isset($data['n2_2_score']) ? $data['n2_2_score'] : 0;
			$data['n2_3_score'] = isset($data['n2_3_score']) ? $data['n2_3_score'] : 0;
			$data['n2_4_score'] = isset($data['n2_4_score']) ? $data['n2_4_score'] : 0;
			$data['n6_1_score'] = isset($data['n6_1_score']) ? $data['n6_1_score'] : 0;
			$data['n6_2_score'] = isset($data['n6_2_score']) ? $data['n6_2_score'] : 0;
			$data['n6_3_score'] = isset($data['n6_3_score']) ? $data['n6_3_score'] : 0;
			$data['n8_1_score'] = isset($data['n8_1_score']) ? $data['n8_1_score'] : 0;
			$data['n8_2_score'] = isset($data['n8_2_score']) ? $data['n8_2_score'] : 0;
		?>

		<label class="n2_1_weight_1" style="font-size: 12px;"><?=$data['n2_1_weight']?></label>
		<label class="n2_1_score_1"><?=$data['n2_1_score']?></label>
		<label class="bmi_1" style="font-size: 12px;"><?=$data['bmi']?></label>
		<label class="n2_1_score_1"><?=$data['n2_1_score']?></label>
		<label class="n2_2_score_1"><?=$data['n2_2_score']?></label>
		<label class="n2_3_score_1"><?=$data['n2_3_score']?></label>
		<label class="n2_4_score_1"><?=$data['n2_4_score']?></label>
		<label class="n3_1_score_1"><?=$data['n3_1_score']?></label>
		<label class="n4_1_score_1"><?=$data['n4_1_score']?></label>
		<label class="n5_1_score_1"><?=$data['n5_1_score']?></label>
		<label class="n5_2_score_1"><?=$data['n5_2_score']?></label>
		<label class="n6_1_score_1"><?=$data['n6_1_score']?></label>
		<label class="n6_2_score_1"><?=$data['n6_2_score']?></label>
		<label class="n6_3_score_1"><?=$data['n6_3_score']?></label>
		<label class="n7_1_score_1"><?=$data['n7_1_score']?></label>
		<label class="n8_score_1"><?=$data['n8_1_score']+$data['n8_2_score']?></label>
		<?php $data['score'] = $data['n2_1_score']+$data['n2_2_score']+$data['n2_3_score']+$data['n3_1_score']+$data['n4_1_score']+$data['n5_1_score']+$data['n5_2_score']+$data['n6_1_score']+$data['n6_2_score']+$data['n6_3_score']+$data['n7_1_score']+$data['n8_1_score']+$data['n8_2_score']; ?>
		<!-- grade -->
		<?php if ($debug==1||$data['score']<=5): ?>
			<label class="grade_a_1"><i class="fa fa-circle-thin" style="font-size: 30px"></i></label>
		<?php endif ?>
		<?php if ($debug==1||($data['score']>=6&&$data['score']<=10)): ?>
			<label class="grade_b_1"><i class="fa fa-circle-thin" style="font-size: 30px"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['score']>=11): ?>
			<label class="grade_c_1"><i class="fa fa-circle-thin" style="font-size: 30px"></i></label>
		<?php endif ?>
		
		<label class="date_1"><?=date_format(date_create(),'d/m/Y')?></label>
		<label class="score_1" style="font-size: 30px"><?=$data['score']?></label>


		<img src="<?=base_url()?>../images/form-naf.jpg" style="width: 100%">
	</div>
	<?php if ($platform=="iOS") { ?>
		<div id="export-box"></div>
	<?php } ?>
	<button id="cmd" class="btn btn-color export">Print</button>
	<script type="text/javascript">
		$( function() {
			// $( "label" ).draggable({
			// 	stop: function( event, ui ) {
			// 		name = $(ui.helper[0]).attr('class').split(' ')[0];
			// 		// console.log('old:'+posi[name]);
			// 		// console.log('name:'+name+' '+ui.position.left+","+ui.position.top);
			// 		posi[name] = [(ui.position.left),(ui.position.top)];
			// 		setJs(ui)
			// 	}
			// });
		} );
		$(document).ready(function() {
			$('#cmd').click(function () {
				save()
			});
			setHtml()
		});

		function save() {
			$('#cmd').hide('fast','',function () {
				<?php if ($platform!="iOS") { ?>
				print();
				<?php }else{ ?>
				toPDF()
				<?php } ?>
	    		setTimeout(function() {
	    			$('#cmd').show('fast');
	    		}, 1000);
            });
		}

		function toPDF() {
			html2canvas($("#export-box canvas"), {
				onrendered: function(canvas) {
					imgData = canvas.toDataURL('image/jpeg', 1.0);              
					doc = new jsPDF("p", "mm", "a4");
	                doc.addImage(imgData, 'JPEG', 0, 0, 210, 290);
	                <?php if ($platform=="iOS") { ?>
	                doc.autoPrint();
					doc.output('dataurl');
					<?php } ?>
	            }
	        });
		}

		function setHtml() {
			for (var k in posi){
			    if (posi.hasOwnProperty(k)) {
			    	console.log(k)
			    	console.log(posi[k])
			         $('.page .'+k).css({
			         	left: posi[k][0],
			         	top: posi[k][1]
			         });
			    }
			}
			<?php if ($platform=="iOS") { ?>
			setTimeout(function() {
				$('.page').hide('fast');
				html2canvas($('.page'), {
					onrendered: function(canvas) {
				    	$('#export-box').html(canvas);
				   	 	toPDF();
					}
				});
			}, 500);
			<?php } ?>
			<?php if ($debug): ?>
			showlabel()	
			<?php endif ?>
			<?php if ($save==1): ?>
			save()
			<?php endif ?>
		}
		
		function setJs(ui) {
			$.ajax({
				url: base_url+'/screening/jsSave/naf',
				type: 'POST',
				data: {posi: posi},
			})
			.done(function(respon) {
				console.log("success");
				console.log(respon);
			})
			.fail(function() {
				console.log("error");
			})
		}

		function showlabel() {
			//show label
			label = $('label')
			for (var i = label.length - 1; i >= 0; i--) {
				$(label[i]).prepend('<label class="'+$(label[i]).attr('class').split(' ')[0]+' sub">'+$(label[i]).attr('class').split(' ')[0]+'</label>')
			}
		}
	</script>
</body>
</html>