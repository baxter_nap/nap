<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
<head>
	<title>Main</title>
	<?php $this->load->view('_config'); ?>
	<script type="text/javascript">
		$(document).ready(function() {
			get_screening_by_patient_ward_id()
		});

		function get_screening_by_patient_ward_id() {
			$.ajax({
				url: base_url+'patients/lists_screening/'+get_last_param(),
				type: 'POST',
				data: {date_start: $('.datepicker.start').val(),date_end: $('.datepicker.end').val()},
			})
			.done(function(res) {
				console.log(res);
				data = JSON.parse(res);
				tmp_html = ''
				for (var i = 0; i < data.length; i++) {
					console.log(data[i]['grade']);
					type = 'Screening'
					count = parseInt(data[i]['count'])+1; 
					if (data[i]['grade']!=undefined) {
						grade = data[i]['grade'].split('-')
						date = formatDate(new Date(data[i]['date']))
						if (grade[0]=="NAF") {
							type = 'Assessment NAF'
						}else if (grade[0]=="NT") {
							type = 'Assessment NT'
						}
						if (grade[1]=="C"||grade[1]=="4"||grade[1]=="5") {
							color = 'red'
							date_next = 'ภายใน 24 ชั่วโมง'
						}else if (grade[1]=="A"||grade[1]=="B"||grade[1]=="1"||grade[1]=="2"||grade[1]=="3") {
							color = 'orange'
							date_next = formatDate(new Date(data[i]['date_next']))
						}
					}else{
						data[i]['grade'] = ''
						color = 'green'
						date = formatDate(new Date(data[i]['date']))
						date_next = formatDate(new Date(data[i]['screening_date_next']))
					}
					
					tmp_html += '<a href="'+base_url+'patients/detail/'+data[i]['hn_code']+'"><div class="well well-color '+color+'">'+data[i]['hn_code']+'<div class="sub"><span></span><label>'+date+'</label> '+type+' ครั้งที่ '+count+' '+data[i]['grade']+' , ต้องประเมินอีกครั้ง ('+date_next+') </div></div></a>'
				}
				$('.list').html(tmp_html)
			})
			.fail(function() {
				console.log("error");
			});
		}
	</script>
</head>
<body>
	<?php $this->load->view('_header'); ?>
	<div class="container patient">
		<div class="row">
			<?php $this->load->view('_left_bar.php'); ?>
			<div class="col-sm-10 col-xs-12 content">
				<div class="row green_2 top-bar">
					<div class="col-xs-10 back"></div>
					<div class="col-xs-2 score"></div>
				</div>
				<div class="row">
					<div class="col-sm-8 col-xs-12 title">เลือกหอผู้ป่วย > <?=$patient_ward['title']?></div>
					<div class="col-sm-4 col-xs-12 btn-bar"></div>
				</div>
				<div class="row">
					<div class="col-sm-12 col-xs-12 form-inline">
						<div class="form-group">
							<label> Date from : </label>
							<input type="text" id="" class="form-control datepicker start" onchange="get_screening_by_patient_ward_id()">
						</div>
						<div class="form-group">
							<label> Date to: </label>
							<input type="text" id="" class="form-control datepicker end" onchange="get_screening_by_patient_ward_id()">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12 col-xs-12 list"></div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>