<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (!isset($_SESSION['hospital_id'])) {
	redirect(base_url(), 'refresh');
}
?>
<div class="se-pre-con"></div>
<header class="container hidden-xs">
	<div class="row">
		<div class="col-xs-6 logo">
			<img src="<?php echo base_url();?>../images/Logo.png" height="60px">
		</div>
		<div class="col-xs-6">
			<nav class="nav">
				<div class="hospital"><?=$_SESSION['hospital_title']?></div>
				<div class="logout"><i class="fa fa-sign-out"></i><a href="<?php echo base_url();?>/login/fn_logout">Logout</a></div>
			</nav>
		</div>
	</div>
</header>
<script type="text/javascript">
$(document).ready(function() {
	$('#menu').click(function(event) {
		$('#menu-sub').slideToggle('fast')
	});
	$('#close').click(function(event) {
		$('#menu-sub').slideUp('fast')
	});
});
</script>
<header class="container header-mobile visible-xs">
	<div class="row menu-sub" id="menu-sub" style="display: none;">
		<a class="row col-xs-12" id="close">
			<div class="col-xs-3"></div>
			<div class="col-xs-9 close text-right">Close <i class="fa fa-times"></i></div>
		</a>
		<a class="row col-xs-12" href="<?php echo base_url(); ?>screening/create">
			<div class="col-xs-3"><img src="<?php echo base_url();?>../images/icon_screening_m.png" height="24px"></div>
			<div class="col-xs-9 title text-right">Screening &amp; Assessment</div>
		</a>
		<a class="row col-xs-12" href="<?php echo base_url(); ?>patients">
			<div class="col-xs-3"><img src="<?php echo base_url();?>../images/icon_patients_m.png" height="24px"></div>
			<div class="col-xs-9 title text-right">Patients List</div>
		</a>
		<a class="row col-xs-12" href="<?php echo base_url(); ?>information">
			<div class="col-xs-3"><img src="<?php echo base_url();?>../images/icon_information_m.png" height="24px"></div>
			<div class="col-xs-9 title text-right">Information</div>
		</a>
		<a class="row col-xs-12" href="<?php echo base_url(); ?>report">
			<div class="col-xs-3"><img src="<?php echo base_url();?>../images/icon_report_m.png" height="24px"></div>
			<div class="col-xs-9 title text-right">Report</div>
		</a>
		<a class="row col-xs-12" href="<?php echo base_url();?>/login/fn_logout">
			<div class="col-xs-3 title"><i class="fa fa-sign-out"></i></div>
			<div class="col-xs-9 title text-right"><i> Logout</i></div>
		</a>
	</div>
	<div class="row">
		<div class="col-xs-4 menu" id="menu">
			<i class="fa fa-bars"></i> เมนู
		</div>
		<div class="col-xs-8">
			<nav class="nav">
				<div class="hospital"><?=$_SESSION['hospital_title']?></div>
			</nav>
		</div>
	</div>
</header>