<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
<head>
	<title>Main</title>
	<?php $this->load->view('_config'); ?>
</head>
<body>
	<?php $this->load->view('_header'); ?>
	<div class="container information">
		<div class="row">
			<?php $this->load->view('_left_bar.php'); ?>
			<div class="col-sm-10 col-xs-12 content">
				<div class="row green_2 top-bar">
					<div class="col-sm-10 col-xs-6 back"></div>
					<div class="col-sm-2 col-xs-6 score"></div>
				</div>
				<div class="row">
					<div class="col-sm-8 col-xs-12 title">Information</div>
					<div class="col-sm-4 col-xs-12 btn-bar"></div>
				</div>
				<div class="row">
					<div class="col-sm-12 col-xs-12">
						<a href="<?=base_url().'information/screening'?>"><div class="well btn-color-vertical blue">Screening</div></a>
						<a href="<?=base_url().'information/naf'?>"><div class="well btn-color-vertical pink">Assessment NAF</div></a>
						<a href="<?=base_url().'information/nt'?>"><div class="well btn-color-vertical violet">Assessment NT</div></a>
						<a href="<?=base_url().'information/calculation'?>"><div class="well btn-color-vertical green">Calculation</div></a>
						<a href="<?=base_url().'information/diagnostic'?>"><div class="well btn-color-vertical orange">Diagnostic Model Criteria Consensus</div></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>