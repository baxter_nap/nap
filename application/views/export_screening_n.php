<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
<head>
	<title>Main</title>
	<?php $this->load->view('_config_export'); ?>
	<script src="<?=base_url()?>../js/jspdf.debug.js"></script>
	<script src="<?=base_url()?>../js/html2canvas.js"></script>
	<script src="<?=base_url()?>../js/posi_screening.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('body').css('overflow', 'auto');
		});
	</script>
</head>
<body class="export">
	<?php
		// echo"<pre>";print_r($data);echo "</pre>";
		// echo"<pre>";print_r($data_old);echo "</pre>";
	$debug = 0;
	$pnum = floor(($num-1)/3)*3;
	$num = $num % 3 == 0 ? 3:$num % 3;
	?>
	<div id="demo"></div>
	<div class="page" style="width: 1000px;height:1455px;overflow: hidden;">
		<label class="name"><?=$data['name']?></label>
		<label class="age"><?=$data['age']?></label>
		<label class="hn_code"><?=$data['hn_code']?></label>
		<label class="date"><?=date_format(date_create($data['date']),'d/m/Y')?></label>
		<label class="date_admission"><?=date_format(date_create($data['date_admission']),'d/m/Y')?></label>
		<label class="diagnose_system_title" style="font-size: 14px;"><?=$data['diagnose_system_title']?></label>
		<label class="diagnose_title"><?=$data['diagnose']?></label>
		<label class="patient_ward_title"><?=$data['patient_ward_id']?></label>
		<?php if ($debug==1||$data['weight_assessment']==1): ?>
			<label class="weight_assessment_1"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['weight_assessment']==2): ?>
			<label class="weight_assessment_2"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['weight_assessment']==3): ?>
			<label class="weight_assessment_3"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<label class="weight_current"><?=$data['weight_current']?></label>
		<?php if ($data['sex']==1): ?>
			<label class="weight_default"><?=$data['height']-100?></label>
		<?php endif ?>
		<?php if ($data['sex']==2): ?>
			<label class="weight_default"><?=$data['height']-110?></label>
		<?php endif ?>
		<label class="height"><?=$data['height']?></label>
		<label class="date_1" style="font-size: 12px;"><?=date_format(date_create(),'d/m/Y')?></label>
		<!-- c_1 -->
		<?php if ($debug==1||$data['c_1']==1): ?>
			<label class="c_1_1_1"><i class="fa fa-circle"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['c_1']==0): ?>
			<label class="c_1_0_1"><i class="fa fa-circle"></i></label>
		<?php endif ?>
		<!-- c_2 -->
		<?php if ($debug==1||$data['c_2']==1): ?>
			<label class="c_2_1_1"><i class="fa fa-circle"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['c_2']==0): ?>
			<label class="c_2_0_1"><i class="fa fa-circle"></i></label>
		<?php endif ?>
		<!-- c_3 -->
		<?php if ($debug==1||$data['c_3']==1): ?>
			<label class="c_3_1_1"><i class="fa fa-circle"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['c_3']==0): ?>
			<label class="c_3_0_1"><i class="fa fa-circle"></i></label>
		<?php endif ?>
		<!-- c_4 -->
		<?php if ($debug==1||$data['c_4']==1): ?>
			<label class="c_4_1_1"><i class="fa fa-circle"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['c_4']==0): ?>
			<label class="c_4_0_1"><i class="fa fa-circle"></i></label>
		<?php endif ?>
		
		<!-- check_result -->
		<?php if ($debug==1||$data['c_1']+$data['c_2']+$data['c_3']+$data['c_4']>=2): ?>
			<label class="check_result_1"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['c_1']+$data['c_2']+$data['c_3']+$data['c_4']<=1): ?>
			<label class="check_result_0"><i class="fa fa-check"></i></label>
		<?php endif ?>

		<img src="<?=base_url()?>../images/form-screening.jpg" style="width: 100%">
	</div>
	<?php if ($platform=="iOS") { ?>
	<div id="export-box"></div>
	<?php } ?>
	<button id="cmd" class="btn btn-color export">Print</button>
	<script type="text/javascript">
		// $( function() {
		// 	$( "label" ).draggable({
		// 		stop: function( event, ui ) {
		// 			name = $(ui.helper[0]).attr('class').split(' ')[0];
		// 			// console.log('old:'+posi[name]);
		// 			// console.log('name:'+name+' '+ui.position.left+","+ui.position.top);
		// 			posi[name] = [(ui.position.left),(ui.position.top)];
		// 			setJs(ui)
		// 		}
		// 	});
		// } );
		$(document).ready(function() {
			$('#cmd').click(function () {
				save()
			});
			setHtml()
		});

		function save() {
			$('#cmd').hide('fast','',function () {
				<?php if ($platform!="iOS") { ?>
					print();
					<?php }else{ ?>
						toPDF()
						<?php } ?>
						setTimeout(function() {
							$('#cmd').show('fast');
						}, 1000);
					});
		}

		function toPDF() {
			html2canvas($("#export-box canvas"), {
				onrendered: function(canvas) {
					imgData = canvas.toDataURL('image/jpeg', 1.0);              
					doc = new jsPDF("p", "mm", "a4");
					doc.addImage(imgData, 'JPEG', 0, 0, 210, 290);
	                // $('#cmd').hide('fast','',function () {
			    	// print();
	                // });
	                <?php if ($platform=="iOS") { ?>
	                	doc.autoPrint();
	                	doc.output('dataurl');
	                	<?php } ?>
	                // window.location.reload();
	                // $("#plugin").attr("src", doc.output('bloburi'));
	            }
	        });
		}

		function setHtml() {
			for (var k in posi){
				if (posi.hasOwnProperty(k)) {
					console.log(k)
					console.log(posi[k])
					$('.page .'+k).css({
						left: posi[k][0],
						top: posi[k][1]
					});
				}
			}
			<?php if ($platform=="iOS") { ?>
				setTimeout(function() {
					$('.page').hide('fast');
					html2canvas($('.page'), {
						onrendered: function(canvas) {
							$('#export-box').html(canvas);
							toPDF();
						}
					});
				}, 500);
				<?php } ?>
				<?php if ($debug): ?>
				showlabel()	
			<?php endif ?>
			<?php if ($save==1): ?>
			save()
		<?php endif ?>
	}

	function setJs(ui) {
		$.ajax({
			url: base_url+'/screening/jsSave/screening',
			type: 'POST',
			data: {posi: posi},
		})
		.done(function(respon) {
			console.log("success");
			console.log(respon);
		})
		.fail(function() {
			console.log("error");
		})
	}

	function showlabel() {
			//show label
			label = $('label')
			for (var i = label.length - 1; i >= 0; i--) {
				$(label[i]).prepend('<label class="'+$(label[i]).attr('class').split(' ')[0]+' sub">'+$(label[i]).attr('class').split(' ')[0]+'</label>')
			}
		}
	</script>
</body>
</html>