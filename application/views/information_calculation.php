<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
<head>
	<title>Main</title>
	<?php $this->load->view('_config'); ?>
</head>
<body>
	<?php $this->load->view('_header'); ?>
	<div class="container information">
		<div class="row">
			<?php $this->load->view('_left_bar.php'); ?>
			<div class="col-sm-10 col-xs-12 content">
				<div class="row green_2 top-bar">
					<div class="col-sm-10 col-xs-6 back"></div>
					<div class="col-sm-2 col-xs-6 score"></div>
				</div>
				<div class="row">
					<div class="col-sm-8 col-xs-12 title"><a href="<?=base_url()?>/information/" style="color: #32439f;">Information</a> > Calculation</div>
					<div class="col-sm-4 col-xs-12 btn-bar"></div>
				</div>
				<div class="row">
					<div class="col-sm-12 col-xs-12">
						<div class="row box">
							<div class="header green_2">
								<div class="col-sm-6 col-xs-12">Calculation</div>
							</div>
							<div class="body">
								<script type="text/javascript">
									function cal_ibw() {
										var sex = $('input[name="sex"]:checked').val()
										var height = $('input[name="height"]').val()
										if (sex==1) {
											$('#weight_ibw').val(height-100)
										}else if(sex==2){
											$('#weight_ibw').val(height-110)
										}else{
											$('#weight_ibw').val(0)
										}
									}
								</script>
								<div class="col-sm-12 col-xs-12">
									<div class="col-sm-12 col-xs-12 form-group">
										<div class="row">
											<div class="col-sm-5 col-xs-12">
												<label for="sex">เพศ</label>
											</div>
											<div class="col-sm-7 col-xs-12">
												<input type="radio" class="mtl with-gap" name="sex" value="1" onchange="cal_ibw()" id="sex_1" value="0">
												<label for="sex_1">ชาย</label>
												<input type="radio" class="mtl with-gap" name="sex" value="2" onchange="cal_ibw()" id="sex_2" value="0">
												<label for="sex_2">หญิง</label>
											</div>
										</div>
									</div>
									<div class="col-sm-12 col-xs-12 form-group">
										<div class="row">
											<div class="col-sm-5 col-xs-12">
												<label for="height">ความสูง</label>
											</div>
											<div class="col-sm-7 col-xs-12">
												<input type="number" name="height" id="height" onkeyup="cal_bmi();cal_ibw()" class="form-control">
												<label class="sub-label" for="height">เซนติเมตร</label>
											</div>
										</div>
									</div>
									<div class="col-sm-12 col-xs-12 form-group">
										<div class="row">
											<div class="col-sm-5 col-xs-12">
												<label for="weight_current">น้ำหนักปัจจุบัน</label>
											</div>
											<div class="col-sm-7 col-xs-12">
												<input type="number" name="weight_current" id="weight_current" onkeyup="cal_bmi();cal_ibw()" class="form-control">
												<label class="sub-label" for="weight_current">กิโลกรัม</label>
											</div>
										</div>
									</div>
									<div class="col-sm-12 col-xs-12 form-group">
										<div class="row">
											<div class="col-sm-5 col-xs-12">
												<label for="weight_ibw">น้ำหนักมาตรฐาน (IBW : Ideal Body Weight)</label>
												<a class="link" data-toggle="collapse" href="#sub-ibw" aria-expanded="false" aria-controls="sub-ibw"><i class="fa fa-chevron-down"></i></a>
												<div class="collapse" id="sub-ibw">
													<div class="well well-sub">
														IBW ชาย = ความสูง (ซม.)-100,<br> IBW หญิง = ความสูง (ซม.)-110
													</div>
												</div>
											</div>
											<div class="col-sm-7 col-xs-12">
												<input type="number" name="weight_ibw" id="weight_ibw" onkeyup="cal_bmi()" class="form-control">
												<label class="sub-label" for="weight_ibw">กิโลกรัม</label>
											</div>
										</div>
									</div>
									<div class="col-sm-12 col-xs-12 form-group">
										<div class="row" style="display: block;">
											<div class="col-sm-5 col-xs-12">
												<label for="bmi">BMI</label>
												<a class="link" data-toggle="collapse" href="#sub-bmi" aria-expanded="false" aria-controls="sub-bmi"><i class="fa fa-chevron-down"></i></a>
												<div class="collapse" id="sub-bmi">
													<div class="well well-sub">
														BMI = น้ำหนักตัว (กก.)/ส่วนสูง (เมตร)<sup>2</sup>
													</div>
												</div>
											</div>
											<div class="col-sm-7 col-xs-12 bmi">
												<input type="number" name="bmi" id="bmi" onkeyup="cal_bmi()" class="form-control">
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row box">
							<div class="header green_2">
								<div class="col-sm-6 col-xs-12">สารอาหาร <a class="link collapsed" style="color: #fff;" data-toggle="collapse" href="#sub-nutrient" onclick="check_focus(this)" aria-expanded="false" aria-controls="sub-nutrient"><i class="fa fa-chevron-down"></i></a></div>
								<script type="text/javascript">
									function check_focus(obj) {
										var gotos = $(obj).attr('href')
										if (gotos=="#sub-nutrient") {
											if ($('#sub-nutrient').attr('class') == 'collapse') {
												setTimeout(function() {
													$('.content').animate({scrollTop: $("#sub-nutrient").offset().top+100},'slow');
												}, 500);
											}
										}else if (gotos=="#sub-protein"){
											if ($('#sub-protein').attr('class') == 'collapse') {
												setTimeout(function() {
													$('.content').animate({scrollTop: $("#sub-protein").offset().top+950},'slow');
												}, 500);
											}
										}
										
									}
								</script>
							</div>
							<div class="body">
								<script type="text/javascript">
									function cal_per_ibw(type) {
										var type_val = $('#'+type).val()
										var weight_ibw = $('#weight_ibw').val()
										var weight_current = $('#weight_current').val()
										var bmi = $('#bmi').val()
										if (type=="energy") {
											if (bmi<=50) {
												$('#'+type+'_result').val(type_val*weight_current)
											}else{
												$('#'+type+'_result').val(type_val*weight_ibw)
											}
										}else if(type=="protein"){
											if (bmi<=30) {
												$('#'+type+'_result').val(type_val*weight_current)
											}else{
												$('#'+type+'_result').val(type_val*weight_ibw)
											}
										}else{
											$('#'+type+'_result').val(type_val*weight_current)
										}
									}
								</script>
								<div class="col-sm-12 col-xs-12">
									<div class="col-sm-12 col-xs-12 form-group">
										<div class="row">
											<div class="col-sm-2 col-xs-12">
												<label for="energy">พลังงาน</label>
											</div>
											<div class="col-sm-5 col-xs-12" style="padding-bottom: 5px;">
												<input type="number" name="energy" id="energy" onkeyup="cal_per_ibw('energy')" class="form-control">
												<label class="sub-label" for="energy">กิโลแคลอรี/กก./วัน</label>
											</div>
											<div class="col-sm-5 col-xs-12" style="padding-bottom: 5px;">
												<input type="number" name="energy_result" id="energy_result" onkeyup="cal_per_ibw('energy')" class="form-control yellow_1" readonly>
												<div class="sub-label yellow_1">กิโลแคลอรี/วัน</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-2 col-xs-12">
												<label for="protein">โปรตีน</label>
											</div>
											<div class="col-sm-5 col-xs-12" style="padding-bottom: 5px;">
												<input type="number" name="protein" id="protein" onkeyup="cal_per_ibw('protein')" class="form-control">
												<label class="sub-label" for="protein">กรัม/กก./วัน</label>
											</div>
											<div class="col-sm-5 col-xs-12" style="padding-bottom: 5px;">
												<input type="number" name="protein_result" id="protein_result" onkeyup="cal_per_ibw('protein')" class="form-control yellow_1" readonly>
												<div class="sub-label yellow_1">กรัม/วัน</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-2 col-xs-12">
												<label for="water">สารน้ำ</label>
											</div>
											<div class="col-sm-5 col-xs-12" style="padding-bottom: 5px;">
												<input type="number" name="water" id="water" onkeyup="cal_per_ibw('water')" class="form-control">
												<label class="sub-label" for="water">มล./กก./วัน</label>
											</div>
											<div class="col-sm-5 col-xs-12" style="padding-bottom: 5px;">
												<input type="number" name="water_result" id="water_result" onkeyup="cal_per_ibw('water')" class="form-control yellow_1" readonly>
												<div class="sub-label yellow_1">มล./วัน</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="collapse" id="sub-nutrient">
							<div class="row box">
								<div class="header green_2 hidden-xs">
									<div class="col-sm-3 col-xs-12">สารอาหาร</div>
									<div class="col-sm-3 col-xs-12">ผู้ป่วยคงที่</div>
									<div class="col-sm-3 col-xs-12">ผู้ป่วยวิกฤต</div>
									<div class="col-sm-3 col-xs-12">ผู้ป่วยอ้วนวิกฤต</div>
								</div>
								<div class="header green_2 visible-xs">
									<div class="col-xs-12">สารอาหาร : พลังงาน (กิโลแคลอรี/กก./วัน)</div>
								</div>
								<div class="body">
									<div class="col-sm-12 col-xs-12">
										<div class="row">
											<div class="col-sm-3 hidden-xs">พลังงาน (กิโลแคลอรี/กก./วัน)</div>
											<div class="visible-xs col-xs-12"><h3>ผู้ป่วยคงที่</h3></div>
											<div class="col-sm-3 col-xs-12">25 – 30 กิโลแคลอรี/กก. (น้ำหนักปัจจุบัน)/วัน</div>
											<div class="visible-xs col-xs-12"><h3>ผู้ป่วยวิกฤต</h3></div>
											<div class="col-sm-3 col-xs-12">20 – 35 กิโลแคลอรี/กก. (น้ำหนักปัจจุบัน)/วัน</div>
											<div class="visible-xs col-xs-12"><h3>ผู้ป่วยอ้วนวิกฤต</h3></div>
											<div class="col-sm-3 col-xs-12">
												<li>BMI 30-50 : 11 – 14  กิโลแคลอรี/กก.(น้ำหนักปัจจุบัน)/วัน</li>
												<li>BMI > 50 : 22 – 25 กิโลแคลอรี/กก.(น้ำหนักมาตรฐาน)/วัน</li>
											</div>
										</div>
									</div>
								</div>
								<div class="header green_2 visible-xs" style="margin-top: 10px;">
									<div class="col-xs-12">สารอาหาร : โปรตีน (กรัม/กก./วัน) <a class="link collapsed" style="color: #fff;" data-toggle="collapse" href="#sub-protein" onclick="check_focus(this)" aria-expanded="false" aria-controls="sub-protein"><i class="fa fa-chevron-down"></i></a></div>
								</div>
								<div class="body">
									<div class="col-sm-12 col-xs-12">
										<div class="row">
											<div class="col-sm-3 hidden-xs">โปรตีน (กรัม/กก./วัน) <a class="link collapsed" style="color: #333;" data-toggle="collapse" href="#sub-protein" onclick="check_focus(this)" aria-expanded="false" aria-controls="sub-protein"><i class="fa fa-chevron-down"></i></a></div>
											<div class="visible-xs col-xs-12"><h3>ผู้ป่วยคงที่</h3></div>
											<div class="col-sm-3 col-xs-12">0.8 – 1.0 กรัม/กก. (น้ำหนักปัจจุบัน)/วัน</div>
											<div class="visible-xs col-xs-12"><h3>ผู้ป่วยวิกฤต</h3></div>
											<div class="col-sm-3 col-xs-12">1.2 – 2.0 กรัม/กก. (น้ำหนักปัจจุบัน)/วัน</div>
											<div class="visible-xs col-xs-12"><h3>ผู้ป่วยอ้วนวิกฤต</h3></div>
											<div class="col-sm-3 col-xs-12">2.0 – 2.5 กรัม/กก.(น้ำหนักมาตรฐาน)/วัน</div>
										</div>
									</div>
								</div>
								<div class="header green_2 visible-xs" style="margin-top: 10px;">
									<div class="col-xs-12">สารอาหาร : สารน้ำ (มล/กก./วัน)</div>
								</div>
								<div class="body">
									<div class="col-sm-12 col-xs-12">
										<div class="row">
											<div class="col-sm-3 hidden-xs">สารน้ำ (มล/กก./วัน)</div>
											<div class="visible-xs col-xs-12"><h3>ผู้ป่วยคงที่</h3></div>
											<div class="col-sm-3 col-xs-12">30 – 40 (มล/กก./วัน)</div>
											<div class="visible-xs col-xs-12"><h3>ผู้ป่วยวิกฤต</h3></div>
											<div class="col-sm-3 col-xs-12">พิจารณาตามสภาพผู้ป่วย</div>
											<div class="visible-xs col-xs-12"><h3>ผู้ป่วยอ้วนวิกฤต</h3></div>
											<div class="col-sm-3 col-xs-12">พิจารณาตามสภาพผู้ป่วย</div>
										</div>
									</div>
								</div>
							</div>
							<p>
								References
								<ol>
									<li>Kreymann KG, et al. Clin Nutr 2006;25(2):210-223.</li>
									<li>McClave SA, et al. J Parenter Enteral Nutr 2016;40(2):159-211.</li>
									<li>Dickerson RN, et al. Nutr Clin Pract 2017;32(suppl 1):86S-93S.</li>
								</ol>
							</p>
						</div>
						<div class="collapse" id="sub-protein">
							<div class="row box">
								<div class="header green_2">
									<div class="col-sm-12 col-xs-12">ความต้องการโปรตีน</div>
									<div class="col-sm-6 col-xs-6">ภาวะ/โรค</div>
									<div class="col-sm-6 col-xs-6">ปริมาณโปรตีน (กรัม/กก/วัน)</div>
								</div>
								<div class="body">
									<div class="col-sm-12 col-xs-12">
										<div class="row">
											<div class="col-sm-6 col-xs-6">ไตเสื่อมระยะ 4 – 5 (ยังไม่ล้างไต)</div>
											<div class="col-sm-6 col-xs-6">0.6 – 0.8</div>
										</div>
									</div>
								</div>
								<div class="body">
									<div class="col-sm-12 col-xs-12">
										<div class="row">
											<div class="col-sm-6 col-xs-6">ผู้ที่มีภาวะโภชนาการปกติ</div>
											<div class="col-sm-6 col-xs-6">0.8 – 1.0</div>
										</div>
									</div>
								</div>
								<div class="body">
									<div class="col-sm-12 col-xs-12">
										<div class="row">
											<div class="col-sm-6 col-xs-6">ผู้สูงอายุที่แข็งแรงดี ผู้เจ็บป่วยเล็กน้อย</div>
											<div class="col-sm-6 col-xs-6">1.0 – 1.2</div>
										</div>
									</div>
								</div>
								<div class="body">
									<div class="col-sm-12 col-xs-12">
										<div class="row">
											<div class="col-sm-6 col-xs-6">ผู้ป่วยภาวะรุนแรง/วิกฤต ผู้สูงอายุที่ขาดอาหาร ผู้สูงอายุที่เจ็บป่วย</div>
											<div class="col-sm-6 col-xs-6">1.2 – 1.5</div>
										</div>
									</div>
								</div>
								<div class="body">
									<div class="col-sm-12 col-xs-12">
										<div class="row">
											<div class="col-sm-6 col-xs-6">ผู้ป่วยอ้วน</div>
											<div class="col-sm-6 col-xs-6">2.0 – 2.5 </div>
										</div>
									</div>
								</div>
								<div class="body">
									<div class="col-sm-12 col-xs-12">
										<div class="row">
											<div class="col-sm-6 col-xs-6">บาดแผลไฟไหม้ น้ำร้อนลวก</div>
											<div class="col-sm-6 col-xs-6">2.0 – 4.0 </div>
										</div>
									</div>
								</div>
							</div>
							<p>
								References
								<ol>
									<li>Deutz NEP, et al. Clin Nutr 2014;33(6):929-936.</li>
									<li>McClave SA, et al. J Parenter Enteral Nutr 2016;40(2):159-211.</li>
									<li>Dickerson RN, et al. Nutr Clin Pract 2017;32(suppl 1):86S-93S.</li>
									<li>Hurt RT, et al. Nutr Clin Pract 2017;32(suppl 1):142S-151S.</li>
								</ol>
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>