<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
<head>
	<title>Main</title>
	<?php $this->load->view('_config'); ?>
</head>
<body>
	<div class="container content">
		<div class="row">
			<div class="logo col-xs-12 text-center">
				<img src="<?=base_url()?>../images/logo-regis.png" height="100px">
				<h3>สมาคมผู้ให้อาหารทางหลอดเลือดดำและทางเดินอาหารแห่งประเทศไทย</h3>
			</div>
		</div>
		<div class="row">
			<hr style="border: 1px solid #666">
		</div>
		<form action="<?=base_url().'register/fn_create'?>" method="post">
			<div class="row regis-form">
				<div class="col-xs-12 form-group">
					<label for="name">1. ชื่อ - นามสกุล (ผู้ข้อใช้รหัสหลัก)</label>
					<input type="text" name="name" class="form-control">
				</div>
			</div>
			<div class="row regis-form">
				<div class="col-xs-12 form-group">
					<label for="name" id="professional_field">2. สาขาวิชาชีพ</label>
					<div class="form-group radio-btn">
						<div class="col-xs-12">
							<input type="radio" onchange="check_show(this)" name="professional_field" id="professional_field_1" value="แพทย์">
							<label class="btn red" for="professional_field_1">แพทย์</label>
							<div class="col-xs-12">
								<div class="col-xs-12 professional_field_sub" id="professional_field_1_sub" style="display: none;">
									<select class="form-control" onchange="check_more(this,5)" name="professional_field_1_sub">
										<option value="" disabled selected>โปรดเลือก</option>
										<option value="ศัลยแพทย์">ศัลยแพทย์</option>
										<option value="อายุรแพทย์">อายุรแพทย์</option>
										<option value="วิสัญญีแพทย์">วิสัญญีแพทย์</option>
										<option value="แพทย์ทั่วไป">แพทย์ทั่วไป</option>
										<option value="5">อื่นๆ</option>
									</select>
									<div class="col-xs-12" id="professional_field_1_sub_more" style="display: none;">
										<input type="text" name="professional_field_1_sub_more" class="form-control" placeholder="โปรดระบุ">
									</div>
								</div>
							</div>
						</div>
						<div class="col-xs-12">
							<input type="radio" onchange="check_show(this)" name="professional_field" id="professional_field_2" value="พยาบาล">
							<label class="btn green" for="professional_field_2">พยาบาล</label>
							<div class="col-xs-12">
								<div class="col-xs-12 professional_field_sub" id="professional_field_2_sub" style="display: none;">
									<select class="form-control" onchange="check_more(this,5)" name="professional_field_2_sub">
										<option value="" disabled selected>โปรดเลือก</option>
										<option value="ศัลยกรรม">ศัลยกรรม</option>
										<option value="อายุรกรรม">อายุรกรรม</option>
										<option value="โภชนบำบัด">โภชนบำบัด</option>
										<option value="หอผู้ป่วยวิกฤติ">หอผู้ป่วยวิกฤติ</option>
										<option value="5">อื่นๆ</option>
									</select>
									<div class="col-xs-12" id="professional_field_2_sub_more" style="display: none;">
										<input type="text" name="professional_field_2_sub_more" class="form-control" placeholder="โปรดระบุ">
									</div>
								</div>
							</div>
						</div>
						<div class="col-xs-12">
							<input type="radio" onchange="check_show(this)" name="professional_field" id="professional_field_3" value="นักกำหนดอาหาร">
							<label class="btn green" for="professional_field_3">นักกำหนดอาหาร</label>
						</div>
						<div class="col-xs-12">
							<input type="radio" onchange="check_show(this)" name="professional_field" id="professional_field_4" value="โภชนาการ">
							<label class="btn green" for="professional_field_4">โภชนาการ</label>
						</div>
					</div>
				</div>
			</div>
			<div class="row regis-form">
				<div class="col-xs-12 form-group">
					<label for="name">3. ชื่อโรงพยาบาล</label>
					<select class="form-control" name="hospital_id" id="hospital_id">
						<option value="" disabled selected>เลือกโรงพยาบาล</option>
						<?php foreach ($hospital as $key => $value): ?>
							<?php if ($value['id']!=1&&$value['id']!=2&&$value['id']!=3): ?>
							<option value="<?=$value['id']?>"><?=$value['title']?></option>
							<?php endif ?>
						<?php endforeach ?>
					</select>
				</div>
			</div>
			<div class="row regis-form">
				<div class="col-xs-12 form-group">
					<label for="name">3.1 รายชื่อหอผู้ป่วย</label>
				</div>
				<div class="col-xs-12 form-group">
					<div class="col-sm-3 col-xs-6">
						<input type="checkbox" class="mtl filled-in" name="patient_ward_1" value="1" id="patient_ward_1">
						<label for="patient_ward_1">ศัลยกรรมรวม</label>
					</div>
					<div class="col-sm-3 col-xs-6">
						<input type="checkbox" class="mtl filled-in" name="patient_ward_2" value="1" id="patient_ward_2">
						<label for="patient_ward_2">ศัลยกรรมชาย</label>
					</div>
					<div class="col-sm-3 col-xs-6">
						<input type="checkbox" class="mtl filled-in" name="patient_ward_3" value="1" id="patient_ward_3">
						<label for="patient_ward_3">ศัลยกรรมหญิง</label>
					</div>
					<div class="col-sm-3 col-xs-6">
						<input type="checkbox" class="mtl filled-in" name="patient_ward_4" value="1" id="patient_ward_4">
						<label for="patient_ward_4">ศัลยกรรม ICU</label>
					</div>
					<div class="col-sm-3 col-xs-6">
						<input type="checkbox" class="mtl filled-in" name="patient_ward_5" value="1" id="patient_ward_5">
						<label for="patient_ward_5">อายุรกรรมรวม</label>
					</div>
					<div class="col-sm-3 col-xs-6">
						<input type="checkbox" class="mtl filled-in" name="patient_ward_6" value="1" id="patient_ward_6">
						<label for="patient_ward_6">อายุรกรรมชาย</label>
					</div>
					<div class="col-sm-3 col-xs-6">
						<input type="checkbox" class="mtl filled-in" name="patient_ward_7" value="1" id="patient_ward_7">
						<label for="patient_ward_7">อายุรกรรมหญิง</label>
					</div>
					<div class="col-sm-3 col-xs-6">
						<input type="checkbox" class="mtl filled-in" name="patient_ward_8" value="1" id="patient_ward_8">
						<label for="patient_ward_8">อายุรกรรม ICU</label>
					</div>
					<div class="col-sm-3 col-xs-6">
						<input type="checkbox" class="mtl filled-in" name="patient_ward_9" value="1" id="patient_ward_9">
						<label for="patient_ward_9">หอผู้ป่วยมะเร็ง</label>
					</div>
					<div class="col-sm-12 col-xs-12">
						<label>* หากมีหอผู้ป่วยอื่นๆ (ใช้ ',' ในการคั่นแต่ละหอผู้ป่วย)</label>
						<textarea name="patient_ward_more" class="form-control" placeholder="เช่น หอผู้ป่วย1,หอผู้ป่วย2,หอผู้ป่วย3"></textarea>
					</div>
				</div>
			</div>
			<div class="row regis-form">
				<div class="col-xs-12 form-group">
					<label for="name">4. จังหวัด</label>
					<select class="form-control" name="province_id" id="province_id">
						<option value="" disabled selected>เลือกจังหวัด</option>
						<?php foreach ($province as $key => $value): ?>
							<option value="<?=$value->name?>"><?=$value->name?></option>
						<?php endforeach ?>
					</select>
				</div>
			</div>
			<div class="row regis-form">
				<div class="col-xs-12 form-group">
					<label for="postcode">5. รหัสไปรษณีย์</label>
					<input type="text" name="postcode" class="form-control">
				</div>
			</div>
			<div class="row regis-form">
				<div class="col-xs-12 form-group">
					<label for="name" id="affiliation">7. ต้นสังกัด</label>
					<div class="form-group radio-btn">
						<div class="col-xs-12">
							<input type="radio" onchange="check_show(this)" name="affiliation" id="affiliation_1" value="สาธารณสุข">
							<label class="btn red" for="affiliation_1">สาธารณสุข</label>
							<div class="col-xs-12">
								<div class="col-xs-12 affiliation_sub" id="affiliation_1_sub" style="display: none;">
									<select class="form-control" name="affiliation_1_sub">
										<option value="" disabled selected>เลือกสาขาย่อย</option>
										<option value="โรงพยาบาลศูนย์">โรงพยาบาลศูนย์</option>
										<option value="โรงพยาบาลทั่วไป">โรงพยาบาลทั่วไป</option>
										<option value="โรงพยาบาลชุมชน">โรงพยาบาลชุมชน</option>
									</select>
								</div>
							</div>
						</div>
						<div class="col-xs-12">
							<input type="radio" onchange="check_show(this)" name="affiliation" id="affiliation_2">
							<label class="btn green" for="affiliation_2">กรมการแพทย์</label>
						</div>
						<div class="col-xs-12">
							<input type="radio" onchange="check_show(this)" name="affiliation" id="affiliation_3">
							<label class="btn green" for="affiliation_3">สำนักการแพทย์</label>
						</div>
						<div class="col-xs-12">
							<input type="radio" onchange="check_show(this)" name="affiliation" id="affiliation_5" value="5">
							<label class="btn green" for="affiliation_5">รพ.เฉพาะทาง</label>
							<div class="col-xs-12">
								<input type="text" name="affiliation_more_5" class="form-control" placeholder="โปรดระบุ">
							</div>
						</div>
						<div class="col-xs-12">
							<input type="radio" onchange="check_show(this)" name="affiliation" id="affiliation_6">
							<label class="btn green" for="affiliation_6">รพ.มหาวิทยาลัย</label>
						</div>
						<div class="col-xs-12">
							<input type="radio" onchange="check_show(this)" name="affiliation" id="affiliation_7">
							<label class="btn green" for="affiliation_7">กระทรวงกลาโหม</label>
						</div>
						<div class="col-xs-12">
							<input type="radio" onchange="check_show(this)" name="affiliation" id="affiliation_8">
							<label class="btn green" for="affiliation_8">กระทรวงมหาดไทย</label>
						</div>
						<div class="col-xs-12">
							<input type="radio" onchange="check_show(this)" name="affiliation" id="affiliation_9" value="9">
							<label class="btn green" for="affiliation_9">โรงพยาบาลในสังกัดอื่น</label>
							<div class="col-xs-12">
								<input type="text" name="affiliation_more_9" class="form-control" placeholder="โปรดระบุ">
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row regis-form">
				<div class="col-xs-12 form-group">
					<label for="postcode">9. ระบุจำนวนผู้ที่จะขอใช้งาน (โดยประมาณ)</label>
					<input type="number" min="1" name="count" class="form-control">
				</div>
			</div>
			<div class="row regis-form">
				<div class="col-xs-12 form-group">
					<label for="email">10. อีเมลล์ที่ใช้สำหรับรับรหัสผ่าน</label>
					<input type="email" name="email" class="form-control" placeholder="เช่น example@example.com">
				</div>
			</div>
			<div class="row">
				<p style="color: #666;font-size: 20px;padding: 15px;" class="font_2">*หลังจากกรอกข้อมูลครบถ้วนเรียบร้อยแล้ว ให้กดปุ่มส่งข้อมูล จากนั้นท่านจะได้รับรหัสผ่าน โดยระบบจะส่งไปทางอีเมล์ที่ระบุไว้ ภายในระยะเวลา 1 วันทำการ</p>
			</div>
			<div class="row">
				<hr style="border: 1px solid #666">
			</div>
			<div class="row">
				<div class="btn-bar">
					<button class="btn btn-color green" onclick="check_required_submit()" type="button">ส่งข้อมูล</button>
				</div>
			</div>
		</form>
	</div>
</body>
<script type="text/javascript">
function check_required_submit() {
	var required = 0;
	var name,alert;
	if($('input[name="name"]').val()==""){
		required=1;
		name = $('input[name="name"]');
		alert = 'ชื่อ - นามสกุล';
	}else if($('input[name="professional_field"]:checked').val()==undefined){
		required=1;
		window.location.href = '#professional_field'
		name = $('input[name="professional_field"]')[0];
		alert = 'สาขาวิชาชีพ';
	}else if($('input[name="professional_field"]:checked').val()=='แพทย์'&&$('select[name="professional_field_1_sub"]').val()==null){
		required=1;
		name = $('select[name="professional_field_1_sub"]');
		alert = 'สาขาวิชาชีพ - แพทย์';
	}else if($('input[name="professional_field"]:checked').val()=='แพทย์'&&$('select[name="professional_field_1_sub"]').val()==5&&$('input[name="professional_field_1_sub_more"]').val()==""){
		required=1;
		name = $('input[name="professional_field_1_sub_more"]');
		alert = 'สาขาวิชาชีพ - แพทย์ - อื่นๆ';
	}else if($('input[name="professional_field"]:checked').val()=='พยาบาล'&&$('select[name="professional_field_2_sub"]').val()==null){
		required=1;
		name = $('select[name="professional_field_2_sub"]');
		alert = 'สาขาวิชาชีพ - พยาบาล';
	}else if($('input[name="professional_field"]:checked').val()=='พยาบาล'&&$('select[name="professional_field_2_sub"]').val()==5&&$('input[name="professional_field_2_sub_more"]').val()==""){
		required=1;
		name = $('input[name="professional_field_2_sub_more"]');
		alert = 'สาขาวิชาชีพ - พยาบาล - อื่นๆ';
	}else if($('select[name="hospital_id"]').val()==null){
		required=1;
		name = $('select[name="hospital_id"]');
		alert = 'ชื่อโรงพยาบาล';
	}else if($('select[name="province_id"]').val()==null){
		required=1;
		name = $('select[name="province_id"]');
		alert = 'จังหวัด';
	}else if($('input[name="postcode"]').val()==""){
		required=1;
		name = $('input[name="postcode"]');
		alert = 'รหัสไปรษณีย์';
	}else if($('input[name="affiliation"]:checked').val()==undefined){
		required=1;
		window.location.href = '#affiliation'
		name = $('input[name="affiliation"]')[0];
		alert = 'ต้นสังกัด';
	}else if($('input[name="affiliation"]:checked').val()=='สาธารณสุข'&&$('select[name="affiliation_1_sub"]').val()==null){
		required=1;
		name = $('select[name="affiliation_1_sub"]');
		alert = 'ต้นสังกัด - สาธารณสุข';
	}else if($('input[name="affiliation"]:checked').val()=='5'&&$('input[name="affiliation_more_5"]').val()==""){
		required=1;
		name = $('input[name="affiliation_more_5"]');
		alert = 'รพ.เฉพาะทาง';
	}else if($('input[name="affiliation"]:checked').val()=='9'&&$('input[name="affiliation_more_9"]').val()==""){
		required=1;
		name = $('input[name="affiliation_more_9"]');
		alert = 'โรงพยาบาลในสังกัดอื่น';
	}else if($('input[name="count"]').val()==""){
		required=1;
		name = $('input[name="count"]');
		alert = 'ระบุจำนวนผู้ที่จะขอใช้งาน';
	}else if($('input[name="email"]').val()==""){
		required=1;
		name = $('input[name="email"]');
		alert = 'อีเมลล์ที่ใช้สำหรับรับรหัสผ่าน';
	}

	if (required==1) {
		$(name).focus();
		swal('กรุณากรอกข้อมูลให้ครบถ้วน','กรุณากรอกข้อมูล '+alert,'warning')
	}else{
		$('form').submit();
	}
}
function check_show(obj) {
	var name = $(obj).attr('name')
	var tmp = $('.'+name+'_sub')
	for (var i = 0; i < tmp.length; i++) {
		if($(tmp[i]).attr('id')==$(obj).attr('id')+'_sub'){
			$(tmp[i]).slideDown('fast');
		}else{
			$(tmp[i]).slideUp('fast');
		}
	}
}
function check_more(obj,fill) {
	if($(obj).val()==fill){
		$('#'+$(obj).attr('name')+'_more').slideDown('fast');
	}else{
		$('#'+$(obj).attr('name')+'_more').slideUp('fast');
	}
}
</script>
</html>