<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
<head>
	<title>Main</title>
	<?php $this->load->view('_config'); ?>
</head>
<body>
	<?php $this->load->view('_header'); ?>
	<div class="container information">
		<div class="row">
			<?php $this->load->view('_left_bar.php'); ?>
			<div class="col-sm-10 col-xs-12 content">
				<div class="row green_2 top-bar">
					<div class="col-sm-10 col-xs-6 back"></div>
					<div class="col-sm-2 col-xs-6 score"></div>
				</div>
				<div class="row header-bar">
					<div class="col-sm-8 col-xs-12 title"><a href="<?=base_url()?>information/" style="color: #32439f;">Information</a> > Assessment NT</div>
					<div class="col-sm-4 col-xs-12 btn-bar">
						<button class="btn" onclick="printpdf()"><i class="fa fa-print"></i> Print / Export</button>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12 col-xs-12">
						<iframe src="https://drive.google.com/file/d/0ByYLIWJzyP5dQzZfZzVVX2gweWc/preview" width="100%"></iframe>
						<script type="text/javascript">
							$(document).ready(function() {
								$('iframe').height(assessment_height)
							});
							function printpdf()
							{
								if (check_device()=="desktop") {
									printJS('../../images/form-nt.pdf')
								}else{
									var win = window.open(base_url+'information/nt_mb', '_blank');
  									win.focus();
								}
							}
						</script>
						<!-- <table class="table table-fixed">
							<col style="width: 7%;"></col>
							<col style="width: 43%;"></col>
							<col style="width: 7%;"></col>
							<col style="width: 43%;"></col>
							<thead class="blue_1" style="color: #fff">
								<tr>
									<th colspan="2">ECOG (Eastern Cooperative Oncology Group) Performance Status</th>
									<th colspan="2">Karnofsky Performance Status</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="text-center">Score</td>
									<td>Description</td>
									<td class="text-center">Score</td>
									<td>Description</td>
								</tr>
								<tr>
									<td rowspan="2" class="text-center">0</td>
									<td rowspan="2">Fully active, able carry on all pre-disease performance without restiction.</td>
									<td class="text-center">100</td>
									<td>Normal, no complaints, no evidence of disease.</td>
								</tr>
								<tr>
									<td class="text-center">90</td>
									<td>Able to carry on normal activity, minor signs or symptoms of disease.</td>
								</tr>
								<tr>
									<td rowspan="2" class="text-center">1</td>
									<td rowspan="2">Restricted in physically strenuous activity but ambulatory and able to carry out work of light or sedentary nature.</td>
									<td class="text-center">80</td>
									<td>Normal activity with effort, some signs or symptoms of disease.</td>
								</tr>
								<tr>
									<td class="text-center">70</td>
									<td>Care for self, unable to carry on normal activity or do active work.</td>
								</tr>
								<tr>
									<td rowspan="2" class="text-center">2</td>
									<td rowspan="2">Ambulatory and capable of all selfcare but unable to carry out any work activities. Up and about more than 50% of waking hours.</td>
									<td class="text-center">60</td>
									<td>Requires occasional assistance, but is able to care for most of his/her needs.</td>
								</tr>
								<tr>
									<td class="text-center">50</td>
									<td>Requires considerable assistance and frequent medical care.</td>
								</tr>
								<tr>
									<td rowspan="2" class="text-center">3</td>
									<td rowspan="2">Capable of only limited selfcare, confined to bed or chair more than 50% of walking hours.</td>
									<td class="text-center">40</td>
									<td>Disabled, requires special care and assistance..</td>
								</tr>
								<tr>
									<td class="text-center">30</td>
									<td>Severly disabled, hospitalization indicated. Death not imminent.</td>
								</tr>
								<tr>
									<td rowspan="2" class="text-center">4</td>
									<td rowspan="2">Completely disabled. Can’t carry on any selfcare. Totally confined to bed or chair.</td>
									<td class="text-center">20</td>
									<td>Very sick, hospitalization indicated. Death not imminent.</td>
								</tr>
								<tr>
									<td class="text-center">10</td>
									<td>Moribund, fatal processes progressing repidly.</td>
								</tr>
							</tbody>
						</table> -->
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>