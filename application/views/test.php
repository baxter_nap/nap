<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
<head>
	<title>Main</title>
	<?php $this->load->view('_config'); ?>
	<script src="<?=base_url()?>../js/form.js"></script>
	<script src="<?=base_url()?>../js/jspdf.debug.js"></script>
	<script src="<?=base_url()?>../js/html2canvas.js"></script>
	<script src="<?=base_url()?>../js/posi.js"></script>
	<style type="text/css">

	</style>
</head>
<body class="export">
	<!-- <embed width="100%" height="100%" style="height: 100vh;" name="plugin" id="plugin" src="" type="application/pdf"> -->
	<?php echo"<pre>";print_r($data);echo "</pre>"; ?>
	<?php
		$debug = 1;
	?>
	<div id="demo"></div>
	<div class="page">
		<label class="name"><?=$data['name']?></label>
		<?php if ($debug==1||$data['sex']==1): ?>
			<label class="sex_1"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['sex']==2): ?>
			<label class="sex_2"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<label class="age"><?=$data['age']?></label>
		<label class="hn_code"><?=$data['hn_code']?></label>
		<label class="date_admission"><?=date_format(date_create($data['date_admission']),'d-m-Y')?></label>
		<label class="diagnose_title"><?=$data['diagnose_title']?></label>
		<label class="n1_1"><?=$data['n1_1']?></label>
		<label class="n1_2"><?=$data['n1_2']?></label>
		<label class="n1_3"><?=$data['n1_3']?></label>
		<!-- n2_1 -->
		<?php if ($debug==1||$data['n2_1']==1): ?>
			<label class="n2_1_1"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n2_1']==2): ?>
			<label class="n2_1_2"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n2_1']==3): ?>
			<label class="n2_1_3"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n2_1']==4): ?>
			<label class="n2_1_4"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<!-- n2_2 -->
		<?php if ($debug==1||$data['n2_2']==1): ?>
			<label class="n2_2_1"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n2_2']==2): ?>
			<label class="n2_2_2"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n2_2']==3): ?>
			<label class="n2_2_3"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n2_2']==4): ?>
			<label class="n2_2_4"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<!-- n2_3 -->
		<?php if ($debug==1||$data['n2_3']==1): ?>
			<label class="n2_3_1"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n2_3']==2): ?>
			<label class="n2_3_2"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n2_3']==3): ?>
			<label class="n2_3_3"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n2_3']==4): ?>
			<label class="n2_3_4"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<!-- n2_4 -->
		<?php if ($debug==1||$data['n2_4']==1): ?>
			<label class="n2_4_1"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n2_4']==2): ?>
			<label class="n2_4_2"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n2_4']==3): ?>
			<label class="n2_4_3"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n2_4']==4): ?>
			<label class="n2_4_4"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<!-- n3_1 -->
		<?php if ($debug==1||$data['n3_1']==1): ?>
			<label class="n3_1_1"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n3_1']==2): ?>
			<label class="n3_1_2"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n3_1']==3): ?>
			<label class="n3_1_3"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n3_1']==4): ?>
			<label class="n3_1_4"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<!-- n4_1 -->
		<?php if ($debug==1||$data['n4_1']==1): ?>
			<label class="n4_1_1"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n4_1']==2): ?>
			<label class="n4_1_2"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n4_1']==3): ?>
			<label class="n4_1_3"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n4_1']==4): ?>
			<label class="n4_1_4"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<!-- n5_1 -->
		<?php if ($debug==1||$data['n5_1']==1): ?>
			<label class="n5_1_1"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n5_1']==2): ?>
			<label class="n5_1_2"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n5_1']==3): ?>
			<label class="n5_1_3"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n5_1']==4): ?>
			<label class="n5_1_4"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<!-- n5_2 -->
		<?php if ($debug==1||$data['n5_2']==1): ?>
			<label class="n5_2_1"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n5_2']==2): ?>
			<label class="n5_2_2"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n5_2']==3): ?>
			<label class="n5_2_3"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n5_2']==4): ?>
			<label class="n5_2_4"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<!-- n6_1 -->
		<?php if ($debug==1||$data['n6_1_1']==1): ?>
			<label class="n6_1_1"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n6_1_2']==2): ?>
			<label class="n6_1_2"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n6_1_3']==3): ?>
			<label class="n6_1_3"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<!-- n6_2 -->
		<?php if ($debug==1||$data['n6_2_1']==1): ?>
			<label class="n6_2_1"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n6_2_2']==2): ?>
			<label class="n6_2_2"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n6_2_3']==3): ?>
			<label class="n6_2_3"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<!-- n6_3 -->
		<?php if ($debug==1||$data['n6_3_1']==1): ?>
			<label class="n6_3_1"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n6_3_2']==2): ?>
			<label class="n6_3_2"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n6_3_3']==3): ?>
			<label class="n6_3_3"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<!-- n7_1 -->
		<?php if ($debug==1||$data['n7_1_1']==1): ?>
			<label class="n7_1_1"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n7_1_2']==2): ?>
			<label class="n7_1_2"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n7_1_3']==3): ?>
			<label class="n7_1_3"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n7_1_4']==3): ?>
			<label class="n7_1_4"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<!-- n8_1 -->
		<?php if ($debug==1||$data['n8_1_1']==1): ?>
			<label class="n8_1_1"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n8_1_2']==2): ?>
			<label class="n8_1_2"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n8_1_3']==3): ?>
			<label class="n8_1_3"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n8_1_4']==3): ?>
			<label class="n8_1_4"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n8_1_5']==3): ?>
			<label class="n8_1_5"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n8_1_6']==3): ?>
			<label class="n8_1_6"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n8_1_7']==3): ?>
			<label class="n8_1_7"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n8_1_8']==3): ?>
			<label class="n8_1_8"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n8_1_9']==3): ?>
			<label class="n8_1_9"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n8_1_10']==3): ?>
			<label class="n8_1_10"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n8_1_11']==3): ?>
			<label class="n8_1_11"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<!-- n8_2 -->
		<?php if ($debug==1||$data['n8_2_1']==1): ?>
			<label class="n8_2_1"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n8_2_2']==2): ?>
			<label class="n8_2_2"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n8_2_3']==3): ?>
			<label class="n8_2_3"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n8_2_4']==3): ?>
			<label class="n8_2_4"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n8_2_5']==3): ?>
			<label class="n8_2_5"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n8_2_6']==3): ?>
			<label class="n8_2_6"><i class="fa fa-check"></i></label>
			<label class="n8_2_6_name"><?=$data['n8_2_6_name']?>asd</label>
		<?php endif ?>



		<img src="<?=base_url()?>../images/form-naf.jpg" width="1300px" alt="">
	</div>
	<div id="export-box"></div>
	<button id="cmd">export</button>
	<script type="text/javascript">
		$( function() {
			$( "label" ).draggable({
				stop: function( event, ui ) {
					name = $(ui.helper[0]).attr('class').split(' ')[0];
					console.log('old:'+posi[name]);
					console.log('name:'+name+' '+ui.position.left+","+ui.position.top);
					posi[name] = [(ui.position.left),(ui.position.top)];
					setJs(ui)
				}
			});
		} );
		$(document).ready(function() {
			$('#cmd').click(function () {
				// $('#loading').toggle('fast');
				html2canvas($('.page'), {
					onrendered: function(canvas) {
				    	// document.body.appendChild(canvas);
				    	$('#export-box').html(canvas);
				   	 	toPDF();
					}
				});
			});
			setHtml()
		});

		function toPDF() {
			html2canvas($("#export-box canvas"), {
				onrendered: function(canvas) {
					imgData = canvas.toDataURL('image/jpeg', 1.0);              
					var doc = new jsPDF("p", "mm", "a4");
					// console.log(width,height);
	                doc.addImage(imgData, 'JPEG', 0, 0, 271, 290);
	                doc.save('sample-file.pdf');
	                window.location.reload();
	                // $("#plugin").attr("src", doc.output('bloburi'));
	            }
	        });
		}

		function setHtml() {
			for (var k in posi){
			    if (posi.hasOwnProperty(k)) {
			    	console.log(k)
			    	console.log(posi[k])
			         $('.page .'+k).css({
			         	left: posi[k][0],
			         	top: posi[k][1]
			         });
			    }
			}
			showlabel()
		}

		function setJs(ui) {
			$.ajax({
				url: base_url+'/screening/jsSave',
				type: 'POST',
				data: {posi: posi},
			})
			.done(function(respon) {
				console.log("success");
				console.log(respon);
			})
			.fail(function() {
				console.log("error");
			})
		}

		showlabel = function () {
			//show label
			label = $('label')
			for (var i = label.length - 1; i >= 0; i--) {
				$(label[i]).prepend('<label class="'+$(label[i]).attr('class').split(' ')[0]+' sub">'+$(label[i]).attr('class').split(' ')[0]+'</label>')
			}
		}
	</script>
</body>
</html>