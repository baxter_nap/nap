<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// if (!isset($_SESSION['username'])) {
// 	redirect(base_url().'index.php', 'refresh');
// }
?>
<header class="container">
	<div class="row">
		<div class="col-xs-6 logo">
			<img src="<?php echo base_url();?>../images/Logo.png" height="60px">
		</div>
		<div class="col-xs-6">
			<nav class="nav">
				<div class="hospital"></div>
				<div class="logout"><i><a href="<?php echo base_url();?>/login/fn_logout_backoffice">Logout</a></i></div>
			</nav>
		</div>
	</div>
</header>