<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
<head>
	<title>Main</title>
	<?php $this->load->view('_config'); ?>
</head>
<body>
	<?php $this->load->view('backoffice/_header_bf'); ?>
	<div class="container admin">
		<div class="row">
			<?php $this->load->view('backoffice/_left_bar_bf.php'); ?>
			<div class="col-xs-10 content">
				<div class="row green_2 top-bar">
					<div class="col-xs-10 back"></div>
					<div class="col-xs-2 score"></div>
				</div>
				<div class="row header-bar">
					<div class="col-xs-8 title">สมาชิก</div>
					<div class="col-xs-4 btn-bar"><a href="<?=base_url().'admin/create'?>" class="btn btn-color">Create</a></div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<table class="table table_fixed">
							<thead class="blue_1" style="color: #fff;">
								<tr>
									<th width="10px">#</th>
									<th>ชื่อ</th>
									<th width="120px">เครื่องมือ</th>
								</tr>
							</thead>
							<tbody>
								<?php $i=0; foreach ($admin as $key => $value): $i++; ?>
								<tr>
									<td><?=$i?></td>
									<td><?=$value['username']?></td>
									<td class="tools"><a href="<?=base_url().'admin/edit/'.$value['id']?>" class="link">Edit</a> | <span onclick="del('admin',<?=$value['id']?>)" class="link">Delete</span></td>
								</tr>
								<?php endforeach ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>