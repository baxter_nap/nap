<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
<head>
	<title>Backoffice | Admin - Edit</title>
	<?php $this->load->view('_config'); ?>
</head>
<body>
	<?php $this->load->view('backoffice/_header_bf'); ?>
	<div class="container admin">
		<div class="row">
			<?php $this->load->view('backoffice/_left_bar_bf.php'); ?>
			<div class="col-xs-10 content">
				<div class="row top-bar">
					<div class="col-xs-8 title">การวินิจฉัยโรค | Edit</div>
					<div class="col-xs-4 btn-bar"></div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<form action="<?php echo base_url(); ?>admin/fn_edit" method="POST">
							<table class="table table-form table-fixed">
								<tr>
									<td width="100px">ชื่อ</td>
									<td><input type="text" name="username" value="<?=$data['username']?>" class="form-control" required></td>
								</tr>
								<tr>
									<td width="100px">รหัสผ่าน</td>
									<td><input type="password" name="password" value="<?=$data['password']?>" class="form-control" required></td>
								</tr>
								<tr>
									<td width="100px">โรงพยาบาล</td>
									<td>
										<select name="hospital_id" class="form-control" required>
											<option value="0" disabled selected>เลือกโรงพยาบาล</option>
										<?php foreach ($hospital as $key => $value): 
											$selected = $data['hospital_id'] == $value['id'] ? "selected" : "";
										?>
											<option value="<?=$value['id']?>" <?=$selected?>><?=$value['title']?></option>
										<?php endforeach ?>
										</select>
									</td>
								</tr>
								<tr>
									<td><input type="hidden" name="id" value="<?=$data['id']?>"></td>
									<td><button class="btn btn-color">Submit</button></td>
								</tr>
							</table>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>