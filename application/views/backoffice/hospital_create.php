<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
<head>
	<title>Main</title>
	<?php $this->load->view('_config'); ?>
</head>
<body>
	<?php $this->load->view('backoffice/_header_bf'); ?>
	<div class="container admin">
		<div class="row">
			<?php $this->load->view('backoffice/_left_bar_bf.php'); ?>
			<div class="col-xs-10 content">
				<div class="row top-bar">
					<div class="col-xs-8 title">โรงพยาบาล | Create</div>
					<div class="col-xs-4 btn-bar"></div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<form action="<?php echo base_url(); ?>hospital/fn_create" method="POST">
							<table class="table table-form table-fixed">
								<tr>
									<td width="150px">รหัสโรงพยาบาล</td>
									<td><input type="text" name="code" class="form-control"></td>
								</tr>
								<tr>
									<td>ชื่อโรงพยาบาล</td>
									<td><input type="text" name="title" class="form-control"></td>
								</tr>
								<tr>
									<td></td>
									<td><button class="btn btn-color">Submit</button></td>
								</tr>
							</table>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>