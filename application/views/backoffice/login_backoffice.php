<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
<head>
	<title>Main</title>
	<?php $this->load->view('_config'); ?>
</head>
<body>
	<div class="container login">
		<div class="row">
			<div class="logo col-xs-6">
				<img src="<?=base_url()?>../images/logo-login.png">
			</div>
			<form action="<?php echo base_url(); ?>login/fn_login_backoffice" method="POST" class="col-xs-6 form">
				<div class="row">
					<div class="col-xs-12 form-group subtitle">
						Backoffice Login
					</div>
					<div class="col-xs-12 form-group">
						<input type="text" name="username" class="form-control" placeholder="Username">
					</div>
					<div class="col-xs-12 form-group">
						<input type="password" name="password" class="form-control" placeholder="Password">
					</div>
					<div class="col-xs-12 form-group">
						<button class="btn col-xs-12 btn-color">Submit</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</body>
</html>