<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
<head>
	<title>Backoffice | Diagnose - Edit</title>
	<?php $this->load->view('_config'); ?>
</head>
<body>
	<?php $this->load->view('backoffice/_header_bf'); ?>
	<div class="container diagnose">
		<div class="row">
			<?php $this->load->view('backoffice/_left_bar_bf.php'); ?>
			<div class="col-xs-10 content">
				<div class="row top-bar">
					<div class="col-xs-8 title">การวินิจฉัยโรค | Create</div>
					<div class="col-xs-4 btn-bar"></div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<form action="<?php echo base_url(); ?>diagnose/fn_edit" method="POST">
							<table class="table table-form table-fixed">
								<tr>
									<td width="100px">ชื่อ</td>
										<td><input type="text" name="title" class="form-control" value="<?=$data['title']?>"></td>
								</tr>
								<tr>
									<td><input type="hidden" name="id" value="<?=$data['id']?>"></td>
									<td><button class="btn btn-color">Submit</button></td>
								</tr>
							</table>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>