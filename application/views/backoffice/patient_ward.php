<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
<head>
	<title>Main</title>
	<?php $this->load->view('_config'); ?>
</head>
<body>
	<?php $this->load->view('backoffice/_header_bf'); ?>
	<div class="container patient_ward">
		<div class="row">
			<?php $this->load->view('backoffice/_left_bar_bf.php'); ?>
			<div class="col-xs-10 content">
				<div class="row green_2 top-bar">
					<div class="col-xs-10 back"></div>
					<div class="col-xs-2 score"></div>
				</div>
				<div class="row header-bar">
					<div class="col-xs-8 title"> <a href="<?=base_url().'hospital/'?>" class="title"><?=$hospital['title']?></a>  | หอผู้ป่วย</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<table class="table table_fixed">
							<thead class="blue_1" style="color: #fff;">
								<tr>
									<th width="10px">#</th>
									<th width="240px">ชื่อหอผู้ป่วย</th>
									<th width="30px">เครื่องมือ</th>
								</tr>
							</thead>
							<tbody>
								<?php
									$i=0;
									foreach ($patient_ward as $key => $value):
								  $i++;
								?>
								<tr>
									<td><?=$i?></td>
									<td><?=$value['title']?></td>
									<td> <a href="<?=base_url().'patient_ward/edit/'.$value['id']?>" class="link">แก้ไข</a> | <span onclick="del('hospital',<?=$value['id']?>)" class="link">ลบ</span></td>
								</tr>
								<?php endforeach ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
