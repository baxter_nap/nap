<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
<head>
	<title>Main</title>
	<?php $this->load->view('_config'); ?>
</head>
<body>
	<?php $this->load->view('backoffice/_header_bf'); ?>
	<div class="container hospital">
		<div class="row">
			<?php $this->load->view('backoffice/_left_bar_bf.php'); ?>
			<div class="col-xs-10 content">
				<div class="row green_2 top-bar">
					<div class="col-xs-10 back"></div>
					<div class="col-xs-2 score"></div>
				</div>
				<div class="row header-bar">
					<div class="col-xs-8 title">โรงพยาบาล</div>
					<div class="col-xs-4 btn-bar"><a href="<?=base_url().'hospital/create'?>" class="btn btn-color">Create</a></div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<table class="table table_fixed">
							<thead class="blue_1" style="color: #fff;">
								<tr>
									<th width="10px">#</th>
									<th width="150px">รหัสโรงพยาบาล</th>
									<th>ชื่อ</th>
									<th width="170px">เครื่องมือ</th>
								</tr>
							</thead>
							<tbody>
								<?php $i=0; foreach ($hospital as $key => $value): $i++; ?>
								<tr>
									<td><?=$i?></td>
									<td><?=$value['code']?></td>
									<td><?=$value['title']?></td>
									<td class="link"><a href="<?=base_url().'patient_ward/index/'.$value['id'] ?>" class="link">หอผู้ป่วย</a> | <a href="<?=base_url().'hospital/edit/'.$value['id']?>" class="link">แก้ไข</a> | <span onclick="del('hospital',<?=$value['id']?>)" class="link">ลบ</span></td>
								</tr>
								<?php endforeach ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
