<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$menu = array('diagnose_system'=>'','hospital'=>'','admin'=>'','manage_member'=>'');
foreach ($menu as $key => $value) {
	foreach ($page as $k => $v) {
		if ($key==$v) {
			$menu[$key] = 'active';
		}
	}
}
?>
<div class="col-xs-2 left_bar bf">
	<a href="<?php echo base_url(); ?>admin/" class="<?php echo $menu['admin']; ?>"><li>สมาชิก</li></a>
	<a href="<?php echo base_url(); ?>hospital/" class="<?php echo $menu['hospital']; ?>"><li>โรงพยาบาล</li></a>
	<a href="<?php echo base_url(); ?>diagnose_system/" class="<?php echo $menu['diagnose_system']; ?>"><li>ระบบวินิจฉัยโรค</li></a>
	<a href="<?php echo base_url(); ?>manage_member/" class="<?php echo $menu['manage_member']; ?>"><li>จัดการสมาชิก</li></a>
</div>
