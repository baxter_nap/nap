<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
<head>
	<title>Main</title>
	<?php $this->load->view('_config'); ?>
</head>
<body>
	<?php $this->load->view('backoffice/_header_bf'); ?>
	<div class="container patient_ward">
		<div class="row">
			<?php $this->load->view('backoffice/_left_bar_bf.php'); ?>
			<div class="col-xs-10 content">
				<div class="row green_2 top-bar">
					<div class="col-xs-10 back"></div>
					<div class="col-xs-2 score"></div>
				</div>
				<div class="row header-bar">
					<div class="col-xs-8 title">จัดการสมาชิก</div>
					<div class="col-xs-4 btn-bar"></div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<table class="table table_fixed">
							<thead class="blue_1" style="color: #fff;">
								<tr>
									<th width="10px">#</th>
									<th width="240px">ชื่อ</th>
									<th>ชื่อโรงพยาบาล</th>
									<th>สถานะ</th>
									<th width="120px">เครื่องมือ</th>
								</tr>
							</thead>
							<tbody>
								<?php $i=0; foreach ($register as $key => $value): $i++; ?>
								<tr>
									<td><?=$i?></td>
									<td><?=$value['name']?></td>
									<td><?=$value['hospital_title']?></td>
									<td><?php echo $value['status']==1 ? '<span class="label label-success">ส่งอีเมลแล้ว</span>' : '<span class="label label-warning">ยังไม่ได้ส่งอีเมล</span>'?></td>
									<td class="tools"><a href="<?=base_url().'manage_member/edit/'.$value['id']?>" class="link">ส่งอีเมล</a> | <span onclick="del('register',<?=$value['id']?>)" class="link">ลบ</span></td>
								</tr>
								<?php endforeach ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>