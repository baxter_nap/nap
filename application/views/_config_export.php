<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<meta charset="utf-8">
<!-- <link rel="apple-touch-icon" href="<?php echo base_url();?>../images/iconWeb.png" /> -->

<!-- css -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>../css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>../css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>../css/sweetalert.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>../css/jquery-ui.min.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<!-- my file -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>../css/font.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>../css/reset-style.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>../css/style.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>../css/color-style.css">

<!-- js -->
<script type="text/javascript" src="<?php echo base_url();?>../js/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>../js/jquery-ui.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>../js/moment.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>../js/sweetalert.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>../js/datatables.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>../js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>../js/Chart.min.js"></script>
<script type="text/javascript">base_url = '<?php echo base_url(); ?>'</script>
<script type="text/javascript" src="<?php echo base_url();?>../js/script.js"></script>


