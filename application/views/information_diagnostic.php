<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
<head>
	<title>Main</title>
	<?php $this->load->view('_config'); ?>
</head>
<body>
	<?php $this->load->view('_header'); ?>
	<div class="container information">
		<div class="row">
			<?php $this->load->view('_left_bar.php'); ?>
			<div class="col-sm-10 col-xs-12 content">
				<div class="row green_2 top-bar">
					<div class="col-sm-10 col-xs-6 back"></div>
					<div class="col-sm-2 col-xs-6 score"></div>
				</div>
				<div class="row">
					<div class="col-sm-8 col-xs-12 title"><a href="<?=base_url()?>/information/" style="color: #32439f;">Information</a> > Diagnostic Model Criteria Consensus </div>
					<div class="col-sm-4 col-xs-12 btn-bar"></div>
				</div>
				<div class="row">
					<div class="col-sm-12 col-xs-12">
						<div class="row box">
							<div class="header orange_1">
								<div class="col-sm-2 col-xs-2">
									Code
								</div>
								<div class="col-sm-5 col-xs-5">
									การวินิฉัย
								</div>
								<div class="col-sm-5 col-xs-5">
									เกณฑ์การวินิจฉัยจากข้อสรุปการประชุม
								</div>
							</div>
							<div class="body">
								<div class="col-sm-2 col-xs-2">
									E41
								</div>
								<div class="col-sm-5 col-xs-5">
									Marasmus หรือ Starvation related malnutrition Cachexia หรือ Chronic diseased malnutrition
								</div>
								<div class="col-sm-5 col-xs-5">
									Triceps skinflod &lt;3 มม. Midarm muscle circumference &lt;15 ซม.
								</div>
							</div>
							<div class="body">
								<div class="col-sm-2 col-xs-2">
									E40
								</div>
								<div class="col-sm-5 col-xs-5">
									Kwashiokor หรือ Acute disease -  หรือ Injury - related malnutrition
								</div>
								<div class="col-sm-5 col-xs-5">
									ระดับอัลบูมินในเลือด &lt; 2.8 กรัม/ดล. ร่วมกับลักษณะดังต่อไปนี้อย่างน้อย 1 ข้อ ได้แก่ แผลหายช้า, แผลกดทับ, ผิวหนังแตก, ผมหลุดร่วงง่าย หรือ บวม
								</div>
							</div>
							<div class="body">
								<div class="col-sm-2 col-xs-2">
									E42
								</div>
								<div class="col-sm-5 col-xs-5">
									Marasmic - Kwashiokor
								</div>
								<div class="col-sm-5 col-xs-5">
									เกณฑ์ Marasmus ร่วมกับ Kwashiokor 
								</div>
							</div>
							<div class="body">
								<div class="col-sm-2 col-xs-2">
									E44.1
								</div>
								<div class="col-sm-5 col-xs-5">
									Mild malnutrition (Mild protein - calorie malnutrition)
								</div>
								<div class="col-sm-5 col-xs-5">
									ดัชนีมวลกาย 17.00-18.49 หรือ NAF: A หรือ NT: 2
								</div>
							</div>
							<div class="body">
								<div class="col-sm-2 col-xs-2">
									E44.0
								</div>
								<div class="col-sm-5 col-xs-5">
									Moderate malnutrition (Moderate protein - calorie malnutrition)
								</div>
								<div class="col-sm-5 col-xs-5">
									ดัชนีมวลกาย 16.00-16.99 หรือ NAF: B หรือ NT: 3
								</div>
							</div>
							<div class="body">
								<div class="col-sm-2 col-xs-2">
									E43
								</div>
								<div class="col-sm-5 col-xs-5">
									Severe malnutrition (Unspecified severe protein - calorie malnutrition)
								</div>
								<div class="col-sm-5 col-xs-5">
									ดัชนีมวลกาย < 16 หรือ NAF: C หรือ NT: 4
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>