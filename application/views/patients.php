<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
<head>
	<title>Main</title>
	<?php $this->load->view('_config'); ?>
</head>
<body>
	<?php $this->load->view('_header'); ?>
	<div class="container patient">
		<div class="row">
			<?php $this->load->view('_left_bar.php'); ?>
			<div class="col-sm-10 col-xs-12 content">
				<div class="row green_2 top-bar">
					<div class="col-xs-10 back"></div>
					<div class="col-xs-2 score"></div>
				</div>
				<div class="row">
					<div class="col-xs-8 title">เลือกหอผู้ป่วย</div>
					<div class="col-xs-4 btn-bar"></div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<?php foreach ($patient_ward as $key => $value): ?>
							<a href="<?=base_url().'patients/lists/'.$value['id']?>"><div class="well"><?=$value['title']?> (<?=$patient_ward_count[$value['id']]?>)</div></a>
						<?php endforeach ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>