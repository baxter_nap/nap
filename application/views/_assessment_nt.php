<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script type="text/javascript">
	required = ['n0_1','n0_2','n1_1','n1_2','n1_3','n1_4','n2_1','n3_1','n4_1','n5_1','n6_1']
	required_name = [
					'ECOG (Eastern Cooperative Oncology Group) Performance Status',
					'Select Karnofsky Performance Status',
					'1.1 เลือกประเภทของอาหารที่ได้รับ',
					'1.2 เลือกปริมาณที่ได้รับ',
					'1.3 เลือกระยะเวลาที่เปลี่ยนแปลง',
					'1.4 เลือกระดับการเปลี่ยนแปลง',
					'2. การเปลี่ยนแปลงของน้ำหนักตัว',
					'3. ภาวะบวมน้ำ (Fluid accumulation)',
					'4. ระดับการสูญเสีย มวลไขมัน (Body fat loss) ประเมินเฉลี่ยทั่วร่างกาย',
					'5. ระดับการสูญเสีย มวลกล้ามเนื้อ (Muscle loss) ประเมินเฉลี่ยทั่วร่างกาย',
					'6. สมรรถภาพกล้ามเนื้อ (ประเมินเฉลี่ยทั่วร่างกาย)'
				]
	sum_all = 0
	$(document).ready(function() {
		check_required(required)
	});
</script>
<div class="head-box row">
	<div class="col-xs-10">
		<div class="title violet_1">แบบประเมิน Assessment NT</div>
		<div class="sub">*ประยุกต์จากแนวคิดความเห็นใหม่สากล - White JV, et al. Consensus Statement : J Acad Nutr Diet 2012, 112(5):730-738</div>
	</div>
	<div class="col-xs-2">
		<div class="title violet_1">คะแนนที่ได้</div>
	</div>
</div>

<div class="assessment-box">
	<div class="row box table-screening table-nt">
		<div class="header no-radius violet_1">
			<div class="col-sm-12 col-xs-12">สภาพผู้ป่วย (Patient performance status score)</div>
		</div>
		<div class="body">
			<div class="col-sm-12 col-xs-12 form-group">
				<div class="col-sm-10 col-xs-10 question_assessment">
					<div class="row">
						<div class="col-sm-12 col-xs-12 label-input focus_n0_1" style="text-decoration: underline;font-size: 22px;">ECOG (Eastern Cooperative Oncology Group) Performance Status</div>
					</div>
					<script type="text/javascript">
						function check_n0_1() {
							$('input[name="n0_2"]').prop('checked',false)
						}
					</script>
					<div class="col-sm-12 col-xs-12 radio-sub">
						<input type="radio" class="mtl with-gap" name="n0_1" onchange="check_n0_1();get_nt()" id="n0_0" value="0">
						<label for="n0_0">0 | Fully active, able to carry on all pre-disease performance without restriction.</label>
						<ul class="sub">
							<div>
								<label style="text-decoration: underline;font-size: 22px;">Select Karnofsky Performance Status</label>
							</div>
							<div>
								<input type="radio" class="mtl with-gap" onchange="get_nt()" name="n0_2" id="n0_1_100" value="100">
								<label for="n0_1_100">100 | Normal, no complaints, no evidence of disease.</label>
							</div>
							<div>
								<input type="radio" class="mtl with-gap" onchange="get_nt()" name="n0_2" id="n0_1_90" value="90">
								<label for="n0_1_90">90 | a\Able to carry on normal activity, minor signs or symptoms of disease.</label>
							</div>
						</ul>
					</div>
					<div class="col-sm-12 col-xs-12 radio-sub">
						<input type="radio" class="mtl with-gap" name="n0_1" onchange="check_n0_1();get_nt()" id="n0_1" value="1">
						<label for="n0_1">1 | Restricted in physically strenuous activity but ambulatory and able to carry out work of light or sedentary nature. e.g., light housework, office work.</label>
						<ul class="sub">
							<div>
								<label style="text-decoration: underline;font-size: 22px;">Select Karnofsky Performance Status</label>
							</div>
							<div>
								<input type="radio" class="mtl with-gap" onchange="get_nt()" name="n0_2" id="n0_1_80" value="80">
								<label for="n0_1_80">80 | Normal activity with effort, some signs or symptoms of disease.</label>
							</div>
							<div>
								<input type="radio" class="mtl with-gap" onchange="get_nt()" name="n0_2" id="n0_1_70" value="70">
								<label for="n0_1_70">70 | Care for self, unable to carry on normal activity or do active work.</label>
							</div>
						</ul>
					</div>
					<div class="col-sm-12 col-xs-12 radio-sub">
						<input type="radio" class="mtl with-gap" name="n0_1" onchange="check_n0_1();get_nt()" id="n0_2" value="2">
						<label for="n0_2">2 | Ambulatory and capable of all selfcare but unable to carry out any work activities. Up and about more than 50% of waking hours.</label>
						<ul class="sub">
							<div>
								<label style="text-decoration: underline;font-size: 22px;">Select Karnofsky Performance Status</label>
							</div>
							<div>
								<input type="radio" class="mtl with-gap" onchange="get_nt()" name="n0_2" id="n0_1_60" value="60">
								<label for="n0_1_60">60 | Requires occasional assistance, but is able to care for most of his/her needs.</label>
							</div>
							<div>
								<input type="radio" class="mtl with-gap" onchange="get_nt()" name="n0_2" id="n0_1_50" value="50">
								<label for="n0_1_50">50 | Requires considerable assistance and frequent medical care.</label>
							</div>
						</ul>
					</div>
					<div class="col-sm-12 col-xs-12 radio-sub">
						<input type="radio" class="mtl with-gap" name="n0_1" onchange="check_n0_1();get_nt()" id="n0_3" value="3">
						<label for="n0_3">3 | Capable of only limited selfcare, confined to bed or chair more than 50% of waking hours.</label>
						<ul class="sub">
							<div>
								<label style="text-decoration: underline;font-size: 22px;">Select Karnofsky Performance Status</label>
							</div>
							<div>
								<input type="radio" class="mtl with-gap" onchange="get_nt()" name="n0_2" id="n0_1_40" value="40">
								<label for="n0_1_40">40 | Disabled, requires special care and assistance.</label>
							</div>
							<div>
								<input type="radio" class="mtl with-gap" onchange="get_nt()" name="n0_2" id="n0_1_30" value="30">
								<label for="n0_1_30">30 | Severly disabled, hospitalization indicated. Death not imminent.</label>
							</div>
						</ul>
					</div>
					<div class="col-sm-12 col-xs-12 radio-sub">
						<input type="radio" class="mtl with-gap" name="n0_1" onchange="check_n0_1();get_nt()" id="n0_4" value="4">
						<label for="n0_4">4 | Completely disabled. Can’t carry on any selfcare. Totally confined to a bed or chair.</label>
						<ul class="sub">
							<div>
								<label style="text-decoration: underline;font-size: 22px;">Select Karnofsky Performance Status</label>
							</div>
							<div>
								<input type="radio" class="mtl with-gap" onchange="get_nt()" name="n0_2" id="n0_1_20" value="20">
								<label for="n0_1_20">20 | Very sick, hospitalization indicated. Death not imminent.</label>
							</div>
							<div>
								<input type="radio" class="mtl with-gap" onchange="get_nt()" name="n0_2" id="n0_1_10" value="10">
								<label for="n0_1_10">10 | Moribund, fatal processes progressing rapidly.</label>
							</div>
						</ul>
					</div>
				</div>
				<div class="col-sm-2 col-xs-2 score" noscore></div>
			</div>
		</div>
		<div class="header no-radius violet_1">
			<div class="col-sm-12 col-xs-12">1. ประวัติการได้รับอาหาร หรือ สารอาหาร</div>
		</div>
		<div class="body">
			<div class="col-sm-12 col-xs-12 form-group">
				<div class="col-sm-10 col-xs-10 question_assessment">
					<div class="row">
						<div class="col-sm-12 col-xs-12">1.1 เลือกประเภทของอาหารที่ได้รับ</div>
					</div>
					<div class="col-sm-6 col-xs-12">
						<input type="radio" class="mtl with-gap" onchange="get_nt();checkn1_4()" value="-" name="n1_1" id="n1_1_1">
						<label for="n1_1_1">กินเอง</label>
					</div>
					<div class="col-sm-6 col-xs-12">
						<input type="radio" class="mtl with-gap" onchange="get_nt();checkn1_4()" value="-" name="n1_1" id="n1_1_2">
						<label for="n1_1_2">TF (Tube feeding)</label>
					</div>
					<div class="col-sm-6 col-xs-12">
						<input type="radio" class="mtl with-gap" onchange="get_nt();checkn1_4()" value="-" name="n1_1" id="n1_1_3">
						<label for="n1_1_3">PN (Parenteral nutrition)</label>
					</div>
					<div class="col-sm-6 col-xs-12">
						<input type="radio" class="mtl with-gap" onchange="get_nt();checkn1_4()" value="-" name="n1_1" id="n1_1_4">
						<label for="n1_1_4">Standard IV</label>
					</div>
					<div class="col-sm-6 col-xs-12">
						<input type="radio" class="mtl with-gap" onchange="get_nt();checkn1_4()" value="-" name="n1_1" id="n1_1_5">
						<label for="n1_1_5">Combination</label>
					</div>
				</div>
				<div class="col-sm-2 col-xs-2 score"></div>
			</div>
		</div>
		<div class="body">
			<div class="col-sm-12 col-xs-12 form-group">
				<div class="col-sm-10 col-xs-10 question_assessment">
					<div class="row">
						<div class="col-sm-12 col-xs-12">1.2 เลือกปริมาณที่ได้รับ</div>
					</div>
					<div class="col-sm-6 col-xs-12">
						<input type="radio" class="mtl with-gap" onchange="get_nt();checkn1_4()" value="-" name="n1_2" id="n1_2_1">
						<label for="n1_2_1">< 10% (NPO, ได้รับน้ำเกลือมาตรฐาน)</label>
					</div>
					<div class="col-sm-6 col-xs-12">
						<input type="radio" class="mtl with-gap" onchange="get_nt();checkn1_4()" value="-" name="n1_2" id="n1_2_2">
						<label for="n1_2_2">10-25% ของปริมาณปกติ หรือ แคลอรี่ ที่ต้องการ</label>
					</div>
					<div class="col-sm-6 col-xs-12">
						<input type="radio" class="mtl with-gap" onchange="get_nt();checkn1_4()" value="-" name="n1_2" id="n1_2_3">
						<label for="n1_2_3">25-50% ของปริมาณปกติ หรือ แคลอรี่ ที่ต้องการ</label>
					</div>
					<div class="col-sm-6 col-xs-12">
						<input type="radio" class="mtl with-gap" onchange="get_nt();checkn1_4()" value="-" name="n1_2" id="n1_2_4">
						<label for="n1_2_4">50-75% ของปริมาณปกติ หรือ แคลอรี่ ที่ต้องการ</label>
					</div>
					<div class="col-sm-6 col-xs-12">
						<input type="radio" class="mtl with-gap" onchange="get_nt();checkn1_4()" value="-" name="n1_2" id="n1_2_5">
						<label for="n1_2_5">75-100% ของปริมาณปกติ หรือ แคลอรี่ ที่ต้องการ</label>
					</div>
				</div>
				<div class="col-sm-2 col-xs-2 score"></div>
			</div>
		</div>
		<div class="body">
			<div class="col-sm-12 col-xs-12 form-group">
				<div class="col-sm-10 col-xs-10 question_assessment">
					<div class="row">
						<div class="col-sm-12 col-xs-12">1.3 เลือกระยะเวลาที่เปลี่ยนแปลง</div>
					</div>
					<div class="col-sm-6 col-xs-12">
						<input type="radio" class="mtl with-gap" onchange="get_nt();checkn1_4()" value="-" name="n1_3" id="n1_3_1">
						<label for="n1_3_1">น้อยกว่าหรือเท่ากับ 7 วัน</label>
					</div>
					<div class="col-sm-6 col-xs-12">
						<input type="radio" class="mtl with-gap" onchange="get_nt();checkn1_4()" value="-" name="n1_3" id="n1_3_2">
						<label for="n1_3_2">8 - 14 วัน</label>
					</div>
					<div class="col-sm-6 col-xs-12">
						<input type="radio" class="mtl with-gap" onchange="get_nt();checkn1_4()" value="-" name="n1_3" id="n1_3_3">
						<label for="n1_3_3">มากกว่า 14 วัน</label>
					</div>
				</div>
				<div class="col-sm-2 col-xs-2 score"></div>
			</div>
		</div>
		<div class="body n1_4" style="display: none;">
			<div class="col-sm-12 col-xs-12 form-group">
				<div class="col-sm-10 col-xs-10 question_assessment">
					<div class="row">
						<div class="col-sm-12 col-xs-12">1.4 เลือกระดับการเปลี่ยนแปลง</div>
					</div>
					<script type="text/javascript">
						function checkn1_4() {
							setTimeout(function() {
								if (qwe.n1_1!=undefined&&qwe.n1_2!=undefined&&qwe.n1_3!=undefined) {
									$('.n1_4').slideDown('fast');
									if ((qwe.n1_2==1&&qwe.n1_3==1)||(qwe.n1_2==2&&qwe.n1_3==2)||(qwe.n1_2==3&&qwe.n1_3==3)) {
										$('.n1_4_0').slideUp('fast');
										$('.n1_4_1').slideDown('fast');
										$('.n1_4_2').slideDown('fast');
										$('.n1_4_3').slideUp('fast');
										$('.n1_4_4').slideUp('fast');
									}else if ((qwe.n1_2==1&&qwe.n1_3==2)||(qwe.n1_2==2&&qwe.n1_3==3)){
										$('.n1_4_0').slideUp('fast');
										$('.n1_4_1').slideUp('fast');
										$('.n1_4_2').slideDown('fast');
										$('.n1_4_3').slideDown('fast');
										$('.n1_4_4').slideUp('fast');
									}else if (qwe.n1_2==1&&qwe.n1_3==3){
										$('.n1_4_0').slideUp('fast');
										$('.n1_4_1').slideUp('fast');
										$('.n1_4_2').slideUp('fast');
										$('.n1_4_3').slideDown('fast');
										$('.n1_4_4').slideDown('fast');
									}else if ((qwe.n1_2==2&&qwe.n1_3==1)||(qwe.n1_2==3&&qwe.n1_3==2)){
										$('.n1_4_0').slideDown('fast');
										$('.n1_4_1').slideDown('fast');
										$('.n1_4_2').slideUp('fast');
										$('.n1_4_3').slideUp('fast');
										$('.n1_4_4').slideUp('fast');
									}else if (qwe.n1_2==4&&qwe.n1_3==3){
										$('.n1_4_0').slideUp('fast');
										$('.n1_4_1').slideDown('fast');
										$('.n1_4_2').slideUp('fast');
										$('.n1_4_3').slideUp('fast');
										$('.n1_4_4').slideUp('fast');
									}else if ((qwe.n1_2==3&&qwe.n1_3==1)||(qwe.n1_2==4&&qwe.n1_3==2)||(qwe.n1_2==5&&qwe.n1_3==3)||(qwe.n1_2==4&&qwe.n1_3==1)||(qwe.n1_2==5&&qwe.n1_3==2)||(qwe.n1_2==5&&qwe.n1_3==1)){
										$('.n1_4_0').slideDown('fast');
										$('.n1_4_1').slideUp('fast');
										$('.n1_4_2').slideUp('fast');
										$('.n1_4_3').slideUp('fast');
										$('.n1_4_4').slideUp('fast');
									}
								}else{
									$('.n1_4').slideUp('fast');
								}
							}, 300);
							
						}
					</script>
					<div class="col-sm-6 col-xs-12 n1_4_0" style="display: none;">
						<input type="radio" class="mtl with-gap" onchange="get_nt()" name="n1_4" id="n1_4_0" value="0">
						<label for="n1_4_0">ปกติ (0)</label>
					</div>
					<div class="col-sm-6 col-xs-12 n1_4_1" style="display: none;">
						<input type="radio" class="mtl with-gap" onchange="get_nt()" name="n1_4" id="n1_4_1" value="1">
						<label for="n1_4_1">มีการเปลี่ยนแปลงเล็กน้อย (1)</label>
					</div>
					<div class="col-sm-6 col-xs-12 n1_4_2" style="display: none;">
						<input type="radio" class="mtl with-gap" onchange="get_nt()" name="n1_4" id="n1_4_2" value="2">
						<label for="n1_4_2">มีการเปลี่ยนแปลงปานกลาง (2)</label>
					</div>
					<div class="col-sm-6 col-xs-12 n1_4_3" style="display: none;">
						<input type="radio" class="mtl with-gap" onchange="get_nt()" name="n1_4" id="n1_4_3" value="3">
						<label for="n1_4_3">มีการเปลี่ยนแปลงมาก (3)</label>
					</div>
					<div class="col-sm-6 col-xs-12 n1_4_4" style="display: none;">
						<input type="radio" class="mtl with-gap" onchange="get_nt()" name="n1_4" id="n1_4_4" value="4">
						<label for="n1_4_4">ผิดปกติรุนแรง (4)</label>
					</div>
				</div>
				<div class="col-sm-2 col-xs-2 score score_get" id="n1_4_score"></div>
			</div>
		</div>
		<div class="header no-radius violet_1">
			<div class="col-sm-12 col-xs-12">2. การเปลี่ยนแปลงของน้ำหนักตัว</div>
			<script type="text/javascript">
				function checkn2_1(val) {
					$('#weight_default').val(val)
					var per = ($('#weight_default').val()*100/$('#weight_current').val())
					$('#percent').val(Math.abs(parseInt(per)))
					cal_score()
				}
				function cal_score() {
					var per = ($('#weight_default').val()*100/$('#weight_current').val())
					var time = $('#time_scope').val()*$('#time_number').val()
					if (per>0) {
						if (time<=7) { // 1 week
							if (per<1) {
								score = 1
							}else if(per>=1&&per<=2){
								score = 2
							}else if(per>2){
								score = 3
							}
						}else if (time<=7&&time<=21) { // 2-3 week
							if (per<2) {
								score = 1
							}else if(per>=2&&per<=3){
								score = 2
							}else if(per>3){
								score = 3
							}
						}else if (time>=22&&time<=89) { // 4 week & 1 month 
							if (per<4) {
								score = 1
							}else if(per>=4&&per<=5){
								score = 2
							}else if(per>5){
								score = 3
							}
						}else if (time>=90&&time<=150) { // 3 month
							if (per<7) {
								score = 1
							}else if(per>=7&&per<=8){
								score = 2
							}else if(per>8){
								score = 3
							}
						}else if (time>=151) { // > 5 month
							if (per<10) {
								score = 1
							}else if(per==10){
								score = 2
							}else if(per>10){
								score = 3
							}
						}
					}else{
						score = 0
					}
					$('.checkn2').html(score);
					get_nt()
				}
			</script>
		</div>
		<div class="body">
			<div class="col-sm-12 col-xs-12 form-group">
				<div class="col-sm-10 col-xs-10 question_assessment">
					<script type="text/javascript">
						$(document).ready(function() {
							$('input[name="n2_1"]').change(function(event) {
								var id = event.target.id
								console.log(id)
								if (id=="n2_1_2") {
									$('.n2_1_2 .weight_default').slideDown('fast');
									$('.n2_1_3 .weight_default').slideUp('fast');
									$('.n2_1_per').slideDown('fast');
									checkn2_1($('.n2_1_2 .weight_default input').val())
								}else if(id=="n2_1_3"){
									$('.n2_1_2 .weight_default').slideUp('fast');
									$('.n2_1_3 .weight_default').slideDown('fast');
									$('.n2_1_per').slideDown('fast');
									checkn2_1($('.n2_1_2 .weight_default input').val())
								}else{
									$('.n2_1_2 .weight_default').slideUp('fast');
									$('.n2_1_3 .weight_default').slideUp('fast');
									$('.n2_1_per').slideUp('fast');
									checkn2_1(0)
								}
							});
						});
						function checkminmax(obj,type,point,min,max) {
							if ($(obj).val()<min||$(obj).val()>max) {
								swal({
									title: "Warning",
									text: type+'ของผู้ป่วยไม่ควรต่ำกว่า '+min+' '+point+' หรือ มากกว่า '+max+' '+point,
									type: "warning",
								},function () {
									$(obj).parent().addClass('has-error');
									$(obj).val("");
									$(obj).focus();
									get_naf()
								})
							}else{
								$(obj).parent().removeClass('has-error');
							}
						}
					</script>
					<input type="hidden" name="weight_default" id="weight_default" class="form-control">
					<div class="col-sm-12 col-xs-12 n2_1_1">
						<input type="radio" class="mtl with-gap" no-get name="n2_1" id="n2_1_1">
						<label for="n2_1_1">เท่าเดิม</label>
					</div>
					<div class="col-sm-12 col-xs-12 n2_1_2 form-inline">
						<input type="radio" class="mtl with-gap" no-get name="n2_1" id="n2_1_2">
						<label for="n2_1_2">เพิ่มขึ้น</label>
						<span class="weight_default form-inline" style="display: none;">
							<input type="number" no-get onkeyup="checkn2_1(this.value)" onchange="checkminmax(this,'เพิ่มขึ้น','กิโลกรัม',0,100)" class="form-control"> กิโลกรัม
						</span>
					</div>
					<div class="col-sm-12 col-xs-12 n2_1_3 form-inline">
						<input type="radio" class="mtl with-gap" no-get name="n2_1" id="n2_1_3">
						<label for="n2_1_3">ลดลง</label>
						<span class="weight_default form-inline" style="display: none;">
							<input type="number" no-get onkeyup="checkn2_1(this.value)" onchange="checkminmax(this,'ลดลง','กิโลกรัม',0,100)" class="form-control"> กิโลกรัม
						</span>
					</div>
					<div class="col-sm-12 col-xs-12 form-inline n2_1_per" style="display: none;">
						<div class="form-group">
						 	คิดเป็น 
							<input type="text" name="percent" id="percent" class="form-control" readonly>
							<label class="sub-label" for="percent">%</label>
						</div>
						<div class="form-group">
							<label> ใน </label>
						</div>
						<div class="form-group">
							<input type="number" name="time_number" id="time_number" no-get onkeyup="cal_score()" class="form-control" value="1">
							<select class="form-control" name="time_scope" id="time_scope" onchange="cal_score()">
								<option value="7">สัปดาห์</option>
								<option value="30">เดือน</option>
							</select>
						</div>
					</div>
				</div>
				<div class="col-sm-2 col-xs-2 score checkn2 score_get" id="n2_1_score"></div>
			</div>
		</div>
		<div class="header no-radius violet_1">
			<div class="col-sm-12 col-xs-12">3. ภาวะบวมน้ำ (Fluid accumulation)</div>
		</div>
		<div class="body">
			<div class="col-sm-12 col-xs-12 form-group">
				<div class="col-sm-10 col-xs-10 question_assessment">
					<div class="col-sm-12 col-xs-12">
						<input type="radio" class="mtl with-gap" onchange="get_nt()" name="n3_1" id="n3_1" value="0">
						<label for="n3_1">ไม่บวม (มือ-แขน ทั้ง 2 ข้าง-หน้าอก-ลำตัว-ท้อง-ขา ทั้ง 2 ข้าง) (0)</label>
					</div>
					<div class="col-sm-12 col-xs-12">
						<input type="radio" class="mtl with-gap" onchange="get_nt()" name="n3_1" id="n3_2" value="1">
						<label for="n3_2">บวมเล็กน้อย : บางแห่ง: ระดับ1<sup>+</sup>-2<sup>+</sup> (รอยบุ๋มลึก 2-4 มม.) (1)</label>
					</div>
					<div class="col-sm-12 col-xs-12">
						<input type="radio" class="mtl with-gap" onchange="get_nt()" name="n3_1" id="n3_3" value="2">
						<label for="n3_3">บวมปานกลาง : มือ-แขน หรือ ขาทั้งสองข้าง ระดับ 2<sup>+</sup>-3<sup>+</sup> (2)</label>
					</div>
					<div class="col-sm-12 col-xs-12">
						<input type="radio" class="mtl with-gap" onchange="get_nt()" name="n3_1" id="n3_4" value="3">
						<label for="n3_4">บวมทั่วตัว : ระดับ 3<sup>+</sup>-4<sup>+</sup> (รอยบุ๋มลึก 6-8 มม.) (3)</label>
					</div>
				</div>
				<div class="col-sm-2 col-xs-2 score"></div>
			</div>
		</div>
		<div class="header no-radius violet_1">
			<div class="col-sm-12 col-xs-12">4. ระดับการสูญเสีย มวลไขมัน (Body fat loss) ประเมินเฉลี่ยทั่วร่างกาย</div>
		</div>
		<div class="body">
			<div class="col-sm-12 col-xs-12 form-group">
				<div class="col-sm-10 col-xs-10 question_assessment">
					<div class="col-sm-6 col-xs-12">
						<input type="radio" class="mtl with-gap" onchange="get_nt()" name="n4_1" id="n4_1" value="0">
						<label for="n4_1">ปกติ (0)</label>
					</div>
					<div class="col-sm-6 col-xs-12">
						<input type="radio" class="mtl with-gap" onchange="get_nt()" name="n4_1" id="n4_2" value="1">
						<label for="n4_2">มีไขมันน้อย (1)</label>
					</div>
					<div class="col-sm-6 col-xs-12">
						<input type="radio" class="mtl with-gap" onchange="get_nt()" name="n4_1" id="n4_3" value="2">
						<label for="n4_3">มีไขมันน้อยมาก (2)</label>
					</div>
					<div class="col-sm-6 col-xs-12">
						<input type="radio" class="mtl with-gap" onchange="get_nt()" name="n4_1" id="n4_4" value="3">
						<label for="n4_4">หนังหุ้มกระดูก (3)</label>
					</div>
				</div>
				<div class="col-sm-2 col-xs-2 score"></div>
			</div>
		</div>
		<div class="header no-radius violet_1">
			<div class="col-sm-12 col-xs-12">5. ระดับการสูญเสีย มวลกล้ามเนื้อ (Muscle loss) ประเมินเฉลี่ยทั่วร่างกาย</div>
		</div>
		<div class="body">
			<div class="col-sm-12 col-xs-12 form-group">
				<div class="col-sm-10 col-xs-10 question_assessment">
					<div class="col-sm-6 col-xs-12">
						<input type="radio" class="mtl with-gap" onchange="get_nt()" name="n5_1" id="n5_1" value="0">
						<label for="n5_1">ปกติ (0)</label>
					</div>
					<div class="col-sm-6 col-xs-12">
						<input type="radio" class="mtl with-gap" onchange="get_nt()" name="n5_1" id="n5_2" value="1">
						<label for="n5_2">กล้ามเนื้อน้อยลง (1)</label>
					</div>
					<div class="col-sm-6 col-xs-12">
						<input type="radio" class="mtl with-gap" onchange="get_nt()" name="n5_1" id="n5_3" value="2">
						<label for="n5_3">กล้ามเนื้อลีบ (2)</label>
					</div>
					<div class="col-sm-6 col-xs-12">
						<input type="radio" class="mtl with-gap" onchange="get_nt()" name="n5_1" id="n5_4" value="3">
						<label for="n5_4">หนังหุ้มกระดูก (3)</label>
					</div>
				</div>
				<div class="col-sm-2 col-xs-2 score"></div>
			</div>
		</div>
		<div class="header no-radius violet_1">
			<div class="col-sm-12 col-xs-12">6. สมรรถภาพกล้ามเนื้อ (ประเมินเฉลี่ยทั่วร่างกาย)</div>
		</div>
		<div class="body">
			<div class="col-sm-12 col-xs-12 form-group">
				<div class="col-sm-10 col-xs-10 question_assessment">
					<div class="col-sm-12 col-xs-12">
						<input type="radio" class="mtl with-gap" onchange="get_nt()" name="n6_1" id="n6_1" value="3">
						<label for="n6_1">ไม่สามารถเคลื่อนไหวได้เลย (อัมพาต) (3)</label>
					</div>
					<div class="col-sm-12 col-xs-12">
						<input type="radio" class="mtl with-gap" onchange="get_nt()" name="n6_1" id="n6_2" value="2">
						<label for="n6_2">สามารถเคลื่อนได้เล็กน้อย แต่เฉพาะในแนว Horizontal (2)</label>
					</div>
					<div class="col-sm-12 col-xs-12">
						<input type="radio" class="mtl with-gap" onchange="get_nt()" name="n6_1" id="n6_3" value="1">
						<label for="n6_3">สามารถเคลื่อน ต้านแรงดึงดูดของโลกในแนว Vertical ได้ (1)</label>
					</div>
					<div class="col-sm-12 col-xs-12">
						<input type="radio" class="mtl with-gap" onchange="get_nt()" name="n6_1" id="n6_4" value="1">
						<label for="n6_4">สามารถเคลื่อนตามแนว Vertical และต้านแรงผู้ตรวจ พอได้ (1)</label>
					</div>
					<div class="col-sm-12 col-xs-12">
						<input type="radio" class="mtl with-gap" onchange="get_nt()" name="n6_1" id="n6_5" value="0">
						<label for="n6_5">มีอาการอ่อนแรงเพียงเล็กน้อย (0)</label>
					</div>
					<div class="col-sm-12 col-xs-12">
						<input type="radio" class="mtl with-gap" onchange="get_nt()" name="n6_1" id="n6_6" value="0">
						<label for="n6_6">ปกติ (0)</label>
					</div>
				</div>
				<div class="col-sm-2 col-xs-2 score score_get" id="n6_1_score"></div>
			</div>
		</div>
		<div class="header no-radius violet_1">
			<div class="col-sm-12 col-xs-12">7. ประเมินความรุนแรงของภาวะเจ็บป่วยเรื้อรัง (มากกว่า 3 เดือน) ที่มีผลกระทบ ต่อ ภาวะโภชนาการ และเมตาบอลิซึม</div>
			<div class="col-sm-12 hidden-xs">(ระดับคะแนน 0=มีโรคแต่คุมได้ดี, 1=เล็กน้อย, 2=ปานกลาง และ 3=รุนแรง)</div>
		</div>
		<div class="body hidden-xs">
			<div class="col-sm-12 col-xs-12 form-group">
				<div class="col-sm-10 col-xs-10 question_assessment">
					<div class="col-sm-8">ระดับผลกระทบต่อภาวะโภชนาการ</div>
					<div class="col-sm-1 text-center">0</div>
					<div class="col-sm-1 text-center">1</div>
					<div class="col-sm-1 text-center">2</div>
					<div class="col-sm-1 text-center">3</div>
				</div>
				<div class="col-sm-2 col-xs-2 score n7_score fs-inherit"></div>
			</div>
		</div>
		<?php
			$n7 = array(
				'7.1 โรคมะเร็ง (Stage I=0, II=1, III=2, IV=3)',
				'7.2 โรคปอด (TB,COPD,...)',
				'7.3 โรคไต (ไตวายเรื้อรังแต่ยังมีปัสสาวะ=2, HD/PD=3)',
				'7.4 โรคตับ (Hepatic Encephalopathy=3)',
				'7.5 HIV (มีอาการ+นน.ลด &#8804; 10% = 1-2, นน.ลด > 10% + wasting = 3)',
				'7.6 โรค/สภาวะอื่นๆ (eg.Short bowel,...)',
				'7.7 ท้องมาน (น้ำประมาณระดับสะดือ=2, เต็มท้อง=3)',
				'7.8 แผลกดทับ (พิจารณาความกว้าง+ลึก ถึงไขมัน=2, กล้ามเนื้อ=3)',
				'7.9 แผลเรื้อรังอื่นๆ (...)'
			);
			$i=0; 
			foreach ($n7 as $key => $value): 
				$i++; 
		?>
		<div class="body">
			<div class="col-sm-12 col-xs-12 form-group">
				<script type="text/javascript">
					function show_radio(obj,id,type) {
						if ($('#'+id).prop('checked')) {
							$('.'+id).show('fast');
						}else{
							$('.'+id).hide('fast');
							$('#'+id+'_0').prop('checked',false);
							$('#'+id+'_1').prop('checked',false);
							$('#'+id+'_2').prop('checked',false);
							$('#'+id+'_3').prop('checked',false);
							delete qwe[id];
							$(obj).closest('.body').find('.score').html('');
						}
						set_score_n(type)
					}
					function set_score_n(n) {
						setTimeout(function() {
							var score = 0;
							var score_count = $('.score_count_'+n)
							for (var i = 0; i < score_count.length; i++) {
								score += $(score_count[i])[0].innerText!="" ? parseInt($(score_count[i]).text()) : 0;
							}
							score = score>3 ? 3 : score;
							$('.'+n+'_score').html(score)

							//sum and set result all
							score_all = $('.body .score')
							sum_all = 0
							check_all_show = 0
							for (var i = 0; i < score_all.length; i++) {
								if ($(score_all[i]).attr('noscore')==undefined) {
									sum_all += $(score_all[i])[0].innerText!="" ? parseInt($(score_all[i]).text()) : 0;
									check_all_show += $(score_all[i])[0].innerText!="" ? 1 : 0;
								}
							}
							$('.result_all').html(sum_all)

							if ($('.table-naf').length>0) {
								set_assessment_result('naf')
							}else if ($('.table-nt').length>0) {
								set_assessment_result('nt')
							}
						}, 300);
						
					}
				</script>
				<div class="col-sm-10 col-xs-10 question_assessment radio-grid">
					<div class="col-sm-8"><input type="checkbox" value="0" class="mtl filled-in get_noset" data-name="n7_<?=$i?>" onchange="show_radio(this,'n7_<?=$i?>','n7');get_nt()" id="n7_<?=$i?>" no-get><label for="n7_<?=$i?>"><?=$value?></label></div>
					<div class="col-sm-1 n7_<?=$i?>" style="display: none;">
						<input type="radio" class="mtl with-gap" onchange="get_nt();set_score_n('n7')" name="n7_<?=$i?>" id="n7_<?=$i?>_0" value="0">
						<label for="n7_<?=$i?>_0">
							<span class="hidden-xs">0</span>
							<span class="visible-xs">มีโรคแต่คุมได้ดี</span>
						</label>
					</div>
					<div class="col-sm-1 n7_<?=$i?>" style="display: none;">
						<input type="radio" class="mtl with-gap" onchange="get_nt();set_score_n('n7')" name="n7_<?=$i?>" id="n7_<?=$i?>_1" value="1">
						<label for="n7_<?=$i?>_1">
							<span class="hidden-xs">1</span>
							<span class="visible-xs">เล็กน้อย</span>
						</label>
					</div>
					<div class="col-sm-1 n7_<?=$i?>" style="display: none;"><input type="radio" class="mtl with-gap" onchange="get_nt();set_score_n('n7')" name="n7_<?=$i?>" id="n7_<?=$i?>_2" value="2">
						<label for="n7_<?=$i?>_2">
							<span class="hidden-xs">2</span>
							<span class="visible-xs">ปานกลาง</span>
						</label>
					</div>
					<div class="col-sm-1 n7_<?=$i?>" style="display: none;">
						<input type="radio" class="mtl with-gap" onchange="get_nt();set_score_n('n7')" name="n7_<?=$i?>" id="n7_<?=$i?>_3" value="3">
						<label for="n7_<?=$i?>_3">
							<span class="hidden-xs">3</span>
							<span class="visible-xs">รุนแรง</span>
						</label>
					</div>
				</div>
				<div class="col-sm-2 col-xs-2 score score_count_n7 fs-inherit" noscore></div>
			</div>
		</div>
		<?php endforeach ?>
		<div class="body grey_1">
			<div class="col-sm-12 col-xs-12">
				หมายเหตุ : ให้คะแนนแต่ละภาวะ แล้วรวมคะแนน แต่ผลรวมสุดท้าย ไม่เกิน3
			</div>
		</div>

		<div class="header no-radius violet_1">
			<div class="col-sm-12 col-xs-12">8. ประเมินความรุนแรงของ ภาวะเจ็บป่วย เฉียบพลัน หรือ กึ่งเฉียบพลัน</div>
			<div class="col-sm-12 hidden-xs">(ระดับคะแนน 0=ไม่มี, 1=เล็กน้อย, 2=ปานกลาง และ 3=รุนแรง)</div>
		</div>
		<div class="body hidden-xs">
			<div class="col-sm-12 col-xs-12 form-group">
				<div class="col-sm-10 col-xs-10 question_assessment">
					<div class="col-sm-8">ระดับผลกระทบต่อภาวะโภชนาการ</div>
					<div class="col-sm-1 text-center">0</div>
					<div class="col-sm-1 text-center">1</div>
					<div class="col-sm-1 text-center">2</div>
					<div class="col-sm-1 text-center">3</div>
				</div>
				<div class="col-sm-2 col-xs-2 score n8_score fs-inherit"></div>
			</div>
		</div>
		<?php
			$n8 = array(
				'8.1 Non neurological trauma',
				'8.2 Head injury, Acute spine injury (GCS 15=0, 14-13=1, 12-8=2, 7-3=3)',
				'8.3 Burn (minor:ตื้น<15%, mod.: ลึก > 5%, major: ตื้น > 20%/ลึก >10%)',
				'8.4 Sepsis (sepsis=1, severe sepsis=2, septic shock=3)',
				'8.5 Recent major operation (1-2 wk.)',
				'8.6 Acute pancreatitis, Hepatitis, Peritonitis, Necrotizing fasciitis ',
				'8.7 โรคอื่นๆ (eg. MI, GI bleed, shock, servere diarrhea, EC-fistula)'
			);
			$i=0; 
			foreach ($n8 as $key => $value): 
				$i++; 
		?>
		<div class="body">
			<div class="col-sm-12 col-xs-12 form-group">
				<div class="col-sm-10 col-xs-10 question_assessment">
					<div class="col-sm-8">
						<input type="checkbox" value="0" class="mtl filled-in get_noset" data-name="n8_<?=$i?>" onchange="show_radio(this,'n8_<?=$i?>','n8');get_nt()" id="n8_<?=$i?>" no-get>
						<label for="n8_<?=$i?>"><?=$value?></label></div>
					<div class="col-sm-1 n8_<?=$i?>" style="display: none;">
						<input type="radio" class="mtl with-gap" onchange="get_nt();set_score_n('n8')" name="n8_<?=$i?>" id="n8_<?=$i?>_0" value="0">
						<label for="n8_<?=$i?>_0">
							<span class="hidden-xs">0</span>
							<span class="visible-xs">ไม่มีผลกระทบ</span>
						</label>
					</div>
					<div class="col-sm-1 n8_<?=$i?>" style="display: none;"><input type="radio" class="mtl with-gap" onchange="get_nt();set_score_n('n8')" name="n8_<?=$i?>" id="n8_<?=$i?>_1" value="1">
						<label for="n8_<?=$i?>_1">
							<span class="hidden-xs">1</span>
							<span class="visible-xs">เล็กน้อย</span>
						</label>
					</div>
					<div class="col-sm-1 n8_<?=$i?>" style="display: none;">
						<input type="radio" class="mtl with-gap" onchange="get_nt();set_score_n('n8')" name="n8_<?=$i?>" id="n8_<?=$i?>_2" value="2">
						<label for="n8_<?=$i?>_2">
							<span class="hidden-xs">2</span>
							<span class="visible-xs">ปานกลาง</span>
						</label>
					</div>
					<div class="col-sm-1 n8_<?=$i?>" style="display: none;">
						<input type="radio" class="mtl with-gap" onchange="get_nt();set_score_n('n8')" name="n8_<?=$i?>" id="n8_<?=$i?>_3" value="3">
						<label for="n8_<?=$i?>_3">
							<span class="hidden-xs">3</span>
							<span class="visible-xs">รุนแรง</span>
						</label>
					</div>
				</div>
				<div class="col-sm-2 col-xs-2 score score_count_n8 fs-inherit" noscore></div>
			</div>
		</div>
		<?php endforeach ?>
		<div class="body grey_1">
			<div class="col-sm-12 col-xs-12">
				หมายเหตุ : ให้คะแนนแต่ละภาวะ แล้วรวมคะแนน แต่ผลรวมสุดท้าย ไม่เกิน3
			</div>
		</div>

		<div class="btn-bar assessment-warning">
			<div class="col-sm-12 col-xs-12 header">คุณยังตอบคำถามไม่ครบ</div>
			<div class="list-required"></div>
		</div>

		<div class="header no-radius grey_2 assessment-result assessment-complete" style="display: none;">
			<div class="col-sm-4 col-xs-12">ผลการประเมิน</div>
			<div class="col-sm-4 col-xs-6"><label class="red_1"><span class="result_all"></span> คะแนน</label></div>
			<div class="col-sm-4 col-xs-6"><label class="red_1 assessment-result-grade"></label></div>
		</div>
		
		<div class="btn-bar assessment-complete" style="display: none;">
			<h3 class="assessment-result-description"></h3>
			<h1 class="assessment-result-time"></h1>
			<button class="btn btn_big btn-color violet" onclick="set_todb('screening/fn_create_nt','patients/lists/','nt')">บันทึกผลการประเมิน</button>
			<h3>หรือบันทึกผลการประเมินและทำการประเมิน NAF ด้วย</h3>
			<button class="btn btn_big btn-color pink" onclick="set_todb('screening/fn_create_nt','screening/create_naf','nt')">Assessment NAF</button>
		</div>
	</div>
