<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
<head>
	<title>Main</title>
	<?php $this->load->view('_config'); ?>
</head>
<body>
	<div class="container login">
		<div class="row">
			<div class="logo col-xs-6">
				<img src="<?=base_url()?>../images/logo-login.png">
			</div>
			<form action="<?php echo base_url(); ?>login/fn_login" method="POST" class="col-xs-6 form" autocomplete>
				<div class="row">
					<div class="col-xs-12 form-group subtitle">
						Login
					</div>
					<div class="col-xs-12 form-group">
						<input type="text" name="code" id="code" class="form-control" placeholder="Enter Hospital Code or Email" value="<?=$code?>">
					</div>
					<div class="col-xs-12 form-group member_code" style="display: none;">
						<input type="text" name="member_code" id="member_code" class="form-control" placeholder="Enter SPENT's member code" value="<?=$member_code?>">
					</div>
					<div class="col-xs-12 form-group">
						<input type="checkbox" class="mtl filled-in" name="set_cookie" id="set_cookie">
						<label for="set_cookie" style="font-size: 18px;">จดจำรหัสผ่าน</label>
					</div>
					<div class="col-xs-12 form-group">
						<button class="btn col-xs-12 btn-color">Submit</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</body>
<script type="text/javascript">
	$(document).ready(function() {
		$('#code').keyup(function(event) {
			if (validateEmail($('#code').val())) {
				$('.member_code').slideDown('fast');
			}else{
				$('.member_code').slideUp('fast');
			}
		});
		$('#code').change(function(event) {
			if (validateEmail($('#code').val())) {
				$('.member_code').slideDown('fast');
			}else{
				$('.member_code').slideUp('fast');
			}
		});
	});
	function validateEmail(email) {
		var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test(email);
	}
</script>
</html>