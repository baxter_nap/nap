<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
<head>
	<title>Main</title>
	<?php $this->load->view('_config'); ?>
</head>
<body>
	<?php $this->load->view('_header'); ?>
	<div class="container report">
		<div class="row">
			<?php $this->load->view('_left_bar.php'); ?>
			<div class="col-sm-10 col-xs-12 content">
				<div class="row green_2 top-bar">
					<div class="col-xs-10 back"></div>
					<div class="col-xs-2 score"></div>
				</div>
				<div class="row">
					<div class="col-xs-8 title">Report</div>
					<div class="col-xs-4 btn-bar"></div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<div class="report_box">
							<div class="col-xs-8">
								<h2 class="head">Screening</h2>
							</div>
							<div class="col-xs-4 form-inline period_box">
								<h3 class="head">
									Period : 
									<select class="form-control period" type="screening">
										<?php for ($i=2017; $i < 2027; $i++) { ?>
										<option value="<?=$i?>"><?=$i?></option>
										<?php } ?>
									</select>
								</h3>
							</div>
							<div class="col-xs-12">
								<h3 class="title">คนไข้ทั้งหมด <span id="screening_case">0</span> คน</h3>
								<canvas id="chart_screening"></canvas>
							</div>
							<div class="col-xs-6 text-center">
								<div class="label_text"><i class="fa fa-circle green_3"></i> คนไข้ที่ไม่ต้องทำ Assessment</div>
							</div>
							<div class="col-xs-6 text-center">
								<div class="label_text"><i class="fa fa-circle red_1"></i> คนไข้ที่ต้องทำ Assessment</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<div class="report_box">
							<div class="col-xs-8">
								<h2 class="head">Assessment : NAF</h2>
							</div>
							<div class="col-xs-4 form-inline period_box">
								<h3 class="head">
									Period : 
									<select class="form-control period" type="naf">
										<?php for ($i=2017; $i < 2027; $i++) { ?>
										<option value="<?=$i?>"><?=$i?></option>
										<?php } ?>
									</select>
								</h3>
							</div>
							<div class="col-xs-12">
								<h3 class="title">คนไข้ทั้งหมด <span id="naf_case">0</span> คน</h3>
								<canvas id="chart_naf"></canvas>
							</div>
							<div class="col-xs-4 text-center">
								<div class="label_text"><i class="fa fa-circle green_3"></i> NAF-A</div>
							</div>
							<div class="col-xs-4 text-center">
								<div class="label_text"><i class="fa fa-circle" style="color: #f39c12"></i> NAF-B</div>
							</div>
							<div class="col-xs-4 text-center">
								<div class="label_text"><i class="fa fa-circle" style="color: #e74c3c"></i> NAF-C</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<div class="report_box">
							<div class="col-xs-8">
								<h2 class="head">Assessment : NT</h2>
							</div>
							<div class="col-xs-4 form-inline period_box">
								<h3 class="head">
									Period : 
									<select class="form-control period" type="nt">
										<?php for ($i=2017; $i < 2027; $i++) { ?>
										<option value="<?=$i?>"><?=$i?></option>
										<?php } ?>
									</select>
								</h3>
							</div>
							<div class="col-xs-12">
								<h3 class="title">คนไข้ทั้งหมด <span id="nt_case">0</span> คน</h3>
								<canvas id="chart_nt"></canvas>
							</div>
							<div class="col-xs-3 text-center">
								<div class="label_text"><i class="fa fa-circle green_3"></i> NT-1</div>
							</div>
							<div class="col-xs-3 text-center">
								<div class="label_text"><i class="fa fa-circle" style="color: #176920"></i> NT-2</div>
							</div>
							<div class="col-xs-3 text-center">
								<div class="label_text"><i class="fa fa-circle" style="color: #f39c12"></i> NT-3</div>
							</div>
							<div class="col-xs-3 text-center">
								<div class="label_text"><i class="fa fa-circle" style="color: #e74c3c"></i> NT-4</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		function chart_screening(data) {
			var screening_ctx = $("#chart_screening");
			myDoughnutChart_screening = new Chart(screening_ctx, {
			    type: 'line',
			    data: data,
			   	options: options
			});
		}
		function chart_naf(data) {
			var chart_naf_ctx = $("#chart_naf");
			myDoughnutChart_chart_naf = new Chart(chart_naf_ctx, {
			    type: 'line',
			    data: data,
			   	options: options
			});
		}
		function chart_nt(data) {
			var chart_nt_ctx = $("#chart_nt");
			myDoughnutChart_chart_nt = new Chart(chart_nt_ctx, {
			    type: 'line',
			    data: data,
			   	options: options
			});
		}
		$(document).ready(function() {
			options = {
		        elements: {
		            line: {
		                tension: 0.1, 
		            }
		        },
		        legend: {
		            display: false,
		        },
		        scales: {
		            yAxes: [{
		            	scaleLabel: {
			            	display: true,
			            	labelString: 'จำนวนคนไข้สะสม',
			            }
		            }]
		        }
		    }
			report_data('2017','screening')
			report_data('2017','naf')
			report_data('2017','nt')
			function report_data(year,type) {
				$.post(base_url+'report/report_data/', {year: year,type: type}, function(data, textStatus, xhr) {
					var get_data = JSON.parse(data)
					console.log(get_data)
					var type = JSON.parse(data).type
					if (type=='screening') {
						set_data = {
							datasets: [{
					        	data: get_data.assessment,
					        	backgroundColor: get_data.assessment_color,
					        	borderColor: get_data.assessment_color,
					        	fill: false
					        },{
					        	data: get_data.screening,
					        	backgroundColor: get_data.screening_color,
					        	borderColor: get_data.screening_color,
					        	fill: false
					        },
					    	],
					    	labels: get_data.labels
					   	},
					   	$('#screening_case').html(get_data.all)
					    chart_screening(set_data)
					}else if (type=='naf') {
						set_data = {
							datasets: [{
					        	data: get_data.NAF_A,
					        	backgroundColor: get_data.NAF_A_color,
					        	borderColor: get_data.NAF_A_color,
					        	fill: false
					        },{
					        	data: get_data.NAF_B,
					        	backgroundColor: get_data.NAF_B_color,
					        	borderColor: get_data.NAF_B_color,
					        	fill: false
					        },{
					        	data: get_data.NAF_C,
					        	backgroundColor: get_data.NAF_C_color,
					        	borderColor: get_data.NAF_C_color,
					        	fill: false
					        }
					    	],
					    	labels: get_data.labels
					   	},
					   	$('#naf_case').html(get_data.all)
					    chart_naf(set_data)
					}else if (type=='nt') {
						set_data = {
							datasets: [{
					        	data: get_data.NT_1,
					        	backgroundColor: get_data.NT_1_color,
					        	borderColor: get_data.NT_1_color,
					        	fill: false
					        },{
					        	data: get_data.NT_2,
					        	backgroundColor: get_data.NT_2_color,
					        	borderColor: get_data.NT_2_color,
					        	fill: false
					        },{
					        	data: get_data.NT_3,
					        	backgroundColor: get_data.NT_3_color,
					        	borderColor: get_data.NT_3_color,
					        	fill: false
					        },{
					        	data: get_data.NT_4,
					        	backgroundColor: get_data.NT_4_color,
					        	borderColor: get_data.NT_4_color,
					        	fill: false
					        }
					    	],
					    	labels: get_data.labels
					   	},
					   	$('#nt_case').html(get_data.all)
					    chart_nt(set_data)
					}
				});
			}
			$('.period').change(function(event) {
				report_data($(this).val(),$(this).attr('type'))
			});
		});
	</script>
</body>
</html>