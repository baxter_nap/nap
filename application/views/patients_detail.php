<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
<head>
	<title>Main</title>
	<?php $this->load->view('_config'); ?>
</head>
<body>
	<?php $this->load->view('_header'); ?>
	<div class="container patient">
		<div class="row">
			<?php $this->load->view('_left_bar.php'); ?>
			<div class="col-sm-10 col-xs-12 content">
				<div class="row green_2 top-bar">
					<div class="col-md-10 col-xs-6 back"></div>
					<div class="col-md-2 col-xs-6 score"></div>
				</div>
				<div class="row">
					<div class="col-md-8 col-xs-12 title"><a href="<?=base_url()?>patients/" style="color: #32439f;">เลือกหอผู้ป่วย</a> > <a href="<?=base_url()?>/patients/lists/<?=$data['screening'][0]['patient_ward_id']?>" style="color: #32439f;"><?=$data['screening'][0]['patient_ward_title']?></a> > <?=$data['screening'][0]['hn_code']?></div>
					<div class="col-md-4 col-xs-12 btn-bar"></div>
				</div>
				<div class="row">
					<div class="col-xs-12 assessment-box">
						<div class="row box">
							<div class="header green_2">
								<div class="col-sm-6 col-xs-12">ข้อมูลผู้ป่วย</div>
							</div>
							<?php $max = count($data['screening'])-1; ?>
							<div class="body">
								<div class="col-sm-12 col-xs-12">
									<div class="col-sm-4 col-xs-12 form-group">
										<label>HN</label>
										<div class="detail"><?=$data['screening'][$max]['hn_code']?></div>
									</div>
									<div class="col-sm-4 col-xs-12 form-group">
										<label>ชื่อผู้ป่วย</label>
										<div class="detail"><?=$data['screening'][$max]['name']?></div>
									</div>
									<div class="col-sm-4 col-xs-12 form-group">
										<label>การวินิจฉัยโรค</label>
										<div class="detail"><?php echo $data['screening'][$max]['diagnose']!="" ? $data['screening'][$max]['diagnose'] : '-' ;?></div>
									</div>
									<div class="col-sm-4 col-xs-12 form-group">
										<label>วันที่เข้ารับการรักษา</label>
										<div class="detail"><?=date_format(date_create($data['screening'][$max]['date_admission']),'d/m/Y')?></div>
									</div>
									<div class="col-sm-4 col-xs-12 form-group">
										<label>วันที่ต้องออกจากโรงพยาบาล</label>
										<div class="detail">ยังไม่มีข้อมูล</div>
									</div>
									<div class="col-sm-4 col-xs-12 form-group">
										<label>จำนวนวันที่ได้รับการรักษา</label>
										<div class="detail"><?=$date_passing?> วัน</div>
									</div>
								</div>
							</div>

							<div class="row box">
								<div class="header green_2">
									<div class="col-sm-6 col-xs-12">การประเมินที่ต้องทำ</div>
								</div>
								<div class="body">
									<div class="col-sm-12 col-xs-12">
										<ul class="list-group">
											<li class="list-group-item header-list">
												<div class="row">
													<div class="col-xs-5">รายการที่ต้องการ</div>
													<div class="col-xs-4">ตารางนัดหมาย</div>
													<div class="col-xs-3">เครื่องมือ</div>
												</div>
											</li>
										<?php if (isset($data['screening'])) { ?>
										<li class="list-group-item">
											<a href="<?=base_url()?>screening/create/<?=$data['screening'][0]['screening_id']?>/<?=count($data['screening'])+1?>" class="link">
												<div class="row blue_1">
													<div class="col-xs-5">Screening ครั้งที่ <?=count($data['screening'])+1?></div>
													<div class="col-xs-4"><?=date_format(date_create($data['screening'][0]['date_next']),'d/m/Y')?></div>
													<div class="col-xs-3">ทำแบบประเมิน</div>
												</div>
											</a>
										</li>
										<?php } ?>
										<?php if (isset($data['assessment_naf'])) { ?>
										<li class="list-group-item">
										<?php if ($data['assessment_naf'][0]['grade']=="NAF-C"&&$data['assessment_naf'][0]['status']==0) { ?>
											<a  class="link" onclick="check_warning('assessment_naf',<?=$data['assessment_naf'][0]['id']?>)">
										<?php }else{ ?>
											<a href="<?=base_url()?>screening/create_naf/<?=$data['assessment_naf'][0]['screening_id']?>/<?=count($data['assessment_naf'])+1?>" class="link">
										<?php } ?>
												<div class="row pink_1">
													<div class="col-xs-5">Assessment NAF ครั้งที่ <?=count($data['assessment_naf'])+1?></div>
													<div class="col-xs-4">
													<?php
														if ($data['assessment_naf'][0]['grade']=="NAF-C"&&$data['assessment_naf'][0]['status']==0) {
															echo '<span class="label_danger"><i class="fa fa-exclamation-triangle"></i> รอพบแพทย์โภชนาการ</span>';
														}else{
															echo date_format(date_create($data['assessment_naf'][0]['date_next']),'d/m/Y');
														}
													?>
													</div>
													<div class="col-xs-3">ทำแบบประเมิน</div>
												</div>
											</a>
										</li>
										<?php } ?>
										<?php if (isset($data['assessment_nt'])) { ?>
										<li class="list-group-item">
										<?php if ($data['assessment_nt'][0]['grade']=="NT-4"&&$data['assessment_nt'][0]['status']==0) { ?>
											<a  class="link" onclick="check_warning('assessment_nt',<?=$data['assessment_nt'][0]['id']?>)">
										<?php }else{ ?>
											<a href="<?=base_url()?>screening/create_nt/<?=$data['assessment_nt'][0]['screening_id']?>/<?=count($data['assessment_nt'])+1?>" class="link">
										<?php } ?>
												<div class="row violet_1">
													<div class="col-xs-5">Assessment NT ครั้งที่ <?=count($data['assessment_nt'])+1?></div>
													<div class="col-xs-4">
													<?php
														if ($data['assessment_nt'][0]['grade']=="NT-4"&&$data['assessment_nt'][0]['status']==0) {
															echo '<span class="label_danger"><i class="fa fa-exclamation-triangle"></i> รอพบแพทย์โภชนาการ</span>';
														}else{
															echo date_format(date_create($data['assessment_nt'][0]['date_next']),'d/m/Y');
														}
													?>
													</div>
													<div class="col-xs-3">ทำแบบประเมิน</div>
												</div>
											</a>
										</li>
										<?php } ?>
										</ul>
									</div>
								</div>
							</div>

							<div class="row box">
								<div class="header green_2">
									<div class="col-sm-6 col-xs-6">ผลการประเมินที่ผ่านมา</div>
									<div class="col-sm-6 col-xs-6 text-right"><!-- <i class="fa fa-print"></i> Print / Export --></div>
								</div>
								<div class="body">
									<div class="col-sm-12 col-xs-12">
										<ul class="nav nav-tabs" id="myTabs" role="tablist">
											<li style="width: 33%;" class="text-center active" role="presentation" aria-controls="screening" role="tab" data-toggle="tab"><a href="#screening" class="link blue_1">Screening Results</a></li>
											<li style="width: 33%;" class="text-center" role="presentation" aria-controls="naf" role="tab" data-toggle="tab"><a href="#naf" class="link pink_1">Assessment : NAF</a></li>
											<li style="width: 33%;" class="text-center" role="presentation" aria-controls="nt" role="tab" data-toggle="tab"><a href="#nt" class="link violet_1">Assessment : NT</a></li>
										</ul>
										<div class="tab-content">
											<div role="tabpanel" class="tab-pane active" id="screening">
												<?php $screening_count = count($data_old['screening']); ?>
												<h3>Screening</h3>
												<p>Detail : </p>
												<ul class="list-group">
													<li class="list-group-item header-list blue_1">
														<div class="row">
															<div class="col-sm-1 col-xs-1">#</div>
															<div class="col-sm-4 col-xs-5">ผลการประเมิน</div>
															<div class="col-sm-4 col-xs-5">วันที่ประเมิน</div>
															<div class="col-sm-2 hidden-xs">เครื่องมือ</div>
														</div>
													</li>
												<?php $i = 1; 
													for ($j=count($data_old['screening'])-1; $j >= 0; $j--) { ?>
													<li class="list-group-item">
													<a onclick="new_tabs('<?=base_url()?>screening/export_screening/<?=$data_old['screening'][$j]['screening_id']?>/-1/<?=$i?>')" class="link">
														<div class="row">
															<div class="col-sm-1 col-xs-1"><?=$i?></div>
															<div class="col-sm-4 col-xs-5">
															<?php
																echo $data_old['screening'][$j]['c_1']+$data_old['screening'][$j]['c_2']+$data_old['screening'][$j]['c_3']+$data_old['screening'][$j]['c_4']>=2 ? '<label class="red_1" style="margin-bottom: 0px">Assessment</label>' : '<label class="green_3" style="margin-bottom: 0px">Screening</label>';
															?>
															</div>
															<div class="col-sm-4 col-xs-5"><?php echo date_format(date_create($data_old['screening'][$j]['date']),'d/m/Y'); ?></div>
															<div class="col-sm-2 hidden-xs">กดเพื่อดู</div>
														</div>
													</a>
													</li>
												<?php $i++; } ?>
												</ul>
											</div>
											<div role="tabpanel" class="tab-pane" id="naf">
												<?php 
													$assessment_naf_count = isset($data_old['assessment_naf']) ? count($data_old['assessment_naf']) : '0';
													$first_naf = "";
													if (isset($data_old['assessment_naf'][0]['grade'])) {
														if ($data['screening'][$max]['bmi']>=17&&$data['screening'][$max]['bmi']<18.50&&$data_old['assessment_naf'][0]['grade']=="NAF-A") {
															$first_naf = "E44.1 - Mild malnutrition (Mild protein - calorie malnutrition)";
														}else if ($data['screening'][$max]['bmi']>=16&&$data['screening'][$max]['bmi']<17&&$data_old['assessment_naf'][0]['grade']=="NAF-B") {
															$first_naf = "E44.0 - Moderate malnutrition (Moderate protein - calorie malnutrition)";
														}else if ($data['screening'][$max]['bmi']<16&&$data_old['assessment_naf'][0]['grade']=="NAF-C") {
															$first_naf = "E43 - Severe malnutrition (Unspecified severe protein - calorie malnutrition)";
														}
													}
												?>
												<h3>Assessment : NAF</h3>
												<p><?=$first_naf;?></p>
												<p>Detail : </p>
												<ul class="list-group">
													<li class="list-group-item header-list pink_1">
														<div class="row">
															<div class="col-sm-1 col-xs-1">#</div>
															<div class="col-sm-3 col-xs-3">ผลการประเมิน</div>
															<div class="col-sm-1 col-xs-3">คะแนน</div>
															<div class="col-sm-4 col-xs-4">วันที่ประเมิน</div>
															<div class="col-sm-2 hidden-xs">เครื่องมือ</div>
														</div>
													</li>
												<?php $i = 1; 
													if (isset($data_old['assessment_naf']))
													for ($j=count($data_old['assessment_naf'])-1; $j >= 0; $j--) { ?>
													<li class="list-group-item">
														<a onclick="new_tabs('<?=base_url()?>screening/export_naf/<?=$data_old['assessment_naf'][$j]['id']?>/-1/<?=$i?>')" class="link">
															<div class="row">
																<div class="col-sm-1 col-xs-1"><?=$i?></div>
																<div class="col-sm-3 col-xs-3"><?=$data_old['assessment_naf'][$j]['grade']?></div>
																<div class="col-sm-1 col-xs-3"><?=$data_old['assessment_naf'][$j]['score']?></div>
																<div class="col-sm-4 col-xs-4"><?php echo date_format(date_create($data_old['assessment_naf'][$j]['date']),'d/m/Y'); ?></div>
																<div class="col-sm-2 hidden-xs">กดเพื่อดู</div>
															</div>
														</a>
													</li>
												<?php $i++; } ?>
												</ul>
												<div class="label-bar text-center">
													<label><i class="fa fa-square" style="color: #176920;"></i> NAF-A</label>
													<label><i class="fa fa-square" style="color: #f39c12;"></i> NAF-B</label>
													<label><i class="fa fa-square" style="color: #e74c3c;"></i> NAF-C</label>
												</div>
												<script type="text/javascript">
													$(document).ready(function() {
														var head_naf = $('.head-naf tr')
														for (var i =56; i < head_naf.length; i++) {
															var tmp_head = $(head_naf[i])
															var tmp_sub = $('.sub-naf tr')[i-53]
															$(tmp_sub).height($(tmp_head).height())
														}
													});
												</script>
												<div style="height: 300px;position: relative;">
													<canvas id="myChart_naf"></canvas>
												</div>
												<script>
													<?php
													if (isset($data_old['assessment_naf'])){
													$score_ar = array();
													$date_ar = array();
													$color_ar = array();
													foreach ($data_old['assessment_naf'] as $key => $value) {
														array_push($score_ar, $value['score']);
														array_push($date_ar, date_format(date_create($value['date']),'d/m/Y'));
														if ($value['score']<=5) {
															array_push($color_ar, '#176920');
														}else if($value['score']>=6&&$value['score']<=10){
															array_push($color_ar, '#f39c12');
														}else if($value['score']>=11){
															array_push($color_ar, '#e74c3c');
														}
													}
													$score_ar = array_reverse($score_ar);
													$date_ar = array_reverse($date_ar);
													$color_ar = array_reverse($color_ar);
													?>
													var ctx_naf = document.getElementById("myChart_naf");
													var myChart_naf = new Chart(ctx_naf, {
														type: 'bar',
														data: {
															labels: ["<?=implode($date_ar,'","')?>"],
															datasets: [{
																data: [<?=implode($score_ar,',')?>],
																backgroundColor: ["<?=implode($color_ar,'","')?>"]
															}]
														},
														options: {
															legend: {
														        display: false
														    },
															maintainAspectRatio: false,
															scales: {
																yAxes: [{
																	ticks: {
																		beginAtZero:true
																	},
																	scaleLabel: {
																		display: true,
																		labelString: 'Score'
																	}
																}],
																xAxes: [{
																	maxBarThickness:200,
																	ticks: {
																		beginAtZero:true
																	},
																	scaleLabel: {
																		display: true,
																		labelString: 'Date'
																	}
																}]
															},
															events: false,
														    tooltips: {
														        enabled: false
														    },
														    hover: {
														        animationDuration: 0
														    },
														    animation: {
														        duration: 1,
														        onComplete: function () {
														            var chartInstance = this.chart,
														                ctx = chartInstance.ctx;
														            ctx.font = Chart.helpers.fontString(18, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
														            ctx.textAlign = 'center';
														            ctx.textBaseline = 'bottom';

														            this.data.datasets.forEach(function (dataset, i) {
														                var meta = chartInstance.controller.getDatasetMeta(i);
														                meta.data.forEach(function (bar, index) {
														                    var data = dataset.data[index];                            
														                    ctx.fillText(data, bar._model.x, bar._model.y - 5);
														                });
														            });
														        }
														    }
														}
													});
													<?php } ?>
												</script>
											</div>
											<div role="tabpanel" class="tab-pane" id="nt">
												<?php 
													$assessment_nt_count = isset($data_old['assessment_nt']) ? count($data_old['assessment_nt']) : '0';
													$first_nt = "";
													if (isset($data_old['assessment_nt'][0]['grade'])) {
														if ($data['screening'][$max]['bmi']>=17&&$data['screening'][$max]['bmi']<18.50&&$data_old['assessment_nt'][0]['grade']=="NT-2") {
															$first_nt = "E44.1 - Mild malnutrition (Mild protein - calorie malnutrition)";
														}else if ($data['screening'][$max]['bmi']>=16&&$data['screening'][$max]['bmi']<17&&$data_old['assessment_nt'][0]['grade']=="NT-3") {
															$first_nt = "E44.0 - Moderate malnutrition (Moderate protein - calorie malnutrition)";
														}else if ($data['screening'][$max]['bmi']<16&&$data_old['assessment_nt'][0]['grade']=="NT-4") {
															$first_nt = "E43 - Severe malnutrition (Unspecified severe protein - calorie malnutrition)";
														}
													}
												?>
												<h3>Assessment : NT</h3>
												<p><?=$first_nt;?></p>
												<p>Detail : </p>
												<ul class="list-group">
													<li class="list-group-item header-list violet_1">
														<div class="row">
															<div class="col-sm-1 col-xs-1">#</div>
															<div class="col-sm-3 col-xs-3">ผลการประเมิน</div>
															<div class="col-sm-1 col-xs-3">คะแนน</div>
															<div class="col-sm-4 col-xs-4">วันที่ประเมิน</div>
															<div class="col-sm-2 hidden-xs">เครื่องมือ</div>
														</div>
													</li>
												<?php $i = 1; 
													if (isset($data_old['assessment_nt']))
													for ($j=count($data_old['assessment_nt'])-1; $j >= 0; $j--) { ?>
													<li class="list-group-item">
														<a onclick="new_tabs('<?=base_url()?>screening/export_nt/<?=$data_old['assessment_nt'][$j]['id']?>/-1/<?=$i?>')" target="_bank" class="link">
															<div class="row">
																<div class="col-sm-1 col-xs-1"><?=$i?></div>
																<div class="col-sm-3 col-xs-3"><?=$data_old['assessment_nt'][$j]['grade']?></div>
																<div class="col-sm-1 col-xs-3"><?=$data_old['assessment_nt'][$j]['score']?></div>
																<div class="col-sm-4 col-xs-4"><?php echo date_format(date_create($data_old['assessment_nt'][$j]['date']),'d/m/Y'); ?></div>
																<div class="col-sm-2 hidden-xs">กดเพื่อดู</div>
															</div>
														</a>
													</li>
												<?php $i++; } ?>
												</ul>
												<div class="label-bar text-center">
													<label><i class="fa fa-square" style="color: #176920;"></i> NT-1,NT-2</label>
													<label><i class="fa fa-square" style="color: #f39c12;"></i> NT-3</label>
													<label><i class="fa fa-square" style="color: #e74c3c;"></i> NT-4</label>
												</div>
												<script type="text/javascript">
													$(document).ready(function() {
														var head_nt = $('.head-nt tr')
														for (var i =56; i < head_nt.length; i++) {
															var tmp_head = $(head_nt[i])
															var tmp_sub = $('.sub-nt tr')[i-53]
															$(tmp_sub).height($(tmp_head).height())
														}
													});
												</script>
												<div style="height: 300px;position: relative;">
													<canvas id="myChart_nt"></canvas>
												</div>
												<script>
													<?php
													if (isset($data_old['assessment_nt'])){
													$score_ar = array();
													$date_ar = array();
													$color_ar = array();
													foreach ($data_old['assessment_nt'] as $key => $value) {
														array_push($score_ar, $value['score']);
														array_push($date_ar, date_format(date_create($value['date']),'d/m/Y'));
														if ($value['score']<=4) {
															array_push($color_ar, '#176920');
														}else if($value['score']>=5&&$value['score']<=7){
															array_push($color_ar, '#176920');
														}else if($value['score']>=8&&$value['score']<=10){
															array_push($color_ar, '#f39c12');
														}else if($value['score']>=11){
															array_push($color_ar, '#e74c3c');
														}
													}
													$score_ar = array_reverse($score_ar);
													$date_ar = array_reverse($date_ar);
													$color_ar = array_reverse($color_ar);
													?>
													var ctx_nt = document.getElementById("myChart_nt");
													var myChart_nt = new Chart(ctx_nt, {
														type: 'bar',
														data: {
															labels: ["<?=implode($date_ar, '","')?>"],
															datasets: [{
																label: 'points of times',
																data: [<?=implode($score_ar, ',')?>],
																backgroundColor: ["<?=implode($color_ar, '","')?>"]
															}]
														},
														options: {
															legend: {
														        display: false
														    },
															maintainAspectRatio: false,
															scales: {
																yAxes: [{
																	ticks: {
																		beginAtZero:true
																	},
																	scaleLabel: {
																		display: true,
																		labelString: 'Score'
																	}
																}],
																xAxes: [{
																	maxBarThickness:200,
																	ticks: {
																		beginAtZero:true
																	},
																	scaleLabel: {
																		display: true,
																		labelString: 'Date'
																	}
																}]
															},
															events: false,
														    tooltips: {
														        enabled: false
														    },
														    hover: {
														        animationDuration: 0
														    },
														    animation: {
														        duration: 1,
														        onComplete: function () {
														            var chartInstance = this.chart,
														                ctx = chartInstance.ctx;
														            ctx.font = Chart.helpers.fontString(18, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
														            ctx.textAlign = 'center';
														            ctx.textBaseline = 'bottom';

														            this.data.datasets.forEach(function (dataset, i) {
														                var meta = chartInstance.controller.getDatasetMeta(i);
														                meta.data.forEach(function (bar, index) {
														                    var data = dataset.data[index];                            
														                    ctx.fillText(data, bar._model.x, bar._model.y - 5);
														                });
														            });
														        }
														    }
														}
													});
													<?php } ?>
												</script>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="btn-bar">
								<button class="btn btn-color red" onclick="checkout('screening','<?=$data['screening'][0]['hn_code']?>','<?=$data['screening'][0]['patient_ward_id']?>',2)">Death</button>
								<button class="btn btn-color" onclick="checkout('screening','<?=$data['screening'][0]['hn_code']?>','<?=$data['screening'][0]['patient_ward_id']?>',1)">Checkout <i class="fa fa-sign-out"></i></button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
	</html>