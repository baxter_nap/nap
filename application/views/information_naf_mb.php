<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
<head>
	<title>Main</title>
	<?php $this->load->view('_config_export'); ?>
	<script src="<?=base_url()?>../js/jspdf.debug.js"></script>
	<script src="<?=base_url()?>../js/html2canvas.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('body').css('overflow', 'auto');
			$('#cmd').click(function () {
				save()
			});
			<?php if ($platform=="iOS") { ?>
			$('.page').hide('fast');
			html2canvas($('.page'), {
				onrendered: function(canvas) {
			    	$('#export-box').html(canvas);
			   	 	toPDF();
				}
			});
			<?php } ?>
		});
		function save() {
			$('#cmd').hide('fast','',function () {
				<?php if ($platform!="iOS") { ?>
				print();
				<?php }else{ ?>
				toPDF()
				<?php } ?>
	    		setTimeout(function() {
	    			$('#cmd').show('fast');
	    		}, 1000);
            });
		}
		function toPDF() {
			html2canvas($("#export-box canvas"), {
				onrendered: function(canvas) {
					imgData = canvas.toDataURL('image/jpeg', 1.0);              
					doc = new jsPDF("p", "mm", "a4");
	                doc.addImage(imgData, 'JPEG', 0, 0, 210, 870);
	                doc.addPage();
	                doc.addImage(imgData, 'JPEG', 0, -290, 210, 870);
	                doc.addPage();
	                doc.addImage(imgData, 'JPEG', 0, -580, 210, 870);
	                <?php if ($platform=="iOS") { ?>
	                doc.autoPrint();
					doc.output('dataurl');
					<?php } ?>
	            }
	        });
		}
	</script>
</head>
<body class="export">
	<div class="page" style="width: 1000px;overflow: hidden;">
		<img src="<?=base_url().'../images/form-naf-1.jpg'?>" width="100%">
		<img src="<?=base_url().'../images/form-naf-2.jpg'?>" width="100%">
		<img src="<?=base_url().'../images/form-naf-3.jpg'?>" width="100%">
	</div>
	<button id="cmd" class="btn btn-color export">Print</button>
	<div id="export-box"></div>
</body>
</html>