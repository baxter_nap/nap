<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$menu = array('screening'=>'','patients'=>'','information'=>'','report'=>'');
foreach ($menu as $key => $value) {
	foreach ($page as $k => $v) {
		if ($key==$v) {
			$menu[$key] = 'active';
		}
	}
}
?>
<div class="col-sm-2 hidden-xs left_bar">
	<a href="<?php echo base_url(); ?>screening/create" class="<?php echo $menu['screening']; ?>"><li><img src="<?php echo base_url(); ?>../images/icon_screening.png" alt="">Screening &amp; Assessment</li></a>
	<?php if (!isset($_SESSION['no_save'])): ?>
	<a href="<?php echo base_url(); ?>patients" class="<?php echo $menu['patients']; ?>"><li><img src="<?php echo base_url(); ?>../images/icon_patients.png" alt="">Patients List</li></a>
	<?php endif ?>
	<a href="<?php echo base_url(); ?>information" class="<?php echo $menu['information']; ?>"><li><img src="<?php echo base_url(); ?>../images/icon_information.png" alt="">Information</li></a>
	<a href="<?php echo base_url(); ?>report" class="<?php echo $menu['report']; ?>"><li><img src="<?php echo base_url(); ?>../images/icon_report.png" alt="">Report</li></a>
</div>
