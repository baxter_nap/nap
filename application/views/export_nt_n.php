<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
<head>
	<title>Main</title>
	<?php $this->load->view('_config_export'); ?>
	<script src="<?=base_url()?>../js/jspdf.debug.js"></script>
	<script src="<?=base_url()?>../js/html2canvas.js"></script>
	<script src="<?=base_url()?>../js/posi_nt.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('body').css('overflow', 'auto');
		});
	</script>
</head>
<body class="export">
	<?php
		$debug = 0;
	?>
	<div id="demo"></div>
	<div class="page" style="width: 1000px;height: 1455px;overflow: hidden;background: #fff;">
		<label class="name" style="font-size: 16px;"><?=isset($data_old['name']) ? $data_old['name'] : '-'; ?></label>
		<?php if ($debug==1||$data_old['sex']==1): ?>
			<label class="sex_1"><i class="fa fa-check"></i></label>
			<!-- <label class="ibw_1"><?=round(50+(0.91*($data_old['height']-152.4)),2)?></label> -->
			<label class="ibw_1"><?=$data_old['height']-100?></label>
		<?php endif ?>
		<?php if ($debug==1||$data_old['sex']==2): ?>
			<label class="sex_2"><i class="fa fa-check"></i></label>
			<!-- <label class="ibw_2"><?=round(45.5+(0.91*($data_old['height']-152.4)),2)?></label> -->
			<label class="ibw_2"><?=$data_old['height']-110?></label>
		<?php endif ?>
		<label class="age"><?=$data_old['age']?></label>
		<label class="hn_code"><?=$data_old['hn_code']?></label>
		<label class="height"><?=$data_old['height']?></label>
		<label class="weight_current"><?=$data_old['weight_current']?></label>
		<!-- <label class="weight_default"><?=$data_old['weight_default']?></label> -->
		<label class="bmi"><?=$data_old['bmi']?></label>
		<label class="date" style="font-size: 12px;"><?=date_format(date_create(),'d/m/Y')?></label>
		<label class="diagnose_title"><?=$data_old['diagnose']?></label>
		<!-- n0_1 -->
		<?php if ($debug==1||$data['n0_1']==0): ?>
			<label class="n0_1_0"><i class="fa fa-circle-thin"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n0_1']==1): ?>
			<label class="n0_1_1"><i class="fa fa-circle-thin"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n0_1']==2): ?>
			<label class="n0_1_2"><i class="fa fa-circle-thin"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n0_1']==3): ?>
			<label class="n0_1_3"><i class="fa fa-circle-thin"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n0_1']==4): ?>
			<label class="n0_1_4"><i class="fa fa-circle-thin"></i></label>
		<?php endif ?>

		<!-- n0_2 -->
		<?php if ($debug==1||$data['n0_2']==100): ?>
			<label class="n0_2_100"><i class="fa fa-circle-thin"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n0_2']==90): ?>
			<label class="n0_2_90"><i class="fa fa-circle-thin"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n0_2']==80): ?>
			<label class="n0_3_80"><i class="fa fa-circle-thin"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n0_2']==70): ?>
			<label class="n0_2_70"><i class="fa fa-circle-thin"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n0_2']==60): ?>
			<label class="n0_2_60"><i class="fa fa-circle-thin"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n0_2']==50): ?>
			<label class="n0_2_50"><i class="fa fa-circle-thin"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n0_2']==40): ?>
			<label class="n0_2_40"><i class="fa fa-circle-thin"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n0_2']==30): ?>
			<label class="n0_2_30"><i class="fa fa-circle-thin"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n0_2']==20): ?>
			<label class="n0_2_20"><i class="fa fa-circle-thin"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n0_2']==10): ?>
			<label class="n0_2_10"><i class="fa fa-circle-thin"></i></label>
		<?php endif ?>
		<!-- n1_1 -->
		<?php if ($debug==1||$data['n1_1']==1): ?>
			<label class="n1_1_1"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n1_1']==2): ?>
			<label class="n1_1_2"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n1_1']==3): ?>
			<label class="n1_1_3"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n1_1']==4): ?>
			<label class="n1_1_4"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n1_1']==5): ?>
			<label class="n1_1_5"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<!-- n1_2 -->
		<?php if ($debug==1||$data['n1_2']==1): ?>
			<label class="n1_2_1"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n1_2']==2): ?>
			<label class="n1_2_2"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n1_2']==3): ?>
			<label class="n1_2_3"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n1_2']==4): ?>
			<label class="n1_2_4"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n1_2']==5): ?>
			<label class="n1_2_5"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<!-- n1_3 -->
		<?php if ($debug==1||$data['n1_3']==1): ?>
			<label class="n1_3_1"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n1_3']==2): ?>
			<label class="n1_3_2"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n1_3']==3): ?>
			<label class="n1_3_3"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<!-- row-1 -->
		<?php if ($debug==1||($data['n1_2']==1&&$data['n1_3']==1&&$data['n1_4_score']==1)): ?>
			<label class="n1_1_1_1"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||($data['n1_2']==1&&$data['n1_3']==1&&$data['n1_4_score']==2)): ?>
			<label class="n1_1_1_2"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||($data['n1_2']==1&&$data['n1_3']==2&&$data['n1_4_score']==2)): ?>
			<label class="n1_1_2_2"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||($data['n1_2']==1&&$data['n1_3']==2&&$data['n1_4_score']==3)): ?>
			<label class="n1_1_2_3"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||($data['n1_2']==1&&$data['n1_3']==3&&$data['n1_4_score']==3)): ?>
			<label class="n1_1_3_3"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||($data['n1_2']==1&&$data['n1_3']==3&&$data['n1_4_score']==4)): ?>
			<label class="n1_1_3_4"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<!-- row-2 -->
		<?php if ($debug==1||($data['n1_2']==2&&$data['n1_3']==1&&$data['n1_4_score']==0)): ?>
			<label class="n1_2_1_0"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||($data['n1_2']==2&&$data['n1_3']==1&&$data['n1_4_score']==1)): ?>
			<label class="n1_2_1_1"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||($data['n1_2']==2&&$data['n1_3']==2&&$data['n1_4_score']==1)): ?>
			<label class="n1_2_2_1"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||($data['n1_2']==2&&$data['n1_3']==2&&$data['n1_4_score']==2)): ?>
			<label class="n1_2_2_2"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||($data['n1_2']==2&&$data['n1_3']==3&&$data['n1_4_score']==2)): ?>
			<label class="n1_2_3_2"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||($data['n1_2']==2&&$data['n1_3']==3&&$data['n1_4_score']==3)): ?>
			<label class="n1_2_3_3"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<!-- row-3 -->
		<?php if ($debug==1||($data['n1_2']==3&&$data['n1_3']==1&&$data['n1_4_score']==0)): ?>
			<label class="n1_3_1_0"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||($data['n1_2']==3&&$data['n1_3']==2&&$data['n1_4_score']==0)): ?>
			<label class="n1_3_2_0"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||($data['n1_2']==3&&$data['n1_3']==2&&$data['n1_4_score']==1)): ?>
			<label class="n1_3_2_1"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||($data['n1_2']==3&&$data['n1_3']==3&&$data['n1_4_score']==1)): ?>
			<label class="n1_3_3_1"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||($data['n1_2']==3&&$data['n1_3']==3&&$data['n1_4_score']==2)): ?>
			<label class="n1_3_3_2"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<!-- row-4 -->
		<?php if ($debug==1||($data['n1_2']==4&&$data['n1_3']==1&&$data['n1_4_score']==0)): ?>
			<label class="n1_4_1_0"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||($data['n1_2']==4&&$data['n1_3']==2&&$data['n1_4_score']==0)): ?>
			<label class="n1_4_2_0"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||($data['n1_2']==4&&$data['n1_3']==3&&$data['n1_4_score']==1)): ?>
			<label class="n1_4_3_1"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<!-- row-5 -->
		<?php if ($debug==1||($data['n1_2']==5&&$data['n1_3']==1&&$data['n1_4_score']==0)): ?>
			<label class="n1_5_1_0"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||($data['n1_2']==5&&$data['n1_3']==2&&$data['n1_4_score']==0)): ?>
			<label class="n1_5_2_0"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||($data['n1_2']==5&&$data['n1_3']==3&&$data['n1_4_score']==0)): ?>
			<label class="n1_5_3_0"><i class="fa fa-check"></i></label>
		<?php endif ?>

		<!-- n1_4_score -->
		<label class="n1_4_score"><?=$data['n1_4_score']?></i></label>

		<!-- n2_1-->
		<?php if ($debug==1||$data['n2_1']==1): ?>
				<label class="n2_1_1"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n2_1']==2): ?>
				<label class="n2_1_2"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n2_1']==3): ?>
				<label class="n2_1_3"><i class="fa fa-check"></i></label>
		<?php endif ?>

		<label class="percent"><?=$data['percent']?></label>
		<label class="weight_default_2"><?=$data['weight_default']?></label>

		<!-- time_number -->
		<?php if ($debug==1||$data['time_scope']==7): ?>
			<label class="time_scope_7"><?=$data['time_number']?></label>
		<?php endif ?>
		<?php if ($debug==1||$data['time_scope']==30): ?>
			<label class="time_scope_30"><?=$data['time_number']?></label>
		<?php endif ?>

		<!-- time_check -->
		<?php if ($debug==1||$data['n2_1']==3): ?>
			<?php if ($debug==1||$data['time_scope']*$data['time_number']==7): ?>
				<label class="time_check_1"><i class="fa fa-check"></i></label>
				<?php if ($debug==1||$data['percent']<1): ?>
					<label class="perccent_check_1_1"><i class="fa fa-check"></i></label>
				<?php endif ?>
				<?php if ($debug==1||($data['percent']>=1&&$data['percent']<=2)): ?>
					<label class="perccent_check_1_2"><i class="fa fa-check"></i></label>
				<?php endif ?>
				<?php if ($debug==1||$data['percent']>2): ?>
					<label class="perccent_check_1_3"><i class="fa fa-check"></i></label>
				<?php endif ?>
			<?php endif ?>
			<?php if ($debug==1||($data['time_scope']*$data['time_number']==14)||($data['time_scope']*$data['time_number']==21)): ?>
				<label class="time_check_2"><i class="fa fa-check"></i></label>
				<?php if ($debug==1||$data['percent']<2): ?>
					<label class="perccent_check_2_1"><i class="fa fa-check"></i></label>
				<?php endif ?>
				<?php if ($debug==1||($data['percent']>=2&&$data['percent']<=3)): ?>
					<label class="perccent_check_2_2"><i class="fa fa-check"></i></label>
				<?php endif ?>
				<?php if ($debug==1||$data['percent']>3): ?>
					<label class="perccent_check_2_3"><i class="fa fa-check"></i></label>
				<?php endif ?>
			<?php endif ?>
			<?php if ($debug==1||$data['time_scope']*$data['time_number']==30): ?>
				<label class="time_check_3"><i class="fa fa-check"></i></label>
				<?php if ($debug==1||$data['percent']<4): ?>
					<label class="perccent_check_3_1"><i class="fa fa-check"></i></label>
				<?php endif ?>
				<?php if ($debug==1||($data['percent']>=4&&$data['percent']<=5)): ?>
					<label class="perccent_check_3_2"><i class="fa fa-check"></i></label>
				<?php endif ?>
				<?php if ($debug==1||$data['percent']>5): ?>
					<label class="perccent_check_3_3"><i class="fa fa-check"></i></label>
				<?php endif ?>
			<?php endif ?>
			<?php if ($debug==1||$data['time_scope']*$data['time_number']==90): ?>
				<label class="time_check_4"><i class="fa fa-check"></i></label>
				<?php if ($debug==1||$data['percent']<7): ?>
					<label class="perccent_check_4_1"><i class="fa fa-check"></i></label>
				<?php endif ?>
				<?php if ($debug==1||($data['percent']>=7&&$data['percent']<=8)): ?>
					<label class="perccent_check_4_2"><i class="fa fa-check"></i></label>
				<?php endif ?>
				<?php if ($debug==1||$data['percent']>8): ?>
					<label class="perccent_check_4_3"><i class="fa fa-check"></i></label>
				<?php endif ?>
			<?php endif ?>
			<?php if ($debug==1||$data['time_scope']*$data['time_number']>=149): ?>
				<label class="time_check_5"><i class="fa fa-check"></i></label>
				<?php if ($debug==1||$data['percent']<10): ?>
					<label class="perccent_check_5_1"><i class="fa fa-check"></i></label>
				<?php endif ?>
				<?php if ($debug==1||($data['percent']==10)): ?>
					<label class="perccent_check_5_2"><i class="fa fa-check"></i></label>
				<?php endif ?>
				<?php if ($debug==1||$data['percent']>10): ?>
					<label class="perccent_check_5_3"><i class="fa fa-check"></i></label>
				<?php endif ?>
			<?php endif ?>
		<?php endif ?>
		<label class="n2_1_score"><?=$data['n2_1_score']?></label>

		<!-- percent_check -->
		<?php if ($debug==1||$data['time_scope']*$data['time_number']==7): ?>
			<label class="time_check_1"><i class="fa fa-check"></i></label>
		<?php endif ?>

		<!-- n3_1-->
		<?php if ($debug==1||$data['n3_1']==1): ?>
			<label class="n3_1_0"><i class="fa fa-circle-thin"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n3_1']==2): ?>
			<label class="n3_1_1"><i class="fa fa-circle-thin"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n3_1']==3): ?>
			<label class="n3_1_2"><i class="fa fa-circle-thin"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n3_1']==4): ?>
			<label class="n3_1_3"><i class="fa fa-circle-thin"></i></label>
		<?php endif ?>
		<label class="n3_1_score"><?=$data['n3_1']-1?></label>

		<!-- n4_1-->
		<?php if ($debug==1||$data['n4_1']==1): ?>
			<label class="n4_1_0"><i class="fa fa-circle-thin"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n4_1']==2): ?>
			<label class="n4_1_1"><i class="fa fa-circle-thin"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n4_1']==3): ?>
			<label class="n4_1_2"><i class="fa fa-circle-thin"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n4_1']==4): ?>
			<label class="n4_1_3"><i class="fa fa-circle-thin"></i></label>
		<?php endif ?>

		<!-- n5_1-->
		<?php if ($debug==1||$data['n5_1']==1): ?>
			<label class="n5_1_0"><i class="fa fa-circle-thin"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n5_1']==2): ?>
			<label class="n5_1_1"><i class="fa fa-circle-thin"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n5_1']==3): ?>
			<label class="n5_1_2"><i class="fa fa-circle-thin"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n5_1']==4): ?>
			<label class="n5_1_3"><i class="fa fa-circle-thin"></i></label>
		<?php endif ?>
		
		<!-- n6_1_score-->
		<?php if ($debug==1||$data['n6_1_score']==0): ?>
			<label class="n6_1_0"><i class="fa fa-circle-thin"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n6_1_score']==1): ?>
			<label class="n6_1_1"><i class="fa fa-circle-thin"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n6_1_score']==2): ?>
			<label class="n6_1_2"><i class="fa fa-circle-thin"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n6_1_score']==3): ?>
			<label class="n6_1_3"><i class="fa fa-circle-thin"></i></label>
		<?php endif ?>

		<!-- n7_1-->
		<?php if ($debug==1||$data['n7_1']==0): ?>
			<label class="n7_1_0"><i class="fa fa-circle-thin"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n7_1']==1): ?>
			<label class="n7_1_1"><i class="fa fa-circle-thin"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n7_1']==2): ?>
			<label class="n7_1_2"><i class="fa fa-circle-thin"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n7_1']==3): ?>
			<label class="n7_1_3"><i class="fa fa-circle-thin"></i></label>
		<?php endif ?>

		<!-- n7_2-->
		<?php if ($debug==1||$data['n7_2']==0): ?>
			<label class="n7_2_0"><i class="fa fa-circle-thin"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n7_2']==1): ?>
			<label class="n7_2_1"><i class="fa fa-circle-thin"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n7_2']==2): ?>
			<label class="n7_2_2"><i class="fa fa-circle-thin"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n7_2']==3): ?>
			<label class="n7_2_3"><i class="fa fa-circle-thin"></i></label>
		<?php endif ?>

		<!-- n7_3-->
		<?php if ($debug==1||$data['n7_3']==0): ?>
			<label class="n7_3_0"><i class="fa fa-circle-thin"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n7_3']==1): ?>
			<label class="n7_3_1"><i class="fa fa-circle-thin"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n7_3']==2): ?>
			<label class="n7_3_2"><i class="fa fa-circle-thin"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n7_3']==3): ?>
			<label class="n7_3_3"><i class="fa fa-circle-thin"></i></label>
		<?php endif ?>
		<!-- n7_4-->
		<?php if ($debug==1||$data['n7_4']==0): ?>
			<label class="n7_4_0"><i class="fa fa-circle-thin"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n7_4']==1): ?>
			<label class="n7_4_1"><i class="fa fa-circle-thin"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n7_4']==2): ?>
			<label class="n7_4_2"><i class="fa fa-circle-thin"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n7_4']==3): ?>
			<label class="n7_4_3"><i class="fa fa-circle-thin"></i></label>
		<?php endif ?>
		<!-- n7_5-->
		<?php if ($debug==1||$data['n7_5']==0): ?>
			<label class="n7_5_0"><i class="fa fa-circle-thin"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n7_5']==1): ?>
			<label class="n7_5_1"><i class="fa fa-circle-thin"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n7_5']==2): ?>
			<label class="n7_5_2"><i class="fa fa-circle-thin"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n7_5']==3): ?>
			<label class="n7_5_3"><i class="fa fa-circle-thin"></i></label>
		<?php endif ?>
		<!-- n7_6-->
		<?php if ($debug==1||$data['n7_6']==0): ?>
			<label class="n7_6_0"><i class="fa fa-circle-thin"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n7_6']==1): ?>
			<label class="n7_6_1"><i class="fa fa-circle-thin"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n7_6']==2): ?>
			<label class="n7_6_2"><i class="fa fa-circle-thin"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n7_6']==3): ?>
			<label class="n7_6_3"><i class="fa fa-circle-thin"></i></label>
		<?php endif ?>
		<!-- n7_7-->
		<?php if ($debug==1||$data['n7_7']==0): ?>
			<label class="n7_7_0"><i class="fa fa-circle-thin"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n7_7']==1): ?>
			<label class="n7_7_1"><i class="fa fa-circle-thin"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n7_7']==2): ?>
			<label class="n7_7_2"><i class="fa fa-circle-thin"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n7_7']==3): ?>
			<label class="n7_7_3"><i class="fa fa-circle-thin"></i></label>
		<?php endif ?>
		<!-- n7_8-->
		<?php if ($debug==1||$data['n7_8']==0): ?>
			<label class="n7_8_0"><i class="fa fa-circle-thin"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n7_8']==1): ?>
			<label class="n7_8_1"><i class="fa fa-circle-thin"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n7_8']==2): ?>
			<label class="n7_8_2"><i class="fa fa-circle-thin"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n7_8']==3): ?>
			<label class="n7_8_3"><i class="fa fa-circle-thin"></i></label>
		<?php endif ?>
		<!-- n7_9-->
		<?php if ($debug==1||$data['n7_9']==0): ?>
			<label class="n7_9_0"><i class="fa fa-circle-thin"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n7_9']==1): ?>
			<label class="n7_9_1"><i class="fa fa-circle-thin"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n7_9']==2): ?>
			<label class="n7_9_2"><i class="fa fa-circle-thin"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n7_9']==3): ?>
			<label class="n7_9_3"><i class="fa fa-circle-thin"></i></label>
		<?php endif ?>
		<label class="n7_score">
			<?php
				$n7_score = 0;
				for ($i=1; $i <=9 ; $i++) { 
					if ($data['n7_'.$i]!=-1) {
						$n7_score += $data['n7_'.$i];
					}
				}
				echo $n7_score>=3 ? '3' : $n7_score;
			?>
		</label>

		<!-- n8_1-->
		<?php if ($debug==1||$data['n8_1']==1): ?>
			<label class="n8_1_1"><i class="fa fa-circle-thin"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n8_1']==2): ?>
			<label class="n8_1_2"><i class="fa fa-circle-thin"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n8_1']==3): ?>
			<label class="n8_1_3"><i class="fa fa-circle-thin"></i></label>
		<?php endif ?>
		<!-- n8_2-->
		<?php if ($debug==1||$data['n8_2']==1): ?>
			<label class="n8_2_1"><i class="fa fa-circle-thin"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n8_2']==2): ?>
			<label class="n8_2_2"><i class="fa fa-circle-thin"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n8_2']==3): ?>
			<label class="n8_2_3"><i class="fa fa-circle-thin"></i></label>
		<?php endif ?>
		<!-- n8_3-->
		<?php if ($debug==1||$data['n8_3']==1): ?>
			<label class="n8_3_1"><i class="fa fa-circle-thin"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n8_3']==2): ?>
			<label class="n8_3_2"><i class="fa fa-circle-thin"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n8_3']==3): ?>
			<label class="n8_3_3"><i class="fa fa-circle-thin"></i></label>
		<?php endif ?>
		<!-- n8_4-->
		<?php if ($debug==1||$data['n8_4']==1): ?>
			<label class="n8_4_1"><i class="fa fa-circle-thin"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n8_4']==2): ?>
			<label class="n8_4_2"><i class="fa fa-circle-thin"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n8_4']==3): ?>
			<label class="n8_4_3"><i class="fa fa-circle-thin"></i></label>
		<?php endif ?>
		<!-- n8_5-->
		<?php if ($debug==1||$data['n8_5']==1): ?>
			<label class="n8_5_1"><i class="fa fa-circle-thin"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n8_5']==2): ?>
			<label class="n8_5_2"><i class="fa fa-circle-thin"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n8_5']==3): ?>
			<label class="n8_5_3"><i class="fa fa-circle-thin"></i></label>
		<?php endif ?>
		<!-- n8_6-->
		<?php if ($debug==1||$data['n8_6']==1): ?>
			<label class="n8_6_1"><i class="fa fa-circle-thin"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n8_6']==2): ?>
			<label class="n8_6_2"><i class="fa fa-circle-thin"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n8_6']==3): ?>
			<label class="n8_6_3"><i class="fa fa-circle-thin"></i></label>
		<?php endif ?>
		<!-- n8_7-->
		<?php if ($debug==1||$data['n8_7']==1): ?>
			<label class="n8_7_1"><i class="fa fa-circle-thin"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n8_7']==2): ?>
			<label class="n8_7_2"><i class="fa fa-circle-thin"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['n8_7']==3): ?>
			<label class="n8_7_3"><i class="fa fa-circle-thin"></i></label>
		<?php endif ?>
		<label class="n8_score">
			<?php
				$n8_score = 0;
				for ($i=1; $i <=7 ; $i++) { 
					if ($data['n8_'.$i]!=-1) {
						$n8_score += $data['n8_'.$i];
					}
				}
				echo $n8_score>=3 ? '3' : $n8_score;
			?>
		</label>

		<!-- grade -->
		<?php if ($debug==1||$data['grade']=="NT-1"): ?>
			<label class="grade_1"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['grade']=="NT-2"): ?>
			<label class="grade_2"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['grade']=="NT-3"): ?>
			<label class="grade_3"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<?php if ($debug==1||$data['grade']=="NT-4"): ?>
			<label class="grade_4"><i class="fa fa-check"></i></label>
		<?php endif ?>
		<label class="score"><?=$data['score']?></label>

		<img src="<?=base_url()?>../images/form-nt.jpg" style="width: 100%">
	</div>
	<?php if ($platform=="iOS") { ?>
		<div id="export-box"></div>
	<?php } ?>
	<button id="cmd" class="btn btn-color export">Print</button>
	<script type="text/javascript">
		$( function() {
			// $( "label" ).draggable({
			// 	stop: function( event, ui ) {
			// 		name = $(ui.helper[0]).attr('class').split(' ')[0];
			// 		// console.log('old:'+posi[name]);
			// 		// console.log('name:'+name+' '+ui.position.left+","+ui.position.top);
			// 		posi[name] = [(ui.position.left),(ui.position.top)];
			// 		setJs(ui)
			// 	}
			// });
		} );
		$(document).ready(function() {
			$('#cmd').click(function () {
				save()
			});
			setHtml()
		});

		function save() {
			$('#cmd').hide('fast','',function () {
				<?php if ($platform!="iOS") { ?>
				print();
				<?php }else{ ?>
				toPDF()
				<?php } ?>
	    		setTimeout(function() {
	    			$('#cmd').show('fast');
	    		}, 1000);
            });
		}

		function toPDF() {
			html2canvas($("#export-box canvas"), {
				onrendered: function(canvas) {
					imgData = canvas.toDataURL('image/jpeg', 1.0);              
					doc = new jsPDF("p", "mm", "a4");
	                doc.addImage(imgData, 'JPEG', 0, 0, 210, 290);
	                <?php if ($platform=="iOS") { ?>
	                doc.autoPrint();
					doc.output('dataurl');
					<?php } ?>
	            }
	        });
		}

		function setHtml() {
			for (var k in posi){
			    if (posi.hasOwnProperty(k)) {
			    	console.log(k)
			    	console.log(posi[k])
			         $('.page .'+k).css({
			         	left: posi[k][0],
			         	top: posi[k][1]
			         });
			    }
			}
			<?php if ($platform=="iOS") { ?>
			setTimeout(function() {
				html2canvas($('.page'), {
					onrendered: function(canvas) {
				    	$('#export-box').html(canvas);
						$('.page').css('display','none');
				   	 	toPDF();
					}
				});
			}, 500);
			<?php } ?>
			<?php if ($debug): ?>
			showlabel()	
			<?php endif ?>
			<?php if ($save==1): ?>
			save()
			<?php endif ?>
		}
		
		function setJs(ui) {
			$.ajax({
				url: base_url+'/screening/jsSave/nt',
				type: 'POST',
				data: {posi: posi},
			})
			.done(function(respon) {
				console.log("success");
				console.log(respon);
			})
			.fail(function() {
				console.log("error");
			})
		}

		function showlabel() {
			//show label
			label = $('label')
			for (var i = label.length - 1; i >= 0; i--) {
				$(label[i]).prepend('<label class="'+$(label[i]).attr('class').split(' ')[0]+' sub">'+$(label[i]).attr('class').split(' ')[0]+'</label>')
			}
		}
	</script>
</body>
</html>