<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
<head>
	<title>Main</title>
	<?php $this->load->view('_config'); ?>
</head>
<body>
	<?php $this->load->view('_header'); ?>
	<div class="container patient">
		<div class="row">
			<?php $this->load->view('_left_bar.php'); ?>
			<div class="col-md-10 col-xs-12 content">
				<div class="row green_2 top-bar">
					<div class="col-md-10 col-xs-6 back"></div>
					<div class="col-md-2 col-xs-6 score"></div>
				</div>
				<div class="row">
					<div class="col-md-8 col-xs-12 title">เลือกหอผู้ป่วย > <a href="<?=base_url()?>/patients/lists/<?=$data['screening'][0]['patient_ward_id']?>" style="color: #32439f;"><?=$data['screening'][0]['patient_ward_title']?></a> > <?=$data['screening'][0]['hn_code']?></div>
					<div class="col-md-4 col-xs-12 btn-bar"></div>
				</div>
				<div class="row">
					<div class="col-xs-12 assessment-box">
						<div class="row box table-screening">
							<div class="header blue_3">
								<div class="col-sm-6 col-xs-12">ข้อมูลผู้ป่วย</div>
							</div>
							<div class="body">
								<div class="col-sm-12 col-xs-12">
									<div class="col-sm-4 col-xs-12 form-group">
										<label>HN</label>
										<div class="detail"><?=$data['screening'][0]['hn_code']?></div>
									</div>
									<div class="col-sm-4 col-xs-12 form-group">
										<label>ชื่อผู้ป่วย</label>
										<div class="detail"><?=$data['screening'][0]['name']?></div>
									</div>
									<div class="col-sm-4 col-xs-12 form-group">
										<label>การวินิจฉัยโรค</label>
										<div class="detail"><?=$data['screening'][0]['diagnose_title']?></div>
									</div>
									<div class="col-sm-4 col-xs-12 form-group">
										<label>วันที่เข้ารับการรักษา</label>
										<div class="detail"><?=date_format(date_create($data['screening'][0]['date_admission']),'d/m/Y')?></div>
									</div>
									<div class="col-sm-4 col-xs-12 form-group">
										<label>วันที่ต้องออกจากโรงพยาบาล</label>
										<div class="detail">ยังไม่มีข้อมูล</div>
									</div>
									<div class="col-sm-4 col-xs-12 form-group">
										<label>จำนวนวันที่ได้รับการรักษา</label>
										<div class="detail"><?=$date_passing?> วัน</div>
									</div>
								</div>
							</div>

							<div class="row box table-screening">
								<div class="header blue_3">
									<div class="col-sm-6 col-xs-12">การประเมินที่ต้องทำ</div>
								</div>
								<div class="body">
									<div class="col-sm-12 col-xs-12">
										<div class="col-sm-1 col-xs-12 form-group">
											<label></label>
										</div>
										<div class="col-sm-6 col-xs-12 form-group">
											<label>รายการที่ต้องทำ</label>
										</div>
										<div class="col-sm-5 col-xs-12 form-group">
											<label>ตางรางนัดหมาย</label>
										</div>
										<?php if (isset($data['assessment_naf'])) { ?>
										<div class="border">
											<div class="col-sm-1 col-xs-12 form-group">
												<input type="checkbox" name="" class="mtl filled-in" id="check_1">
												<label for="check_1"></label>
											</div>
											<div class="col-sm-6 col-xs-12 form-group">
												<label>Assessment NAF ครั้งที่ <?=count($data['assessment_naf'])+1?></label>
											</div>
											<div class="col-sm-5 col-xs-12 form-group">
												<label><?=date_format(date_create($data['assessment_naf'][0]['date_next']),'d/m/Y')?></label>
											</div>
										</div>
										<? } ?>
										<?php if (isset($data['assessment_nt'])) { ?>
										<div class="border">
											<div class="col-sm-1 col-xs-12 form-group">
												<input type="checkbox" name="" class="mtl filled-in" id="check_1">
												<label for="check_1"></label>
											</div>
											<div class="col-sm-6 col-xs-12 form-group">
												<label>Assessment NT ครั้งที่ <?=count($data['assessment_nt'])+1?></label>
											</div>
											<div class="col-sm-5 col-xs-12 form-group">
												<label><?=date_format(date_create($data['assessment_nt'][0]['date_next']),'d/m/Y')?></label>
											</div>
										</div>
										<? } ?>
									</div>
								</div>
							</div>

							<div class="row box table-screening">
								<div class="header blue_3">
									<div class="col-sm-6 col-xs-6">ผลการประเมินที่ผ่านมา</div>
									<div class="col-sm-6 col-xs-6 text-right"><a href="<?=base_url().'screening/test/'.$data['screening'][0]['screening_id']?>"><i class="fa fa-print"></i> Print / Export</a></div>
								</div>
								<div class="body">
									<div class="col-sm-12 col-xs-12">
										<ul class="nav nav-tabs" id="myTabs" role="tablist">
											<li style="width: 33%;" class="text-center active" role="presentation" aria-controls="screening" role="tab" data-toggle="tab"><a href="#screening">Screening Results</a></li>
											<li style="width: 33%;" class="text-center" role="presentation" aria-controls="naf" role="tab" data-toggle="tab"><a href="#naf">Assessment : NAF</a></li>
											<li style="width: 33%;" class="text-center" role="presentation" aria-controls="nt" role="tab" data-toggle="tab"><a href="#nt">Assessment : NT</a></li>
										</ul>
										<div class="tab-content">
											<div role="tabpanel" class="tab-pane active" id="screening">
												<?php $screening_count = count($data['screening']); ?>
												<h3>Screening</h3>
												<p>Detail : </p>
												<div class="panel">
													<div class="col-xs-2">#</div>
												</div>
												<!-- <table class="table table-border table-fixed">
													<col style="width: 500px"></col>
													<col></col>
													<tr>
														<th>Issue</th>
														<td rowspan="9" style="padding: 0;">
															<div class="sub-table" style="">
																<table class="table table-border table-fixed">
																	<?php for ($i=0; $i < $screening_count; $i++) { ?>
																	<col style="width: 50px"></col>
																	<col style="width: 50px"></col>
																	<?php } ?>
																	<tr>
																		<th colspan="<?=$screening_count*2?>">monitoring</th>
																	</tr>
																	<tr>
																	<?php foreach ($data['screening'] as $key => $value): ?>
																		<td colspan="2"><?=date_format(date_create($value['date']),'d/m/Y')?></td>
																	<?php endforeach ?>
																	</tr>
																	<tr>
																	<?php for ($i=0; $i < $screening_count; $i++) { ?>
																		<td>ใช่</td>
																		<td>ไม่ใช่</td>
																	<?php } ?>
																	</tr>
																	<tr>
																	<?php 
																		foreach ($data['screening'] as $key => $value){
																			$c_1 = $value['c_1']==1 ? '<td><label class="circle"></label></td><td></td>' : '<td></td>
																			<td><label class="circle"></label></td>' ;
																			echo $c_1;
																		}
																	?>
																	</tr>
																	<?php 
																		foreach ($data['screening'] as $key => $value){
																			$c_2 = $value['c_2']==1 ? '<td><label class="circle"></label></td><td></td>' : '<td></td>
																			<td><label class="circle"></label></td>' ;
																			echo $c_2;
																		}
																	?>
																	<tr>
																	<?php 
																		foreach ($data['screening'] as $key => $value){
																			$c_3 = $value['c_3']==1 ? '<td><label class="circle"></label></td><td></td>' : '<td></td>
																			<td><label class="circle"></label></td>' ;
																			echo $c_3;
																		}
																	?>
																	</tr>
																	<tr>
																	<?php 
																		foreach ($data['screening'] as $key => $value){
																			$c_4 = $value['c_4']==1 ? '<td><label class="circle"></label></td><td></td>' : '<td></td>
																			<td><label class="circle"></label></td>' ;
																			echo $c_4;
																		}
																	?>
																	</tr>
																	<tr>
																	<?php 
																		foreach ($data['screening'] as $key => $value){
																			$c_yes = $value['c_1']+$value['c_2']+$value['c_3']+$value['c_4'];
																			$c_no = 4-$c_yes;
																	?>
																		<td><?=$c_yes?></td>
																		<td><?=$c_no?></td>
																	<?php } ?>
																	</tr>
																	<tr>
																	<?php 
																		foreach ($data['screening'] as $key => $value){
																			$c_yes = $value['c_1']+$value['c_2']+$value['c_3']+$value['c_4'];
																			if ($c_yes<=1) {
																				echo '<td colspan="2"><label class="green_3" style="margin-bottom: 0px">Screening</label></td>';
																			}else{
																				echo '<td colspan="2"><label class="red_1" style="margin-bottom: 0px">Assessment</label></td>';
																			}
																	?>
																	<?php } ?>
																	</tr>
																</table>
															</div>
														</td>
													</tr>
													<tr>
														<td>วันที่ประเมิน</td>
													</tr>
													<tr>
														<td>&nbsp;</td>
													</tr>
													<tr>
														<td>1. ผู้ป่วยมีน้ำหนักตัวลดลง โดยไม่ได้ตั้งใจในช่วง 6 เดือนที่ผ่านมาหรือไม่</td>
													</tr>
													<tr>
														<td>2. ผู้ป่วยได้รับอาหารน้อยกว่าที่เคยได้ (เกินกว่า 7 วัน)</td>
													</tr>
													<tr>
														<td>3. BMI < 18.5 หรือ > 25.0 Kg/m2 หรือไม่</td>
													</tr>
													<tr>
														<td>4. ผู้ป่วยมรภาวะโรควิกฤต หรือกึ่งวิกฤตร่วมด้วยหรือไม่</td>
													</tr>
													<tr>
														<td>คะแนนที่ได้</td>
													</tr>
													<tr>
														<td>ผลการประเมิน</td>
													</tr>
												</table> -->
											</div>
											<div role="tabpanel" class="tab-pane" id="naf">
												<?php 
													$assessment_naf_count = isset($data['assessment_naf']) ? count($data['assessment_naf']) : '0';
												?>
												<h3>Assessment : NAF</h3>
												<p>Detail : </p>
												<!-- <table class="table table-border table-fixed head-naf">
													<col style="width: 500px"></col>
													<col></col>
													<tr>
														<th>Issue</th>
														<td rowspan="53" style="padding: 0;">
															<div class="sub-table" style="">
																<?php if ($assessment_naf_count!=0): ?>
																<table class="table table-border table-fixed sub-naf">
																	<?php for ($i=0; $i < $assessment_naf_count; $i++) { ?>
																	<col width="70px"></col>
																	<col width="70px"></col>
																	<?php } ?>
																	<tr>
																		<th colspan="<?=$assessment_naf_count*2?>">monitoring</th>
																	</tr>
																	<tr>
																	<?php foreach ($data['assessment_naf'] as $key => $value): ?>
																		<td colspan="2"><?=date_format(date_create($value['date']),'d/m/Y')?></td>
																	<?php endforeach ?>
																	</tr>
																	<tr>
																	<?php for ($i=0; $i < $assessment_naf_count; $i++) { ?>
																		<td>คำตอบ</td>
																		<td>คะแนน</td>
																	<?php } ?>
																	</tr>
																	<tr>
																	<?php for ($i=0; $i < $assessment_naf_count; $i++) { ?>
																		<td><label></label></td>
																		<td></td>
																	<?php } ?>
																	</tr>
																	<tr>
																	<?php foreach ($data['assessment_naf'] as $key => $value): ?>
																		<td><?=$value['n1_1']?></td>
																		<td></td>
																	<?php endforeach ?>
																	</tr>
																	<tr>
																	<?php foreach ($data['assessment_naf'] as $key => $value): ?>
																		<td><?=$value['n1_2']?></td>
																		<td></td>
																	<?php endforeach ?>
																	</tr>
																	<tr>
																	<?php foreach ($data['assessment_naf'] as $key => $value): ?>
																		<td><?=$value['n1_3']?></td>
																		<td></td>
																	<?php endforeach ?>
																	</tr>
																	<tr>
																	<?php for ($i=0; $i < $assessment_naf_count; $i++) { ?>
																		<td><label></label></td>
																		<td></td>
																	<?php } ?>
																	</tr>
																	<tr>
																	<?php foreach ($data['assessment_naf'] as $key => $value): ?>
																		<td><?=$value['n2_1_weight']?></td>
																		<td></td>
																	<?php endforeach ?>
																	</tr>
																	<tr>
																	<?php 
																		foreach ($data['assessment_naf'] as $key => $value): 
																			$txt = array(
																				'1' => 'ชั่งในท่านอน', 
																				'2' => 'ชั่งในท่ายืน', 
																				'3' => 'ชั่งไม่ได้', 
																				'4' => 'ญาติบอก', 
																			);
																			$score = array(
																				'1' => 1, 
																				'2' => 0, 
																				'3' => 0, 
																				'4' => 0, 
																			);
																	?>
																		<td><?=$txt[$value['n2_1']]?></td>
																		<td><?=$score[$value['n2_1']]?></td>
																	<?php endforeach ?>
																	</tr>
																	<tr>
																	<?php 
																		foreach ($data['assessment_naf'] as $key => $value): 
																			$bmi = round($value['n2_1_weight']/pow($value['n1_1']/100,2),2);
																			$score = array(
																				'0' => 0, 
																				'1' => 2, 
																				'2' => 1, 
																				'3' => 0, 
																				'4' => 1, 
																			);
																	?>
																		<td><?=$bmi?></td>
																		<td><?=$score[$value['n2_2']]?></td>
																	<?php endforeach ?>
																	</tr>
																	<tr>
																	<?php 
																		foreach ($data['assessment_naf'] as $key => $value): 
																			$txt = array(
																				'0' => '', 
																				'1' => '<= 2.5 g/dl', 
																				'2' => '2.6 - 2.9 g/dl', 
																				'3' => '3.0 - 3.5 g/dl', 
																				'4' => '> 3.5 g/dl', 
																			);
																			$score = array(
																				'0' => 0, 
																				'1' => 3, 
																				'2' => 2, 
																				'3' => 1, 
																				'4' => 0, 
																			);
																	?>
																		<td><?=$txt[$value['n2_3']]?></td>
																		<td><?=$score[$value['n2_3']]?></td>
																	<?php endforeach ?>
																	</tr>
																	<tr>
																	<?php 
																		foreach ($data['assessment_naf'] as $key => $value): 
																			$txt = array(
																				'0' => '', 
																				'1' => '<= 1,000 cells/mm3', 
																				'2' => '1,001-1,200 cells/mm3', 
																				'3' => '1,201-1,500 cells/mm3', 
																				'4' => '> 1,501 cells/mm3', 
																			);
																			$score = array(
																				'0' => 0, 
																				'1' => 3, 
																				'2' => 2, 
																				'3' => 1, 
																				'4' => 0, 
																			);
																	?>
																		<td><?=$txt[$value['n2_4']]?></td>
																		<td><?=$score[$value['n2_4']]?></td>
																	<?php endforeach ?>
																	</tr>
																	<tr>
																	<?php 
																		foreach ($data['assessment_naf'] as $key => $value): 
																			$txt = array(
																				'1' => 'ผอมมาก', 
																				'2' => 'ผอม', 
																				'3' => 'อ้วนมาก', 
																				'4' => 'ปกติ-อ้วนปานกลาง', 
																			);
																			$score = array(
																				'1' => 2, 
																				'2' => 1, 
																				'3' => 1, 
																				'4' => 0, 
																			);
																	?>
																		<td><?=$txt[$value['n3_1']]?></td>
																		<td><?=$score[$value['n3_1']]?></td>
																	<?php endforeach ?>
																	</tr>
																	<tr>
																	<?php 
																		foreach ($data['assessment_naf'] as $key => $value): 
																			$txt = array(
																				'1' => 'ลดลง / ผอมลง', 
																				'2' => 'เพิ่มขึ้น / อ้วนขึ้น', 
																				'3' => 'เพิ่มขึ้น / อ้วนขึ้น', 
																				'4' => 'คงเดิม', 
																			);
																			$score = array(
																				'1' => 2, 
																				'2' => 1, 
																				'3' => 1, 
																				'4' => 0, 
																			);
																	?>
																		<td><?=$txt[$value['n4_1']]?></td>
																		<td><?=$score[$value['n4_1']]?></td>
																	<?php endforeach ?>
																	</tr>
																	<tr>
																	<?php for ($i=0; $i < $assessment_naf_count; $i++) { ?>
																		<td><label></label></td>
																		<td></td>
																	<?php } ?>
																	</tr>
																	<tr>
																	<?php 
																		foreach ($data['assessment_naf'] as $key => $value): 
																			$txt = array(
																				'1' => 'อาหารน้ำๆ', 
																				'2' => 'อาหารเหลวๆ', 
																				'3' => 'อาหารนุ่มกว่าปกติ', 
																				'4' => 'อาหารเหมือนปกติ', 
																			);
																			$score = array(
																				'1' => 2, 
																				'2' => 2, 
																				'3' => 1, 
																				'4' => 0, 
																			);
																	?>
																		<td><?=$txt[$value['n5_1']]?></td>
																		<td><?=$score[$value['n5_1']]?></td>
																	<?php endforeach ?>
																	</tr>
																	<tr>
																	<?php 
																		foreach ($data['assessment_naf'] as $key => $value): 
																			$txt = array(
																				'1' => 'กินน้อยมาก', 
																				'2' => 'กินน้อยลง', 
																				'3' => 'กินมากขึ้น', 
																				'4' => 'กินเท่าปกติ', 
																			);
																			$score = array(
																				'1' => 2, 
																				'2' => 1, 
																				'3' => 0, 
																				'4' => 0, 
																			);
																	?>
																		<td><?=$txt[$value['n5_2']]?></td>
																		<td><?=$score[$value['n5_2']]?></td>
																	<?php endforeach ?>
																	</tr>
																	<tr>
																	<?php for ($i=0; $i < $assessment_naf_count; $i++) { ?>
																		<td><label></label></td>
																		<td></td>
																	<?php } ?>
																	</tr>
																	<tr>
																	<?php for ($i=0; $i < $assessment_naf_count; $i++) { ?>
																		<td><label></label></td>
																		<td></td>
																	<?php } ?>
																	</tr>
																	<tr>
																	<?php foreach ($data['assessment_naf'] as $key => $value): ?>
																		<td>
																		<?php echo $value['n6_1_1']==1 ? '<label class="circle"></label>' : '<label></label>'; ?>
																		</td>
																		<td><?php echo $value['n6_1_1']==1 ? 2 : 0; ?></td>
																	<?php endforeach ?>
																	</tr>
																	<tr>
																	<?php foreach ($data['assessment_naf'] as $key => $value): ?>
																		<td>
																		<?php echo $value['n6_1_2']==1 ? '<label class="circle"></label>' : '<label></label>'; ?>	
																		</td>
																		<td><?php echo $value['n6_1_2']==1 ? 0 : 0; ?></td>
																	<?php endforeach ?>
																	</tr>
																	<tr>
																	<?php foreach ($data['assessment_naf'] as $key => $value): ?>
																		<td>
																		<?php echo $value['n6_1_3']==1 ? '<label class="circle"></label>' : '<label></label>'; ?>
																		</td>
																		<td><?php echo $value['n6_1_3']==1 ? 2 : 0; ?></td>
																	<?php endforeach ?>
																	</tr>
																	<tr>
																	<?php for ($i=0; $i < $assessment_naf_count; $i++) { ?>
																		<td><label></label></td>
																		<td></td>
																	<?php } ?>
																	</tr>
																	<tr>
																	<?php foreach ($data['assessment_naf'] as $key => $value): ?>
																		<td>
																		<?php echo $value['n6_2_1']==1 ? '<label class="circle"></label>' : '<label></label>'; ?>
																		</td>
																		<td><?php echo $value['n6_2_1']==1 ? 2 : 0; ?></td>
																	<?php endforeach ?>
																	</tr>
																	<tr>
																	<?php foreach ($data['assessment_naf'] as $key => $value): ?>
																		<td>
																		<?php echo $value['n6_2_2']==1 ? '<label class="circle"></label>' : '<label></label>'; ?>	
																		</td>
																		<td><?php echo $value['n6_2_2']==1 ? 2 : 0; ?></td>
																	<?php endforeach ?>
																	</tr>
																	<tr>
																	<?php foreach ($data['assessment_naf'] as $key => $value): ?>
																		<td>
																		<?php echo $value['n6_2_3']==1 ? '<label class="circle"></label>' : '<label></label>'; ?>
																		</td>
																		<td><?php echo $value['n6_2_3']==1 ? 0 : 0; ?></td>
																	<?php endforeach ?>
																	</tr>
																	<tr>
																	<?php for ($i=0; $i < $assessment_naf_count; $i++) { ?>
																		<td><label></label></td>
																		<td></td>
																	<?php } ?>
																	</tr>
																	<tr>
																	<?php foreach ($data['assessment_naf'] as $key => $value): ?>
																		<td>
																		<?php echo $value['n6_3_1']==1 ? '<label class="circle"></label>' : '<label></label>'; ?>
																		</td>
																		<td><?php echo $value['n6_3_1']==1 ? 2 : 0; ?></td>
																	<?php endforeach ?>
																	</tr>
																	<tr>
																	<?php foreach ($data['assessment_naf'] as $key => $value): ?>
																		<td>
																		<?php echo $value['n6_3_2']==1 ? '<label class="circle"></label>' : '<label></label>'; ?>	
																		</td>
																		<td><?php echo $value['n6_3_2']==1 ? 2 : 0; ?></td>
																	<?php endforeach ?>
																	</tr>
																	<tr>
																	<?php foreach ($data['assessment_naf'] as $key => $value): ?>
																		<td>
																		<?php echo $value['n6_3_3']==1 ? '<label class="circle"></label>' : '<label></label>'; ?>
																		</td>
																		<td><?php echo $value['n6_3_3']==1 ? 0 : 0; ?></td>
																	<?php endforeach ?>
																	</tr>
																	<tr>
																	<?php 
																		foreach ($data['assessment_naf'] as $key => $value): 
																			$txt = array(
																				'1' => 'นอนติดเตียง', 
																				'2' => 'ต้องมีผู้ช่วยบ้าง', 
																				'3' => 'นั่งๆ นอนๆ', 
																				'4' => 'ปกติ', 
																			);
																			$score = array(
																				'1' => 2, 
																				'2' => 1, 
																				'3' => 0, 
																				'4' => 0, 
																			);
																	?>
																		<td><?=$txt[$value['n7_1']]?></td>
																		<td><?=$score[$value['n7_1']]?></td>
																	<?php endforeach ?>
																	</tr>
																	<tr>
																	<?php for ($i=0; $i < $assessment_naf_count; $i++) { ?>
																		<td><label></label></td>
																		<td></td>
																	<?php } ?>
																	</tr>
																	<tr>
																	<?php foreach ($data['assessment_naf'] as $key => $value): ?>
																		<td>
																		<?php echo $value['n8_1_1']==1 ? '<label class="circle"></label>' : '<label></label>'; ?>
																		</td>
																		<td><?php echo $value['n8_1_1']==1 ? 3 : 0; ?></td>
																	<?php endforeach ?>
																	</tr>
																	<tr>
																	<?php foreach ($data['assessment_naf'] as $key => $value): ?>
																		<td>
																		<?php echo $value['n8_1_2']==1 ? '<label class="circle"></label>' : '<label></label>'; ?>
																		</td>
																		<td><?php echo $value['n8_1_2']==1 ? 3 : 0; ?></td>
																	<?php endforeach ?>
																	</tr>
																	<tr>
																	<?php foreach ($data['assessment_naf'] as $key => $value): ?>
																		<td>
																		<?php echo $value['n8_1_3']==1 ? '<label class="circle"></label>' : '<label></label>'; ?>
																		</td>
																		<td><?php echo $value['n8_1_3']==1 ? 3 : 0; ?></td>
																	<?php endforeach ?>
																	</tr>
																	<tr>
																	<?php foreach ($data['assessment_naf'] as $key => $value): ?>
																		<td>
																		<?php echo $value['n8_1_4']==1 ? '<label class="circle"></label>' : '<label></label>'; ?>
																		</td>
																		<td><?php echo $value['n8_1_4']==1 ? 3 : 0; ?></td>
																	<?php endforeach ?>
																	</tr>
																	<tr>
																	<?php foreach ($data['assessment_naf'] as $key => $value): ?>
																		<td>
																		<?php echo $value['n8_1_5']==1 ? '<label class="circle"></label>' : '<label></label>'; ?>
																		</td>
																		<td><?php echo $value['n8_1_5']==1 ? 3 : 0; ?></td>
																	<?php endforeach ?>
																	</tr>
																	<tr>
																	<?php foreach ($data['assessment_naf'] as $key => $value): ?>
																		<td>
																		<?php echo $value['n8_1_6']==1 ? '<label class="circle"></label>' : '<label></label>'; ?>
																		</td>
																		<td><?php echo $value['n8_1_6']==1 ? 3 : 0; ?></td>
																	<?php endforeach ?>
																	</tr>
																	<tr>
																	<?php foreach ($data['assessment_naf'] as $key => $value): ?>
																		<td>
																		<?php echo $value['n8_1_7']==1 ? '<label class="circle"></label>' : '<label></label>'; ?>
																		</td>
																		<td><?php echo $value['n8_1_7']==1 ? 3 : 0; ?></td>
																	<?php endforeach ?>
																	</tr>
																	<tr>
																	<?php foreach ($data['assessment_naf'] as $key => $value): ?>
																		<td>
																		<?php echo $value['n8_1_8']==1 ? '<label class="circle"></label>' : '<label></label>'; ?>
																		</td>
																		<td><?php echo $value['n8_1_8']==1 ? 3 : 0; ?></td>
																	<?php endforeach ?>
																	</tr>
																	<tr>
																	<?php foreach ($data['assessment_naf'] as $key => $value): ?>
																		<td>
																		<?php echo $value['n8_1_9']==1 ? '<label class="circle"></label>' : '<label></label>'; ?>
																		</td>
																		<td><?php echo $value['n8_1_9']==1 ? 3 : 0; ?></td>
																	<?php endforeach ?>
																	</tr>
																	<tr>
																	<?php foreach ($data['assessment_naf'] as $key => $value): ?>
																		<td>
																		<?php echo $value['n8_1_10']==1 ? '<label class="circle"></label>' : '<label></label>'; ?>
																		</td>
																		<td><?php echo $value['n8_1_10']==1 ? 3 : 0; ?></td>
																	<?php endforeach ?>
																	</tr>
																	<tr>
																	<?php foreach ($data['assessment_naf'] as $key => $value): ?>
																		<td>
																		<?php echo $value['n8_1_11']==1 ? '<label class="circle"></label>' : '<label></label>'; ?>
																		</td>
																		<td><?php echo $value['n8_1_11']==1 ? 3 : 0; ?></td>
																	<?php endforeach ?>
																	</tr>
																	<tr>
																	<?php for ($i=0; $i < $assessment_naf_count; $i++) { ?>
																		<td><label></label></td>
																		<td></td>
																	<?php } ?>
																	</tr>
																	<tr>
																	<?php foreach ($data['assessment_naf'] as $key => $value): ?>
																		<td>
																		<?php echo $value['n8_2_1']==1 ? '<label class="circle"></label>' : '<label></label>'; ?>
																		</td>
																		<td><?php echo $value['n8_2_1']==1 ? 6 : 0; ?></td>
																	<?php endforeach ?>
																	</tr>
																	<tr>
																	<?php foreach ($data['assessment_naf'] as $key => $value): ?>
																		<td>
																		<?php echo $value['n8_2_2']==1 ? '<label class="circle"></label>' : '<label></label>'; ?>
																		</td>
																		<td><?php echo $value['n8_2_2']==1 ? 6 : 0; ?></td>
																	<?php endforeach ?>
																	</tr>
																	<tr>
																	<?php foreach ($data['assessment_naf'] as $key => $value): ?>
																		<td>
																		<?php echo $value['n8_2_3']==1 ? '<label class="circle"></label>' : '<label></label>'; ?>
																		</td>
																		<td><?php echo $value['n8_2_3']==1 ? 6 : 0; ?></td>
																	<?php endforeach ?>
																	</tr>
																	<tr>
																	<?php foreach ($data['assessment_naf'] as $key => $value): ?>
																		<td>
																		<?php echo $value['n8_2_4']==1 ? '<label class="circle"></label>' : '<label></label>'; ?>
																		</td>
																		<td><?php echo $value['n8_2_4']==1 ? 6 : 0; ?></td>
																	<?php endforeach ?>
																	</tr>
																	<tr>
																	<?php foreach ($data['assessment_naf'] as $key => $value): ?>
																		<td>
																		<?php echo $value['n8_2_5']==1 ? '<label class="circle"></label>' : '<label></label>'; ?>
																		</td>
																		<td><?php echo $value['n8_2_5']==1 ? 6 : 0; ?></td>
																	<?php endforeach ?>
																	</tr>
																	<tr>
																	<?php foreach ($data['assessment_naf'] as $key => $value): ?>
																		<td>
																		<?php echo $value['n8_2_6']==1 ? '<label class="circle"></label>' : '<label></label>'; ?>
																		</td>
																		<td><?php echo $value['n8_2_6']==1 ? 6 : 0; ?></td>
																	<?php endforeach ?>
																	</tr>
																	<tr>
																	<?php foreach ($data['assessment_naf'] as $key => $value): ?>
																		<td></td>
																		<td><?=$value['score']?></td>
																	<?php endforeach ?>
																	</tr>
																	<tr>
																	<?php foreach ($data['assessment_naf'] as $key => $value): ?>
																		<td colspan="2"><?=$value['grade']?></td>
																	<?php endforeach ?>
																	</tr>
																</table>
																<?php endif ?>
															</div>
														</td>
													</tr>
													<tr>
														<td>วันที่ประเมิน</td>
													</tr>
													<tr>
														<td>&nbsp;</td>
													</tr>
													<tr>
														<td>ส่วนสูง / ความยาวตัว / ความยาวช่วงแขน</td>
													</tr>
													<tr>
														<td style="padding-left: 30px">ส่วนสูง</td>
													</tr>
													<tr>
														<td style="padding-left: 30px">ความยาวตัว</td>
													</tr>
													<tr>
														<td style="padding-left: 30px">ความยาวช่วงแขน</td>
													</tr>
													<tr>
														<td>น้ำหนักและค่าดัชนีมวลกาย</td>
													</tr>
													<tr>
														<td style="padding-left: 30px">น้ำหนัก</td>
													</tr>
													<tr>
														<td style="padding-left: 30px">ประเมินโดย</td>
													</tr>
													<tr>
														<td style="padding-left: 30px">BMI</td>
													</tr>
													<tr>
														<td style="padding-left: 30px">ผล Albumin</td>
													</tr>
													<tr>
														<td style="padding-left: 30px">ผล TLC (Total Lymphocyte Count)</td>
													</tr>
													<tr>
														<td>รูปร่างของผู้ป่วย</td>
													</tr>
													<tr>
														<td>น้ำหนักที่เปลี่ยนไปใน 4 สัปดาห์</td>
													</tr>
													<tr>
														<td>อาหารที่กินในช่วง 2 สัปดาห์</td>
													</tr>
													<tr>
														<td style="padding-left: 30px">ลักษณะอาหาร</td>
													</tr>
													<tr>
														<td style="padding-left: 30px">ปริมาณอาหาร</td>
													</tr>
													<tr>
														<td>อาการต่อเนื่อง</td>
													</tr>
													<tr>
														<td style="padding-left: 30px">ปัญหาทางการเคี้ยว</td>
													</tr>
													<tr>
														<td style="padding-left: 60px">สำลัก</td>
													</tr>
													<tr>
														<td style="padding-left: 60px">กลืนได้ปกติ</td>
													</tr>
													<tr>
														<td style="padding-left: 60px">เคี้ยว/กลืนลำบาก/ได้อาหารทางสายยาง</td>
													</tr>
													<tr>
														<td style="padding-left: 30px">ปัญหาระบบทางเดินอาหาร</td>
													</tr>
													<tr>
														<td style="padding-left: 60px">ท้องเสีย</td>
													</tr>
													<tr>
														<td style="padding-left: 60px">ปวดท้อง</td>
													</tr>
													<tr>
														<td style="padding-left: 60px">ปกติ</td>
													</tr>
													<tr>
														<td style="padding-left: 30px">ปัญหาระหว่างกินอาหาร</td>
													</tr>
													<tr>
														<td style="padding-left: 60px">อาเจียน</td>
													</tr>
													<tr>
														<td style="padding-left: 60px">คลื่นไส้</td>
													</tr>
													<tr>
														<td style="padding-left: 60px">ปกติ</td>
													</tr>
													<tr>
														<td>ความสามารถในการเข้าถึงอาหาร</td>
													</tr>
													<tr>
														<td>โรคที่เป็นอยู่</td>
													</tr>
													<tr>
														<td style="padding-left: 30px">DM (เบาหวาน)</td>
													</tr>
													<tr>
														<td style="padding-left: 30px">CKD-ESRD (ไตเรื้อรัง)</td>
													</tr>
													<tr>
														<td style="padding-left: 30px">Solid Cancer (มะเร็งทั่วไป)</td>
													</tr>
													<tr>
														<td style="padding-left: 30px">Septicemia (ติดเชื้อในกระแสเลือด)</td>
													</tr>
													<tr>
														<td style="padding-left: 30px">Chronic heart failure (หัวใจล้มเหลวเรื้อรัง)</td>
													</tr>
													<tr>
														<td style="padding-left: 30px">Hip fracture (ข้อสะโพกหัก)</td>
													</tr>
													<tr>
														<td style="padding-left: 30px">COPD (ปอดอุดกั้นเรื้อรัง)</td>
													</tr>
													<tr>
														<td style="padding-left: 30px">Severe head injury (บาดเจ็บที่ศีรษะรุนแรง)</td>
													</tr>
													<tr>
														<td style="padding-left: 30px">>= 2o of burnn (แผลไฟไหม้ระดับ 2 ขึ้นไป)</td>
													</tr>
													<tr>
														<td style="padding-left: 30px">CLD/Cirrhosis/Hepati cencaph (ตับเรื้อรั้ง)</td>
													</tr>
													<tr>
														<td style="padding-left: 30px">อื่นๆ</td>
													</tr>
													<tr>
														<td>โรคที่มีความรุนแรงน้อยถึงปานกลาง</td>
													</tr>
													<tr>
														<td style="padding-left: 30px">Severe pneumonia (ปวดบวมขั้นรุงแรง)</td>
													</tr>
													<tr>
														<td style="padding-left: 30px">Critically ill (ผู้ป่วยวิกฤติ)</td>
													</tr>
													<tr>
														<td style="padding-left: 30px">Multiple fracture (กระดูกหักหลายตำแหน่ง)</td>
													</tr>
													<tr>
														<td style="padding-left: 30px">Stroke/CVA (อำมพาต)</td>
													</tr>
													<tr>
														<td style="padding-left: 30px">Malignant hematologic disease/Bone marrow transplant (มะเร็งเม็ดเลือด/ปลูกถ่ายไขกระดูก)</td>
													</tr>
													<tr>
														<td style="padding-left: 30px">อื่นๆ </td>
													</tr>
													<tr>
														<td>คะแนนรวมที่ได้</td>
													</tr>
													<tr>
														<td>ผลการประเมิน</td>
													</tr>
												</table> -->
												<script type="text/javascript">
													$(document).ready(function() {
														var head_naf = $('.head-naf tr')
														for (var i =56; i < head_naf.length; i++) {
															var tmp_head = $(head_naf[i])
															var tmp_sub = $('.sub-naf tr')[i-53]
															$(tmp_sub).height($(tmp_head).height())
														}
													});
												</script>
												<div style="height: 300px;position: relative;">
													<canvas id="myChart_naf"></canvas>
												</div>
												<script>
													<?php
														$score_ar = array();
														$date_ar = array();
														foreach ($data['assessment_naf'] as $key => $value) {
															array_push($score_ar, $value['score']);
															array_push($date_ar, date_format(date_create($value['date']),'d/m/Y'));
														}
													?>
													var ctx_naf = document.getElementById("myChart_naf");
													var myChart_naf = new Chart(ctx_naf, {
														type: 'bar',
														data: {
															labels: ["<?=implode($date_ar,'","')?>"],
															datasets: [{
																label: 'points of times',
																data: [<?=implode($score_ar,',')?>],
																backgroundColor: [
																'rgba(234, 80, 80, 1)',
																'rgba(255, 137, 61, 1)',
																],
																borderWidth: 1
															}]
														},
														options: {
															maintainAspectRatio: false,
															scales: {
																yAxes: [{
																	ticks: {
																		beginAtZero:true
																	},
																	scaleLabel: {
																		display: true,
																		labelString: 'Points'
																	}
																}],
																xAxes: [{
																	ticks: {
																		beginAtZero:true
																	},
																	scaleLabel: {
																		display: true,
																		labelString: 'Times'
																	}
																}]
															}
														}
													});
												</script>
											</div>
											<div role="tabpanel" class="tab-pane" id="nt">
												<?php $assessment_nt_count = isset($data['assessment_nt']) ? count($data['assessment_nt']) : '0'; ?>
												<h3>Assessment : NT</h3>
												<p>Detail : </p>
												<!-- <table class="table table-border table-fixed head-nt">
													<col style="width: 500px"></col>
													<col></col>
													<tr>
														<th>Issue</th>
														<td rowspan="35" style="padding: 0;">
															<div class="sub-table" style="">
																<?php if ($assessment_nt_count!=0): ?>
																<table class="table table-border table-fixed sub-nt">
																	<?php for ($i=0; $i < $assessment_nt_count; $i++) { ?>
																	<col width="150px"></col>
																	<col width="70px"></col>
																	<?php } ?>
																	<tr>
																		<th colspan="<?=$assessment_nt_count*2?>">monitoring</th>
																	</tr>
																	<tr>
																	<?php foreach ($data['assessment_nt'] as $key => $value): ?>
																		<td colspan="2"><?=date_format(date_create($value['date']),'d/m/Y')?></td>
																	<?php endforeach ?>
																	</tr>
																	<tr>
																	<?php for ($i=0; $i < $assessment_nt_count; $i++) { ?>
																		<td>คำตอบ</td>
																		<td>คะแนน</td>
																	<?php } ?>
																	</tr>
																	<tr>
																	<?php for ($i=0; $i < $assessment_nt_count; $i++) { ?>
																		<td><label></label></td>
																		<td></td>
																	<?php } ?>
																	</tr>
																	<tr>
																	<?php 
																		foreach ($data['assessment_nt'] as $key => $value): 
																			$txt = array(
																				'1' => 'กินเอง', 
																				'2' => 'TF (Tube feeding)', 
																				'3' => 'PN (Parenteral nutrition)', 
																				'4' => 'Standard IV', 
																			);
																	?>
																		<td><?=$txt[$value['n1_1']]?></td>
																		<td rowspan="3"><?=$value['n1_4']?></td>
																	<?php endforeach ?>
																	</tr>
																	<tr>
																	<?php 
																		foreach ($data['assessment_nt'] as $key => $value): 
																			$txt = array(
																				'1' => '< 10% (NPO, ได้รับน้ำเกลือมาตรฐาน)', 
																				'2' => '10-25% ของปริมาณปกติ หรือ แคลอรี่ ที่ต้องการ', 
																				'3' => '25-50% ของปริมาณปกติ หรือ แคลอรี่ ที่ต้องการ', 
																				'4' => '50-75% ของปริมาณปกติ หรือ แคลอรี่ ที่ต้องการ', 
																				'5' => '75-100% ของปริมาณปกติ หรือ แคลอรี่ ที่ต้องการ'
																			);
																	?>
																		<td><?=$txt[$value['n1_2']]?></td>
																	<?php endforeach ?>
																	</tr>
																	<tr>
																	<?php 
																		foreach ($data['assessment_nt'] as $key => $value): 
																			$txt = array(
																				'1' => 'น้อยกว่าหรือเท่ากับ 7 วัน', 
																				'2' => '8 - 14 วัน', 
																				'3' => 'มากกว่า 14 วัน', 
																			);
																	?>
																		<td><?=$txt[$value['n1_3']]?></td>
																	<?php endforeach ?>
																	</tr>
																	<tr>
																	<?php for ($i=0; $i < $assessment_nt_count; $i++) { ?>
																		<td><label></label></td>
																		<td></td>
																	<?php } ?>
																	</tr>
																	<tr>
																	<?php 
																		foreach ($data['assessment_nt'] as $key => $value): 
																			$txt = array(
																				'1' => 'เท่าเดิม', 
																				'2' => 'เพิ่มขึ้น', 
																				'3' => 'ลดลง', 
																			);
																			$time = $value['time_number']*$value['time_scope'];
																			if ($value['percent']>0) {
																				if ($time<=7) { // 1 week
																					if ($value['percent']<1) {
																						$score = 1;
																					}else if($value['percent']>=1&&$value['percent']<=2){
																						$score = 2;
																					}else if($value['percent']>2){
																						$score = 3;
																					}
																				}else if ($time<=7&&$time<=21) { // 2-3 week
																					if ($value['percent']<2) {
																						$score = 1;
																					}else if($value['percent']>=2&&$value['percent']<=3){
																						$score = 2;
																					}else if($value['percent']>3){
																						$score = 3;
																					}
																				}else if ($time>=22&&$time<=89) { // 4 week & 1 month 
																					if ($value['percent']<4) {
																						$score = 1;
																					}else if($value['percent']>=4&&$value['percent']<=5){
																						$score = 2;
																					}else if($value['percent']>5){
																						$score = 3;
																					}
																				}else if ($time>=90&&$time<=150) { // 3 month
																					if ($value['percent']<7) {
																						$score = 1;
																					}else if($value['percent']>=7&&$value['percent']<=8){
																						$score = 2;
																					}else if($value['percent']>8){
																						$score = 3;
																					}
																				}else if ($time>=151) { // > 5 month
																					if ($value['percent']<10) {
																						$score = 1;
																					}else if($value['percent']==10){
																						$score = 2;
																					}else if($value['percent']>10){
																						$score = 3;
																					}
																				}
																			}else{
																				$score = 0;
																			}
																	?>
																		<td><?=$txt[$value['n2_1']]?></td>
																		<td rowspan="3"><?=$score?></td>
																	<?php endforeach ?>
																	</tr>
																	<tr>
																	<?php 
																		foreach ($data['assessment_nt'] as $key => $value): 
																			$txt = array(
																				'7' => 'สัปดาห์', 
																				'30' => 'เดือน',  
																			);
																	?>
																		<td><?=$value['time_number'].' '.$txt[$value['time_scope']]?></td>
																	<?php endforeach ?>
																	</tr>
																	<tr>
																	<?php 
																		foreach ($data['assessment_nt'] as $key => $value): 
																	?>
																		<td><?=$value['percent']?> %</td>
																	<?php endforeach ?>
																	</tr>
																	<tr>
																	<?php 
																		foreach ($data['assessment_nt'] as $key => $value): 
																			$txt = array(
																				'1' => 'ไม่บวม', 
																				'2' => 'บวมเล็กน้อย', 
																				'3' => 'บวมปานกลาง', 
																				'4' => 'บวมทั่วตัว', 
																			);
																			$score = array(
																				'1' => 0, 
																				'2' => 1, 
																				'3' => 2, 
																				'4' => 3, 
																			);
																	?>
																		<td><?=$txt[$value['n3_1']]?></td>
																		<td><?=$score[$value['n3_1']]?></td>
																	<?php endforeach ?>
																	</tr>
																	<tr>
																	<?php 
																		foreach ($data['assessment_nt'] as $key => $value): 
																			$txt = array(
																				'1' => 'ปกติ', 
																				'2' => 'มีไขมันน้อย', 
																				'3' => 'มีไขมันน้อยมาก', 
																				'4' => 'หนังหุ้มกระดูก', 
																			);
																			$score = array(
																				'1' => 0, 
																				'2' => 1, 
																				'3' => 2, 
																				'4' => 3, 
																			);
																	?>
																		<td><?=$txt[$value['n4_1']]?></td>
																		<td><?=$score[$value['n4_1']]?></td>
																	<?php endforeach ?>
																	</tr>
																	<tr>
																	<?php 
																		foreach ($data['assessment_nt'] as $key => $value): 
																			$txt = array(
																				'1' => 'ปกติ', 
																				'2' => 'กล้ามเนื้อน้อยลง', 
																				'3' => 'กล้ามเนื้อลีบ', 
																				'4' => 'หนังหุ้มกระดูก', 
																			);
																			$score = array(
																				'1' => 0, 
																				'2' => 1, 
																				'3' => 2, 
																				'4' => 3, 
																			);
																	?>
																		<td><?=$txt[$value['n5_1']]?></td>
																		<td><?=$score[$value['n5_1']]?></td>
																	<?php endforeach ?>
																	</tr>
																	<tr>
																	<?php 
																		foreach ($data['assessment_nt'] as $key => $value): 
																			$txt = array(
																				'1' => 'ไม่สามารถเคลื่อนไหวได้เลย', 
																				'2' => 'สามารถเคลื่อนได้เล็กน้อย แต่เฉพาะในแนว Horizontal', 
																				'3' => 'สามารถเคลื่อน ต้านแรงดึงดูดของโลกในแนว Virtical ได้', 
																				'4' => 'สามารถเคลื่อนตามแนว Virtical และต้านแรงผู้ตรวจ พอได้', 
																				'5' => 'มีอาการอ่อนแรงเพียงเล็กน้อย', 
																				'6' => 'ปกติ', 
																			);
																			$score = array(
																				'1' => 3, 
																				'2' => 2, 
																				'3' => 1, 
																				'4' => 1, 
																				'5' => 0, 
																				'6' => 0, 
																			);
																	?>
																		<td><?=$txt[$value['n6_1']]?></td>
																		<td><?=$score[$value['n6_1']]?></td>
																	<?php endforeach ?>
																	</tr>
																	<tr>
																	<?php for ($i=0; $i < $assessment_nt_count; $i++) { ?>
																		<td><label></label></td>
																		<td></td>
																	<?php } ?>
																	</tr>
																	<tr>
																	<?php foreach ($data['assessment_nt'] as $key => $value): ?>
																		<td>
																		<?php echo $value['n7_1']==1 ? '<label class="circle"></label>' : '-'; ?>
																		</td>
																		<td><?=$value['n7_1']?></td>
																	<?php endforeach ?>
																	</tr>
																	<tr>
																	<?php foreach ($data['assessment_nt'] as $key => $value): ?>
																		<td>
																		<?php echo $value['n7_2']==1 ? '<label class="circle"></label>' : '-'; ?>
																		</td>
																		<td><?=$value['n7_2']?></td>
																	<?php endforeach ?>
																	</tr>
																	<tr>
																	<?php foreach ($data['assessment_nt'] as $key => $value): ?>
																		<td>
																		<?php echo $value['n7_3']==1 ? '<label class="circle"></label>' : '-'; ?>
																		</td>
																		<td><?=$value['n7_3']?></td>
																	<?php endforeach ?>
																	</tr>
																	<tr>
																	<?php foreach ($data['assessment_nt'] as $key => $value): ?>
																		<td>
																		<?php echo $value['n7_4']==1 ? '<label class="circle"></label>' : '-'; ?>
																		</td>
																		<td><?=$value['n7_4']?></td>
																	<?php endforeach ?>
																	</tr>
																	<tr>
																	<?php foreach ($data['assessment_nt'] as $key => $value): ?>
																		<td>
																		<?php echo $value['n7_5']==1 ? '<label class="circle"></label>' : '-'; ?>
																		</td>
																		<td><?=$value['n7_5']?></td>
																	<?php endforeach ?>
																	</tr>
																	<tr>
																	<?php foreach ($data['assessment_nt'] as $key => $value): ?>
																		<td>
																		<?php echo $value['n7_6']==1 ? '<label class="circle"></label>' : '-'; ?>
																		</td>
																		<td><?=$value['n7_6']?></td>
																	<?php endforeach ?>
																	</tr>
																	<tr>
																	<?php foreach ($data['assessment_nt'] as $key => $value): ?>
																		<td>
																		<?php echo $value['n7_7']==1 ? '<label class="circle"></label>' : '-'; ?>
																		</td>
																		<td><?=$value['n7_7']?></td>
																	<?php endforeach ?>
																	</tr>
																	<tr>
																	<?php foreach ($data['assessment_nt'] as $key => $value): ?>
																		<td>
																		<?php echo $value['n7_8']==1 ? '<label class="circle"></label>' : '-'; ?>
																		</td>
																		<td><?=$value['n7_8']?></td>
																	<?php endforeach ?>
																	</tr>
																	<tr>
																	<?php foreach ($data['assessment_nt'] as $key => $value): ?>
																		<td>
																		<?php echo $value['n7_9']==1 ? '<label class="circle"></label>' : '-'; ?>
																		</td>
																		<td><?=$value['n7_9']?></td>
																	<?php endforeach ?>
																	</tr>
																	<tr>
																	<?php for ($i=0; $i < $assessment_nt_count; $i++) { ?>
																		<td><label></label></td>
																		<td></td>
																	<?php } ?>
																	</tr>
																	<tr>
																	<?php foreach ($data['assessment_nt'] as $key => $value): ?>
																		<td>
																		<?php echo $value['n8_1']==1 ? '<label class="circle"></label>' : '-'; ?>
																		</td>
																		<td><?=$value['n8_1']?></td>
																	<?php endforeach ?>
																	</tr>
																	<tr>
																	<?php foreach ($data['assessment_nt'] as $key => $value): ?>
																		<td>
																		<?php echo $value['n8_2']==1 ? '<label class="circle"></label>' : '-'; ?>
																		</td>
																		<td><?=$value['n8_2']?></td>
																	<?php endforeach ?>
																	</tr>
																	<tr>
																	<?php foreach ($data['assessment_nt'] as $key => $value): ?>
																		<td>
																		<?php echo $value['n8_3']==1 ? '<label class="circle"></label>' : '-'; ?>
																		</td>
																		<td><?=$value['n8_3']?></td>
																	<?php endforeach ?>
																	</tr>
																	<tr>
																	<?php foreach ($data['assessment_nt'] as $key => $value): ?>
																		<td>
																		<?php echo $value['n8_4']==1 ? '<label class="circle"></label>' : '-'; ?>
																		</td>
																		<td><?=$value['n8_4']?></td>
																	<?php endforeach ?>
																	</tr>
																	<tr>
																	<?php foreach ($data['assessment_nt'] as $key => $value): ?>
																		<td>
																		<?php echo $value['n8_5']==1 ? '<label class="circle"></label>' : '-'; ?>
																		</td>
																		<td><?=$value['n8_5']?></td>
																	<?php endforeach ?>
																	</tr>
																	<tr>
																	<?php foreach ($data['assessment_nt'] as $key => $value): ?>
																		<td>
																		<?php echo $value['n8_6']==1 ? '<label class="circle"></label>' : '-'; ?>
																		</td>
																		<td><?=$value['n8_6']?></td>
																	<?php endforeach ?>
																	</tr>
																	<tr>
																	<?php foreach ($data['assessment_nt'] as $key => $value): ?>
																		<td>
																		<?php echo $value['n8_7']==1 ? '<label class="circle"></label>' : '-'; ?>
																		</td>
																		<td><?=$value['n8_7']?></td>
																	<?php endforeach ?>
																	</tr>
																	<tr>
																	<?php for ($i=0; $i < $assessment_nt_count; $i++) { ?>
																		<td><label></label></td>
																		<td><?=$value['score']?></td>
																	<?php } ?>
																	</tr>
																	<tr>
																	<?php for ($i=0; $i < $assessment_nt_count; $i++) { ?>
																		<td colspan="2"><?=$value['grade']?></td>
																	<?php } ?>
																	</tr>
																</table>
																<?php endif ?>
															</div>
														</td>
													</tr>
													<tr>
														<td>วันที่ประเมิน</td>
													</tr>
													<tr>
														<td>&nbsp;</td>
													</tr>
													<tr>
														<td>ประวัติการได้รับอาหารหรือสารอาหาร</td>
													</tr>
													<tr>
														<td style="padding-left: 30px">ประเภท</td>
													</tr>
													<tr>
														<td style="padding-left: 30px">ปริมาณ</td>
													</tr>
													<tr>
														<td style="padding-left: 30px">ระยะเวลา</td>
													</tr>
													<tr>
														<td>การเปลี่ยนเเปลงของน้ำหนักตัว</td>
													</tr>
													<tr>
														<td style="padding-left: 30px">น้ำหนักตัว</td>
													</tr>
													<tr>
														<td style="padding-left: 30px">ระยะเวลา</td>
													</tr>
													<tr>
														<td style="padding-left: 30px">%น้ำหนักที่เปลี่ยแปลง</td>
													</tr>
													<tr>
														<td>ภาวะบวมน้ำ ( Fluid accumulation )</td>
													</tr>
													<tr>
														<td>ระดับการสูญเสีย มวลไขมัน ( Body fat loss ) ประเมินเฉลี่ยทั่วร่างกาย 4 สัปดาห์</td>
													</tr>
													<tr>
														<td>ระดับการสูญเสีย มวลกล้ามเนื้อ (Muscle loss) ประเมินเฉลี่ยทั่วร่างกาย 2 สัปดาห์</td>
													</tr>
													<tr>
														<td>สมรรถภาพกล้ามเนื้อ ( ประเมินเฉลี่ยทั่วร่างกาย )</td>
													</tr>
													<tr>
														<td>ประเมินความรุนแรงของภาวะเจ็บป่วย เรื้อรัง ( >3 เดือน)</td>
													</tr>
													<tr>
														<td style="padding-left: 30px">โรคมะเร็ง</td>
													</tr>
													<tr>
														<td style="padding-left: 30px">โรคปอด</td>
													</tr>
													<tr>
														<td style="padding-left: 30px">โรคไต</td>
													</tr>
													<tr>
														<td style="padding-left: 30px">โรคตับ</td>
													</tr>
													<tr>
														<td style="padding-left: 30px">HIV</td>
													</tr>
													<tr>
														<td style="padding-left: 30px">โรค/สภาวะอื่นๆ</td>
													</tr>
													<tr>
														<td style="padding-left: 30px">ท้องมาน</td>
													</tr>
													<tr>
														<td style="padding-left: 30px">แผลกดทับ</td>
													</tr>
													<tr>
														<td style="padding-left: 30px">แผลเรื้อรังอื่นๆ</td>
													</tr>
													<tr>
														<td>ประเมินความรุนเเรง ของ ภาวะเจ็บป่วย เฉียบพลัน หรือกึ่งเฉียบพลัน</td>
													</tr>
													<tr>
														<td style="padding-left: 30px">Non neurological trauma</td>
													</tr>
													<tr>
														<td style="padding-left: 30px">Head injury, Acute spine injury</td>
													</tr>
													<tr>
														<td style="padding-left: 30px">Burn</td>
													</tr>
													<tr>
														<td style="padding-left: 30px">Sepsis</td>
													</tr>
													<tr>
														<td style="padding-left: 30px">Recent major operation</td>
													</tr>
													<tr>
														<td style="padding-left: 30px">Acute pancreatitis, Hepatitis, Peritonitis, Necrotizing fasciitis</td>
													</tr>
													<tr>
														<td style="padding-left: 30px">โรคอื่นๆ</td>
													</tr>
													<tr>
														<td>คะแนนรวมที่ได้</td>
													</tr>
													<tr>
														<td>ผลการประเมิน</td>
													</tr>
												</table> -->
												<script type="text/javascript">
													$(document).ready(function() {
														var head_nt = $('.head-nt tr')
														for (var i =56; i < head_nt.length; i++) {
															var tmp_head = $(head_nt[i])
															var tmp_sub = $('.sub-nt tr')[i-53]
															$(tmp_sub).height($(tmp_head).height())
														}
													});
												</script>
												<div style="height: 300px;position: relative;">
													<canvas id="myChart_nt"></canvas>
												</div>
												<script>
													<?php
														$score_ar = array();
														$date_ar = array();
														foreach ($data['assessment_nt'] as $key => $value) {
															array_push($score_ar, $value['score']);
															array_push($date_ar, date_format(date_create($value['date']),'d/m/Y'));
														}
													?>
													var ctx_nt = document.getElementById("myChart_nt");
													var myChart_nt = new Chart(ctx_nt, {
														type: 'bar',
														data: {
															labels: ["<?=implode($date_ar, '","')?>"],
															datasets: [{
																label: 'points of times',
																data: [<?=implode($score_ar, ',')?>],
																backgroundColor: [
																'rgba(234, 80, 80, 1)',
																'rgba(255, 137, 61, 1)',
																],
																borderWidth: 1
															}]
														},
														options: {
															maintainAspectRatio: false,
															scales: {
																yAxes: [{
																	ticks: {
																		beginAtZero:true
																	},
																	scaleLabel: {
																		display: true,
																		labelString: 'Points'
																	}
																}],
																xAxes: [{
																	ticks: {
																		beginAtZero:true
																	},
																	scaleLabel: {
																		display: true,
																		labelString: 'Times'
																	}
																}]
															}
														}
													});
												</script>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="btn-bar">
								<button class="btn btn-color">Checkout <i class="fa fa-sign-out"></i></button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
	</html>