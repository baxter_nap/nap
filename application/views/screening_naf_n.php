<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
<head>
	<title>Main</title>
	<?php $this->load->view('_config'); ?>
	<script type="text/javascript">
		$(document).ready(function() {
			$(window).on('beforeunload', function(){
                return 'คุณจะออกจากการทำฟอร์มนี้ใช่หรือไม่';
           	});
		});
		function tmp_naf() {
			$.post(base_url+'screening/save_tmp/naf', {data: qwe});
		}
	</script>
</head>
<body>
	<?php $this->load->view('_header'); ?>
	<div class="container user">
		<div class="row">
			<?php $this->load->view('_left_bar.php'); ?>
			<div class="col-sm-10 col-xs-12 content">
				<div class="row green_2 top-bar">
					<div class="col-sm-10 col-xs-6 back">
						<!-- <a href="<?=base_url().'screening/edit/'.$_SESSION['last_screening_id']?>">< ย้อนกลับ</a> -->
					</div>
					<div class="col-sm-2 col-xs-6 score text-right">คะแนนรวม = <span class="result_all"></span></div>
				</div>
				<div class="row">
					<div class="col-sm-8 col-xs-12 title">Screening > Assessment NAF ครั้งที่ 1</div>
					<div class="col-sm-4 col-xs-12 btn-bar"></div>
				</div>
				<div class="row">
					<div class="col-sm-12 col-xs-12">
						<div class="row box table-screening">
							<div class="header blue_1">
								<div class="col-sm-6 col-xs-12">ข้อมูลผู้ป่วย</div>
								<div class="col-sm-6 col-xs-12">วันที่ทำการประเมิน : <?=date('d/m/Y')?></div>
							</div>
							<div class="body screening-history">
								<div class="col-sm-12 col-xs-12">
									<div class="col-sm-6 col-xs-12 form-group">
										<label>HN</label>
										<div class="detail">
											<?=$_SESSION['screening']['data']['hn_code']?>
											<input type="hidden" name="hn_code" id="hn_code" value="<?=$_SESSION['screening']['data']['hn_code']?>">
											<input type="hidden" name="bmi" id="bmi" value="<?=$_SESSION['screening']['data']['bmi']?>" no-get>
										</div>
									</div>
									<div class="col-sm-6 col-xs-12 form-group">
										<label>ชื่อผู้ป่วย</label>
										<div class="detail"><?php echo !isset($_SESSION['screening']['data']['name']) ? '-' : $_SESSION['screening']['data']['name']; ?></div>
									</div>
									<div class="col-sm-6 col-xs-12 form-group">
										<label>หอผู้ป่วย</label>
										<div class="detail"><?php echo !isset($_SESSION['screening']['data']['patient_ward_id']) ? '-' : $_SESSION['screening']['data']['patient_ward_id'];?></div>
									</div>
									<div class="col-sm-6 col-xs-12 form-group">
										<label>การวินิจฉัยโรค</label>
										<div class="detail"><?php echo $_SESSION['screening']['data']['diagnose']=="" ? '-' : $_SESSION['screening']['data']['diagnose']; ?></div>
									</div>
								</div>
							</div>
							<div class="col-sm-12 hidden-xs cursor-bar">
								<label onclick="toggle_show(this,165,85)" class=cursor><i class="fa fa-angle-down"></i></label>
							</div>
						</div>
						<?php require_once(APPPATH.'views/_assessment_naf_n.php'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>