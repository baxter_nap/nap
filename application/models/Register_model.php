<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Register_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

	public function get_register()
	{
		$this->db->select('*,`hospital`.`title` as `hospital_title`,`register`.`id` as id');
		$this->db->where('hospital.is_del',0);
		$this->db->where('register.is_del',0);
		$this->db->join('hospital','`register`.`hospital_id` = `hospital`.`id`');
		$query = $this->db->get('register');
		return $query->result_array();
	}

	public function get_register_by_id($id)
	{
		$sql = "SELECT *,`hospital`.`title` as `hospital_title`,`register`.`id` as id
			FROM `register`
			INNER JOIN `hospital` ON `register`.`hospital_id` = `hospital`.`id`
			WHERE `hospital`.`is_del` = 0 AND `register`.`id` = ?";
		$query = $this->db->query($sql,array($id));
		$result = $query->result_array();
		return $result[0];
	}

	public function insert_register($tmp)
	{
		// echo"<pre>";print_r($tmp);echo "</pre>";
		$data['name'] = $tmp['name'];
		$data['professional_field'] = $tmp['professional_field'];
		if ($tmp['professional_field']=="แพทย์") {
			$data['professional_field_sub'] = $tmp['professional_field_1_sub']==5 ? $tmp['professional_field_1_sub_more'] : $tmp['professional_field_1_sub'];
		}else if ($tmp['professional_field']=="พยาบาล") {
			$data['professional_field_sub'] = $tmp['professional_field_2_sub']==5 ? $tmp['professional_field_2_sub_more'] : $tmp['professional_field_2_sub'];
		}else{
			$data['professional_field_sub'] = "";
		}
		$data['hospital_id'] = $tmp['hospital_id'];

		$data['patient_ward_1'] = isset($tmp['patient_ward_1']) ? 1 : 0;
		$data['patient_ward_2'] = isset($tmp['patient_ward_2']) ? 1 : 0;
		$data['patient_ward_3'] = isset($tmp['patient_ward_3']) ? 1 : 0;
		$data['patient_ward_4'] = isset($tmp['patient_ward_4']) ? 1 : 0;
		$data['patient_ward_5'] = isset($tmp['patient_ward_5']) ? 1 : 0;
		$data['patient_ward_6'] = isset($tmp['patient_ward_6']) ? 1 : 0;
		$data['patient_ward_7'] = isset($tmp['patient_ward_7']) ? 1 : 0;
		$data['patient_ward_8'] = isset($tmp['patient_ward_8']) ? 1 : 0;
		$data['patient_ward_9'] = isset($tmp['patient_ward_9']) ? 1 : 0;
		$data['patient_ward_more'] = $tmp['patient_ward_more'];

		$data['province_id'] = $tmp['province_id'];
		$data['postcode'] = $tmp['postcode'];
		$data['affiliation'] = $tmp['affiliation'];
		if ($data['affiliation']=="สาธารณสุข") {
			$data['affiliation_sub'] = $tmp['affiliation_1_sub'];
		}else{
			$data['affiliation_sub'] = "";
		}
		if ($data['affiliation']==5) {
			$data['affiliation'] = $tmp['affiliation_more_5'];
		}
		if ($data['affiliation']==9) {
			$data['affiliation'] = $tmp['affiliation_more_9'];
		}
		
		$data['count'] = $tmp['count'];
		$data['email'] = $tmp['email'];

		$query = $this->db->insert('register', $data);
		if ($query) {
			$this->alert("success","เรียบร้อย","ระบบได้ทำการบันทึกข้อมูลเรียบร้อบแล้ว","false","register");
		}else{
			$this->alert("error","Error!","Contact super admin");
		}
	}

	public function update_register($data)
	{
		$this->db->where('id', $data['id']);
		$query = $this->db->update('register',$data);
		if ($query) {
			$this->alert("success","Success","You edit register success","false","register");
		}else{
			$this->alert("error","Error!","Contact super admin");
		}
	}

	public function delete_register($data)
	{
		$this->db->set('is_del', 1);
		$this->db->where('id', $data['id']);
		$query = $this->db->update('register');
		if ($query) {
			echo json_encode(array('status'=>1,'redirect'=>'manage_member'));
		}else{
			echo json_encode(array('status'=>0,'redirect'=>'manage_member'));
		}
	}

	public function send_mail($data)
	{
		$code = $data['code'];
		$name = $data['name'];
		$email = $data['email'];

		$up['status'] = 1;
		$up['id'] = $data['id'];

		$this->load->library('email');

		$config['charset'] = 'utf-8';
		$config['wordwrap'] = TRUE;
		$config['mailtype'] = 'html';

		$this->email->initialize($config);

		$this->email->from('spentthai@gmail.com', 'Admin');
		$this->email->to($email);

		$this->email->subject('รหัสผ่านใช้งานระบบ Nutrition Assessment program by SPENT');

		$msg = '<img src="http://www.screening.com.122.155.5.153.no-domain.name/images/logo-regis.png" height="100px">';
		$msg .= '<h2>สมาคมผู้ให้อาหารทางหลอดเลือดดำและทางเดินอาหารแห่งประเทศไทย</h2>';
		$msg .= '<hr style="border: 1px solid #666;">';
		$msg .= '<h3>เรียนคุณ '.$name.'</h3>';
		$msg .= '<h3>รหัสผ่านประจำโรงพยาบาลของคุณคือ : <span style="color: #176920;">'.$code.'</span></h3>';
		$msg .= '<p>สามารถเข้าใช้งานผ่านลิ้งค์นี้ <a href="http://www.screening.com.122.155.5.153.no-domain.name/">ลิ้งค์เข้าสู่ระบบ Nutrition Assessment program by SPENT</a></p>';
		$msg .= '<hr style="border: 1px solid #666;">';
		$msg .= '<h3>พบปัญหาหรือมีข้อสงสัยกรุณาติดต่อ</h3>';
		$msg .= '<p>E-mail : spentthai@gmail.com</p>';
		$this->email->message($msg);
		if ($this->email->send()) {
			$this->db->update('register',$up);
			$this->alert("success","Success","You send email success","false","manage_member");
		}else{
			$this->alert("error","Error!","Contact super admin");
		}
	}

	public function alert($type,$title,$msg,$cfbtn="true",$redirect="")
	{
		if ($redirect!="") {
			$time = "timer: 2000,";
			$link = ',function(){
				window.location.href = "'.base_url().$redirect.'"
			}';
		}else{
			$time = "";
			$link = "";
		}
		echo '<link rel="stylesheet" type="text/css" href="'.base_url().'../css/sweetalert.css">';
		echo '<script src="'.base_url().'../js/sweetalert.min.js"></script>';
		echo '&nbsp;<script>swal({
			title: "'.$title.'",
			text: "'.$msg.'",
			type: "'.$type.'",
			'.$time.'
			showConfirmButton: '.$cfbtn.'
		}'.$link.');</script>';
	}
}