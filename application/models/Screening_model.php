<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Screening_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Date_model');
		if (!isset($_SESSION['hospital_id'])) {
			redirect(base_url(), 'refresh');
		}
	}

	public function get_screening()
	{
		$this->db->where('screening.is_del',0);
		$query = $this->db->get('screening');
		$result = $query->result_array();
		return $result;
	}

	public function get_screening_by_date($date)
	{
		$hospital_id = $_SESSION['hospital_id'];
		$sql = "SELECT s1.c_1,s1.c_2,s1.c_3,s1.c_4
			FROM screening s1 
			LEFT JOIN screening s2 ON s1.hn_code = s2.hn_code AND s1.id < s2.id 
			WHERE s1.is_del = 0 AND s2.id IS NULL AND s1.date LIKE '$date%' AND s1.hospital_id = '$hospital_id'";
		$query = $this->db->query($sql);
		$result = $query->result_array();
		// echo"<pre>";print_r($result);echo "</pre>";
		return $result;
	}

	public function get_assessment_naf_by_date($date)
	{
		$hospital_id = $_SESSION['hospital_id'];
		$sql = "SELECT as1.grade
			FROM assessment_naf as1 
			LEFT JOIN assessment_naf as2 ON as1.hn_code = as2.hn_code AND as1.id < as2.id 
			INNER JOIN screening s1 ON as1.screening_id = s1.id
			WHERE s1.is_del = 0 AND as2.id IS NULL AND as1.date LIKE '$date%' AND s1.hospital_id = '$hospital_id'";
		$query = $this->db->query($sql);
		$result = $query->result_array();
		// echo"<pre>";print_r($result);echo "</pre>";
		return $result;
	}

	public function get_assessment_nt_by_date($date)
	{
		$hospital_id = $_SESSION['hospital_id'];
		$sql = "SELECT as1.grade
			FROM assessment_nt as1 
			LEFT JOIN assessment_nt as2 ON as1.hn_code = as2.hn_code AND as1.id < as2.id 
			INNER JOIN screening s1 ON as1.screening_id = s1.id
			WHERE s1.is_del = 0 AND as2.id IS NULL AND as1.date LIKE '$date%' AND s1.hospital_id = '$hospital_id'";
		$query = $this->db->query($sql);
		$result = $query->result_array();
		// echo"<pre>";print_r($result);echo "</pre>";
		return $result;
	}

	public function get_screening_by_id($id)
	{
		$this->db->select('*,patient_ward.title as patient_ward_title,diagnose_system.title as diagnose_system_title,screening.id as screening_id,screening.date_next as screening_date_next');
		$this->db->where('screening.id',$id);
		$this->db->where('patient_ward.is_del',0);
		$this->db->where('screening.is_del',0);
		$this->db->where('diagnose_system.is_del',0);
		$this->db->join('patient_ward', 'patient_ward.id = screening.patient_ward_id');
		$this->db->join('diagnose_system', 'diagnose_system.id = screening.diagnose_system_id');
		$query = $this->db->get('screening');
		$result = $query->result_array();
		return $result[0];
	}

	public function get_screening_by_hn_code($hn_code,$order_by="date_next")
	{
		$hn_code = strtoupper($hn_code);
		$result = array();
		$this->db->select('*,patient_ward.title as patient_ward_title,patient_ward.id as patient_ward_id,diagnose_system.title as diagnose_system_title,screening.id as screening_id,screening.date_next as screening_date_next,screening.id as id');
		$this->db->where('screening.hn_code',$hn_code);
		$this->db->where('patient_ward.is_del',0);
		$this->db->where('screening.is_del',0);
		$this->db->where('diagnose_system.is_del',0);
		$this->db->join('patient_ward', 'patient_ward.id = screening.patient_ward_id');
		$this->db->join('diagnose_system', 'diagnose_system.id = screening.diagnose_system_id');
		$this->db->order_by("screening.".$order_by, "desc");
		$query = $this->db->get('screening');
		$result['screening'] = $query->result_array();
		//naf
		$this->db->select('*');
		$this->db->where('hn_code',$hn_code);
		$this->db->order_by($order_by, "desc");
		$query = $this->db->get('assessment_naf');
		if (count($query->result_array())!=0) {
			$result['assessment_naf'] = $query->result_array();
		}
		//nt
		$this->db->select('*');
		$this->db->where('hn_code',$hn_code);
		$this->db->order_by($order_by, "desc");
		$query = $this->db->get('assessment_nt');
		if (count($query->result_array())!=0) {
			$result['assessment_nt'] = $query->result_array();
		}
		return $result;
	}

	public function get_screening_by_hn_code_old($hn_code,$date=-1)
	{
		$hn_code = strtoupper($hn_code);
		$result = array();
		$this->db->select('*,patient_ward.title as patient_ward_title,patient_ward.id as patient_ward_id,diagnose_system.title as diagnose_system_title,screening.id as screening_id,screening.date_next as screening_date_next');
		$this->db->where('screening.hn_code',$hn_code);
		if ($date!=-1) {
			$this->db->where('screening.date = ',$date);
		}
		$this->db->where('screening.hospital_id = ',$_SESSION['hospital_id']);
		$this->db->where('screening.is_del = ',0);
		$this->db->join('patient_ward', 'patient_ward.id = screening.patient_ward_id');
		$this->db->join('diagnose_system', 'diagnose_system.id = screening.diagnose_system_id');
		$this->db->order_by('screening.id', 'desc');
		$query = $this->db->get('screening');
		$result['screening'] = $query->result_array();
		return $result;
	}


	public function get_screening_by_id_export($id)
	{
		$this->db->select('
			screening.name,
			screening.age,
			screening.hn_code,
			screening.date,
			screening.date_admission,
			screening.diagnose,
			diagnose_system.title as diagnose_system_title,
			patient_ward.title as patient_ward_title,
			screening.sex,
			screening.bmi,
			screening.weight_current,
			screening.weight_assessment,
			screening.height,
			screening.c_1,
			screening.c_2,
			screening.c_3,
			screening.c_4,
		');
		$this->db->where('screening.id',$id);
		$this->db->where('patient_ward.is_del',0);
		$this->db->where('screening.is_del',0);
		$this->db->where('diagnose_system.is_del',0);
		$this->db->join('patient_ward', 'patient_ward.id = screening.patient_ward_id');
		$this->db->join('diagnose_system', 'diagnose_system.id = screening.diagnose_system_id');
		$query = $this->db->get('screening');
		$result = $query->result_array();
		return $result[0];
	}

	public function get_screening_naf_by_id_export($id)
	{
		$this->db->select('
			screening.name,
			screening.sex,
			screening.age,
			screening.hn_code,
			screening.date_admission,
			screening.diagnose,
			diagnose_system.title as diagnose_system_title,
			screening.sex,
			assessment_naf.n1_1,
			assessment_naf.n1_2,
			assessment_naf.n1_3,
			assessment_naf.n2_1,
			assessment_naf.n2_2,
			assessment_naf.n2_3,
			assessment_naf.n2_4,
			assessment_naf.n3_1,
			assessment_naf.n4_1,
			assessment_naf.n5_1,
			assessment_naf.n5_2,
			assessment_naf.n6_1_1,
			assessment_naf.n6_1_2,
			assessment_naf.n6_1_3,
			assessment_naf.n6_2_1,
			assessment_naf.n6_2_2,
			assessment_naf.n6_2_3,
			assessment_naf.n6_3_1,
			assessment_naf.n6_3_2,
			assessment_naf.n6_3_3,
			assessment_naf.n7_1,
			assessment_naf.n8_1_1,
			assessment_naf.n8_1_2,
			assessment_naf.n8_1_3,
			assessment_naf.n8_1_4,
			assessment_naf.n8_1_5,
			assessment_naf.n8_1_6,
			assessment_naf.n8_1_7,
			assessment_naf.n8_1_8,
			assessment_naf.n8_1_9,
			assessment_naf.n8_1_10,
			assessment_naf.n8_1_11,
			assessment_naf.n8_1_11_name,
			assessment_naf.n8_2_1,
			assessment_naf.n8_2_2,
			assessment_naf.n8_2_3,
			assessment_naf.n8_2_4,
			assessment_naf.n8_2_5,
			assessment_naf.n8_2_6,
			assessment_naf.n8_2_6_name,
			assessment_naf.n2_1_weight,
			assessment_naf.bmi,
			assessment_naf.n2_1_score,
			assessment_naf.n2_2_score,
			assessment_naf.n2_3_score,
			assessment_naf.n2_4_score,
			assessment_naf.n3_1_score,
			assessment_naf.n4_1_score,
			assessment_naf.n5_1_score,
			assessment_naf.n5_2_score,
			assessment_naf.n6_1_score,
			assessment_naf.n6_2_score,
			assessment_naf.n6_3_score,
			assessment_naf.n7_1_score,
			assessment_naf.n8_1_score,
			assessment_naf.n8_2_score,
			assessment_naf.score,
			assessment_naf.date,
		');
		$this->db->where('assessment_naf.id',$id);
		$this->db->where('patient_ward.is_del',0);
		$this->db->where('screening.is_del',0);
		$this->db->where('diagnose_system.is_del',0);
		$this->db->join('patient_ward', 'patient_ward.id = screening.patient_ward_id');
		$this->db->join('diagnose_system', 'diagnose_system.id = screening.diagnose_system_id');
		$this->db->join('assessment_naf', 'assessment_naf.screening_id = screening.id');
		$query = $this->db->get('screening');
		$result = $query->result_array();
		return $result[0];
	}

	public function get_screening_nt_by_id_export($id)
	{
		$this->db->select('
			screening.name,
			screening.sex,
			screening.age,
			screening.hn_code,
			screening.height,
			screening.bmi,
			assessment_nt.weight_current,
			screening.date_admission,
			screening.diagnose,
			diagnose_system.title as diagnose_system_title,
			screening.sex,
			assessment_nt.n0_1,
			assessment_nt.n0_2,
			assessment_nt.n1_1,
			assessment_nt.n1_2,
			assessment_nt.n1_3,
			assessment_nt.n1_4,
			assessment_nt.n1_4_score,
			assessment_nt.n2_1,
			assessment_nt.n2_1_score,
			assessment_nt.weight_default,
			assessment_nt.percent,
			assessment_nt.time_number,
			assessment_nt.time_scope,
			assessment_nt.n3_1,
			assessment_nt.n4_1,
			assessment_nt.n5_1,
			assessment_nt.n6_1,
			assessment_nt.n6_1_score,
			assessment_nt.n7_1,
			assessment_nt.n7_2,
			assessment_nt.n7_3,
			assessment_nt.n7_4,
			assessment_nt.n7_5,
			assessment_nt.n7_6,
			assessment_nt.n7_7,
			assessment_nt.n7_8,
			assessment_nt.n7_9,
			assessment_nt.n8_1,
			assessment_nt.n8_2,
			assessment_nt.n8_3,
			assessment_nt.n8_4,
			assessment_nt.n8_5,
			assessment_nt.n8_6,
			assessment_nt.n8_7,
			assessment_nt.score,
			assessment_nt.date,
			assessment_nt.grade
		');
		$this->db->where('assessment_nt.id',$id);
		$this->db->where('patient_ward.is_del',0);
		$this->db->where('screening.is_del',0);
		$this->db->where('diagnose_system.is_del',0);
		$this->db->join('patient_ward', 'patient_ward.id = screening.patient_ward_id');
		$this->db->join('diagnose_system', 'diagnose_system.id = screening.diagnose_system_id');
		$this->db->join('assessment_nt', 'assessment_nt.screening_id = screening.id');
		$query = $this->db->get('screening');
		$result = $query->result_array();
		return $result[0];
	}

	//get_lsit 
	public function get_screening_by_patient_ward_id($data)
	{
		$list_id = array();
		$patient_ward_id = $data['patient_ward_id'];
		if (!isset($data['date_start'])) {
			$data['date_start'] = "";
		}
		if (!isset($data['date_end'])) {
			$data['date_end'] = "";
		}
		if ($data['date_start']!="") {
			$date_start = $data['date_start'];
		}
		if ($data['date_end']!="") {
			$date_end = $data['date_end'].' 23:59:59.999';
		}
		//naf
		$this->db->select('*,patient_ward.title as patient_ward_title,diagnose_system.title as diagnose_system_title,screening.date_next as screening_date_next,assessment_naf.date_next as date_next,screening.id as screening_id,assessment_naf.id as assessment_naf_id');
		$this->db->where('screening.patient_ward_id',$patient_ward_id);
		$this->db->where('patient_ward.is_del',0);
		$this->db->where('screening.is_del',0);
		$this->db->where('diagnose_system.is_del',0);
		if ($data['date_start']!="") {
			$this->db->where('assessment_naf.date_next >= ',$date_start);
		}
		if ($data['date_end']!="") {
			$this->db->where('assessment_naf.date_next <= ',$date_end);
		}
		$this->db->join('patient_ward', 'patient_ward.id = screening.patient_ward_id');
		$this->db->join('assessment_naf', 'assessment_naf.hn_code = screening.hn_code');
		$this->db->join('diagnose_system', 'diagnose_system.id = screening.diagnose_system_id');
		$this->db->group_by('assessment_naf.hn_code');
		$query_naf = $this->db->get('screening');
		$result_naf = $query_naf->result_array();
		$i=0;
		foreach ($result_naf as $key => $value) {
			array_push($list_id, $value['screening_id']);
			$this->db->where('screening.patient_ward_id',$patient_ward_id);
			$this->db->where('screening.is_del',0);
			if ($data['date_start']!="") {
				$this->db->where('assessment_naf.date_next >= ',$date_start);
			}
			if ($data['date_end']!="") {
				$this->db->where('assessment_naf.date_next <= ',$date_end);
			}
			$this->db->join('assessment_naf', 'assessment_naf.hn_code = screening.hn_code');
			$this->db->group_by('assessment_naf.id');
			$this->db->where('assessment_naf.hn_code',$value['hn_code']);
			$count_query_naf = $this->db->get('screening');
			$hn_code = $value['hn_code'];
			$result_naf[$i++]['count'] = count($count_query_naf->result_array());
		}
		

		//nt
		$this->db->select('*,patient_ward.title as patient_ward_title,diagnose_system.title as diagnose_system_title,screening.date_next as screening_date_next,assessment_nt.date_next as date_next,screening.id as screening_id,assessment_nt.id as assessment_nt_id');
		$this->db->where('screening.patient_ward_id',$patient_ward_id);
		$this->db->where('patient_ward.is_del',0);
		$this->db->where('screening.is_del',0);
		$this->db->where('diagnose_system.is_del',0);
		if ($data['date_start']!="") {
			$this->db->where('assessment_nt.date_next >= ',$date_start);
		}
		if ($data['date_end']!="") {
			$this->db->where('assessment_nt.date_next <= ',$date_end);
		}
		$this->db->join('patient_ward', 'patient_ward.id = screening.patient_ward_id');
		$this->db->join('assessment_nt', 'assessment_nt.hn_code = screening.hn_code');
		$this->db->join('diagnose_system', 'diagnose_system.id = screening.diagnose_system_id');
		$this->db->group_by('assessment_nt.hn_code'); 
		$query_nt = $this->db->get('screening');
		$result_nt = $query_nt->result_array();
		$i=0;
		foreach ($result_nt as $key => $value) {
			array_push($list_id, $value['screening_id']);
			$this->db->where('screening.patient_ward_id',$patient_ward_id);
			$this->db->where('screening.is_del',0);
			if ($data['date_start']!="") {
				$this->db->where('assessment_nt.date_next >= ',$date_start);
			}
			if ($data['date_end']!="") {
				$this->db->where('assessment_nt.date_next <= ',$date_end);
			}
			$this->db->join('assessment_nt', 'assessment_nt.hn_code = screening.hn_code');
			$this->db->group_by('assessment_nt.id');
			$this->db->where('assessment_nt.hn_code',$value['hn_code']);
			$count_query_nt = $this->db->get('screening');
			$result_nt[$i++]['count'] = count($count_query_nt->result_array());
		}

		$where_1 = "";
		$where_2 = "";
		$where_3 = "";
		if (count($list_id)>0) {
			$where_1 = "screening.id NOT IN (".implode($list_id,',').") AND";
		}
		if ($data['date_start']!="") {
			$where_2 = "screening.date_next >= '$date_start' AND";
		}
		if ($data['date_end']!="") {
			$where_2 = "screening.date_next <= '$date_end' AND";
		}
		$this->db->select('*,patient_ward.title as patient_ward_title,diagnose_system.title as diagnose_system_title,MAX(screening.date_next) as screening_date_next,screening.hn_code as hn_code,COUNT(screening.id) as count,MAX(screening.date) as date,MAX(screening.date_next) as date_next,MAX(screening.id) as screening_id');
		$this->db->where('screening.patient_ward_id',$patient_ward_id);
		$this->db->where('patient_ward.is_del',0);
		$this->db->where('screening.is_del',0);
		$this->db->where('diagnose_system.is_del',0);
		if (count($list_id)>0) {
			$this->db->where_not_in('screening.id', $list_id);
		}
		if ($data['date_start']!="") {
			$this->db->where('screening.date_next >= ',$date_start);
		}
		if ($data['date_end']!="") {
			$this->db->where('screening.date_next <= ',$date_end);
		}
		$this->db->where('assessment_nt.hn_code IS NULL');
		$this->db->where('assessment_naf.hn_code IS NULL');
		$this->db->join('patient_ward', 'patient_ward.id = screening.patient_ward_id');
		$this->db->join('diagnose_system', 'diagnose_system.id = screening.diagnose_system_id');
		$this->db->join('assessment_nt', 'assessment_nt.hn_code = screening.hn_code','left');
		$this->db->join('assessment_naf', 'assessment_naf.hn_code = screening.hn_code','left');
		$this->db->group_by('screening.hn_code'); 
		$this->db->order_by('screening.date_next','desc');
		$query_screening = $this->db->get('screening');
		$result_screening = $query_screening->result_array();

		// echo "<h1>NAF</h1>";
		// echo"<pre>";print_r($result_naf);echo "</pre>";
		// echo "<h1>NT</h1>";
		// echo"<pre>";print_r($result_nt);echo "</pre>";
		// echo "<h1>SCREENING</h1>";
		// echo"<pre>";print_r($result_screening);echo "</pre>";
		$tmp_sort = array_merge(array_merge($result_naf,$result_nt),$result_screening);
		// echo"<pre>";print_r($tmp_sort);echo "</pre>";

		$tmp_date_next = array();
		foreach ($tmp_sort as $key => $value) {
			if (isset($value['screening_id'])) {
				array_push($tmp_date_next, array(
					'screening_id' => $value['screening_id'],
					'assessment_naf_id' => isset($value['assessment_naf_id']) ? $value['assessment_naf_id'] : '',
					'assessment_nt_id' => isset($value['assessment_nt_id']) ? $value['assessment_nt_id'] : '',
					'date_next' => $value['date_next'])
				);
			}
		}
		usort($tmp_date_next, array($this,'sortFunction'));
		$tmp_result = array();
		foreach ($tmp_date_next as $key => $value) {
			foreach ($tmp_sort as $k => $v) {
				if ((isset($value['assessment_naf_id'])&&isset($value['assessment_naf_id']))&&($value['assessment_naf_id']==""&&$value['assessment_naf_id']=="")) {
					if (isset($value['screening_id'])&&isset($v['screening_id'])&&$value['screening_id']==$v['screening_id']) {
						$check = 0;
						foreach ($tmp_result as $kk => $vv) {
							if ($vv['screening_id']==$v['screening_id']) {
								$check = 1;
							}
						}
						if ($check==0) {
							array_push($tmp_result, $v);
						}
					}
				}else{
					if (isset($v['assessment_naf_id'])) {
						if ($value['assessment_naf_id']==$v['assessment_naf_id']) {
							$check = 0;
							foreach ($tmp_result as $kk => $vv) {
								if (isset($vv['assessment_naf_id'])&&$vv['assessment_naf_id']==$v['assessment_naf_id']) {
									$check = 1;
								}
							}
							if ($check==0) {
								array_push($tmp_result, $v);
							}
						}
					}
					if (isset($v['assessment_nt_id'])) {
						if ($value['assessment_nt_id']==$v['assessment_nt_id']) {
							$check = 0;
							foreach ($tmp_result as $kk => $vv) {
								if (isset($vv['assessment_nt_id'])&&$vv['assessment_nt_id']==$v['assessment_nt_id']) {
									$check = 1;
								}
							}
							if ($check==0) {
								array_push($tmp_result, $v);
							}
						}
					}
				}
				
			}
		}
		// echo"<pre>";print_r($tmp_result);echo "</pre>";
		return $tmp_result;
	}

	public function get_screening_patient_ward($data)
	{
		$list_id = array();
		$patient_ward_id = $data['patient_ward_id'];
		if ($data['date_start']!="") {
			$date_start = $data['date_start'];
		}
		if ($data['date_end']!="") {
			$date_end = $data['date_end'].' 23:59:59.999';
		}
		//naf
		$this->db->select('*,patient_ward.title as patient_ward_title,diagnose_system.title as diagnose_system_title,screening.date_next as screening_date_next,assessment_naf.date_next as date_next,screening.id as screening_id,COUNT(assessment_naf.id) as count,assessment_naf.id as assessment_naf_id');
		$this->db->where('screening.patient_ward_id',$patient_ward_id);
		$this->db->where('patient_ward.is_del',0);
		$this->db->where('screening.is_del',0);
		$this->db->where('diagnose_system.is_del',0);
		if ($data['date_start']!="") {
			$this->db->where('assessment_naf.date_next >= ',$date_start);
		}
		if ($data['date_end']!="") {
			$this->db->where('assessment_naf.date_next <= ',$date_end);
		}
		$this->db->join('patient_ward', 'patient_ward.id = screening.patient_ward_id');
		$this->db->join('assessment_naf', 'assessment_naf.hn_code = screening.hn_code');
		$this->db->join('diagnose_system', 'diagnose_system.id = screening.diagnose_system_id');
		$this->db->group_by('assessment_naf.hn_code');
		$query_naf = $this->db->get('screening');
		$result_naf = $query_naf->result_array();
		// echo"<pre>";print_r($result_naf);echo "</pre>";
		foreach ($result_naf as $key => $value) {
			array_push($list_id, $value['screening_id']);
		}

		//nt
		$this->db->select('*,patient_ward.title as patient_ward_title,diagnose_system.title as diagnose_system_title,screening.date_next as screening_date_next,assessment_nt.date_next as date_next,screening.id as screening_id,COUNT(assessment_nt.id) as count,assessment_nt.id as assessment_nt_id');
		$this->db->where('screening.patient_ward_id',$patient_ward_id);
		$this->db->where('patient_ward.is_del',0);
		$this->db->where('screening.is_del',0);
		$this->db->where('diagnose_system.is_del',0);
		if ($data['date_start']!="") {
			$this->db->where('assessment_nt.date_next >= ',$date_start);
		}
		if ($data['date_end']!="") {
			$this->db->where('assessment_nt.date_next <= ',$date_end);
		}
		$this->db->join('patient_ward', 'patient_ward.id = screening.patient_ward_id');
		$this->db->join('assessment_nt', 'assessment_nt.hn_code = screening.hn_code');
		$this->db->join('diagnose_system', 'diagnose_system.id = screening.diagnose_system_id');
		$this->db->group_by('assessment_nt.hn_code'); 
		$query_nt = $this->db->get('screening');
		$result_nt = $query_nt->result_array();
		// echo"<pre>";print_r($result_nt);echo "</pre>";
		foreach ($result_nt as $key => $value) {
			array_push($list_id, $value['screening_id']);
		}

		// $this->db->close();
		
		$where_1 = "";
		$where_2 = "";
		$where_3 = "";
		if (count($list_id)>0) {
			$where_1 = "screening.id NOT IN (".implode($list_id,',').") AND";
		}
		if ($data['date_start']!="") {
			$where_2 = "screening.date_next >= '$date_start' AND";
		}
		if ($data['date_end']!="") {
			$where_2 = "screening.date_next <= '$date_end' AND";
		}
		$this->db->select('*,patient_ward.title as patient_ward_title,diagnose_system.title as diagnose_system_title,MAX(screening.date_next) as screening_date_next,screening.hn_code as hn_code,COUNT(screening.id) as count,MAX(screening.date) as date,MAX(screening.date_next) as date_next,MAX(screening.id) as screening_id');
		$this->db->where('screening.patient_ward_id',$patient_ward_id);
		$this->db->where('patient_ward.is_del',0);
		$this->db->where('screening.is_del',0);
		$this->db->where('diagnose_system.is_del',0);
		if (count($list_id)>0) {
			$this->db->where_not_in('screening.id', $list_id);
		}
		if ($data['date_start']!="") {
			$this->db->where('screening.date_next >= ',$date_start);
		}
		if ($data['date_end']!="") {
			$this->db->where('screening.date_next <= ',$date_end);
		}
		$this->db->where('assessment_nt.hn_code IS NULL');
		$this->db->where('assessment_naf.hn_code IS NULL');
		$this->db->join('patient_ward', 'patient_ward.id = screening.patient_ward_id');
		$this->db->join('diagnose_system', 'diagnose_system.id = screening.diagnose_system_id');
		$this->db->join('assessment_nt', 'assessment_nt.hn_code = screening.hn_code','left');
		$this->db->join('assessment_naf', 'assessment_naf.hn_code = screening.hn_code','left');
		$this->db->group_by('screening.hn_code'); 
		$this->db->order_by('screening.date_next','desc'); 
		$query_screening = $this->db->get('screening');
		$result_screening = $query_screening->result_array();

		$tmp_sort = array_merge(array_merge($result_naf,$result_nt),$result_screening);

		$tmp_date_next = array();
		foreach ($tmp_sort as $key => $value) {
			array_push($tmp_date_next, array(
				'screening_id' => $value['screening_id'],
				'assessment_naf_id' => isset($value['assessment_naf_id']) ? $value['assessment_naf_id'] : '',
				'assessment_nt_id' => isset($value['assessment_nt_id']) ? $value['assessment_nt_id'] : '',
				'date_next' => $value['date_next'])
			);
		}
		usort($tmp_date_next, array($this,'sortFunction'));
		$tmp_result = array();
		foreach ($tmp_date_next as $key => $value) {
			foreach ($tmp_sort as $k => $v) {
				if ($value['assessment_naf_id']==""&&$value['assessment_naf_id']=="") {
					if ($value['screening_id']==$v['screening_id']) {
						$check = 0;
						foreach ($tmp_result as $kk => $vv) {
							if ($vv['screening_id']==$v['screening_id']) {
								$check = 1;
							}
						}
						if ($check==0) {
							array_push($tmp_result, $v);
						}
					}
				}else{
					if (isset($v['assessment_naf_id'])) {
						if ($value['assessment_naf_id']==$v['assessment_naf_id']) {
							$check = 0;
							foreach ($tmp_result as $kk => $vv) {
								if (isset($vv['assessment_naf_id'])&&$vv['assessment_naf_id']==$v['assessment_naf_id']) {
									$check = 1;
								}
							}
							if ($check==0) {
								array_push($tmp_result, $v);
							}
						}
					}
					if (isset($v['assessment_nt_id'])) {
						if ($value['assessment_nt_id']==$v['assessment_nt_id']) {
							$check = 0;
							foreach ($tmp_result as $kk => $vv) {
								if (isset($vv['assessment_nt_id'])&&$vv['assessment_nt_id']==$v['assessment_nt_id']) {
									$check = 1;
								}
							}
							if ($check==0) {
								array_push($tmp_result, $v);
							}
						}
					}
				}
				
			}
		}
		return $tmp_result;
	}

	public function sortFunction( $a, $b ) {
	    return strtotime($a['date_next']) - strtotime($b['date_next']);
	}

	public function get_last_id_by_nh_code($hn_code)
	{
		$this->db->where("hn_code",$hn_code);
		$this->db->order_by("id", "desc");
		$this->db->limit(1); 
		$query = $this->db->get("screening");
		$result = $query->result_array();
		return $result[0]['id'];
	}

	public function insert_screening($data)
	{
		$data = $data['data'];
		$data['date'] = $this->Date_model->put_date(0);
		$data['date_next'] = $this->Date_model->put_date(7);
		$data['hn_code'] = strtoupper($data['hn_code']);
		$data['hospital_id'] = $_SESSION['hospital_id'];
		$data_old = $this->get_screening_by_hn_code_old($data['hn_code'],$data['date']);
		if (isset($data_old['screening'][0])) {
			$this->db->where('id', $data_old['screening'][0]['screening_id']);
			$query = $this->db->update('screening',$data);
			$screening_id = $data_old['screening'][0]['screening_id'];
			$this->session->last_screening_id = $this->get_last_id_by_nh_code($data['hn_code']);
			if ($query) {
				echo json_encode(array('status' => 'success', 'msg' => 'บันทึกผลการ Screening เรียบร้อยแล้ว','patient_ward' => $data_old['screening'][0]['patient_ward_id'],'screening_id' => $screening_id));
			}else{
				echo json_encode(array('status' => 'error', 'msg' => 'Contact super admin'));
			}
		}else{
			$query = $this->db->insert('screening', $data);
			$screening_id = $this->db->insert_id();
			$this->session->last_screening_id = $screening_id;
			if ($query) {
				echo json_encode(array('status' => 'success', 'msg' => 'บันทึกผลการ Screening เรียบร้อยแล้ว','patient_ward' => $data['patient_ward_id'],'screening_id' => $screening_id));
			}else{
				echo json_encode(array('status' => 'error', 'msg' => 'Contact super admin'));
			}
		}
	}

	public function insert_screening_naf($data)
	{
		$data = $data['data'];
		$date_next = $data['date_next'];
		$data['date'] = date('Y-m-d');
		$data['date_next'] = $this->Date_model->put_date($date_next);
		$data['screening_id'] = $_SESSION['last_screening_id'];
		$data['hn_code'] = strtoupper($data['hn_code']);
		$query = $this->db->insert('assessment_naf', $data);
		if ($query) {
			$patient_ward = $this->get_screening_by_id($data['screening_id']);
			echo json_encode(array('status' => 'success', 'msg' => 'บันทึกผลการ Screening เรียบร้อยแล้ว','patient_ward' => $patient_ward['patient_ward_id'],'screening_id' => $data['screening_id']));
		}else{
			echo json_encode(array('status' => 'error', 'msg' => 'Contact super admin'));
		}
	}

	public function insert_screening_nt($data)
	{
		$data = $data['data'];
		$date_next = $data['date_next'];
		$data['date'] = date('Y-m-d');
		$data['date_next'] = $this->Date_model->put_date($date_next);
		$data['screening_id'] = $_SESSION['last_screening_id'];
		$data['hn_code'] = strtoupper($data['hn_code']);
		$query = $this->db->insert('assessment_nt', $data);
		if ($query) {
			$patient_ward = $this->get_screening_by_id($data['screening_id']);
			echo json_encode(array('status' => 'success', 'msg' => 'บันทึกผลการ Screening เรียบร้อยแล้ว','patient_ward' => $patient_ward['patient_ward_id'],'screening_id' => $data['screening_id']));
		}else{
			echo json_encode(array('status' => 'error', 'msg' => 'Contact super admin'));
		}
	}

	public function update_screening($data)
	{
		$data = $data['data'];
		$data['id'] = $_SESSION['last_screening_id'];
		$data['date_next'] = $this->Date_model->put_date(7);
		$data['date'] = date('Y-m-d');
		$data['hn_code'] = strtoupper($data['hn_code']);
		$data['hospital_id'] = $_SESSION['hospital_id'];
		$this->db->where('id', $data['id']);
		$query = $this->db->update('screening',$data);
		if ($query) {
			$patient_ward = $this->get_screening_by_id($data['id']);
			echo json_encode(array('status' => 'success', 'msg' => 'บันทึกผลการ Screening เรียบร้อยแล้ว','patient_ward' => $patient_ward['patient_ward_id'],'screening_id' => $data['id']));
		}else{
			echo json_encode(array('status' => 'error', 'msg' => 'Contact super admin'));
		}
	}

	public function delete_screening($data)
	{
		$this->db->set('is_del', 1);
		$this->db->where('id', $data['id']);
		$query = $this->db->update('screening');
		if ($query) {
			echo json_encode(array('status'=>1,'redirect'=>'screening'));
		}else{
			echo json_encode(array('status'=>0,'redirect'=>'screening'));
		}
	}

	public function update_status($data)
	{
		$this->db->set('status', 1);
		$this->db->where('id', $data['id']);
		$query = $this->db->update($data['type']);
		if ($query) {
			echo json_encode(array('status'=>1));
		}else{
			echo json_encode(array('status'=>0));
		}
	}

	public function checkout_screening($data)
	{
		$this->db->set('is_del', 1);
		$this->db->set('status', $data['status']);
		$this->db->where('hn_code', $data['hn_code']);
		$query = $this->db->update('screening');
		if ($query) {
			echo "1";
		}else{
			echo "0";
		}
	}

	public function alert($type,$title,$msg,$cfbtn="true",$redirect="")
	{
		if ($redirect!="") {
			$time = "timer: 2000,";
			$link = ',function(){
				window.location.href = "'.base_url().$redirect.'"
			}';
		}else{
			$time = "";
			$link = "";
		}
		echo '<link rel="stylesheet" type="text/css" href="'.base_url().'../css/sweetalert.css">';
		echo '<script src="'.base_url().'../js/sweetalert.min.js"></script>';
		echo '&nbsp;<script>swal({
			title: "'.$title.'",
			text: "'.$msg.'",
			type: "'.$type.'",
			'.$time.'
			showConfirmButton: '.$cfbtn.'
		}'.$link.');</script>';
	}
}