<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Diagnose_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

	public function get_diagnose()
	{
		$this->db->where('is_del',0);
		$query = $this->db->get('diagnose');
		return $query->result_array();
	}

	public function get_diagnose_by_id($id)
	{
		$sql = "SELECT * FROM `diagnose` WHERE `is_del` = 0 AND `id` = ?";
		$query = $this->db->query($sql,array($id));
		$result = $query->result_array();
		return $result[0];
	}

	public function insert_diagnose($data)
	{
		$query = $this->db->insert('diagnose', $data);
		if ($query) {
			$this->alert("success","Success","You add diagnose success","false","diagnose");
		}else{
			$this->alert("error","Error!","Contact super admin");
		}
	}

	public function update_diagnose($data)
	{
		$this->db->where('id', $data['id']);
		$query = $this->db->update('diagnose',$data);
		if ($query) {
			$this->alert("success","Success","You edit diagnose success","false","diagnose");
		}else{
			$this->alert("error","Error!","Contact super admin");
		}
	}

	public function delete_diagnose($data)
	{
		$this->db->set('is_del', 1);
		$this->db->where('id', $data['id']);
		$query = $this->db->update('diagnose');
		if ($query) {
			echo json_encode(array('status'=>1,'redirect'=>'diagnose'));
		}else{
			echo json_encode(array('status'=>0,'redirect'=>'diagnose'));
		}
	}

	public function alert($type,$title,$msg,$cfbtn="true",$redirect="")
	{
		if ($redirect!="") {
			$time = "timer: 2000,";
			$link = ',function(){
				window.location.href = "'.base_url().$redirect.'"
			}';
		}else{
			$time = "";
			$link = "";
		}
		echo '<link rel="stylesheet" type="text/css" href="'.base_url().'../css/sweetalert.css">';
		echo '<script src="'.base_url().'../js/sweetalert.min.js"></script>';
		echo '&nbsp;<script>swal({
			title: "'.$title.'",
			text: "'.$msg.'",
			type: "'.$type.'",
			'.$time.'
			showConfirmButton: '.$cfbtn.'
		}'.$link.');</script>';
	}
}