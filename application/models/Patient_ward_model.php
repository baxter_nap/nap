<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Patient_ward_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

	public function get_patient_ward()
	{
		$this->db->select('*,hospital.title as hospital_title,patient_ward.title as title,patient_ward.id as id');
		$this->db->where('patient_ward.is_del',0);
		$this->db->where('hospital.is_del',0);
		$this->db->join('hospital', 'hospital.id = patient_ward.hospital_id');
		$query = $this->db->get('patient_ward');
		return $query->result_array();
	}



	public function get_patient_ward_by_id($id)
	{
		$this->db->select('*,hospital.title as hospital_title,patient_ward.title as title,patient_ward.id as id');
		$this->db->where('patient_ward.is_del',0);
		$this->db->where('hospital.is_del',0);
		$this->db->where('patient_ward.id',$id);
		$this->db->join('hospital', 'hospital.id = patient_ward.hospital_id');
		$query = $this->db->get('patient_ward');
		$result = $query->result_array();
		return $result[0];
	}

	public function get_patient_ward_by_hospital_id($hospital_id)
	{
		$this->db->select('*,hospital.title as hospital_title,patient_ward.title as title,patient_ward.id as id');
		$this->db->where('patient_ward.is_del',0);
		$this->db->where('hospital.is_del',0);
		$this->db->where('hospital.id',$hospital_id);
		$this->db->join('hospital', 'hospital.id = patient_ward.hospital_id');
		$this->db->order_by('patient_ward.title', 'ASC');
		$query = $this->db->get('patient_ward');
		return $query->result_array();
	}

	public function insert_patient_ward($data)
	{
		$query = $this->db->insert('patient_ward', $data);

		if ($query) {
			$this->alert("success","Success","You add patient_ward success","false","patient_ward");
		}else{
			$this->alert("error","Error!","Contact super admin");
		}
	}

	public function update_patient_ward($data)
	{
		$this->db->where('id', $data['id']);
		$query = $this->db->update('patient_ward',$data);
		if ($query) {
			$this->alert("success","Success","You edit patient_ward success","false","patient_ward/index/".
		$data['hospital_id']);
		}else{
			$this->alert("error","Error!","Contact super admin");
		}
	}

	public function delete_patient_ward($data)
	{
		$this->db->where('is_del', 1);
		$this->db->where('id', $data['id']);
		$query = $this->db->update('patient_ward');
		if ($query) {
			echo json_encode(array('status'=>1,'redirect'=>'patient_ward'));
		}else{
			echo json_encode(array('status'=>0,'redirect'=>'patient_ward'));
		}
	}

	public function alert($type,$title,$msg,$cfbtn="true",$redirect="")
	{
		if ($redirect!="") {
			$time = "timer: 2000,";
			$link = ',function(){
				window.location.href = "'.base_url().$redirect.'"
			}';
		}else{
			$time = "";
			$link = "";
		}
		echo '<link rel="stylesheet" type="text/css" href="'.base_url().'../css/sweetalert.css">';
		echo '<script src="'.base_url().'../js/sweetalert.min.js"></script>';
		echo '&nbsp;<script>swal({
			title: "'.$title.'",
			text: "'.$msg.'",
			type: "'.$type.'",
			'.$time.'
			showConfirmButton: '.$cfbtn.'
		}'.$link.');</script>';
	}
}
