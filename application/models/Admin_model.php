<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Admin_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

	public function get_admin()
	{
		$this->db->where('is_del',0);
		$query = $this->db->get('admin');
		return $query->result_array();
	}

	public function get_admin_by_id($id)
	{
		$sql = "SELECT * FROM `admin` WHERE `is_del` = 0 AND `id` = ?";
		$query = $this->db->query($sql,array($id));
		$result = $query->result_array();
		return $result[0];
	}

	public function insert_admin($data)
	{
		$query = $this->db->insert('admin', $data);
		if ($query) {
			$this->alert("success","Success","You add admin success","false","admin");
		}else{
			$this->alert("error","Error!","Contact super admin");
		}
	}

	public function update_admin($data)
	{
		$this->db->where('id', $data['id']);
		$query = $this->db->update('admin',$data);
		if ($query) {
			$this->alert("success","Success","You edit admin success","false","admin");
		}else{
			$this->alert("error","Error!","Contact super admin");
		}
	}

	public function delete_admin($data)
	{
		$this->db->set('is_del', 1);
		$this->db->where('id', $data['id']);
		$query = $this->db->update('admin');
		if ($query) {
			echo json_encode(array('status'=>1,'redirect'=>'admin'));
		}else{
			echo json_encode(array('status'=>0,'redirect'=>'admin'));
		}
	}

	public function alert($type,$title,$msg,$cfbtn="true",$redirect="")
	{
		if ($redirect!="") {
			$time = "timer: 2000,";
			$link = ',function(){
				window.location.href = "'.base_url().$redirect.'"
			}';
		}else{
			$time = "";
			$link = "";
		}
		echo '<link rel="stylesheet" type="text/css" href="'.base_url().'../css/sweetalert.css">';
		echo '<script src="'.base_url().'../js/sweetalert.min.js"></script>';
		echo '&nbsp;<script>swal({
			title: "'.$title.'",
			text: "'.$msg.'",
			type: "'.$type.'",
			'.$time.'
			showConfirmButton: '.$cfbtn.'
		}'.$link.');</script>';
	}
}