<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Date_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

	public function put_date($day,$type=-1)
	{
		if ($type==-1) {
			$date = date('Y-m-d');
			$date = strtotime($date);
			return date('Y-m-d',strtotime("+".$day." day", $date));
		}else{
			$monthNames = array("มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม");
			$date = date('Y-m-d');
			$date = strtotime($date);
			$date = date('Y-m-d',strtotime("+".$day." day", $date));
			$tmp_d = explode('-', $date);
			$d = $tmp_d[2];
			$tmp_m = explode('-', $date);
			$m = $monthNames[(int)$tmp_m[1]-1];
			$tmp_y = explode('-', $date);
			$y = $tmp_y[0];
			return $d.' '.$m.' '.$y;
		}
		
	}
}