<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Login_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('cookie');
	}

	public function login($data)
	{
		if (count(explode('@', $data['code']))==1) {
			$this->db->where('code',$data['code']);
			$query = $this->db->get('hospital');
			if ($query->num_rows()==1) {
				$resultAr = $query->result_array();
				$result = $resultAr[0];
				if (isset($data['set_cookie'])) {
					delete_cookie('member_code');
					set_cookie('code',$data['code'],time() + (10 * 365 * 24 * 60 * 60));
				}
				$this->session->hospital_id = $result['id'];
				$this->session->hospital_title = $result['title'];
				$this->alert("success","Welcome","Nutrition Assessment Program","false","screening/create");
			}else{
				$this->alert("error","Error!","Hospital Code Fail.","false","/");
			}
		}else{
			$this->db->where('email',$data['code']);
			$this->db->where('code',$data['member_code']);
			$query = $this->db->get('member');
			if ($query->num_rows()==1) {
				$resultAr = $query->result_array();
				$result = $resultAr[0];
				if (isset($data['set_cookie'])) {
					set_cookie('code',$data['code'],time() + (10 * 365 * 24 * 60 * 60));
					set_cookie('member_code',$data['member_code'],time() + (10 * 365 * 24 * 60 * 60));
				}
				$this->session->hospital_id = $result['id'];
				$this->session->hospital_title = $result['email'];
				$this->session->no_save = 1;
				$this->alert("success","Welcome","Nutrition Assessment Program","false","screening/create");
			}else{
				$this->alert("error","Error!","Hospital Code Fail.","false","/");
			}
		}
	}

	public function login_backoffice($data)
	{
		$this->db->where('username',$data['username']);
		$this->db->where('password',$data['password']);
		$this->db->where('is_del',0);
		$query = $this->db->get('admin');
		if ($query->num_rows()==1) {
			$resultAr = $query->result_array();
			$result = $resultAr[0];
			$this->session->id = $result['id'];
			$this->session->username = $result['username'];
			$this->alert("success","Welcome","Your welcome","false","diagnose_system");
		}else{
			$this->alert("error","Error!","Username or password Fail.","false","backoffice/");
		}
	}


	public function insert_screening($data)
	{
		// $data = $data['data'];
		$data = $this->input->post();
		$query = $this->db->insert('screening_test', $data);
		if ($query) {
			echo json_encode(array('status' => 'success', 'msg' => 'บันทึกผลการ Screening เรียบร้อยแล้ว'));
		}else{
			echo json_encode(array('status' => 'error', 'msg' => 'Contact super admin'));
		}
	}

	public function alert($type,$title,$msg,$cfbtn="true",$redirect="")
	{
		if ($redirect!="") {
			$time = "timer: 2500,";
			$link = ',function(){
				window.location.href = "'.base_url().$redirect.'"
			}';
		}else{
			$time = "";
			$link = "";
		}
		echo '<link rel="stylesheet" type="text/css" href="'.base_url().'../css/sweetalert.css">';
		echo '<script src="'.base_url().'../js/sweetalert.min.js"></script>';
		echo '&nbsp;<script>swal({
			title: "'.$title.'",
			text: "'.$msg.'",
			type: "'.$type.'",
			'.$time.'
			showConfirmButton: '.$cfbtn.'
		}'.$link.');</script>';
	}
}