<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Hospital_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

	public function get_hospital()
	{
		$this->db->where('is_del',0);
		$query = $this->db->get('hospital');
		return $query->result_array();
	}

	public function get_hospital_by_id($id)
	{
		$sql = "SELECT * FROM `hospital` WHERE `is_del` = 0 AND `id` = ?";
		$query = $this->db->query($sql,array($id));
		$result = $query->result_array();
		return $result[0];
	}

	public function insert_hospital($data)
	{
		$query = $this->db->insert('hospital', $data);
		$hospital = $this->db->insert_id();
		$p_ward   = array('ศัลยกรรมรวม',
						  				'ศัลยกรรมชาย',
											'ศัลยกรรมหญิง',
											'ศัลยกรรม ICU',
											'อายุรกรรมรวม',
											'ศัลยกรรม ICU',
											'อายุรกรรมหญิง' ,
											'หอผู้ป่วยมะเร็ง'
							);
		foreach ($p_ward as $key => $value) {
			$tmp = array('title'=>$value,'hospital_id'=>$hospital);
			$Pquery = $this->db->insert('patient_ward', $tmp);
		}

		if ($query == true && $Pquery == true) {
			$this->alert("success","Success","You add hospital success","false","hospital");
		}else{
			$this->alert("error","Error!","Contact super admin");
		}
	}

	public function update_hospital($data)
	{
		$this->db->where('id', $data['id']);
		$query = $this->db->update('hospital',$data);
		if ($query) {
			$this->alert("success","Success","You edit hospital success","false","hospital");
		}else{
			$this->alert("error","Error!","Contact super admin");
		}
	}

	public function delete_hospital($data)
	{
		$this->db->set('is_del', 1);
		$this->db->where('id', $data['id']);
		$query = $this->db->update('hospital');
		if ($query) {
			echo json_encode(array('status'=>1,'redirect'=>'hospital'));
		}else{
			echo json_encode(array('status'=>0,'redirect'=>'hospital'));
		}
	}

	public function alert($type,$title,$msg,$cfbtn="true",$redirect="")
	{
		if ($redirect!="") {
			$time = "timer: 2000,";
			$link = ',function(){
				window.location.href = "'.base_url().$redirect.'"
			}';
		}else{
			$time = "";
			$link = "";
		}
		echo '<link rel="stylesheet" type="text/css" href="'.base_url().'../css/sweetalert.css">';
		echo '<script src="'.base_url().'../js/sweetalert.min.js"></script>';
		echo '&nbsp;<script>swal({
			title: "'.$title.'",
			text: "'.$msg.'",
			type: "'.$type.'",
			'.$time.'
			showConfirmButton: '.$cfbtn.'
		}'.$link.');</script>';
	}
}
