<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Information_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

	public function get_information()
	{
		$sql = "SELECT *,`information`.`id` AS `id`
			FROM `information` 
			WHERE `information`.`is_del` = 0";
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function get_patient_ward()
	{
		$this->db->where('is_del',0);
		$query = $this->db->get('patient_ward');
		return $query->result_array();
	}

	public function get_information_by_id($id)
	{
		$sql = "SELECT * FROM `information` WHERE `is_del` = 0 AND `id` = ?";
		$query = $this->db->query($sql,array($id));
		$result = $query->result();
		return $result[0];
	}

	public function insert_information($data)
	{
		$query = $this->db->insert('information', $data);
		if ($query) {
			$this->alert("success","Success","You add information success","false","information");
		}else{
			$this->alert("error","Error!","Contact super admin");
		}
	}

	public function update_information($data)
	{
		$this->db->where('id', $data['id']);
		$query = $this->db->update('information',$data);
		if ($query) {
			$this->alert("success","Success","You add information success","false","information");
		}else{
			$this->alert("error","Error!","Contact super admin");
		}
	}

	public function delete_information($data)
	{
		$this->db->where('is_del', 1);
		$this->db->where('id', $data['id']);
		$query = $this->db->update('information');
		if ($query) {
			echo json_encode(array('status'=>1,'redirect'=>'information'));
		}else{
			echo json_encode(array('status'=>0,'redirect'=>'information'));
		}
	}

	public function alert($type,$title,$msg,$cfbtn="true",$redirect="")
	{
		if ($redirect!="") {
			$time = "timer: 2000,";
			$link = ',function(){
				window.location.href = "'.base_url().$redirect.'"
			}';
		}else{
			$time = "";
			$link = "";
		}
		echo '<link rel="stylesheet" type="text/css" href="'.base_url().'../css/sweetalert.css">';
		echo '<script src="'.base_url().'../js/sweetalert.min.js"></script>';
		echo '&nbsp;<script>swal({
			title: "'.$title.'",
			text: "'.$msg.'",
			type: "'.$type.'",
			'.$time.'
			showConfirmButton: '.$cfbtn.'
		}'.$link.');</script>';
	}
}