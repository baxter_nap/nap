<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Backoffice extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		// $this->load->model('User_model');
	}
	public function index()
	{
		$this->load->view('backoffice/login_backoffice');
	}
	public function create()
	{
		$data['page'] = array('backoffice');
		$data['rule'] = $this->Rule_model->get_rule();
		$this->load->view('backoffice/backoffice_create',$data);
	}

	public function edit($id)
	{
		$data['page'] = array('backoffice');
		$data['data'] = $this->User_model->get_backoffice_by_id($id);
		$this->load->view('backoffice/backoffice_edit',$data);
	}


}
