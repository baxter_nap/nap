<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Register_model');
		$this->load->model('Hospital_model');
	}

	public function index()
	{
		$data['hospital'] = $this->Hospital_model->get_hospital();
		$th = json_decode(file_get_contents(base_url().'../js/province.json'));
		$data['province'] = $th->th;
		$this->load->view('register',$data);
	}

	public function fn_create()
	{
		$data = $this->input->post();
		$this->Register_model->insert_register($data);
	}

	public function fn_delete()
	{
		$data = $this->input->post();
		$this->Register_model->delete_register($data);
	}
}
