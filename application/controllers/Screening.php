<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Screening extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Screening_model');
		$this->load->model('Patient_ward_model');
		$this->load->model('Diagnose_system_model');
		$this->load->library('user_agent');
	}

	public function index()
	{
		$data['page'] = array('screening');
		$data['list'] = $this->Screening_model->get_screening();
		$this->load->view('screening',$data);
	}

	public function export_screening($id,$save=-1,$num=-1)
	{
		$data['data'] = $this->Screening_model->get_screening_by_id_export($id);
		$data['data_old'] = $this->Screening_model->get_screening_by_hn_code($data['data']['hn_code'],"id");
		$data['save'] = $save;
		$data['num'] = $num;
		$data['platform'] = $this->agent->platform();
		$this->load->view('export_screening',$data);
	}

	public function export_naf($id,$save=-1,$num=-1)
	{
		$data['data'] = $this->Screening_model->get_screening_naf_by_id_export($id);
		$data['data_old'] = $this->Screening_model->get_screening_by_hn_code($data['data']['hn_code'],"id");
		$data['save'] = $save;
		$data['num'] = $num;
		$data['platform'] = $this->agent->platform();
		$this->load->view('export_naf',$data);
	}

	public function export_nt($id,$save=-1,$num=-1)
	{
		$data['data'] = $this->Screening_model->get_screening_nt_by_id_export($id);
		$data['data_old'] = $this->Screening_model->get_screening_by_hn_code($data['data']['hn_code'],"id");
		$data['save'] = $save;
		$data['num'] = $num;
		$data['platform'] = $this->agent->platform();
		$this->load->view('export_nt',$data);
	}

	public function save_tmp($type=-1)
	{
		if ($type=='screening') {
			$this->session->screening = $this->input->post();
		}else if($type=='naf'){
			$this->session->naf = $this->input->post();
		}else if($type=='nt'){
			$this->session->nt = $this->input->post();
		}
	}

	public function export_screening_n($save=-1,$num=-1)
	{
		$data['data'] = $_SESSION['screening']['data'];
		$data['save'] = $save;
		$data['num'] = $num;
		$data['platform'] = $this->agent->platform();
		$this->load->view('export_screening_n',$data);
	}

	public function export_naf_n($save=-1,$num=-1)
	{
		$data['data'] = $_SESSION['naf']['data'];
		$data['data_old'] = $_SESSION['screening']['data'];
		$data['save'] = $save;
		$data['num'] = $num;
		$data['platform'] = $this->agent->platform();
		$this->load->view('export_naf_n',$data);
	}

	public function export_nt_n($save=-1,$num=-1)
	{
		$data['data'] = $_SESSION['nt']['data'];
		$data['data_old'] = $_SESSION['screening']['data'];
		$data['save'] = $save;
		$data['num'] = $num;
		$data['platform'] = $this->agent->platform();
		$this->load->view('export_nt_n',$data);
	}

	public function jsSave($name)
	{
		$myfile = fopen("js/posi_".$name.".js", "w") or die("Unable to open file!");

		$txt = "posi = {};\n";
		fwrite($myfile,$txt);
		ksort($_POST["posi"]);
		foreach ($_POST["posi"] as $key => $value) {
			$txt = "posi[\"".$key."\"] = [".$value[0].",".$value[1]."];\n";
			fwrite($myfile,$txt);
		}
		fclose($myfile);
	}

	public function create($id=-1)
	{
		$data['page'] = array('screening');
		$data['date'] = $this->Date_model->put_date(0,'th');
		$data['date_next'] = $this->Date_model->put_date(7,'th');
		$data['patient_ward'] = $this->Patient_ward_model->get_patient_ward_by_hospital_id($this->session->userdata('hospital_id'));
		$data['diagnose_system'] = $this->Diagnose_system_model->get_diagnose_system();
		$data['id'] = $id;
		if ($id!=-1) {
			$data['data'] = $this->Screening_model->get_screening_by_id($id);
		}else{
			$data['data']['hn_code'] = "";
			$data['data']['name'] = "";
			$data['data']['patient_ward_id'] = 0;
			$data['data']['diagnose_system_id'] = 0;
			$data['data']['diagnose'] = "";
			$data['data']['diagnose_system_id'] = "";
			$data['data']['date_admission'] = "";
			$data['data']['sex'] = "";
			$data['data']['age'] = "";
		}
		if (!isset($_SESSION['no_save'])) {
			$this->load->view('screening_create',$data);
		}else{
			unset($_SESSION['screening']);
			$this->load->view('screening_create_n',$data);
		}
	}

	public function edit($id)
	{
		$this->session->last_screening_id = $id;
		$data['page'] = array('screening');
		$data['data'] = $this->Screening_model->get_screening_by_id($id);
		$data['patient_ward'] = $this->Patient_ward_model->get_patient_ward_by_hospital_id($this->session->userdata('hospital_id'));
		$data['diagnose_system'] = $this->Diagnose_system_model->get_diagnose_system();
		$this->load->view('screening_edit',$data);
	}

	public function get_screening_by_hn_code_old()
	{
		header('Access-Control-Allow-Origin: *');
		$data['data'] = $this->Screening_model->get_screening_by_hn_code_old($_POST['hn_code']);
		echo json_encode($data['data']);
	}

	public function create_naf($id=-1,$count=1)
	{
		$data['page'] = array('screening');
		if ($id!=-1) {
			$_SESSION['last_screening_id'] = $id;
		}
		if (!isset($_SESSION['last_screening_id'])) {
			if (!isset($_SESSION['screening'])) {
				redirect(base_url().'index.php', 'refresh');
			}else{
				unset($_SESSION['naf']);
				$this->load->view('screening_naf_n',$data);
			}
		}else{
			$data['data'] = $this->Screening_model->get_screening_by_id($_SESSION['last_screening_id']);
			$data['count'] = $count;
			$this->load->view('screening_naf',$data);
		}
	}

	public function create_nt($id=-1,$count=1)
	{
		$data['page'] = array('screening');
		if ($id!=-1) {
			$_SESSION['last_screening_id'] = $id;
		}
		if (!isset($_SESSION['last_screening_id'])) {
			if (!isset($_SESSION['screening'])) {
				redirect(base_url().'index.php', 'refresh');
			}else{
				unset($_SESSION['nt']);
				$this->load->view('screening_nt_n',$data);
			}
		}else{
			$data['data'] = $this->Screening_model->get_screening_by_id($_SESSION['last_screening_id']);
			$data['count'] = $count;
			$this->load->view('screening_nt',$data);
		}
	}

	public function fn_update_status()
	{
		$data = $this->input->post();
		$this->Screening_model->update_status($data);
	}

	public function fn_create()
	{
		$data = $this->input->post();
		$this->Screening_model->insert_screening($data);
	}

	public function fn_create_naf()
	{
		$data = $this->input->post();
		$this->Screening_model->insert_screening_naf($data);
	}

	public function fn_create_nt()
	{
		$data = $this->input->post();
		$this->Screening_model->insert_screening_nt($data);
	}

	public function fn_edit()
	{
		$data = $this->input->post();
		$this->Screening_model->update_screening($data);
	}

	public function fn_checkout()
	{
		$data = $this->input->post();
		$this->Screening_model->checkout_screening($data);
	}

	public function fn_delete()
	{
		$data = $this->input->post();
		$this->Screening_model->delete_screening($data);
	}
}
