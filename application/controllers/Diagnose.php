<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Diagnose extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('diagnose_model');
	}

	public function index()
	{
		$data = array();
		$data['page'] = array('diagnose');
		$data['diagnose'] = $this->diagnose_model->get_diagnose();
		$this->load->view('backoffice/diagnose',$data);
	}

	public function create()
	{
		$data['page'] = array('diagnose');
		$this->load->view('backoffice/diagnose_create',$data);
	}

	public function edit($id)
	{
		$data['page'] = array('diagnose');
		$data['data'] = $this->diagnose_model->get_diagnose_by_id($id);
		$this->load->view('backoffice/diagnose_edit',$data);
	}

	public function fn_create()
	{
		$data = $this->input->post();
		$this->diagnose_model->insert_diagnose($data);
	}

	public function fn_edit()
	{
		$data = $this->input->post();
		$this->diagnose_model->update_diagnose($data);
	}

	public function fn_delete()
	{
		$data = $this->input->post();
		$this->diagnose_model->delete_diagnose($data);
	}
}
