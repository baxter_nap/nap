<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin_model');
		$this->load->model('hospital_model');
	}

	public function index()
	{
		$data = array();
		$data['page'] = array('admin');
		$data['admin'] = $this->admin_model->get_admin();
		$this->load->view('backoffice/admin',$data);
	}

	public function create()
	{
		$data['page'] = array('admin');
		$data['hospital'] = $this->hospital_model->get_hospital();
		$this->load->view('backoffice/admin_create',$data);
	}

	public function edit($id)
	{
		$data['page'] = array('admin');
		$data['hospital'] = $this->hospital_model->get_hospital();
		$data['data'] = $this->admin_model->get_admin_by_id($id);
		$this->load->view('backoffice/admin_edit',$data);
	}

	public function fn_create()
	{
		$data = $this->input->post();
		$this->admin_model->insert_admin($data);
	}

	public function fn_edit()
	{
		$data = $this->input->post();
		$this->admin_model->update_admin($data);
	}

	public function fn_delete()
	{
		$data = $this->input->post();
		$this->admin_model->delete_admin($data);
	}
}
