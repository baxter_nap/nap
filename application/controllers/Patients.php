<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Patients extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Patient_ward_model');
		$this->load->model('Screening_model');
		$this->load->model('Date_model');
	}

	public function index()
	{
		$data = array();
		$data['page'] = array('patients');
		$data['patient_ward'] = $this->Patient_ward_model->get_patient_ward_by_hospital_id($_SESSION['hospital_id']);
		foreach ($data['patient_ward'] as $key => $value) {
			$param = array();
			$param['patient_ward_id'] = $value['id'];
			$param['date_start'] = "";
			$param['date_end'] = "";
			$count = $this->Screening_model->get_screening_patient_ward($param);
			$data['patient_ward_count'][$value['id']] = count($count);
		}
		$this->load->view('patients',$data);
	}

	public function lists($id)
	{
		$data = array();
		$data['page'] = array('patients');
		$data['patient_ward'] = $this->Patient_ward_model->get_patient_ward_by_id($id);
		$this->load->view('patients_list',$data);
	}

	public function lists_screening($id)
	{
		$data = $this->input->post();
		$data['patient_ward_id'] = $id;
		echo json_encode($this->Screening_model->get_screening_by_patient_ward_id($data));
	}

	public function detail($hn_code)
	{
		$data = array();
		$data['page'] = array('patients');
		$data['data'] = $this->Screening_model->get_screening_by_hn_code($hn_code);
		if (count($data['data']['screening'])>0) {
			$data['data_old'] = $this->Screening_model->get_screening_by_hn_code($hn_code,"id");
			$data['date_passing'] = $this->cal_date_pass($data['data']['screening'][0]['date_admission']);
			$this->load->view('patients_detail',$data);
		}else{
			redirect(base_url().'patients/');
		}
	}

	public function report()
	{
		$data = array();
		$data['page'] = array('patients');
		$this->load->view('report',$data);
	}

	public function create()
	{
		$data['page'] = array('patients');
		$this->load->view('patients_create',$data);
	}

	public function create_naf()
	{
		$data['page'] = array('patients');
		$this->load->view('patients_naf',$data);
	}

	public function create_nt()
	{
		$data['page'] = array('patients');
		$this->load->view('patients_nt',$data);
	}

	public function cal_date_pass($date)
	{
		$now = time();
		$your_date = strtotime($date);
		$datediff = $now - $your_date;
		return floor($datediff / (60 * 60 * 24)) < 0 ? 0 : floor($datediff / (60 * 60 * 24));
	}
}
