<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manage_member extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Patient_ward_model');
		$this->load->model('Register_model');
		$this->load->model('Hospital_model');
	}

	public function index()
	{
		$data = array();
		$data['page'] = array('manage_member');
		$data['register'] = $this->Register_model->get_register();
		$this->load->view('backoffice/manage_member',$data);
	}

	public function edit($id)
	{
		$data['page'] = array('manage_member');
		$data['data'] = $this->Register_model->get_register_by_id($id);
		$this->load->view('backoffice/manage_member_edit',$data);
	}

	public function send_mail()
	{
		$data = $this->input->post();
		$this->Register_model->send_mail($data);
	}

	public function fn_create()
	{
		$data = $this->input->post();
		$this->Patient_ward_model->insert_manage_member($data);
	}

	public function fn_edit()
	{
		$data = $this->input->post();
		$this->Patient_ward_model->update_manage_member($data);
	}

	public function fn_delete()
	{
		$data = $this->input->post();
		$this->Patient_ward_model->delete_manage_member($data);
	}
}
