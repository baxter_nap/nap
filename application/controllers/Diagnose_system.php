<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Diagnose_system extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('diagnose_system_model');
	}

	public function index()
	{
		$data = array();
		$data['page'] = array('diagnose_system');
		$data['diagnose_system'] = $this->diagnose_system_model->get_diagnose_system();
		$this->load->view('backoffice/diagnose_system',$data);
	}

	public function create()
	{
		$data['page'] = array('diagnose_system');
		$this->load->view('backoffice/diagnose_system_create',$data);
	}

	public function edit($id)
	{
		$data['page'] = array('diagnose_system');
		$data['data'] = $this->diagnose_system_model->get_diagnose_system_by_id($id);
		$this->load->view('backoffice/diagnose_system_edit',$data);
	}

	public function fn_create()
	{
		$data = $this->input->post();
		$this->diagnose_system_model->insert_diagnose_system($data);
	}

	public function fn_edit()
	{
		$data = $this->input->post();
		$this->diagnose_system_model->update_diagnose_system($data);
	}

	public function fn_delete()
	{
		$data = $this->input->post();
		$this->diagnose_system_model->delete_diagnose_system($data);
	}
}
