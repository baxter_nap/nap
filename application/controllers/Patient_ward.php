<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Patient_ward extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Patient_ward_model');
		$this->load->model('Hospital_model');
	}

	public function index($id)
	{
		$data = array();
		$data['page'] = array('patient_ward');
		$data['patient_ward'] = $this->Patient_ward_model->get_patient_ward_by_hospital_id($id);
		$data['hospital'] = $this->Hospital_model->get_hospital_by_id($id);
		$this->load->view('backoffice/patient_ward',$data);
	}

	public function detail()
	{
		$data = array();
		$data['page'] = array('patient_ward');
		$this->load->view('backoffice/patient_ward_detail',$data);
	}

	public function create()
	{
		$data['page'] = array('patient_ward');
		$data['hospital'] = $this->Hospital_model->get_hospital();
		$this->load->view('backoffice/patient_ward_create',$data);
	}

	public function edit($id)
	{
		$data['page'] = array('patient_ward');
		$data['hospital'] = $this->Hospital_model->get_hospital();
		$data['data'] = $this->Patient_ward_model->get_patient_ward_by_id($id);
		$this->load->view('backoffice/patient_ward_edit',$data);
	}

	public function fn_create()
	{
		$data = $this->input->post();
		$this->Patient_ward_model->insert_patient_ward($data);
	}

	public function fn_edit()
	{
		$data = $this->input->post();
		$this->Patient_ward_model->update_patient_ward($data);
	}

	public function fn_delete()
	{
		$data = $this->input->post();
		$this->Patient_ward_model->delete_patient_ward($data);
	}
}
