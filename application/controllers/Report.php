<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Patient_ward_model');
		$this->load->model('Screening_model');
		$this->load->model('Date_model');
	}

	public function index()
	{
		$data = array();
		$data['page'] = array('report');
		
		$this->load->view('report',$data);
	}

	public function report_data()
	{
		$data = $this->input->post();
		$month = array('01','02','03','04','05','06','07','08','09','10','11','12');
		$result['type'] = $data['type'];
		if ($data['type']=="screening") {
			$result['assessment'] = array();
			$result['screening'] = array();
			$result['assessment_color'] = "#50b026";
			$result['screening_color'] = "#ed4d4d";
			$result['all'] = 0;
			$result['labels'] = array('ม.ค.','ก.พ.','มี.ค.','เม.ย.','พ.ค.','มิ.ย.','ก.ค.','ส.ค.','ก.ย.','ต.ค.','พ.ย.','ธ.ค');
			foreach ($month as $key => $value) {
				$date = $data['year'].'-'.$value;
				$data['data'] = $this->Screening_model->get_screening_by_date($date);
				$screening = 0;
				$assessment = 0;
				foreach ($data['data'] as $k => $v) {
					if($v['c_1']+$v['c_2']+$v['c_2']+$v['c_3']>1){
						$assessment+=1;
					}else{
						$screening+=1;
					}
					$result['all']+=1;
				}
				array_push($result['assessment'], $assessment);
				array_push($result['screening'], $screening);
			}
		}else if ($data['type']=="naf") {
			$result['NAF_A'] = array();
			$result['NAF_B'] = array();
			$result['NAF_C'] = array();
			$result['NAF_A_color'] = "#50b026";
			$result['NAF_B_color'] = "#f39c12";
			$result['NAF_C_color'] = "#e74c3c";
			$result['all'] = 0;
			$result['labels'] = array('ม.ค.','ก.พ.','มี.ค.','เม.ย.','พ.ค.','มิ.ย.','ก.ค.','ส.ค.','ก.ย.','ต.ค.','พ.ย.','ธ.ค');
			foreach ($month as $key => $value) {
				$date = $data['year'].'-'.$value;
				$data['data'] = $this->Screening_model->get_assessment_naf_by_date($date);
				$NAF_A = 0;
				$NAF_B = 0;
				$NAF_C = 0;
				foreach ($data['data'] as $k => $v) {
					if($v['grade']=="NAF-A"){
						$NAF_A+=1;
					}else if($v['grade']=="NAF-B"){
						$NAF_B+=1;
					}else if($v['grade']=="NAF-C"){
						$NAF_C+=1;
					}
					$result['all']+=1;
				}
				array_push($result['NAF_A'], $NAF_A);
				array_push($result['NAF_B'], $NAF_B);
				array_push($result['NAF_C'], $NAF_C);
			}
		}else if ($data['type']=="nt") {
			$result['NT_1'] = array();
			$result['NT_2'] = array();
			$result['NT_3'] = array();
			$result['NT_4'] = array();
			$result['NT_1_color'] = "#50b026";
			$result['NT_2_color'] = "#176920";
			$result['NT_3_color'] = "#f39c12";
			$result['NT_4_color'] = "#e74c3c";
			$result['all'] = 0;
			$result['labels'] = array('ม.ค.','ก.พ.','มี.ค.','เม.ย.','พ.ค.','มิ.ย.','ก.ค.','ส.ค.','ก.ย.','ต.ค.','พ.ย.','ธ.ค');
			foreach ($month as $key => $value) {
				$date = $data['year'].'-'.$value;
				$data['data'] = $this->Screening_model->get_assessment_nt_by_date($date);
				$NT_1 = 0;
				$NT_2 = 0;
				$NT_3 = 0;
				$NT_4 = 0;
				foreach ($data['data'] as $k => $v) {
					if($v['grade']=="NT-1"){
						$NT_1+=1;
					}else if($v['grade']=="NT-2"){
						$NT_2+=1;
					}else if($v['grade']=="NT-3"){
						$NT_3+=1;
					}else if($v['grade']=="NT-4"){
						$NT_4+=1;
					}
					$result['all']+=1;
				}
				array_push($result['NT_1'], $NT_1);
				array_push($result['NT_2'], $NT_2);
				array_push($result['NT_3'], $NT_3);
				array_push($result['NT_4'], $NT_4);
			}
		}
		echo json_encode($result);
	}
}
