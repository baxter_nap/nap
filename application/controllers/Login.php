<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Login_model');
		$this->load->helper('cookie');
	}

	public function index()
	{
		$data['code'] = get_cookie('code');
		$data['member_code'] = get_cookie('member_code');
		if (isset($_SESSION['hospital_id'])) {
			redirect(base_url().'screening/create');
		}
		$this->load->view('login',$data);
	}

	public function fn_login()
	{
		$data = $this->input->post();
		$this->Login_model->login($data);
	}

	public function fn_login_backoffice()
	{
		$data = $this->input->post();
		$this->Login_model->login_backoffice($data);
	}

	public function fn_test()
	{
		$data = $this->input->post();
		$this->Login_model->insert_screening($data);
	}

	public function fn_logout()
	{
		$this->session->sess_destroy();
		redirect(base_url());
	}

	public function fn_logout_backoffice()
	{
		$this->session->sess_destroy();
		redirect(base_url().'backoffice');
	}
}
