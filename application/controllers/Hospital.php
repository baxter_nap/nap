<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Hospital extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('hospital_model');
	}

	public function index()
	{
		$data = array();
		$data['page'] = array('hospital');
		$data['hospital'] = $this->hospital_model->get_hospital();
		$this->load->view('backoffice/hospital',$data);
	}

	public function create()
	{
		$data['page'] = array('hospital');
		$this->load->view('backoffice/hospital_create',$data);
	}

	public function edit($id)
	{
		$data['page'] = array('hospital');
		$data['data'] = $this->hospital_model->get_hospital_by_id($id);
		$this->load->view('backoffice/hospital_edit',$data);
	}

	public function fn_create() {
		$data = $this->input->post();
		$this->hospital_model->insert_hospital($data);
	}

	public function fn_edit()
	{
		$data = $this->input->post();
		$this->hospital_model->update_hospital($data);
	}

	public function fn_delete()
	{
		$data = $this->input->post();
		$this->hospital_model->delete_hospital($data);
	}
}
