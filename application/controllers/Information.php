<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Information extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Information_model');
		$this->load->library('user_agent');
	}

	public function index()
	{
		$data = array();
		$data['page'] = array('information');
		$this->load->view('information',$data);
	}

	public function lists()
	{
		$data = array();
		$data['page'] = array('information');
		$this->load->view('information_list',$data);
	}

	public function screening()
	{
		$data['page'] = array('information');
		$this->load->view('information_screening',$data);
	}

	public function screening_mb()
	{
		$data['page'] = array('information');
		$data['platform'] = $this->agent->platform();
		$this->load->view('information_screening_mb',$data);
	}

	public function naf()
	{
		$data['page'] = array('information');
		$this->load->view('information_naf',$data);
	}

	public function naf_mb()
	{
		$data['page'] = array('information');
		$data['platform'] = $this->agent->platform();
		$this->load->view('information_naf_mb',$data);
	}

	public function nt()
	{
		$data['page'] = array('information');
		$this->load->view('information_nt',$data);
	}

	public function nt_mb()
	{
		$data['page'] = array('information');
		$data['platform'] = $this->agent->platform();
		$this->load->view('information_nt_mb',$data);
	}

	public function calculation()
	{
		$data['page'] = array('information');
		$this->load->view('information_calculation',$data);
	}



	public function diagnostic()
	{
		$data['page'] = array('information');
		$this->load->view('information_diagnostic',$data);
	}
}
