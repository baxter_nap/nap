<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Config extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Config_model');
	}

	public function index()
	{
		$data['page'] = array('config');
		$data['config'] = $this->Config_model->get_config();
		$this->load->view('config',$data);
	}

	public function json()
	{
		header('Access-Control-Allow-Origin: *');
		$data = $this->Config_model->get_config();
		$datas['data'] = $data[0];
		$datas['length'] = count($data);
		echo json_encode($datas);
	}
}
